
let gajiJobUrl = baseUrl + "gaji/job-level/";
let gajiJobSimpan = gajiJobUrl + "simpan/";

function simpanJob(){
	$form = $("#form-job");
	data = $form.serializeObject();
	data["GAJI_POKOK"] = readCurrency(data["GAJI_POKOK"]);
	data["TUNJANGAN_JABATAN"] = readCurrency(data["TUNJANGAN_JABATAN"]);
	simpleAjax({
		url: gajiJobSimpan + "job",
		data: data,
		success: function(response){
			if(response.result == ResultCode.OK){
				window.location.reload();
			}else{
				console.log(response.message);
				showError("Penyimpanan Data Gaji Job Level Gagal", response.message);
			}
		}
	});
}
function simpanTunjanganKinerja(){
	let rows = [];
	let $rows = $(".row-tki");
	$rows.each(function(){
		let $row = $(this);
		let row = readRow($row);
		rows.push(row);
	});
	let data = {
		"ID_JOB": $("input[name=\"ID_JOB\"]").val(),
		"rows": rows
	};
	simpleAjax({
		url: gajiJobSimpan + "tki",
		data: data,
		success: function(response){
			if(response.result == ResultCode.OK){
				window.location.reload();
			}else{
				console.log(response.message);
				showError("Penyimpanan Tunjangan Kinerja Job Level Gagal", response.message);
			}
		}
	});
}
function simpanUpahLembur(){
	let rows = [];
	let $rows = $(".row-ul");
	$rows.each(function(){
		let $row = $(this);
		let row = readRow($row);
		rows.push(row);
	});
	let data = {
		"ID_JOB": $("input[name=\"ID_JOB\"]").val(),
		"rows": rows
	};
	simpleAjax({
		url:gajiJobSimpan + "ul",
		data: data,
		success: function(response){
			if(response.result == ResultCode.OK){
				window.location.reload();
			}else{
				console.log(response.message);
				showError("Penyimpanan Upah Lembur Job Level Gagal", response.message);
			}
		}
	});
}

function simpanTunjanganMasaKerja(){
	let rows = [];
	let $rows = $(".row-tmk");
	$rows.each(function(){
		let $row = $(this);
		let row = readRow($row);
		row["ID_TMK"] = $row.data("id");
		rows.push(row);
	});
	let data = {
		"ID_JOB": $("input[name=\"ID_JOB\"]").val(),
		"rows": rows
	};
	simpleAjax({
		url: gajiJobSimpan + "tmk",
		data: data,
		success: function(response){
			if(response.result == ResultCode.OK){
				window.location.reload();
			}else{
				console.log(response.message);
				showError("Penyimpanan Tunjangan Masa Kerja Job Level Gagal", response.message);
			}
		}
	});
}
//ready

$(document).ready(function(){
	$("#btn-simpan-job").click(function(){
        askConfirmation(
            'Konfirmasi Penyimpanan Data Gaji Job Level', 
            'Apa Anda yakin data sudah benar?', 
            false, function(){
				simpanJob();
            }
        );
	});
	$("#btn-simpan-tki").click(function(){
        askConfirmation(
            'Konfirmasi Penyimpanan Tunjangan Kinerja Job Level', 
            'Apa Anda yakin data sudah benar?', 
            false, function(){
				simpanTunjanganKinerja();
            }
        );
	});
	$("#btn-simpan-ul").click(function(){
        askConfirmation(
            'Konfirmasi Penyimpanan Upah Lembur Job Level', 
            'Apa Anda yakin data sudah benar?', 
            false, function(){
				simpanUpahLembur();
            }
        );
	});
	$("#btn-simpan-tmk").click(function(){
        askConfirmation(
            'Konfirmasi Penyimpanan Tunjangan Masa Kerja Job Level', 
            'Apa Anda yakin data sudah benar?', 
            false, function(){
				simpanTunjanganMasaKerja();
            }
        );
	});
});