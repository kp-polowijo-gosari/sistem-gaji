

function tambahRowTkhjAktif(data){
	$row = instantiateTemplate(templateRowAktif);
	fillTemplate($row, data);
	$("#table-aktif").DataTable().rows.add($row).draw(false);
	return $row;
}

function hapusRowTkhjAktif($rowAktif){
	$row = instantiateTemplate(templateRowDihapus);
	copyElementData($rowAktif, $row);
	$("#table-aktif").DataTable().row($rowAktif).remove().draw(false);
	$("#table-dihapus").DataTable().rows.add($row).draw(false);
}

function kembalikanRowTkhjDihapus($rowDihapus){
	$row = instantiateTemplate(templateRowAktif);
	copyElementData($rowDihapus, $row);
	$("#table-dihapus").DataTable().row($rowDihapus).remove().draw(false);
	$("#table-aktif").DataTable().rows.add($row).draw(false);
}

function tambahTkhj(idTkh){
	data = {
		"ID_TKH": idTkh,
		//"ID_JABATAN": gajiTkhIdJabatan
	};
	//console.log(JSON.stringify(data));
	simpleAjax({
		url: gajiTkhjUrl + "tambah",
		data: data,
		success: function(response){
			$("#modal-tambah-tkh").modal("hide");
			if(response.result == ResultCode.OK){
				tambahRowTkhjAktif(response.data);
			}else{
				if(response.kembalikan){
					let $idDihapus = $('#table-dihapus .id-tkh[value="' + data["ID_TKH"] + '"]');
					if($idDihapus.length > 0){
						let $rowDihapus = $idDihapus.parents("tr");
						if($rowDihapus.length > 0){
							kembalikanTkhj($rowDihapus);
							return;
						}
					}
				}
				console.log(response.message);
				showError("Penambahan Tkh Gagal", response.message);
			}
		}
	});
}

function hapusTkhj($row){
	//alert(getValSearch($row, "ID_TKH"));
	simpleAjax({
		url: gajiTkhjUrl + "hapus",
		data: {"ID_TKH":getValSearch($row, "ID_TKH")},
		success: function(response){
			if(response.result == ResultCode.OK){
				hapusRowTkhjAktif($row);
			}else{
				console.log(response.message);
				showError("Penghapusan Tkh Gagal", response.message);
			}
		}
	});
}

function kembalikanTkhj($row){
	simpleAjax({
		url: gajiTkhjUrl + "kembalikan",
		data: {"ID_TKH":getValSearch($row, "ID_TKH")},
		success: function(response){
			if(response.result == ResultCode.OK){
				kembalikanRowTkhjDihapus($row);
			}else{
				console.log(response.message);
				showError("Pengembalian Tkh Gagal", response.message);
			}
		}
	});
}

$(document).ready(function(){
	$("#btn-tambah-tkhj").click(function(){
		pilihTkh(function(data){
			tambahTkhj(data["ID_TKH"]);
		});
	});
    $("#table-aktif").on("click", ".btn-hapus-tkh", function(){
		let $row = $(this).parents("tr");
		let name = $row.find(".nama-tkh").text();
		let message = 'Apa Anda yakin ingin menghapus tkh ' + name + '?';
		message = message + " Anda masih bisa mengembalikan tunjangan khusus yang dihapus dari tab 'Dihapus' nanti.";
        askConfirmation(
            'Konfirmasi Penghapusan Tunjangan Khusus', 
            message, 
            true, function(){
                hapusTkhj($row);
            }
        );
    });
	
    
    $("#table-dihapus").on("click", ".btn-kembalikan-tkh", function(){
		let $row = $(this).parents("tr");
		let name = $row.find(".nama-tkh").text();
		let message = 'Apa Anda yakin ingin mengembalikan tunjangan khusus ' + name + '?';
        askConfirmation(
            'Konfirmasi Pengembalian Tunjangan Khusus', 
            message, 
            true, function(){
				kembalikanTkhj($row);
            }
        );
    });
});