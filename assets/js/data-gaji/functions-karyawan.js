
let gajiTkhUrl = baseUrl + "gaji/tunjangan-khusus/";

function tambahRowTkhAktif(data){
	$row = instantiateTemplate(templateRowAktif);
	fillTemplate($row, data);
	$("#table-aktif").DataTable().rows.add($row).draw(false);
	return $row;
}

function hapusRowTkhAktif($rowAktif){
	$row = instantiateTemplate(templateRowDihapus);
	copyElementData($rowAktif, $row);
	$("#table-aktif").DataTable().row($rowAktif).remove().draw(false);
	$("#table-dihapus").DataTable().rows.add($row).draw(false);
}

function kembalikanRowTkhDihapus($rowDihapus){
	$row = instantiateTemplate(templateRowAktif);
	copyElementData($rowDihapus, $row);
	$("#table-dihapus").DataTable().row($rowDihapus).remove().draw(false);
	$("#table-aktif").DataTable().rows.add($row).draw(false);
}

function tambahTkh(){
	$form = $("#form-tambah-tkh");
	data = readForm($form);
	//console.log(JSON.stringify(data));
	simpleAjax({
		url: gajiTkhUrl + "tambah",
		data: data,
		success: function(response){
			$("#modal-tambah-tkh").modal("hide");
			if(response.result == ResultCode.OK){
				tambahRowTkhAktif(response.data);
			}else{
				console.log(response.message);
				showError("Penambahan Tkh Gagal", response.message);
			}
		}
	});
}

function hapusTkh($row){
	//alert(getValSearch($row, "ID_TKH"));
	simpleAjax({
		url: gajiTkhUrl + "hapus",
		data: {"ID_TKH":getValSearch($row, "ID_TKH")},
		success: function(response){
			if(response.result == ResultCode.OK){
				hapusRowTkhAktif($row);
			}else{
				console.log(response.message);
				showError("Penghapusan Tkh Gagal", response.message);
			}
		}
	});
}


function ubahTkh($row){
	let data = readRow($row);
	simpleAjax({
		url: gajiTkhUrl + "ubah",
		data: data,
		success: function(response){
			if(response.result == ResultCode.OK){
				fillTemplate($row, response.data);
				prepareSwitchOffMode($row, ".edit-mode");
				editMode($row, false);
			}else{
				console.log(response.message);
				showError("Perubahan Tkh Gagal", response.message);
			}
		}
	});
}

function kembalikanTkh($row){
	simpleAjax({
		url: gajiTkhUrl + "kembalikan",
		data: {"ID_TKH":getValSearch($row, "ID_TKH")},
		success: function(response){
			if(response.result == ResultCode.OK){
				kembalikanRowTkhDihapus($row);
			}else{
				console.log(response.message);
				showError("Pengembalian Tkh Gagal", response.message);
			}
		}
	});
}

$(document).ready(function(){
	$("#btn-tambah-tkh").click(function(){
		resetForm($("#form-tambah-tkh"));
		$("#modal-tambah-tkh").modal("show");
	});
	$("#btn-submit-tambah-tkh").click(function(){
		tambahTkh();
	});
    $("#table-aktif").on("click", ".btn-hapus-tkh", function(){
		let $row = $(this).parents("tr");
		let name = $row.find(".nama-tkh").text();
		let message = 'Apa Anda yakin ingin menghapus tkh ' + name + '?';
		message = message + " Anda masih bisa mengembalikan tunjangan khusus yang dihapus dari tab 'Dihapus' nanti.";
        askConfirmation(
            'Konfirmasi Penghapusan Tunjangan Khusus', 
            message, 
            true, function(){
                hapusTkh($row);
            }
        );
    });
	
    
    $("#table-aktif").on("click", ".btn-simpan-tkh", function(){
        $btn = $(this);
        $row = $btn.parents("tr");
		ubahTkh($row);
    });
	
    $("#table-dihapus").on("click", ".btn-kembalikan-tkh", function(){
		let $row = $(this).parents("tr");
		let name = $row.find(".nama-tkh").text();
		let message = 'Apa Anda yakin ingin mengembalikan tunjangan khusus ' + name + '?';
        askConfirmation(
            'Konfirmasi Pengembalian Tunjangan Khusus', 
            message, 
            true, function(){
				kembalikanTkh($row);
            }
        );
    });
});