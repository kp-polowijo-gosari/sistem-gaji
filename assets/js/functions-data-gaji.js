
$(document).ready(function(){
	
    $(".btn-hapus-lembur").click(function(){
        askConfirmation(
            'Konfirmasi Penghapusan', 
            'Apa Anda yakin ingin menghapus lembur 2 Januari 2020?', 
            true, function(){
                
            }
        );
    });
    
    
    $(".btn-hapus-angsuran").click(function(){
        askConfirmation(
            'Konfirmasi Penghapusan', 
            'Apa Anda yakin ingin menghapus angsuran Sepeda Motor Anugrah Putra Eko Wibowo?', 
            true, function(){
                
            }
        );
    });
    
    
    
    
    
    $(".btn-ubah-absen").click(function(){
        let $btn = $(this);
        let $row = $btn.parents(".absen-row")
        
        $row.find(".edit-mode").removeClass("d-none");
        $row.find(".normal-mode").addClass("d-none");
        
        $row.find(".edit-toggle").removeAttr("disabled");
        
        let $tanggal = $row.find(".tanggal");
        let $tanggalField = $row.find(".tanggal-field");
        $tanggalField.val($tanggal.text());
        
        let $keterangan = $row.find(".keterangan");
        let $keteranganField = $row.find(".keterangan-field");
        $keteranganField.val($keterangan.text());
        
        let $jam = $row.find(".jam");
        let $jamField = $row.find(".jam-field");
        $jamField.val($jam.text());
    });
    
    $(".btn-simpan-absen").click(function(){
        let $btn = $(this);
        let $row = $btn.parents(".absen-row");
        
        $row.find(".edit-mode").addClass("d-none");
        $row.find(".normal-mode").removeClass("d-none");
        
        $row.find(".edit-toggle").attr("disabled","true");
        
        let $tanggal = $row.find(".tanggal");
        let $tanggalField = $row.find(".tanggal-field");
        $tanggal.text($tanggalField.val());
        
        let $keterangan = $row.find(".keterangan");
        let $keteranganField = $row.find(".keterangan-field");
        $keterangan.text($keteranganField.val());
        
        let $jam = $row.find(".jam");
        let $jamField = $row.find(".jam-field");
        $jam.text($jamField.val());
    });
    
    $(".btn-ubah-lembur").click(function(){
        let $btn = $(this);
        let $row =$btn.parents(".lembur-row");
		
        $row.find(".edit-mode").removeClass("d-none");
        $row.find(".normal-mode").addClass("d-none");
		
        $row.find(".edit-toggle").removeAttr("disabled");
		
        let $keterangan = $row.find(".keterangan");
        let $keteranganField = $row.find(".keterangan-field");
        $keteranganField.val($keterangan.text());
		
        let $jam = $row.find(".jam");
        let $jamField = $row.find(".jam-field");
        $jamField.val($jam.text());
		
		
        let $tanggal = $row.find(".tanggal");
        let $tanggalField = $row.find(".tanggal-field");
        $tanggalField.val($tanggal.text());
    });
    
    $(".btn-simpan-lembur").click(function(){
        let $btn = $(this);
        let $row =$btn.parents(".lembur-row");
		
        $row.find(".edit-mode").addClass("d-none");
        $row.find(".normal-mode").removeClass("d-none");
		
        $row.find(".edit-toggle").attr("disabled", "true");
		
        let $keterangan = $row.find(".keterangan");
        let $keteranganField = $row.find(".keterangan-field");
        $keterangan.text($keteranganField.val());
		
        let $jam = $row.find(".jam");
        let $jamField = $row.find(".jam-field");
        $jam.text($jamField.val());
		
		
        let $tanggal = $row.find(".tanggal");
        let $tanggalField = $row.find(".tanggal-field");
        $tanggal.text($tanggalField.val());
		
		let $shift = $row.find(".shift");
		let $shiftField = $row.find(".shift-field");
		$shift.text($shiftField.find("option:selected").text());
		
    });
    
    
    
    $(".btn-ubah-data-gaji-karyawan").click(function(){
        let $btn = $(this);
        let $row =$btn.parents(".data-gaji-karyawan-row");
		
        $row.find(".edit-mode").removeClass("d-none");
        $row.find(".normal-mode").addClass("d-none");
        
        $row.find(".edit-toggle").removeAttr("disabled");
		
        let $gajiDilaporkan = $row.find(".gaji-dilaporkan");
        let $gajiDilaporkanField = $row.find(".gaji-dilaporkan-field");
        $gajiDilaporkanField.val($gajiDilaporkan.text());
		
        let $gajiLain = $row.find(".gaji-lain");
        let $gajiLainField = $row.find(".gaji-lain-field");
        $gajiLainField.val($gajiLain.text());
		
    });
    
    $(".btn-simpan-data-gaji-karyawan").click(function(){
        let $btn = $(this);
        let $row =$btn.parents(".data-gaji-karyawan-row");
		
        $row.find(".edit-mode").addClass("d-none");
        $row.find(".normal-mode").removeClass("d-none");
        $row.find(".edit-toggle").attr("disabled", "true");
		
        let $gajiDilaporkan = $row.find(".gaji-dilaporkan");
        let $gajiDilaporkanField = $row.find(".gaji-dilaporkan-field");
        $gajiDilaporkan.text($gajiDilaporkanField.val());
		
        let $gajiLain = $row.find(".gaji-lain");
        let $gajiLainField = $row.find(".gaji-lain-field");
        $gajiLain.text($gajiLainField.val());
		
		let $kinerja = $row.find(".kinerja");
		let $kinerjaField = $row.find(".kinerja-field");
		$kinerja.text($kinerjaField.find("option:selected").text());
		
		let $koperasi = $row.find(".koperasi");
		let $koperasiField = $row.find(".koperasi-field");
		$koperasi.text($koperasiField.find("option:selected").text());
    });
});