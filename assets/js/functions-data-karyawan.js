
//ready

$(document).ready(function(){
	
	//a
	
    $(".btn-hapus-jabatan").click(function(){
        askConfirmation(
            'Konfirmasi Penghapusan', 
            'Apa Anda yakin ingin menghapus Manager R&D?', 
            true, function(){
                
            }
        );
    });
    $(".btn-hapus-karyawan").click(function(){
        askConfirmation(
            'Konfirmasi Penghapusan', 
            'Apa Anda yakin ingin menghapus Anugrah Putra Eko Wibowo?', 
            true, function(){
                
            }
        );
    });
	
    $(".btn-hapus-shift").click(function(){
        askConfirmation(
            'Konfirmasi Penghapusan', 
            'Apa Anda yakin ingin menghapus shift I?', 
            true, function(){
                
            }
        );
    });
    
	
    
    
    $(".btn-ubah-job").click(function(){
        $btn = $(this);
        $row =$btn.parents(".job-row");
        $row.find(".edit-mode").removeClass("d-none");
        $row.find(".normal-mode").addClass("d-none");
        $namaJob = $row.find(".nama-job");
        $namaJobField = $row.find(".nama-job-field");
        $namaJobField.val($namaJob.text());
    });
    
    $(".btn-simpan-job").click(function(){
        let $btn = $(this);
        let $row =$btn.parents(".job-row");
        $row.find(".edit-mode").addClass("d-none");
        $row.find(".normal-mode").removeClass("d-none");
        let $namaJob = $row.find(".nama-job");
        let $namaJobField = $row.find(".nama-job-field");
        $namaJob.text($namaJobField.val());
    });
    
    $(".btn-ubah-jabatan").click(function(){
        $btn = $(this);
        $row =$btn.parents(".jabatan-row");
		
        $row.find(".edit-mode").removeClass("d-none");
        $row.find(".normal-mode").addClass("d-none");
		
        $jabatanName = $row.find(".jabatan-name");
        $jabatanNameField = $row.find(".jabatan-name-field");
        $jabatanNameField.val($jabatanName.text());
		
    });
    
    $(".btn-simpan-jabatan").click(function(){
        $btn = $(this);
        $row =$btn.parents(".jabatan-row");
		
        $row.find(".edit-mode").addClass("d-none");
        $row.find(".normal-mode").removeClass("d-none");
		
        $jabatanName = $row.find(".jabatan-name");
        $jabatanNameField = $row.find(".jabatan-name-field");
        $jabatanName.text($jabatanNameField.val());
		
		$jabatanJobName = $row.find(".nama-job");
		$jabatanJobNameField = $row.find(".nama-job-field");
		$jabatanJobName.text($jabatanJobNameField.find("option:selected").text());
    });
    
    $(".btn-ubah-shift").click(function(){
        $btn = $(this);
        $row =$btn.parents(".shift-row");
        $row.find(".edit-mode").removeClass("d-none");
        $row.find(".normal-mode").addClass("d-none");
		
        $shift = $row.find(".shift");
        $shiftField = $row.find(".shift-field");
        $shiftField.val($shift.text());
		
        $lembur1 = $row.find(".lembur-1");
        $lembur1Field = $row.find(".lembur-1-field");
        $lembur1Field.val($lembur1.text());
        
        $lembur2 = $row.find(".lembur-2");
        $lembur2Field = $row.find(".lembur-2-field");
        $lembur2Field.val($lembur2.text());
    });
    
    $(".btn-simpan-shift").click(function(){
        let $btn = $(this);
        let $row =$btn.parents(".shift-row");
        $row.find(".edit-mode").addClass("d-none");
        $row.find(".normal-mode").removeClass("d-none");
		
        let $shift = $row.find(".shift");
        let $shiftField = $row.find(".shift-field");
        $shift.text($shiftField.val());
		
        let $lembur1 = $row.find(".lembur-1");
        let $lembur1Field = $row.find(".lembur-1-field");
        $lembur1.text($lembur1Field.val());
        
        let $lembur2 = $row.find(".lembur-2");
        let $lembur2Field = $row.find(".lembur-2-field");
        $lembur2.text($lembur2Field.val());
    });
    
    $(".btn-ubah-karyawan").click(function(){
        $(".edit-mode").removeClass("d-none");
        $(".normal-mode").addClass("d-none");
		
        $(".edit-toggle").removeAttr("disabled");
		
		
    });
    
    $(".btn-simpan-karyawan").click(function(){
        $(".edit-mode").addClass("d-none");
        $(".normal-mode").removeClass("d-none");
		
        $(".edit-toggle").attr("disabled", "true");
		
    });
});