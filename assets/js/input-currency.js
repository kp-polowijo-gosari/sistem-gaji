// Jquery Dependency

var ic_thousand = ".";
var ic_comma = ",";
var ic_sign = "Rp ";
var ic_query = "input[data-type='currency']";



function formatNumber(n) {
  // format number 1000000 to 1,234,567
  return cleanNumber(n).replace(/\B(?=(\d{3})+(?!\d))/g, ic_thousand)
}

function cleanNumber(n){
	return n.replace(/\D/g, "");
}

function readCurrency(input_val){
	input_val = input_val + "";
  // check for decimal
  if (input_val.indexOf(ic_comma) >= 0) {

    // get position of first decimal
    // this prevents multiple decimals from
    // being entered
    var decimal_pos = input_val.indexOf(ic_comma);

    // split number by decimal point
    var left_side = input_val.substring(0, decimal_pos);
    var right_side = input_val.substring(decimal_pos);
	
	left_side = cleanNumber(left_side) * 1;
	right_side = cleanNumber(right_side);
	right_side *= Math.pow(10, -right_side.length);
	return left_side + right_side;
  }else{
	return cleanNumber(input_val) * 1;
  }
}

function formatCurrency(input_val){
	input_val = input_val + "";
  // check for decimal
  if (input_val.indexOf(ic_comma) >= 0) {

    // get position of first decimal
    // this prevents multiple decimals from
    // being entered
    var decimal_pos = input_val.indexOf(ic_comma);

    // split number by decimal point
    var left_side = input_val.substring(0, decimal_pos);
    var right_side = input_val.substring(decimal_pos);

    // add commas to left side of number
    left_side = formatNumber(left_side);

    // validate right side
    right_side = formatNumber(right_side);
    
    // On blur make sure 2 numbers after decimal
    if (blur === "blur") {
      right_side += "00";
    }
    
    // Limit decimal to only 2 digits
    right_side = right_side.substring(0, 2);

    // join number by .
    input_val = ic_sign + left_side + ic_comma + right_side;

  } else {
    // no decimal entered
    // add commas to number
    // remove all non-digits
    input_val = formatNumber(input_val);
    input_val = ic_sign + input_val;
    
    // final formatting
    if (blur === "blur") {
      input_val += ic_comma + "00";
    }
  }
  return input_val;
}

function formatCurrencyFull(input, blur) {
  // appends $ to value, validates decimal side
  // and puts cursor back in right position.
  
  // get input value
  var input_val = input.val();
  
  // don't validate empty input
  if (input_val === "") { return; }
  
  // original length
  var original_len = input_val.length;

  // initial caret position 
  var caret_pos = input.prop("selectionStart");
  
  var input_val = formatCurrency(input_val);
  
  // send updated string to input
  input.val(input_val);

  // put caret back in the right position
  var updated_len = input_val.length;
  caret_pos = updated_len - original_len + caret_pos;
  input[0].setSelectionRange(caret_pos, caret_pos);
}

function initInputCurrency($parent){
	let $els = $parent;
	if(!$parent.is(ic_query))
		$els = $parent.find(ic_query);
	console.log($els.length);
	if($els.length == 0) return;
	$els.each(function(){
		let $el = $(this);
		formatCurrencyFull($el, "blur");
	});
	$els.on({
		keyup: function() {
		  formatCurrencyFull($(this));
		},
		blur: function() { 
		  formatCurrencyFull($(this), "blur");
		}
	});
}

$(document).ready(function(){
	$(ic_query).each(function(){
		initInputCurrency($(this));
	});
});