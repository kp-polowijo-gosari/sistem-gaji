
$(document).ready(function(){
	
    
    $(".btn-hapus-akun").click(function(){
        askConfirmation(
            'Konfirmasi Penghapusan Akun', 
            'Apa Anda yakin ingin menghapus akun Anugrah Putra Eko Wibowo?', 
            true, function(){
                
            }
        );
    });
    
    
    
    $(".btn-simpan-password").click(function(){
        askConfirmation(
            'Konfirmasi Perubahan Password', 
            'Apa Anda yakin ingin mengubah password?', 
            true, function(){
                $("#password-collapse").collapse("hide");
                $(".edit-password-mode").addClass("d-none");
                $(".btn-simpan-password").removeClass("d-none");
            }
        );
    });
    
    
    
    $(".btn-reset-password").click(function(){
        askConfirmation(
            'Konfirmasi Reset Password', 
            'Apa Anda yakin ingin mereset password?', 
            true, function(){
                showNotice(
                    "Reset Password Berhasil",
                    "Password telah direset menjadi password acak yang telah dikirim ke email Anda. Silahkan cek email Anda, login, lalu ubah password Anda",
                    false, function(){}
                );
            }
        );
    });
    
    
});