//func karyawan


function tambahRowKarAktif(data){
	$row = instantiateTemplate(templateRowAktif);
	fillTemplate($row, data);
	$("#table-aktif").DataTable().rows.add($row).draw(false);
	return $row;
}

function hapusRowKarAktif($rowAktif){
	$row = instantiateTemplate(templateRowDihapus);
	copyElementData($rowAktif, $row);
	$("#table-aktif").DataTable().row($rowAktif).remove().draw(false);
	$("#table-dihapus").DataTable().rows.add($row).draw(false);
	
}
function kembalikanRowKarDihapus($rowDihapus){
	$row = instantiateTemplate(templateRowAktif);
	copyElementData($rowDihapus, $row);
	$("#table-dihapus").DataTable().row($rowDihapus).remove().draw(false);
	$("#table-aktif").DataTable().rows.add($row).draw(false);
}

karKarUrl = baseUrl + "karyawan/karyawan/";

function tambahKaryawan(){
	$form = $("#form-karyawan");
	data = $form.serialize();
	console.log("data: " + JSON.stringify(data));
	simpleAjax({
		url: karKarUrl + "tambah",
		data: data,
		success: function(response){
			if(response.result == ResultCode.OK){
				window.location = response.redirect;
			}else{
				console.log(response.message);
				showError("Penambahan Karyawan Gagal", response.message);
			}
		}
	});
}

function hapusKaryawan1(idKar){
	simpleAjax({
		url: karKarUrl + "hapus",
		data: {"ID_KAR":idKar},
		success: function(response){
			if(response.result == ResultCode.OK){
				window.location = response.redirect;
			}else{
				console.log(response.message);
				showError("Penghapusan Karyawan Gagal", response.message);
			}
		}
	});
}

function ubahKaryawan(){
	$form = $("#form-karyawan");
	data = $form.serialize();
	simpleAjax({
		url: karKarUrl + "ubah",
		data: data,
		success: function(response){
			if(response.result == ResultCode.OK){
				window.location.reload();
			}else{
				console.log(response.message);
				showError("Perubahan Karyawan Gagal", response.message);
			}
		}
	});
}

function kembalikanKaryawan1(idKar){
	let data = {"ID_KAR":idKar};
	simpleAjax({
		url: karKarUrl + "kembalikan",
		data: data,
		success: function(response){
			if(response.result == ResultCode.OK){
				window.location = response.redirect;
			}else{
				console.log(response.message);
				showError("Pengembalian Karyawan Gagal", response.message);
			}
		}
	});
}

function calcMasaKerja(){
	
	let tahunMasuk = $("input[name=\"tanggal-masuk\"]").val();
	let masaKerja = calcAgeStr(tahunMasuk);
	if(masaKerja.trim() == "") masaKerja = "Baru";
	$(".masa-kerja").text(masaKerja);
}

function hapusKaryawan($row){
	simpleAjax({
		url: karKarUrl + "hapus",
		data: {"ID_KAR":getValSearch($row, "ID_KAR")},
		success: function(response){
			if(response.result == ResultCode.OK){
				hapusRowKarAktif($row);
			}else{
				console.log(response.message);
				showError("Penghapusan Karyawan Gagal", response.message);
			}
		}
	});
}


function kembalikanKaryawan($row){
	simpleAjax({
		url: karKarUrl + "kembalikan",
		data: {"ID_KAR":getValSearch($row, "ID_KAR")},
		success: function(response){
			if(response.result == ResultCode.OK){
				kembalikanRowKarDihapus($row);
			}else{
				console.log(response.message);
				showError("Pengembalian Karyawan Gagal", response.message);
			}
		}
	});
}

//ready

$(document).ready(function(){
	//kar
	$("#btn-pilih-jabatan").click(function(){
		pilihJabatan(function(data){
			$("#input-id-jabatan").val(data["ID_JABATAN"]);
			$("#btn-pilih-jabatan").text(data["NAMA_JABATAN"] + " (" + data["NAMA_JOB"] + ")");
		});
	});
	$("#btn-pilih-subdept").click(function(){
		pilihSubdept(function(data){
			$("#input-id-subdept").val(data["ID_SUBDEPT"]);
			$("#btn-pilih-subdept").text(data["NAMA_DEPT"] + " / " + data["NAMA_SUBDEPT"]);
		});
	});
	$(".btn-batal-kar").click(function(){
		editMode($("#content"), false);
		setFormEnabled("#form-karyawan", false);
		resetForm($("#form-karyawan"));
	});
	$(".btn-ubah-kar").click(function(){
		editMode($("#content"), true);
		setFormEnabled("#form-karyawan", true);
	});
	
	$(".btn-submit-add-kar").click(function(){
        askConfirmation(
            'Konfirmasi Penambahan karyawan', 
            'Apa Anda yakin data sudah benar?', 
            false, function(){
				tambahKaryawan();
            }
        );
	});
	
	$(".btn-simpan-kar").click(function(){
        askConfirmation(
            'Konfirmasi Perubahan karyawan', 
            'Apa Anda yakin data sudah benar?', 
            false, function(){
				ubahKaryawan();
            }
        );
	});
	
	$("input[name=\"TANGGAL_MASUK\"]").change(function(){
		calcMasaKerja();
	});
	
    $("#btn-hapus-kar").click(function(){
		let name = $("input[name=\"NAMA_KAR\"]").val();
		let message = "Apa Anda yakin ingin menghapus karyawan bernama " + name + "? Anda masih bisa mengembalikan karyawan yang dihapus dari tab 'Dihapus' nanti.";
        askConfirmation(
            'Konfirmasi Penghapusan Karyawan', 
            message, 
            true, function(){
                hapusKaryawan1($("input[name=\"ID_KAR\"]").val());
            }
        );
    });
    $("#btn-kembalikan-kar").click(function(){
		let name = $("input[name=\"NAMA_KAR\"]").val();
		let message = "Apa Anda yakin ingin mengembalikan karyawan bernama " + name + "?";
        askConfirmation(
            'Konfirmasi Penghapusan Karyawan', 
            message, 
            false, function(){
                kembalikanKaryawan1($("input[name=\"ID_KAR\"]").val());
            }
        );
    });
	
    $("#table-aktif").on("click", ".btn-hapus-kar", function(){
		let $row = $(this).parents("tr");
		let name = $row.find(".nama-kar").text();
		let jumlahKar = $row.find(".jumlah-kar").text();
		let message = 'Apa Anda yakin ingin menghapus karyawan ' + name + '?';
		let additions = [];
		if(jumlahKar > 0){
			additions.push(jumlahKar + " karyawan");
		}
		if(jumlahKar > 0){
			additions.push(jumlahKar + " karyawan");
		}
		if(additions.length > 0){
			message = message + " Sebanyak " + additions.join(" dan ") + " yang ada di dalamnya juga akan ikut terhapus.";
		}
		message = message + " Anda masih bisa mengembalikan karyawan yang dihapus dari tab 'Dihapus' nanti.";
        askConfirmation(
            'Konfirmasi Penghapusan Karyawan', 
            message, 
            true, function(){
                hapusKaryawan($row);
            }
        );
    });
    
    $("#table-aktif").on("click", ".btn-simpan-kar", function(){
        $btn = $(this);
        $row = $btn.parents("tr");
		ubahKaryawan($row);
    });
	
    $("#table-dihapus").on("click", ".btn-kembalikan-kar", function(){
		let $row = $(this).parents("tr");
		let name = $row.find(".nama-kar").text();
		let jumlahKar = $row.find(".jumlah-kar").text();
		let message = 'Apa Anda yakin ingin mengembalikan karyawan ' + name + '?';
		let additions = [];
		if(jumlahKar > 0){
			additions.push(jumlahKar + " karyawan");
		}
		if(additions.length > 0){
			message = message + " Sebanyak " + additions.join(" dan ") + " yang ada di dalamnya juga akan ikut dikembalikan.";
		}
        askConfirmation(
            'Konfirmasi Pengembalian Karyawan', 
            message, 
            true, function(){
				kembalikanKaryawan($row);
            }
        );
    });
	
});