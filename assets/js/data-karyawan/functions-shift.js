
//func shift 

function tambahRowShiftAktif(data){
	$row = instantiateTemplate(templateRowAktif);
	fillTemplate($row, data);
	$("#table-aktif").DataTable().rows.add($row).draw(false);
	return $row;
}

function hapusRowShiftAktif($rowAktif){
	$row = instantiateTemplate(templateRowDihapus);
	copyElementData($rowAktif, $row);
	$("#table-aktif").DataTable().row($rowAktif).remove().draw(false);
	$("#table-dihapus").DataTable().rows.add($row).draw(false);
	
}

function kembalikanRowShiftDihapus($rowDihapus){
	$row = instantiateTemplate(templateRowAktif);
	copyElementData($rowDihapus, $row);
	$("#table-dihapus").DataTable().row($rowDihapus).remove().draw(false);
	$("#table-aktif").DataTable().rows.add($row).draw(false);
}

function tambahShift(){
	$form = $("#form-add-shift");
	data = $form.serializeObject();
	simpleAjax({
		url: karShiftUrl + "tambah",
		data: data,
		success: function(response){
			$("#modal-add-shift").modal("hide");
			if(response.result == ResultCode.OK){
				tambahRowShiftAktif(response.data);
			}else{
				console.log(response.message);
				showError("Penambahan Shift Gagal", response.message);
			}
		}
	});
}

let karShiftUrl = baseUrl + "karyawan/shift/";

function hapusShift($row){
	simpleAjax({
		url: karShiftUrl + "hapus",
		data: {"ID_SHIFT":getValSearch($row, "ID_SHIFT")},
		success: function(response){
			if(response.result == ResultCode.OK){
				hapusRowShiftAktif($row);
			}else{
				console.log(response.message);
				showError("Penghapusan Shift Gagal", response.message);
			}
		}
	});
}


function ubahShift($row){
	data = readRow($row);
	simpleAjax({
		url: karShiftUrl + "ubah",
		data: data,
		success: function(response){
			if(response.result == ResultCode.OK){
				fillTemplate($row, response.data);
				prepareSwitchOffMode($row, ".edit-mode");
				editMode($row, false);
			}else{
				console.log(response.message);
				showError("Perubahan Shift Gagal", response.message);
			}
		}
	});
}

function kembalikanShift($row){
	simpleAjax({
		url: karShiftUrl + "kembalikan",
		data: {"ID_SHIFT":getValSearch($row, "ID_SHIFT")},
		success: function(response){
			if(response.result == ResultCode.OK){
				kembalikanRowShiftDihapus($row);
			}else{
				console.log(response.message);
				showError("Pengembalian Shift Gagal", response.message);
			}
		}
	});
}

//ready

$(document).ready(function(){
	//shift
	
	$("#btn-submit-add-shift").click(function(){
		tambahShift();
	});
	
    $("#table-aktif").on("click", ".btn-hapus-shift", function(){
		let $row = $(this).parents("tr");
		let name = $row.find(".nama-shift").text();
		let jumlahJabatan = $row.find(".jumlah-jabatan").text();
		let jumlahKar = $row.find(".jumlah-kar").text();
		let message = 'Apa Anda yakin ingin menghapus shift ' + name + '?';
		let additions = [];
		if(jumlahJabatan > 0){
			additions.push(jumlahJabatan + " jabatan");
		}
		if(jumlahKar > 0){
			additions.push(jumlahKar + " karyawan");
		}
		if(additions.length > 0){
			message = message + " Sebanyak " + additions.join(" dan ") + " yang ada di dalamnya juga akan ikut terhapus.";
		}
		message = message + " Anda masih bisa mengembalikan shift yang dihapus dari tab 'Dihapus' nanti.";
        askConfirmation(
            'Konfirmasi Penghapusan Shift', 
            message, 
            true, function(){
                hapusShift($row);
            }
        );
    });
	
    
    $("#table-aktif").on("click", ".btn-simpan-shift", function(){
        $btn = $(this);
        $row =$btn.parents("tr");
		ubahShift($row);
    });
	
    $("#table-dihapus").on("click", ".btn-kembalikan-shift", function(){
		let $row = $(this).parents("tr");
		let name = $row.find(".nama-shift").text();
		let message = 'Apa Anda yakin ingin mengembalikan shift ' + name + '?';
		let additions = [];
        askConfirmation(
            'Konfirmasi Pengembalian Shift', 
            message, 
            true, function(){
				kembalikanShift($row);
            }
        );
    });
	
	
});