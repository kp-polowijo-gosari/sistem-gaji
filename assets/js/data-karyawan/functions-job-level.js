
//func job level


function tambahRowJobAktif(data){
	$row = instantiateTemplate(templateRowAktif);
	fillTemplate($row, data);
	$("#table-aktif").DataTable().rows.add($row).draw(false);
	return $row;
}

function hapusRowJobAktif($rowAktif){
	$row = instantiateTemplate(templateRowDihapus);
	copyElementData($rowAktif, $row);
	$("#table-aktif").DataTable().row($rowAktif).remove().draw(false);
	$("#table-dihapus").DataTable().rows.add($row).draw(false);
	
}
function kembalikanRowJobDihapus($rowDihapus){
	$row = instantiateTemplate(templateRowAktif);
	copyElementData($rowDihapus, $row);
	$("#table-dihapus").DataTable().row($rowDihapus).remove().draw(false);
	$("#table-aktif").DataTable().rows.add($row).draw(false);
}

let karJobUrl = baseUrl + "karyawan/job-level/";

function tambahJob(){
	$form = $("#form-add-job");
	data = $form.serializeObject();
	simpleAjax({
		url: karJobUrl + "tambah",
		data: data,
		success: function(response){
			$("#modal-add-job").modal("hide");
			if(response.result == ResultCode.OK){
				tambahRowJobAktif(response.data);
			}else{
				console.log(response.message);
				showError("Penambahan Job Gagal", response.message);
			}
		}
	});
}

function hapusJob($row){
	simpleAjax({
		url: karJobUrl + "hapus",
		data: {"ID_JOB":getValSearch($row, "ID_JOB")},
		success: function(response){
			if(response.result == ResultCode.OK){
				hapusRowJobAktif($row);
			}else{
				console.log(response.message);
				showError("Penghapusan Job Gagal", response.message);
			}
		}
	});
}


function ubahJob($row){
	data = readRow($row);
	console.log(JSON.stringify(data));
	simpleAjax({
		url: karJobUrl + "ubah",
		data: data,
		success: function(response){
			if(response.result == ResultCode.OK){
				fillTemplate($row, response.data);
				prepareSwitchOffMode($row, ".edit-mode");
				editMode($row, false);
			}else{
				console.log(response.message);
				showError("Perubahan Job Gagal", response.message);
			}
		}
	});
}

function kembalikanJob($row){
	simpleAjax({
		url: karJobUrl + "kembalikan",
		data: {"ID_JOB":getValSearch($row, "ID_JOB")},
		success: function(response){
			if(response.result == ResultCode.OK){
				kembalikanRowJobDihapus($row);
			}else{
				console.log(response.message);
				showError("Pengembalian Job Gagal", response.message);
			}
		}
	});
}

//ready

$(document).ready(function(){
	//job
	
	$("#btn-submit-add-job").click(function(){
		tambahJob();
	});
	
    $("#table-aktif").on("click", ".btn-hapus-job", function(){
		let $row = $(this).parents("tr");
		let name = $row.find(".nama-job").text();
		let jumlahJabatan = $row.find(".jumlah-jabatan").text();
		let jumlahKar = $row.find(".jumlah-kar").text();
		let message = 'Apa Anda yakin ingin menghapus job ' + name + '?';
		let additions = [];
		if(jumlahJabatan > 0){
			additions.push(jumlahJabatan + " jabatan");
		}
		if(jumlahKar > 0){
			additions.push(jumlahKar + " karyawan");
		}
		if(additions.length > 0){
			message = message + " Sebanyak " + additions.join(" dan ") + " yang ada di dalamnya juga akan ikut terhapus.";
		}
		message = message + " Anda masih bisa mengembalikan job yang dihapus dari tab 'Dihapus' nanti.";
        askConfirmation(
            'Konfirmasi Penghapusan Job', 
            message, 
            true, function(){
                hapusJob($row);
            }
        );
    });
	
    
    $("#table-aktif").on("click", ".btn-simpan-job", function(){
        $btn = $(this);
        $row =$btn.parents("tr");
		ubahJob($row);
    });
	
    $("#table-dihapus").on("click", ".btn-kembalikan-job", function(){
		let $row = $(this).parents("tr");
		let name = $row.find(".nama-job").text();
		let jumlahJabatan = $row.find(".jumlah-jabatan").text();
		let jumlahKar = $row.find(".jumlah-kar").text();
		let message = 'Apa Anda yakin ingin mengembalikan job ' + name + '?';
		let additions = [];
		if(jumlahJabatan > 0){
			additions.push(jumlahJabatan + " jabatan");
		}
		if(jumlahKar > 0){
			additions.push(jumlahKar + " karyawan");
		}
		if(additions.length > 0){
			message = message + " Sebanyak " + additions.join(" dan ") + " yang ada di dalamnya juga akan ikut dikembalikan.";
		}
        askConfirmation(
            'Konfirmasi Pengembalian Job', 
            message, 
            true, function(){
				kembalikanJob($row);
            }
        );
    });
	
	
});