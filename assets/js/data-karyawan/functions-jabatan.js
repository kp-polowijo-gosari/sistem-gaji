//func jabatan

function tambahRowJabatanAktif(data){
	$row = instantiateTemplate(templateRowAktif);
	fillTemplate($row, data);
	$("#table-aktif").DataTable().rows.add($row).draw(false);
	return $row;
}

function hapusRowJabatanAktif($rowAktif){
	$row = instantiateTemplate(templateRowDihapus);
	copyElementData($rowAktif, $row);
	$("#table-aktif").DataTable().row($rowAktif).remove().draw(false);
	$("#table-dihapus").DataTable().rows.add($row).draw(false);
	
}
function kembalikanRowJabatanDihapus($rowDihapus){
	$row = instantiateTemplate(templateRowAktif);
	copyElementData($rowDihapus, $row);
	$("#table-dihapus").DataTable().row($rowDihapus).remove().draw(false);
	$("#table-aktif").DataTable().rows.add($row).draw(false);
}

let karJabatanUrl = baseUrl + "karyawan/jabatan/";

function tambahJabatan(){
	$form = $("#form-add-jabatan");
	data = $form.serializeObject();
	simpleAjax({
		url: karJabatanUrl + "tambah",
		data: data,
		success: function(response){
			$("#modal-add-jabatan").modal("hide");
			if(response.result == ResultCode.OK){
				tambahRowJabatanAktif(response.data);
			}else{
				console.log(response.message);
				showError("Penambahan Jabatan Gagal", response.message);
			}
		}
	});
}

function hapusJabatan($row){
	simpleAjax({
		url: karJabatanUrl + "hapus",
		data: {"ID_JABATAN":getValSearch($row, "ID_JABATAN")},
		success: function(response){
			if(response.result == ResultCode.OK){
				hapusRowJabatanAktif($row);
			}else{
				console.log(response.message);
				showError("Penghapusan Jabatan Gagal", response.message);
			}
		}
	});
}


function ubahJabatan($row){
	data = readRow($row);
	console.log(JSON.stringify(data));
	simpleAjax({
		url: karJabatanUrl + "ubah",
		data: data,
		success: function(response){
			if(response.result == ResultCode.OK){
				fillTemplate($row, response.data);
				prepareSwitchOffMode($row, ".edit-mode");
				editMode($row, false);
			}else{
				console.log(response.message);
				showError("Perubahan Jabatan Gagal", response.message);
			}
		}
	});
}

function kembalikanJabatan($row){
	simpleAjax({
		url: karJabatanUrl + "kembalikan",
		data: {"ID_JABATAN":getValSearch($row, "ID_JABATAN")},
		success: function(response){
			if(response.result == ResultCode.OK){
				kembalikanRowJabatanDihapus($row);
			}else{
				console.log(response.message);
				showError("Pengembalian Jabatan Gagal", response.message);
			}
		}
	});
}

//ready

$(document).ready(function(){
	//jabatan
	$("#btn-submit-add-jabatan").click(function(){
		tambahJabatan();
	});
	
    $("#table-aktif").on("click", ".btn-hapus-jabatan", function(){
		let $row = $(this).parents("tr");
		let name = $row.find(".nama-jabatan").text();
		let jumlahJabatan = $row.find(".jumlah-jabatan").text();
		let jumlahKar = $row.find(".jumlah-kar").text();
		let message = 'Apa Anda yakin ingin menghapus jabatan ' + name + '?';
		let additions = [];
		if(jumlahJabatan > 0){
			additions.push(jumlahJabatan + " jabatan");
		}
		if(jumlahKar > 0){
			additions.push(jumlahKar + " karyawan");
		}
		if(additions.length > 0){
			message = message + " Sebanyak " + additions.join(" dan ") + " yang ada di dalamnya juga akan ikut terhapus.";
		}
		message = message + " Anda masih bisa mengembalikan jabatan yang dihapus dari tab 'Dihapus' nanti.";
        askConfirmation(
            'Konfirmasi Penghapusan Jabatan', 
            message, 
            true, function(){
                hapusJabatan($row);
            }
        );
    });
	
    $("#table-aktif").on("click", ".btn-simpan-jabatan", function(){
        $btn = $(this);
        $row =$btn.parents("tr");
		ubahJabatan($row);
    });
	
    $("#table-dihapus").on("click", ".btn-kembalikan-jabatan", function(){
		let $row = $(this).parents("tr");
		let name = $row.find(".nama-jabatan").text();
		let jumlahKar = $row.find(".jumlah-kar").text();
		let message = 'Apa Anda yakin ingin mengembalikan jabatan ' + name + '?';
		let additions = [];
		if(jumlahKar > 0){
			additions.push(jumlahKar + " karyawan");
		}
		if(additions.length > 0){
			message = message + " Sebanyak " + additions.join(" dan ") + " yang ada di dalamnya juga akan ikut dikembalikan.";
		}
        askConfirmation(
            'Konfirmasi Pengembalian Jabatan', 
            message, 
            true, function(){
				kembalikanJabatan($row);
            }
        );
    });
	
	
});