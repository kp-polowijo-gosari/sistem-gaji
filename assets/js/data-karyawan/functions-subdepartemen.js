//func subdepartemen


function tambahRowSubdeptAktif(data){
	$row = instantiateTemplate(templateRowAktif);
	fillTemplate($row, data);
	$("#table-aktif").DataTable().rows.add($row).draw(false);
	return $row;
}

function hapusRowSubdeptAktif($rowAktif){
	$row = instantiateTemplate(templateRowDihapus);
	copyElementData($rowAktif, $row);
	$("#table-aktif").DataTable().row($rowAktif).remove().draw(false);
	$("#table-dihapus").DataTable().rows.add($row).draw(false);
	
}
function kembalikanRowSubdeptDihapus($rowDihapus){
	$row = instantiateTemplate(templateRowAktif);
	copyElementData($rowDihapus, $row);
	$("#table-dihapus").DataTable().row($rowDihapus).remove().draw(false);
	$("#table-aktif").DataTable().rows.add($row).draw(false);
}

function tambahSubdepartemen(){
	$form = $("#form-add-subdept");
	data = $form.serializeObject();
	simpleAjax({
		url: karSubdeptUrl + "tambah",
		data: data,
		success: function(response){
			$("#modal-add-subdept").modal("hide");
			if(response.result == ResultCode.OK){
				tambahRowSubdeptAktif(response.data);
			}else{
				console.log(response.message);
				showError("Penambahan Subdepartemen Gagal", response.message);
			}
		}
	});
}

function hapusSubdepartemen($row){
	simpleAjax({
		url: karSubdeptUrl + "hapus",
		data: {"ID_SUBDEPT":getValSearch($row, "ID_SUBDEPT")},
		success: function(response){
			if(response.result == ResultCode.OK){
				hapusRowSubdeptAktif($row);
			}else{
				console.log(response.message);
				showError("Penghapusan Subdepartemen Gagal", response.message);
			}
		}
	});
}

let karSubdeptUrl = baseUrl + "karyawan/subdepartemen/";

function ubahSubdepartemen($row){
	data = readRow($row);
	simpleAjax({
		url: karSubdeptUrl + "ubah",
		data: data,
		success: function(response){
			if(response.result == ResultCode.OK){
				fillTemplate($row, response.data);
				prepareSwitchOffMode($row, ".edit-mode");
				editMode($row, false);
			}else{
				console.log(response.message);
				showError("Perubahan Subdepartemen Gagal", response.message);
			}
		}
	});
}

function kembalikanSubdepartemen($row){
	simpleAjax({
		url: karSubdeptUrl + "kembalikan",
		data: {"ID_SUBDEPT":getValSearch($row, "ID_SUBDEPT")},
		success: function(response){
			if(response.result == ResultCode.OK){
				kembalikanRowSubdeptDihapus($row);
			}else{
				console.log(response.message);
				showError("Pengembalian Subdepartemen Gagal", response.message);
			}
		}
	});
}

//ready

$(document).ready(function(){
	//subdept
	$("#btn-submit-add-subdept").click(function(){
		tambahSubdepartemen();
	});
	
    $("#table-aktif").on("click", ".btn-hapus-subdept", function(){
		let $row = $(this).parents("tr");
		let name = $row.find(".nama-subdept").text();
		let jumlahSubdept = $row.find(".jumlah-subdept").text();
		let jumlahKar = $row.find(".jumlah-kar").text();
		let message = 'Apa Anda yakin ingin menghapus subdepartemen ' + name + '?';
		let additions = [];
		if(jumlahSubdept > 0){
			additions.push(jumlahSubdept + " subdepartemen");
		}
		if(jumlahKar > 0){
			additions.push(jumlahKar + " karyawan");
		}
		if(additions.length > 0){
			message = message + " Sebanyak " + additions.join(" dan ") + " yang ada di dalamnya juga akan ikut terhapus.";
		}
		message = message + " Anda masih bisa mengembalikan subdepartemen yang dihapus dari tab 'Dihapus' nanti.";
        askConfirmation(
            'Konfirmasi Penghapusan Subdepartemen', 
            message, 
            true, function(){
                hapusSubdepartemen($row);
            }
        );
    });
	
    $("#table-aktif").on("click", ".btn-simpan-subdept", function(){
        $btn = $(this);
        $row =$btn.parents("tr");
		ubahSubdepartemen($row);
    });
	
    $("#table-dihapus").on("click", ".btn-kembalikan-subdept", function(){
		let $row = $(this).parents("tr");
		let name = $row.find(".nama-subdept").text();
		let jumlahKar = $row.find(".jumlah-kar").text();
		let message = 'Apa Anda yakin ingin mengembalikan subdepartemen ' + name + '?';
		let additions = [];
		if(jumlahKar > 0){
			additions.push(jumlahKar + " karyawan");
		}
		if(additions.length > 0){
			message = message + " Sebanyak " + additions.join(" dan ") + " yang ada di dalamnya juga akan ikut dikembalikan.";
		}
        askConfirmation(
            'Konfirmasi Pengembalian Subdepartemen', 
            message, 
            true, function(){
				kembalikanSubdepartemen($row);
            }
        );
    });
	
	
});