//func departemen

function tambahRowDeptAktif(data){
	$row = instantiateTemplate(templateRowAktif);
	fillTemplate($row, data);
	$("#table-aktif").DataTable().rows.add($row).draw(false);
	return $row;
}

function hapusRowDeptAktif($rowAktif){
	$row = instantiateTemplate(templateRowDihapus);
	copyElementData($rowAktif, $row);
	$("#table-aktif").DataTable().row($rowAktif).remove().draw(false);
	$("#table-dihapus").DataTable().rows.add($row).draw(false);
	
}
function kembalikanRowDeptDihapus($rowDihapus){
	$row = instantiateTemplate(templateRowAktif);
	copyElementData($rowDihapus, $row);
	$("#table-dihapus").DataTable().row($rowDihapus).remove().draw(false);
	$("#table-aktif").DataTable().rows.add($row).draw(false);
}

let karDeptUrl = baseUrl + "karyawan/departemen/";

function tambahDepartemen(){
	$form = $("#form-add-dept");
	data = $form.serializeObject();
	simpleAjax({
		url: karDeptUrl + "tambah",
		data: data,
		success: function(response){
			$("#modal-add-dept").modal("hide");
			if(response.result == ResultCode.OK){
				tambahRowDeptAktif(response.data);
			}else{
				console.log(response.message);
				showError("Penambahan Departemen Gagal", response.message);
			}
		}
	});
}

function hapusDepartemen($row){
	simpleAjax({
		url: karDeptUrl + "hapus",
		data: {"ID_DEPT":getValSearch($row, "ID_DEPT")},
		success: function(response){
			if(response.result == ResultCode.OK){
				hapusRowDeptAktif($row);
			}else{
				console.log(response.message);
				showError("Penghapusan Departemen Gagal", response.message);
			}
		}
	});
}


function ubahDepartemen($row){
	data = readRow($row);
	simpleAjax({
		url: karDeptUrl + "ubah",
		data: data,
		success: function(response){
			if(response.result == ResultCode.OK){
				fillTemplate($row, response.data);
				prepareSwitchOffMode($row, ".edit-mode");
				editMode($row, false);
			}else{
				console.log(response.message);
				showError("Perubahan Departemen Gagal", response.message);
			}
		}
	});
}

function kembalikanDepartemen($row){
	simpleAjax({
		url: karDeptUrl + "kembalikan",
		data: {"ID_DEPT":getValSearch($row, "ID_DEPT")},
		success: function(response){
			if(response.result == ResultCode.OK){
				kembalikanRowDeptDihapus($row);
			}else{
				console.log(response.message);
				showError("Pengembalian Departemen Gagal", response.message);
			}
		}
	});
}

//ready

$(document).ready(function(){
	//dept
	$("#btn-submit-add-dept").click(function(){
		tambahDepartemen();
	});
	
    $("#table-aktif").on("click", ".btn-hapus-dept", function(){
		let $row = $(this).parents("tr");
		let name = $row.find(".nama-dept").text();
		let jumlahSubdept = $row.find(".jumlah-subdept").text();
		let jumlahKar = $row.find(".jumlah-kar").text();
		let message = 'Apa Anda yakin ingin menghapus departemen ' + name + '?';
		let additions = [];
		if(jumlahSubdept > 0){
			additions.push(jumlahSubdept + " subdepartemen");
		}
		if(jumlahKar > 0){
			additions.push(jumlahKar + " karyawan");
		}
		if(additions.length > 0){
			message = message + " Sebanyak " + additions.join(" dan ") + " yang ada di dalamnya juga akan ikut terhapus.";
		}
		message = message + " Anda masih bisa mengembalikan departemen yang dihapus dari tab 'Dihapus' nanti.";
        askConfirmation(
            'Konfirmasi Penghapusan Departemen', 
            message, 
            true, function(){
                hapusDepartemen($row);
            }
        );
    });
	
    $("#table-aktif").on("click", ".btn-simpan-dept", function(){
        $btn = $(this);
        $row =$btn.parents("tr");
		ubahDepartemen($row);
    });
	
    $("#table-dihapus").on("click", ".btn-kembalikan-dept", function(){
		let $row = $(this).parents("tr");
		let name = $row.find(".nama-dept").text();
		let jumlahSubdept = $row.find(".jumlah-subdept").text();
		let jumlahKar = $row.find(".jumlah-kar").text();
		let message = 'Apa Anda yakin ingin mengembalikan departemen ' + name + '?';
		let additions = [];
		if(jumlahSubdept > 0){
			additions.push(jumlahSubdept + " subdepartemen");
		}
		if(jumlahKar > 0){
			additions.push(jumlahKar + " karyawan");
		}
		if(additions.length > 0){
			message = message + " Sebanyak " + additions.join(" dan ") + " yang ada di dalamnya juga akan ikut dikembalikan.";
		}
        askConfirmation(
            'Konfirmasi Pengembalian Departemen', 
            message, 
            true, function(){
				kembalikanDepartemen($row);
            }
        );
    });
	
	
});