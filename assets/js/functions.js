function hapusAkun(index){
    $("#btn-confirm-delete").off("click");
    $("#btn-confirm-delete").click(function(){
        onHapusAkun(index);
    });
    $("#modal-hapus-akun").modal("show")
}

function onHapusAkun(index){
    $("#modal-hapus-akun").modal("hide");
}

function simpanPasswordBaru(index){
    $("#btn-confirm-change-password").off("click");
    $("#btn-confirm-change-password").click(function(){
        onSimpanPasswordBaru(index);
    });

    $("#modal-ganti-password").modal("show");
}

function onSimpanPasswordBaru (index){
    $("#modal-ganti-password").modal("hide");
}

function tambahAngsuran(index){
    $("#btn-simpan-angsuran").off("click");
    $("#btn-simpan-angsuran").click(function(){
        onSimpanAngsuran(index);
    });
    
    $("#modal-tambah-angsuran").modal("show");
}

function onSimpanAngsuran (index){
    $("#modal-tambah-angsuran").modal("hide");
}

function hapusAngsuran(index){
    $("#btn-confirm-hapus-angsuran").off("click");
    $("#btn-confirm-hapus-angsuran").click(function(){
        onHapusAngsuran(index);
    });
    
    $("#modal-hapus-angsuran").modal("show");
}

function onHapusAngsuran(index){
    $("#modal-hapus-angsuran").modal("hide");
}

$(document).ready(function(){
    $(".btn-hapus-akun").click(function(){
       hapusAkun(0);
    });
    
    $(".btn-simpan-password-baru").click(function(){
        simpanPasswordBaru(0);
    });
    
    $("#btn-tambah-angsuran").click(function(){
        tambahAngsuran(0);
    });
    
    $(".btn-hapus-angsuran").click(function(){
        hapusAngsuran(0);
    });
    
    $(".btn-edit-angsuran").click(function(){
        editAngsuran(0);
    });
})