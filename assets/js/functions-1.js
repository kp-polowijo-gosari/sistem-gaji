const ResultCode = {
	OK: 0,
	FAIL: 1,
	FAIL_PRIVILEGE: 2,
	FAIL_LOGOUT: 3
};

var getUrl = window.location;
var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1] + "/";

function setAttr($el, key, val, isData=false){
	$el.attr(key, val);
	$el.prop(key, val);
	if(!isData){
		$el.data(key, val);
		setAttr($el, "data-" + key, val, true);
	}
}

function getVal($el){
	if($el == null || $el.length == 0) return null;
	let type = $el.attr("valtype");
	let val = null;
	switch (type) {
		case "input":
		case "value":
		case "val":
			val = $el.val();
			//console.log("getVal:input:" + $el.attr("class") + ":" + val);
			break;
		case "text":
			val = $el.html();
			//console.log("getVal:text:" + $el.attr("class") + ":" + val);
			break;
		case "modal":
			val = $el.text();
			//console.log("getVal:modal:" + $el.attr("class") + ":" + val);
			break;
		case "href":
			val = $el.attr("href");
			break;
		case "select":
			val = getSelectedOption($el).attr("value");
			break;
	}
	if($el.attr("data-type") == "currency"){
		val = readCurrency(val);
	}
	return val;
}

function setVal($el, val){
	if($el == null || $el.length == 0) return false;
	let type = $el.attr("valtype");
	if($el.attr("data-type") == "currency")
		val = formatCurrency(val);
	switch (type) {
		case "input":
		case "value":
		case "val":
			setAttr($el, "value", val);
			$el.val(val);
			//console.log("setVal:input:" + $el.attr("class") + ":" + val);
			return true;
		case "text":
			$el.html(val);
			//console.log("setVal:text:" + $el.attr("class") + ":" + val);
			return true;
		case "modal":
			$el.text(val);
			//console.log("setVal:modal:" + $el.attr("class") + ":" + val);
			return true;
		case "href":
			setAttr($el, "href", val);
			//console.log("setVal:href:" + $el.attr("class") + ":" + val);
			return true;
		case "select":
			selectOptionWithValue($el, val);
			//console.log("setVal:select:" + $el.attr("class") + ":" + val);
			return true;
	}
	return false;
}

function prepareSwitchOffMode($row, query, attr="valtarget"){
	let $normals = $row.find(query+"["+attr+"]");
	$normals.each(function(){
		let $normal = $(this);
		if(!$normal.attr(attr))
			return;
		let val = getVal($normal);
		let target = $normal.attr(attr);
		let $target = $row.find(target);
		let result = setVal($target, val);
		//console.assert($target.length == 1 && result && getVal($target) == val);
		//console.log($normal.attr("class") + ":" + val + ":" + $target.length + ":" + result + ":" + getVal($target));
		if($normal.attr("valtype") == "select"){
			let val2 = getSelectedOption($normal).text();
			let target2 = $normal.attr(attr + "2");
			let $target2 = $row.find(target2);
			let result2 = setVal($target2, val2);
		}
	});
}

function getValSearch($row, key){
	let query = '[name="' + key + '"]';
	let $el = $row.find(query);
	let val = getVal($el);
	//alert(query + ":" + $el.length + ":" + $el.attr("class") + ":" + val);
	return val;
}
function setValSearch($row, key, val){
	let query = '[name="' + key + '"]';
	let $el = $row.find(query);
	let result = $el.length > 0;
	if(result)
		result = setVal($el, val);
	return result;
}
function fillTemplate($template, data){
	for (var key in data) {
		// check if the property/key is defined in the object itself, not in parent
		if (data.hasOwnProperty(key)) {       
			let val = data[key];
			let result = setValSearch($template, key, val);
		}
	}
	prepareSwitchOffMode($template, ".normal-mode");
}

function simpleAjax(data){
	let ajax = {
		method: "POST",
		data: {},
		dataType: "JSON",
		error: function (xhr, ajaxOptions, thrownError) {
			console.log(xhr.responseText);
			alert(xhr.status);
		}
	};
	for (var prop in data) {
		if (data.hasOwnProperty(prop)) {
			ajax[prop] = data[prop];
		}
	}
	return $.ajax(ajax);
}

function copyElementData($src, $target){
	/*
	let $texts = $src.find("[copytext]");
	$texts.each(function(){
		let $text = $(this);
		let t = $text.attr("copytext");
		let $t = $target.find(t);
		$t.html($text.html());
	});
	let $vals = $src.find("[copyval]");
	$vals.each(function(){
		let $val = $(this);
		let v = $text.attr("copyval");
		let $v = $target.find(v);
		$v.html($val.html());
	});
	*/
	
	let $srcEls = $src.find("[name]");
	$srcEls.each(function(){
		let $srcEl = $(this);
		let key = $srcEl.attr("name");
		let val = getVal($srcEl);
		let result = setValSearch($target, key, val);
	});
	
	prepareSwitchOffMode($target, ".normal-mode");
}

function scrollTo(el){
	$('html, body').animate({
		scrollTop: $(el).offset().top
	}, 750);
}

function instantiateTemplate(template){
	template = template.replace(/^\s*|\s*$/g, '');
	template = template.replace(/\\r\\n/gm, '');
	let $el = $($.parseHTML(template));
	initInputCurrency($el);
	return $el;
}

function calcAge(year){
	
    var today = new Date();
    var birthDate = year;
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0){
		--age;
		m+=12;
	}else if (m === 0 && today.getDate() < birthDate.getDate()) {
		--age;
		m=11;
    }
   return [age, m];
}

function calcAgeStr(year){
	let age = calcAge(new Date(year));
	
	let s = "";
	if(age[0] > 0)
		s += age[0] + " tahun";
	if(age[1] > 0)
		s += " " + age[1] + " bulan";
	return s;
}

function editMode($row, edit){
	$row.find(edit ? ".normal-mode" : ".edit-mode").addClass("d-none");
	$row.find(edit ? ".edit-mode" : ".normal-mode").removeClass("d-none");
	if(edit)
		$(".edit-toggle").attr("disabled", true);
	else
		$(".edit-toggle").removeAttr("disabled");
}

$.fn.serializeObject = function() {
	var o = {};
	var a = this.serializeArray();
	$.each(a, function() {
		if (o[this.name]) {
			if (!o[this.name].push) {
				o[this.name] = [o[this.name]];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};

var navActive = false;
function toggleNav(active=null){
    if(active==null)
        navActive = !navActive;
    else
        navActive = active;
    if(navActive){
        $("#sidenav").addClass("active");
        $("#sidenav-overlay").addClass("active");
        $("#sidenav-overlay").css("display","block");
    }else{
        $("#sidenav").removeClass("active");
        $("#sidenav-overlay").removeClass("active");
        window.setTimeout(function(){
            if(!navActive){
                $("#sidenav-overlay").css("display","none");
            }
        }, 500);
    }
}
var _onConfirm = null;
function askConfirmation(title, body, danger, onConfirm){
    let $modal = $("#modal-confirmation");
    $modal.find(".modal-title").html(title);
    $modalHeader = $modal.find(".modal-header");
    $modal.find(".modal-text").html(body);
    $btnConfirm = $modal.find(".btn-confirm");
    if(danger){
        $btnConfirm.removeClass("btn-primary");
        $btnConfirm.addClass("btn-danger");
        $modalHeader.removeClass("bg-primary");
        $modalHeader.addClass("bg-danger");
    }else{
        $btnConfirm.removeClass("btn-danger");
        $btnConfirm.addClass("btn-primary");
        $modalHeader.removeClass("bg-danger");
        $modalHeader.addClass("bg-primary");
    }
    _onConfirm = onConfirm;
    $modal.modal("show");
}

var _onCloseNotice = null;
function showNotice(title, body, danger=false, onCloseNotice=null){
    let $modal = $("#modal-notice");
    $modal.find(".modal-title").html(title);
    $modalHeader = $modal.find(".modal-header");
    $modal.find(".modal-text").html(body);
    $btnConfirm = $modal.find(".btn-confirm");
    if(danger){
        $modalHeader.removeClass("bg-primary");
        $modalHeader.addClass("bg-danger");
    }else{
        $modalHeader.removeClass("bg-danger");
        $modalHeader.addClass("bg-primary");
    }
    _onCloseNotice = onCloseNotice;
    $modal.modal("show");
}

function showError(title, body, onCloseNotice=null){
	showNotice(title, body, true, onCloseNotice);
}

function selectTabLaporanGaji(tab){
    $table = $("#laporan-gaji");
    if(tab != "utama"){
        $table.find(".utama-mode").addClass("d-none");
    }
    if(tab != "gaji"){
        $table.find(".gaji-mode").addClass("d-none");
    }
    if(tab != "potongan"){
        $table.find(".potongan-mode").addClass("d-none");
    }
    $table.find("." + tab + "-mode").removeClass("d-none");
    
    $(".tab-gaji").removeClass("active");
    $("#tab-gaji-" + tab).addClass("active");
}

function getTable($table){
    if(!$table.is("table")) {
        $table = $table.find("table");
    }
    return $table;
}

function getDataTable($table){
    return getTable($table).DataTable();
}

function iresize($iframe){
    let scrollHeight = $iframe.contents().find("html").height();
    $iframe.height(scrollHeight);
}

function getSelectedOption($select){
	return $select.find("option:selected");
}
function getSelectedValue($select){
	return getSelectedOption($select).attr("value");
}
function getSelectedText($select){
	return getSelectedOption($select).text();
}


function getOptionWithValue($select, value){
	return $select.find('option[value="' + value + '"]');
}

function selectOptionWithValue($select, value){
	let $option = getOptionWithValue($select, value);
	if($option.length == 0) return null;
	$select.find("option").removeAttr("selected");
	$option.attr("selected", "selected");
	$select.change();
	return $option.text();
}

function readRow2($row, data=null){
	if (data == null) data = {};
	let $srcEls = $row.find("[name]");
	let ret = {};
	$srcEls.each(function(){
		let $srcEl = $(this);
		let key = $srcEl.attr("name");
		let val = getVal($srcEl);
		ret[key] = val;
	});
	
	return ret;
}

function readRow($row, data = null){
	if (data == null) data = {};
	let $inputs = $row.find("input");
	$inputs.each(function(){
		let $input = $(this);
		let val = $input.val();
		if($input.attr("data-type") == "currency"){
			val = readCurrency(val);
		}
		let key = $input.attr("name");
		data[key] = val;
	});
	let $selects = $row.find("select");
	$selects.each(function(){
		let $select = $(this);
		let $selected = getSelectedOption($select);
		let val = $selected.attr("value");
		let key = $select.attr("name");
		data[key] = val;
	});
	let $textAreas = $row.find("textarea");
	$textAreas.each(function(){
		let $ta = $(this);
		let key = $ta.attr("name");
		let val = $ta.text();
		data[key] = val;
	});
	return data;
}
function resetForm($form){
	let $inputs = $form.find("input");
	$inputs.each(function(){
		let $input = $(this);
		$input.val($input.attr("data-default"));
	});
	let $selects = $form.find("select");
	$selects.each(function(){
		let $select = $(this);
		let def = $select.attr("data-default");
		let $options = $select.find("option");
		$options.each(function(){
			$option = $(this);
			if($option.val() == def)
				$option.attr("selected", "selected");
			else
				$option.removeAttr("selected");
		});
	});
	let $textAreas = $form.find("textarea");
	$textAreas.each(function(){
		let $ta = $(this);
		$ta.val($ta.attr("data-default"));
	});
}
function readForm($form){
	let data = $form.serializeObject();
	readRow($form, data);
	return data;
}

function setFormEnabled(formQuery, enabled){
	let query = [
		formQuery + " input",
		formQuery + " textarea",
		formQuery + " button",
		formQuery + " .form-control",
		formQuery + " .edit-toggle"
	];
	if(enabled){
		$(query.join(", ")).removeProp("disabled");
		$(query.join(", ")).removeAttr("disabled");
	}else{
		$(query.join(", ")).prop("disabled", "true");
		$(query.join(", ")).attr("disabled", "true");
	}
}

var _onPilihJabatan = null;
function pilihJabatan(onPilihJabatan){
	_onPilihJabatan = onPilihJabatan;
    $("#modal-pilih-jabatan").modal('show');
}
var _onPilihSubdept = null;
function pilihSubdept(onPilihSubdept){
	_onPilihSubdept = onPilihSubdept;
    $("#modal-pilih-subdept").modal('show');
}
var _onPilihTkh = null;
function pilihTkh(onPilihTkh){
	_onPilihTkh = onPilihTkh;
    $("#modal-pilih-tkh").modal('show');
}
function refreshOnBack(){
	//https://stackoverflow.com/questions/43043113/how-to-force-reloading-a-page-when-using-browser-back-button
	window.addEventListener( "pageshow", function ( event ) {
	  var historyTraversal = event.persisted || 
							 ( typeof window.performance != "undefined" && 
								  window.performance.navigation.type === 2 );
	  if ( historyTraversal ) {
		// Handle page restore.
		window.location.reload();
	  }
	});
}
$(document).ready(function(){
	
	$(".table-edit tr").each(function(){
		prepareSwitchOffMode($(this), ".normal-mode");
	});
	
    $(".table-edit").on("click", ".btn-ubah", function(){
        let $btn = $(this);
        let $row = $btn.parents("tr");
		prepareSwitchOffMode($row, ".normal-mode");
		editMode($row, true);
    });
	
	
    $(".table-edit").on("click", ".btn-batal-ubah", function(){
        let $btn = $(this);
        let $row = $btn.parents("tr");
		prepareSwitchOffMode($row, ".normal-mode");
		editMode($row, false);
    });
	
	$(".row-pilih-jabatan").click(function(){
		$("#modal-pilih-jabatan").modal("hide");
		let $row = $(this);
		let data = readRow2($row);
		_onPilihJabatan(data);
	});
	$(".row-pilih-subdept").click(function(){
		$("#modal-pilih-subdept").modal("hide");
		let $row = $(this);
		let data = readRow2($row);
		_onPilihSubdept(data);
	});
	$(".row-pilih-tkh").click(function(){
		$("#modal-pilih-tkh").modal("hide");
		let $row = $(this);
		let data = readRow2($row);
		_onPilihTkh(data);
	});
    iFrameResize(
        { 
            log: true,
            heightCalculationMethod: "documentElementOffset",
            widthCalculationMtehod: "taggedElement"
        }, '.iresize'
    );
	
    $(".clickable[data-href]").click(function() {
        window.location = $(this).data("href");
    });
    $("#sidebar-toggle").click(function(){
        toggleNav();
    });
    $("#sidenav-overlay").click(function(){
        toggleNav(false);
    });
    $("#sidenav-overlay").hide();
    $("#modal-confirmation .btn-confirm").click(function(){
        _onConfirm();
        $("#modal-confirmation").modal("hide");
    });
    
    $("table").on("click", ".btn-ubah", function(){
		$row = $(this).parents(".data-row");
	});
	
    $("table").on("click", ".btn-batal-ubah", function(){
		$row = $(this).parents(".data-row");
		if($row.length > 0)
			editMode($row, false);
	});
    
    $(".jabatan-row-pilih").click(function(){
        $("#modal-pilih-jabatan").modal("hide");
    });
    
    $(".tab-gaji").click(function(evt){
        let $btn = $(this);
        evt.preventDefault();
        selectTabLaporanGaji($btn.attr("data-tab"));
    });
    selectTabLaporanGaji("utama");
    
    $("[data-show]").click(function(){
        let $btn = $(this);
        let target = $btn.data("show");
        $(target).removeClass("d-none");
    });
    
    $("[data-toggleshow]").click(function(){
        let $btn = $(this);
        let target = $btn.data("toggleshow");
        $(target).toggleClass("d-none");
    });
    
    $("[data-hide]").click(function(){
        let $btn = $(this);
        let target = $btn.data("hide");
        $(target).addClass("d-none");
    });
    
    $(".btn-validasi").click(function(){
        askConfirmation(
            "Konfirmasi Validasi",
            "Apa Anda yakin data ini sudah fix?",
            false, function(){
                $(".invalidated-mode").addClass("d-none");
                $(".validated-mode").removeClass("d-none");
            }
        );
    });
    
    
    $(".btn-batal-validasi").click(function(){
        askConfirmation(
            "Konfirmasi Pembatalan Validasi",
            "Apa Anda yakin mambatalkan validasi?",
            false, function(){
                $(".invalidated-mode").removeClass("d-none");
                $(".validated-mode").addClass("d-none");
            }
        );
    });
    $(".btn-logout").click(function(evt){
        evt.preventDefault();
        let url = $(this).attr("href");
        askConfirmation(
            'Konfirmasi Logout', 
            'Apa Anda yakin ingin logout?', 
            true, function(){
                window.location = url;
            }
        );
    });
});