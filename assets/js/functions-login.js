

function login(form){
	simpleAjax({
		url: baseUrl + "login/login",
		data: form.serialize(),
		success: function(response){
			if(response.result == ResultCode.OK){
				window.location.reload(true);
			}else{
				showError("Login Gagal",  response.message, function(){
					if(response.redirect){
						window.location = response.redirect;
					}else if (typeof response.reload === 'undefined' || response.reload === null){
						window.location.reload(response.reload);
					}
				});
			}
		}
	});
}
$(document).ready(function(){
	
	//LOGIN Function
	$("#form-login").submit(function(e){
		e.preventDefault();
		login($(this));
	});
	
    
});