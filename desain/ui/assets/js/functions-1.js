var navActive = false;
function toggleNav(active=null){
    if(active==null)
        navActive = !navActive;
    else
        navActive = active;
    if(navActive){
        $("#sidenav").addClass("active");
        $("#sidenav-overlay").addClass("active");
        $("#sidenav-overlay").css("display","block");
    }else{
        $("#sidenav").removeClass("active");
        $("#sidenav-overlay").removeClass("active");
        window.setTimeout(function(){
            if(!navActive){
                $("#sidenav-overlay").css("display","none");
            }
        }, 500);
    }
}
var _onConfirm = null;
function askConfirmation(title, body, danger, onConfirm){
    let $modal = $("#modal-confirmation");
    $modal.find(".modal-title").html(title);
    $modalHeader = $modal.find(".modal-header");
    $modal.find(".modal-text").html(body);
    $btnConfirm = $modal.find(".btn-confirm");
    if(danger){
        $btnConfirm.removeClass("btn-primary");
        $btnConfirm.addClass("btn-danger");
        $modalHeader.removeClass("bg-primary");
        $modalHeader.addClass("bg-danger");
    }else{
        $btnConfirm.removeClass("btn-danger");
        $btnConfirm.addClass("btn-primary");
        $modalHeader.removeClass("bg-danger");
        $modalHeader.addClass("bg-primary");
    }
    _onConfirm = onConfirm;
    $modal.modal("show");
}

var _onCloseNotice = null;
function showNotice(title, body, danger=false, onCloseNotice=null){
    let $modal = $("#modal-notice");
    $modal.find(".modal-title").html(title);
    $modalHeader = $modal.find(".modal-header");
    $modal.find(".modal-text").html(body);
    $btnConfirm = $modal.find(".btn-confirm");
    if(danger){
        $modalHeader.removeClass("bg-primary");
        $modalHeader.addClass("bg-danger");
    }else{
        $modalHeader.removeClass("bg-danger");
        $modalHeader.addClass("bg-primary");
    }
    _onCloseNotice = onCloseNotice;
    $modal.modal("show");
}

function selectTabLaporanGaji(tab){
    $table = $("#laporan-gaji");
    if(tab != "utama"){
        $table.find(".utama-mode").addClass("d-none");
    }
    if(tab != "gaji"){
        $table.find(".gaji-mode").addClass("d-none");
    }
    if(tab != "potongan"){
        $table.find(".potongan-mode").addClass("d-none");
    }
    $table.find("." + tab + "-mode").removeClass("d-none");
    
    $(".tab-gaji").removeClass("active");
    $("#tab-gaji-" + tab).addClass("active");
}

function getTable($table){
    if(!$table.is("table")) {
        $table = $table.find("table");
    }
    return $table;
}

function getDataTable($table){
    return getTable($table).DataTable();
}

function iresize($iframe){
    let scrollHeight = $iframe.contents().find("html").height();
    $iframe.height(scrollHeight);
}

$(document).ready(function(){
    iFrameResize(
        { 
            log: true,
            heightCalculationMethod: "documentElementOffset",
            widthCalculationMtehod: "taggedElement"
        }, '.iresize'
    );
    $(".datatable").each(function(){
        let table = getDataTable($(this));
        
        table.on( 'order.dt search.dt', function () {
            table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    })
    $(".column-search").each(function(){
        let $table = $(this);
        if(!$table.is("table")){
            $table = $table.find("table");
        }
        let table = $table.DataTable();
        $thead = $table.find('thead');
        $thead.find('tr').clone(true).appendTo($thead);
        $thead.find('tr:eq(1) th').each( function (i) {
            $thSearch = $(this);
            $(this).removeClass("sorting sorting_asc sorting_desc");
            var title = $(this).text();
            let classes = $(this).attr("class");
            $(this).html( '<div class="inner-addon left-addon"><span class="glyphicon glyphicon-search"></span><input type="text" class="w-100" /></div>' );
            
            $(this).addClass(classes);

            $( 'input', this ).on( 'keyup change', function () {
                if ( table.column(i).search() !== this.value ) {
                    table
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            } );
        } );
    });
    
    $(".clickable[data-href]").click(function() {
        window.location = $(this).data("href");
    });
    $("#sidebar-toggle").click(function(){
        toggleNav();
    });
    $("#sidenav-overlay").click(function(){
        toggleNav(false);
    });
    $("#sidenav-overlay").hide();
    $("#modal-confirmation .btn-confirm").click(function(){
        _onConfirm();
        $("#modal-confirmation").modal("hide");
    });
    
    $(".btn-hapus-dept").click(function(){
        askConfirmation(
            'Konfirmasi Penghapusan', 
            'Apa Anda yakin ingin menghapus Research and Development?', 
            true, function(){
                
            }
        );
    });
    $(".btn-hapus-job").click(function(){
        askConfirmation(
            'Konfirmasi Penghapusan', 
            'Apa Anda yakin ingin menghapus Manager?', 
            true, function(){
                
            }
        );
    });
    $(".btn-hapus-jabatan").click(function(){
        askConfirmation(
            'Konfirmasi Penghapusan', 
            'Apa Anda yakin ingin menghapus Manager R&D?', 
            true, function(){
                
            }
        );
    });
    $(".btn-hapus-tk").click(function(){
        askConfirmation(
            'Konfirmasi Penghapusan', 
            'Apa Anda yakin ingin menghapus Gtw untuk Manager R&D?', 
            true, function(){
                
            }
        );
    });
    $(".btn-hapus-lembur").click(function(){
        askConfirmation(
            'Konfirmasi Penghapusan', 
            'Apa Anda yakin ingin menghapus lembur 2 Januari 2020?', 
            true, function(){
                
            }
        );
    });
    
    $(".btn-hapus-karyawan").click(function(){
        askConfirmation(
            'Konfirmasi Penghapusan', 
            'Apa Anda yakin ingin menghapus Anugrah Putra Eko Wibowo?', 
            true, function(){
                
            }
        );
    });
    
    $(".btn-hapus-angsuran").click(function(){
        askConfirmation(
            'Konfirmasi Penghapusan', 
            'Apa Anda yakin ingin menghapus angsuran Sepeda Motor Anugrah Putra Eko Wibowo?', 
            true, function(){
                
            }
        );
    });
    
    $(".btn-hapus-shift").click(function(){
        askConfirmation(
            'Konfirmasi Penghapusan', 
            'Apa Anda yakin ingin menghapus shift I?', 
            true, function(){
                
            }
        );
    });
    
    
    
    $(".btn-hapus-akun").click(function(){
        askConfirmation(
            'Konfirmasi Penghapusan Akun', 
            'Apa Anda yakin ingin menghapus akun Anugrah Putra Eko Wibowo?', 
            true, function(){
                
            }
        );
    });
    
    $(".btn-logout").click(function(evt){
        evt.preventDefault();
        let url = $(this).attr("href");
        askConfirmation(
            'Konfirmasi Logout', 
            'Apa Anda yakin ingin logout?', 
            true, function(){
                window.location = url;
            }
        );
    });
    
    
    $(".btn-simpan-password").click(function(){
        askConfirmation(
            'Konfirmasi Perubahan Password', 
            'Apa Anda yakin ingin mengubah password?', 
            true, function(){
                $("#password-collapse").collapse("hide");
                $(".edit-password-mode").addClass("d-none");
                $(".btn-simpan-password").removeClass("d-none");
            }
        );
    });
    
    
    
    $(".btn-reset-password").click(function(){
        askConfirmation(
            'Konfirmasi Reset Password', 
            'Apa Anda yakin ingin mereset password?', 
            true, function(){
                showNotice(
                    "Reset Password Berhasil",
                    "Password telah direset menjadi password acak yang telah dikirim ke email Anda. Silahkan cek email Anda, login, lalu ubah password Anda",
                    false, function(){}
                );
            }
        );
    });
    
    $(".btn-ubah-dept").click(function(){
        let $btn = $(this);
        let $row =$btn.parents(".dept-row");
        $row.find(".edit-mode").removeClass("d-none");
        $row.find(".normal-mode").addClass("d-none");
        let $deptName = $row.find(".dept-name");
        let $deptNameField = $row.find(".dept-name-field");
        $deptNameField.val($deptName.text());
    });
    
    $(".btn-simpan-dept").click(function(){
        $btn = $(this);
        $row =$btn.parents(".dept-row");
        $row.find(".edit-mode").addClass("d-none");
        $row.find(".normal-mode").removeClass("d-none");
        $deptName = $row.find(".dept-name");
        $deptNameField = $row.find(".dept-name-field");
        $deptName.text($deptNameField.val());
    });
    
    
    $(".btn-ubah-job").click(function(){
        $btn = $(this);
        $row =$btn.parents(".job-row");
        $row.find(".edit-mode").removeClass("d-none");
        $row.find(".normal-mode").addClass("d-none");
        $jobName = $row.find(".job-name");
        $jobNameField = $row.find(".job-name-field");
        $jobNameField.val($jobName.text());
    });
    
    $(".btn-simpan-job").click(function(){
        let $btn = $(this);
        let $row =$btn.parents(".job-row");
        $row.find(".edit-mode").addClass("d-none");
        $row.find(".normal-mode").removeClass("d-none");
        let $jobName = $row.find(".job-name");
        let $jobNameField = $row.find(".job-name-field");
        $jobName.text($jobNameField.val());
    });
    
    $(".btn-ubah-jabatan").click(function(){
        $btn = $(this);
        $row =$btn.parents(".jabatan-row");
		
        $row.find(".edit-mode").removeClass("d-none");
        $row.find(".normal-mode").addClass("d-none");
		
        $jabatanName = $row.find(".jabatan-name");
        $jabatanNameField = $row.find(".jabatan-name-field");
        $jabatanNameField.val($jabatanName.text());
		
    });
    
    $(".btn-simpan-jabatan").click(function(){
        $btn = $(this);
        $row =$btn.parents(".jabatan-row");
		
        $row.find(".edit-mode").addClass("d-none");
        $row.find(".normal-mode").removeClass("d-none");
		
        $jabatanName = $row.find(".jabatan-name");
        $jabatanNameField = $row.find(".jabatan-name-field");
        $jabatanName.text($jabatanNameField.val());
		
		$jabatanJobName = $row.find(".job-name");
		$jabatanJobNameField = $row.find(".job-name-field");
		$jabatanJobName.text($jabatanJobNameField.find("option:selected").text());
    });
    
    $(".btn-ubah-tk").click(function(){
        $btn = $(this);
        $row =$btn.parents(".tk-row");
		
        $row.find(".edit-mode").removeClass("d-none");
        $row.find(".normal-mode").addClass("d-none");
		
        $tkName = $row.find(".tk-name");
        $tkNameField = $row.find(".tk-name-field");
        $tkNameField.val($tkName.text());
		
        $tkValue = $row.find(".tk-value");
        $tkValueField = $row.find(".tk-value-field");
        $tkValueField.val($tkValue.text());
		
		
    });
    
    $(".btn-simpan-tk").click(function(){
        $btn = $(this);
        $row =$btn.parents(".tk-row");
		
        $row.find(".edit-mode").addClass("d-none");
        $row.find(".normal-mode").removeClass("d-none");
		
        $tkName = $row.find(".tk-name");
        $tkNameField = $row.find(".tk-name-field");
        $tkName.text($tkNameField.val());
		
        $tkValue = $row.find(".tk-value");
        $tkValueField = $row.find(".tk-value-field");
        $tkValue.text($tkValueField.val());
		
    });
    
    
    $(".btn-ubah-shift").click(function(){
        $btn = $(this);
        $row =$btn.parents(".shift-row");
        $row.find(".edit-mode").removeClass("d-none");
        $row.find(".normal-mode").addClass("d-none");
		
        $shift = $row.find(".shift");
        $shiftField = $row.find(".shift-field");
        $shiftField.val($shift.text());
		
        $lembur1 = $row.find(".lembur-1");
        $lembur1Field = $row.find(".lembur-1-field");
        $lembur1Field.val($lembur1.text());
        
        $lembur2 = $row.find(".lembur-2");
        $lembur2Field = $row.find(".lembur-2-field");
        $lembur2Field.val($lembur2.text());
    });
    
    $(".btn-simpan-shift").click(function(){
        let $btn = $(this);
        let $row =$btn.parents(".shift-row");
        $row.find(".edit-mode").addClass("d-none");
        $row.find(".normal-mode").removeClass("d-none");
		
        let $shift = $row.find(".shift");
        let $shiftField = $row.find(".shift-field");
        $shift.text($shiftField.val());
		
        let $lembur1 = $row.find(".lembur-1");
        let $lembur1Field = $row.find(".lembur-1-field");
        $lembur1.text($lembur1Field.val());
        
        let $lembur2 = $row.find(".lembur-2");
        let $lembur2Field = $row.find(".lembur-2-field");
        $lembur2.text($lembur2Field.val());
    });
    
    $(".jabatan-row-pilih").click(function(){
        $("#modal-pilih-jabatan").modal("hide");
    });
    
    $(".btn-ubah-absen").click(function(){
        let $btn = $(this);
        let $row = $btn.parents(".absen-row")
        
        $row.find(".edit-mode").removeClass("d-none");
        $row.find(".normal-mode").addClass("d-none");
        
        $row.find(".edit-toggle").removeAttr("disabled");
        
        let $tanggal = $row.find(".tanggal");
        let $tanggalField = $row.find(".tanggal-field");
        $tanggalField.val($tanggal.text());
        
        let $keterangan = $row.find(".keterangan");
        let $keteranganField = $row.find(".keterangan-field");
        $keteranganField.val($keterangan.text());
        
        let $jam = $row.find(".jam");
        let $jamField = $row.find(".jam-field");
        $jamField.val($jam.text());
    });
    
    $(".btn-simpan-absen").click(function(){
        let $btn = $(this);
        let $row = $btn.parents(".absen-row");
        
        $row.find(".edit-mode").addClass("d-none");
        $row.find(".normal-mode").removeClass("d-none");
        
        $row.find(".edit-toggle").attr("disabled","true");
        
        let $tanggal = $row.find(".tanggal");
        let $tanggalField = $row.find(".tanggal-field");
        $tanggal.text($tanggalField.val());
        
        let $keterangan = $row.find(".keterangan");
        let $keteranganField = $row.find(".keterangan-field");
        $keterangan.text($keteranganField.val());
        
        let $jam = $row.find(".jam");
        let $jamField = $row.find(".jam-field");
        $jam.text($jamField.val());
    });
    
    $(".btn-ubah-lembur").click(function(){
        let $btn = $(this);
        let $row =$btn.parents(".lembur-row");
		
        $row.find(".edit-mode").removeClass("d-none");
        $row.find(".normal-mode").addClass("d-none");
		
        $row.find(".edit-toggle").removeAttr("disabled");
		
        let $keterangan = $row.find(".keterangan");
        let $keteranganField = $row.find(".keterangan-field");
        $keteranganField.val($keterangan.text());
		
        let $jam = $row.find(".jam");
        let $jamField = $row.find(".jam-field");
        $jamField.val($jam.text());
		
		
        let $tanggal = $row.find(".tanggal");
        let $tanggalField = $row.find(".tanggal-field");
        $tanggalField.val($tanggal.text());
    });
    
    $(".btn-simpan-lembur").click(function(){
        let $btn = $(this);
        let $row =$btn.parents(".lembur-row");
		
        $row.find(".edit-mode").addClass("d-none");
        $row.find(".normal-mode").removeClass("d-none");
		
        $row.find(".edit-toggle").attr("disabled", "true");
		
        let $keterangan = $row.find(".keterangan");
        let $keteranganField = $row.find(".keterangan-field");
        $keterangan.text($keteranganField.val());
		
        let $jam = $row.find(".jam");
        let $jamField = $row.find(".jam-field");
        $jam.text($jamField.val());
		
		
        let $tanggal = $row.find(".tanggal");
        let $tanggalField = $row.find(".tanggal-field");
        $tanggal.text($tanggalField.val());
		
		let $shift = $row.find(".shift");
		let $shiftField = $row.find(".shift-field");
		$shift.text($shiftField.find("option:selected").text());
		
    });
    
    $(".tab-gaji").click(function(evt){
        let $btn = $(this);
        evt.preventDefault();
        selectTabLaporanGaji($btn.attr("data-tab"));
    });
    selectTabLaporanGaji("utama");
    
    
    $(".btn-ubah-karyawan").click(function(){
        $(".edit-mode").removeClass("d-none");
        $(".normal-mode").addClass("d-none");
		
        $(".edit-toggle").removeAttr("disabled");
		
		
    });
    
    $(".btn-simpan-karyawan").click(function(){
        $(".edit-mode").addClass("d-none");
        $(".normal-mode").removeClass("d-none");
		
        $(".edit-toggle").attr("disabled", "true");
		
    });
    
    
    $(".btn-ubah-data-gaji-karyawan").click(function(){
        let $btn = $(this);
        let $row =$btn.parents(".data-gaji-karyawan-row");
		
        $row.find(".edit-mode").removeClass("d-none");
        $row.find(".normal-mode").addClass("d-none");
        
        $row.find(".edit-toggle").removeAttr("disabled");
		
        let $gajiDilaporkan = $row.find(".gaji-dilaporkan");
        let $gajiDilaporkanField = $row.find(".gaji-dilaporkan-field");
        $gajiDilaporkanField.val($gajiDilaporkan.text());
		
        let $gajiLain = $row.find(".gaji-lain");
        let $gajiLainField = $row.find(".gaji-lain-field");
        $gajiLainField.val($gajiLain.text());
		
    });
    
    $(".btn-simpan-data-gaji-karyawan").click(function(){
        let $btn = $(this);
        let $row =$btn.parents(".data-gaji-karyawan-row");
		
        $row.find(".edit-mode").addClass("d-none");
        $row.find(".normal-mode").removeClass("d-none");
        $row.find(".edit-toggle").attr("disabled", "true");
		
        let $gajiDilaporkan = $row.find(".gaji-dilaporkan");
        let $gajiDilaporkanField = $row.find(".gaji-dilaporkan-field");
        $gajiDilaporkan.text($gajiDilaporkanField.val());
		
        let $gajiLain = $row.find(".gaji-lain");
        let $gajiLainField = $row.find(".gaji-lain-field");
        $gajiLain.text($gajiLainField.val());
		
		let $kinerja = $row.find(".kinerja");
		let $kinerjaField = $row.find(".kinerja-field");
		$kinerja.text($kinerjaField.find("option:selected").text());
		
		let $koperasi = $row.find(".koperasi");
		let $koperasiField = $row.find(".koperasi-field");
		$koperasi.text($koperasiField.find("option:selected").text());
    });
    $(".datatable.export").each(function(){
        let table = getDataTable($(this));
        
        new $.fn.dataTable.Buttons( table, {
            buttons: [
                {
                extend:    'csv',
                text:      'CSV',
                titleAttr: 'CSV',
                className: 'btn dropdown-item mx-1',
                exportOptions: {
                    columns: ':visible'
                },
                init: function(api, node, config) {
                   $(node).removeClass('btn-default btn-secondary');
                }
                },
                {
                extend:    'excel',
                text:      'Excel',
                titleAttr: 'Excel',
                className: 'btn dropdown-item mx-1',
                exportOptions: {
                    columns: ':visible'
                },
                init: function(api, node, config) {
                   $(node).removeClass('btn-default btn-secondary');
                }
                },
                {
                extend:    'pdf',
                text:      'PDF',
                titleAttr: 'PDF',
                orientation: 'landscape',
                papersize: 'A4',
                className: 'btn dropdown-item mx-1',
                exportOptions: {
                    columns: ':visible'
                },
                init: function(api, node, config) {
                   $(node).removeClass('btn-default btn-secondary');
                }
                },               
                {
                extend:    'print',
                text:      'Cetak',
                titleAttr: 'Cetak',
                orientation: 'landscape',
                papersize: 'A4',
                className: 'btn dropdown-item mx-1',
                exportOptions: {
                    columns: ':visible'
                },
                init: function(api, node, config) {
                   $(node).removeClass('btn-default btn-secondary');
                }
                },  
            ]
        } );
        table.buttons().container().appendTo('#tableActions');
    });
    
    $("[data-show]").click(function(){
        let $btn = $(this);
        let target = $btn.data("show");
        $(target).removeClass("d-none");
    });
    
    $("[data-toggleshow]").click(function(){
        let $btn = $(this);
        let target = $btn.data("toggleshow");
        $(target).toggleClass("d-none");
    });
    
    $("[data-hide]").click(function(){
        let $btn = $(this);
        let target = $btn.data("hide");
        $(target).addClass("d-none");
    });
    
    $(".btn-validasi").click(function(){
        askConfirmation(
            "Konfirmasi Validasi",
            "Apa Anda yakin data ini sudah fix?",
            false, function(){
                $(".invalidated-mode").addClass("d-none");
                $(".validated-mode").removeClass("d-none");
            }
        );
    });
    
    
    $(".btn-batal-validasi").click(function(){
        askConfirmation(
            "Konfirmasi Pembatalan Validasi",
            "Apa Anda yakin mambatalkan validasi?",
            false, function(){
                $(".invalidated-mode").removeClass("d-none");
                $(".validated-mode").addClass("d-none");
            }
        );
    });
});