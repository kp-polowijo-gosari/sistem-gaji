/*
SQLyog Ultimate v13.1.1 (64 bit)
MySQL - 10.1.34-MariaDB : Database - sistem_gaji
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

insert  into `agama`(`ID_AGAMA`,`NAMA_AGAMA`) values 
(1,'Islam'),
(2,'Katolik'),
(3,'Kristen'),
(4,'Hindu'),
(5,'Buddha'),
(6,'Konghucu');

insert  into `departemen`(`ID_DEPT`) values 
(1),
(2),
(3),
(4),
(5),
(7),
(8),
(9),
(10),
(11),
(15),
(16),
(17),
(18),
(19),
(20),
(21),
(22),
(23),
(24),
(25),
(26),
(27),
(28),
(29),
(30),
(31),
(32),
(33),
(34),
(36),
(37),
(38),
(39),
(40),
(41),
(42),
(43),
(44);

insert  into `jenis_kelamin`(`ID_JK`,`NAMA_JK`) values 
(1,'Laki - laki'),
(2,'Perempuan');

insert  into `job_level`(`ID_JOB_LEVEL`) values 
(1),
(2),
(3),
(4),
(5),
(6);

insert  into `kinerja`(`KINERJA`) values 
('A'),
('B'),
('C');

insert  into `pendidikan`(`ID_PENDIDIKAN`,`NAMA_PENDIDIKAN`,`TINGKAT_PENDIDIKAN`) values 
(1,'SD',1),
(2,'MI',1),
(3,'SMP',2),
(4,'MTS',2),
(5,'SMA',3),
(6,'SMK',3),
(7,'MAN',3),
(8,'MA',3),
(9,'D1',4),
(10,'D2',5),
(11,'D3',6),
(12,'D4',7),
(13,'S1',8),
(14,'S2',9),
(15,'S3',10);

insert  into `pengaturan`(`PERIODE_MULAI_PENG`,`PERIODE_AKHIR_PENG`,`BPJS_KERJA_PER`,`BPJS_KERJA_KAR`,`BPJS_KES_PER`,`BPJS_KES_KAR`,`UMK`,`IURAN_RUMAH`,`IURAN_KOP`,`DAFTAR_KOP`,`UANG_MAKAN`,`UANG_TRANSPORT`,`NAMA_OWNER`) values 
('2020-01-01',NULL,7,3,4,1,4197000,20000,25000,275000,12000,4000,'Kiki');

insert  into `peran_user`(`ID_PU`,`NAMA_PU`) values 
(1,'Pengawas'),
(2,'Admin Biasa'),
(3,'Admin Akun'),
(4,'Admin Utama'),
(5,'Super Admin');

insert  into `periode`(`PERIODE`,`KAR_VALID`,`JOB_VALID`,`TK_VALID`,`LEMBUR_VALID`,`ABSEN_VALID`,`ANG_VALID`,`POT_ANG_VALID`) values 
('2020-01-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
('2020-02-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
('2020-03-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL);

insert  into `perusahaan`(`ID_PER`) values 
(1),
(2),
(3),
(4),
(5);

insert  into `riwayat_departemen`(`ID_DEPT`,`PERIODE_MULAI_DEPT`,`PERIODE_AKHIR_DEPT`,`ID_PER`,`NAMA_DEPT`) values 
(1,'2020-01-01',NULL,1,'Produksi'),
(2,'2020-01-01',NULL,1,'Support ADM 1'),
(3,'2020-01-01',NULL,1,'Sales & Marketing'),
(4,'2020-01-01',NULL,1,'Support ADM 2'),
(5,'2020-01-01',NULL,2,'Produksi'),
(7,'2020-01-01',NULL,4,'Produksi'),
(8,'2020-01-01',NULL,5,'Produksi');

insert  into `riwayat_job`(`ID_JOB_LEVEL`,`PERIODE_MULAI_JOB`,`PERIODE_AKHIR_JOB`,`NAMA_JOB`,`GAJI_POKOK`,`TUNJANGAN_JABATAN`) values 
(1,'2020-01-01',NULL,'Manager',3000000,3400000),
(2,'2020-01-01',NULL,'Supervisor',2500000,1700000),
(3,'2020-01-01',NULL,'Leader',2100000,700000),
(4,'2020-01-01',NULL,'Level III.2',1750000,0),
(5,'2020-01-01',NULL,'Level IV.1',1500000,0),
(6,'2020-01-01',NULL,'Level IV.2',1250000,0);

insert  into `riwayat_perusahaan`(`ID_PER`,`PERIODE_MULAI_PER`,`PERIODE_AKHIR_PER`,`NAMA_PER`) values 
(1,'2020-01-01',NULL,'PT. POLOWIJO GOSARI INDONESIA'),
(2,'2020-01-01',NULL,'PT. POLOWIJO GOSARI PUPUK INDONESIA '),
(3,'2020-01-01',NULL,'PT. GALASARI GUNUNG SEJAHTERA'),
(4,'2020-01-01',NULL,'PT. GALASARI AGRO NIAGA SEJAHTERA'),
(5,'2020-01-01',NULL,'PT. SARI GUNUNG POLOWIJO');

insert  into `riwayat_shift`(`ID_SHIFT`,`PERIODE_MULAI_SHIFT`,`PERIODE_AKHIR_SHIFT`,`NAMA_SHIFT`,`MAX_LEMBUR_1`,`MAX_LEMBUR_2`) values 
(1,'2020-01-01',NULL,'I',1,6),
(2,'2020-01-01',NULL,'II',1,6),
(3,'2020-01-01',NULL,'III',1,6),
(4,'2020-01-01',NULL,'OFF',0,7);

insert  into `riwayat_subdepartemen`(`ID_SUBDEPT`,`PERIODE_MULAI_SUBDEPT`,`PERIODE_AKHIR_SUBDEPT`,`ID_DEPT`,`NAMA_SUBDEPT`) values 
(1,'2020-01-01',NULL,1,'Produksi'),
(2,'2020-01-01',NULL,1,'Pemeliharaan'),
(3,'2020-01-01',NULL,1,'Laborat'),
(4,'2020-01-01',NULL,2,'Keuangan'),
(5,'2020-01-01',NULL,2,'Logistik'),
(6,'2020-01-01',NULL,2,'PPIC/CANDAL'),
(7,'2020-01-01',NULL,3,'Pemasaran'),
(8,'2020-01-01',NULL,4,'HRD, SEKRET & HUKUM'),
(9,'2020-01-01',NULL,4,'HRD, SEKRET & HUKUM');

insert  into `riwayat_tmk`(`ID_TMK`,`PERIODE_MULAI_TMK`,`PERIODE_AKHIR_TMK`,`BATAS_BAWAH_TMK`,`BATAS_ATAS_TMK`,`NILAI_TMK`) values 
(1,'2020-01-01',NULL,3,6,150000),
(2,'2020-01-01',NULL,6,9,325000),
(3,'2020-01-01',NULL,9,12,500000),
(4,'2020-01-01',NULL,12,15,675000),
(5,'2020-01-01',NULL,15,18,850000),
(6,'2020-01-01',NULL,18,21,1025000),
(7,'2020-01-01',NULL,21,24,1200000),
(8,'2020-01-01',NULL,24,27,1375000),
(9,'2020-01-01',NULL,27,30,1550000),
(10,'2020-01-01',NULL,30,99,1725000),
(11,'2020-01-01',NULL,3,6,125000),
(12,'2020-01-01',NULL,6,9,275000),
(13,'2020-01-01',NULL,9,12,425000),
(14,'2020-01-01',NULL,12,15,575000),
(15,'2020-01-01',NULL,15,18,725000),
(16,'2020-01-01',NULL,18,21,875000),
(17,'2020-01-01',NULL,21,24,1025000),
(18,'2020-01-01',NULL,24,27,1175000),
(19,'2020-01-01',NULL,27,30,1325000),
(20,'2020-01-01',NULL,30,99,1475000),
(21,'2020-01-01',NULL,3,6,100000),
(22,'2020-01-01',NULL,6,9,225000),
(23,'2020-01-01',NULL,9,12,350000),
(24,'2020-01-01',NULL,12,15,475000),
(25,'2020-01-01',NULL,15,18,600000),
(26,'2020-01-01',NULL,18,21,725000),
(27,'2020-01-01',NULL,21,24,850000),
(28,'2020-01-01',NULL,24,27,975000),
(29,'2020-01-01',NULL,27,30,1100000),
(30,'2020-01-01',NULL,30,99,1225000),
(31,'2020-01-01',NULL,3,6,80000),
(32,'2020-01-01',NULL,6,9,180000),
(33,'2020-01-01',NULL,9,12,280000),
(34,'2020-01-01',NULL,12,15,380000),
(35,'2020-01-01',NULL,15,18,480000),
(36,'2020-01-01',NULL,18,21,580000),
(37,'2020-01-01',NULL,21,24,680000),
(38,'2020-01-01',NULL,24,27,780000),
(39,'2020-01-01',NULL,27,30,880000),
(40,'2020-01-01',NULL,30,99,980000),
(41,'2020-01-01',NULL,3,6,70000),
(42,'2020-01-01',NULL,6,9,160000),
(43,'2020-01-01',NULL,9,12,250000),
(44,'2020-01-01',NULL,12,15,340000),
(45,'2020-01-01',NULL,15,18,430000),
(46,'2020-01-01',NULL,18,21,520000),
(47,'2020-01-01',NULL,21,24,610000),
(48,'2020-01-01',NULL,24,27,700000),
(49,'2020-01-01',NULL,27,30,790000),
(50,'2020-01-01',NULL,30,99,880000),
(51,'2020-01-01',NULL,3,6,60000),
(52,'2020-01-01',NULL,6,9,140000),
(53,'2020-01-01',NULL,9,12,220000),
(54,'2020-01-01',NULL,12,15,300000),
(55,'2020-01-01',NULL,15,18,380000),
(56,'2020-01-01',NULL,18,21,460000),
(57,'2020-01-01',NULL,21,24,540000),
(58,'2020-01-01',NULL,24,27,620000),
(59,'2020-01-01',NULL,27,30,700000),
(60,'2020-01-01',NULL,30,99,780000);

insert  into `riwayat_tunjangan_khusus`(`ID_TKH`,`PERIODE_MULAI_TKH`,`PERIODE_AKHIR_TKH`,`NAMA_TKH`,`NILAI_TKH`) values 
(1,'2020-01-01',NULL,'Shift',100000),
(2,'2020-01-01',NULL,'Ka. Gudang',75000),
(3,'2020-01-01',NULL,'Weeloader',150000),
(4,'2020-01-01',NULL,'Forklift',100000),
(5,'2020-01-01',NULL,'Driver',150000),
(6,'2020-01-01',NULL,'Site Plant',75000),
(7,'2020-01-01',NULL,'Koordinator',150000),
(8,'2020-01-01',NULL,'Kasir',500000);

insert  into `riwayat_tunjangan_kinerja`(`ID_JOB_LEVEL`,`KINERJA`,`PERIODE_MULAI_TKI`,`PERIODE_AKHIR_TKI`,`NILAI_TKI`) values 
(1,'A','2020-01-01',NULL,1000000),
(1,'B','2020-01-01',NULL,750000),
(1,'C','2020-01-01',NULL,500000),
(2,'A','2020-01-01',NULL,1000000),
(2,'B','2020-01-01',NULL,750000),
(2,'C','2020-01-01',NULL,500000),
(3,'A','2020-01-01',NULL,1000000),
(3,'B','2020-01-01',NULL,750000),
(3,'C','2020-01-01',NULL,500000),
(4,'A','2020-01-01',NULL,1000000),
(4,'B','2020-01-01',NULL,750000),
(4,'C','2020-01-01',NULL,500000),
(5,'A','2020-01-01',NULL,1000000),
(5,'B','2020-01-01',NULL,750000),
(5,'C','2020-01-01',NULL,500000),
(6,'A','2020-01-01',NULL,1000000),
(6,'B','2020-01-01',NULL,750000),
(6,'C','2020-01-01',NULL,500000);

insert  into `riwayat_ul`(`ID_JOB_LEVEL`,`JAM_LEMBUR`,`PERIODE_MULAI_UL`,`PERIODE_AKHIR_UL`,`UPAH_LEMBUR`) values 
(1,1,'2020-01-01',NULL,25000),
(1,2,'2020-01-01',NULL,25000),
(1,3,'2020-01-01',NULL,25000),
(2,1,'2020-01-01',NULL,20000),
(2,2,'2020-01-01',NULL,20000),
(2,3,'2020-01-01',NULL,20000),
(3,1,'2020-01-01',NULL,15000),
(3,2,'2020-01-01',NULL,15000),
(3,3,'2020-01-01',NULL,15000),
(4,1,'2020-01-01',NULL,10500),
(4,2,'2020-01-01',NULL,14000),
(4,3,'2020-01-01',NULL,21000),
(5,1,'2020-01-01',NULL,10500),
(5,2,'2020-01-01',NULL,14000),
(5,3,'2020-01-01',NULL,21000),
(6,1,'2020-01-01',NULL,10500),
(6,2,'2020-01-01',NULL,14000),
(6,3,'2020-01-01',NULL,21000);

insert  into `riwayat_user`(`ID_USER`,`ID_PU`,`WAKTU_MULAI_USER`,`WAKTU_AKHIR_USER`,`NAMA_USER`,`EMAIL_USER`,`PASSWORD_USER`) values 
(1,5,'2020-02-05 11:50:16',NULL,'Super Admin Pokoknya','rizqinur2010@gmail.com','admin');

insert  into `shift`(`ID_SHIFT`) values 
(1),
(2),
(3),
(4);

insert  into `status_karyawan`(`ID_SK`,`NAMA_SK`) values 
(1,'Tetap'),
(2,'Kontrak');

insert  into `status_koperasi`(`ID_STATUS_KOP`,`NAMA_STATUS_KOP`) values 
(0,'Bukan Anggota'),
(1,'Daftar Baru'),
(2,'Anggota');

insert  into `subdepartemen`(`ID_SUBDEPT`) values 
(1),
(2),
(3),
(4),
(5),
(6),
(7),
(8),
(9);

insert  into `tunjangan_khusus`(`ID_TKH`) values 
(1),
(2),
(3),
(4),
(5),
(6),
(7),
(8);

insert  into `tunjangan_kinerja`(`ID_JOB_LEVEL`,`KINERJA`) values 
(1,'A'),
(1,'B'),
(1,'C'),
(2,'A'),
(2,'B'),
(2,'C'),
(3,'A'),
(3,'B'),
(3,'C'),
(4,'A'),
(4,'B'),
(4,'C'),
(5,'A'),
(5,'B'),
(5,'C'),
(6,'A'),
(6,'B'),
(6,'C');

insert  into `tunjangan_masa_kerja`(`ID_TMK`,`ID_JOB_LEVEL`) values 
(1,1),
(2,1),
(3,1),
(4,1),
(5,1),
(6,1),
(7,1),
(8,1),
(9,1),
(10,1),
(11,2),
(12,2),
(13,2),
(14,2),
(15,2),
(16,2),
(17,2),
(18,2),
(19,2),
(20,2),
(21,3),
(22,3),
(23,3),
(24,3),
(25,3),
(26,3),
(27,3),
(28,3),
(29,3),
(30,3),
(31,4),
(32,4),
(33,4),
(34,4),
(35,4),
(36,4),
(37,4),
(38,4),
(39,4),
(40,4),
(41,5),
(42,5),
(43,5),
(44,5),
(45,5),
(46,5),
(47,5),
(48,5),
(49,5),
(50,5),
(51,6),
(52,6),
(53,6),
(54,6),
(55,6),
(56,6),
(57,6),
(58,6),
(59,6),
(60,6);

insert  into `upah_lembur`(`ID_JOB_LEVEL`,`JAM_LEMBUR`) values 
(1,1),
(1,2),
(1,3),
(2,1),
(2,2),
(2,3),
(3,1),
(3,2),
(3,3),
(4,1),
(4,2),
(4,3),
(5,1),
(5,2),
(5,3),
(6,1),
(6,2),
(6,3);

insert  into `user`(`ID_USER`,`USERNAME`) values 
(1,'admin');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
