/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2/16/2020 3:59:31 PM                         */
/*==============================================================*/


/*==============================================================*/
/* Table: ABSEN                                                 */
/*==============================================================*/
create table ABSEN
(
   ID_ABSEN             int not null auto_increment  comment '',
   ID_KAR               int not null  comment '',
   TANGGAL_ABSEN        date not null  comment '',
   KETERANGAN_ABSEN     varchar(50) not null  comment '',
   LAMA_ABSEN           smallint not null  comment '',
   primary key (ID_ABSEN)
);

/*==============================================================*/
/* Table: AGAMA                                                 */
/*==============================================================*/
create table AGAMA
(
   ID_AGAMA             int not null auto_increment  comment '',
   NAMA_AGAMA           varchar(15) not null  comment '',
   primary key (ID_AGAMA)
);

/*==============================================================*/
/* Table: ANGSURAN                                              */
/*==============================================================*/
create table ANGSURAN
(
   ID_ANG               int not null auto_increment  comment '',
   ID_KAR               int  comment '',
   primary key (ID_ANG)
);

/*==============================================================*/
/* Table: DEPARTEMEN                                            */
/*==============================================================*/
create table DEPARTEMEN
(
   ID_DEPT              int not null auto_increment  comment '',
   primary key (ID_DEPT)
);

/*==============================================================*/
/* Table: GAJI                                                  */
/*==============================================================*/
create table GAJI
(
   PERIODE              date not null  comment '',
   ID_KAR               int not null  comment '',
   KINERJA              char(1)  comment '',
   LAIN_LAIN            int not null default 0  comment '',
   PPH21                int not null default 0  comment '',
   primary key (PERIODE, ID_KAR)
);

/*==============================================================*/
/* Table: JABATAN                                               */
/*==============================================================*/
create table JABATAN
(
   ID_JABATAN           int not null auto_increment  comment '',
   primary key (ID_JABATAN)
);

/*==============================================================*/
/* Table: JENIS_KELAMIN                                         */
/*==============================================================*/
create table JENIS_KELAMIN
(
   ID_JK                int not null auto_increment  comment '',
   NAMA_JK              varchar(11) not null  comment '',
   primary key (ID_JK)
);

/*==============================================================*/
/* Table: JOB_LEVEL                                             */
/*==============================================================*/
create table JOB_LEVEL
(
   ID_JOB_LEVEL         int not null auto_increment  comment '',
   primary key (ID_JOB_LEVEL)
);

/*==============================================================*/
/* Table: KARYAWAN                                              */
/*==============================================================*/
create table KARYAWAN
(
   ID_KAR               int not null auto_increment  comment '',
   primary key (ID_KAR)
);

/*==============================================================*/
/* Table: KINERJA                                               */
/*==============================================================*/
create table KINERJA
(
   KINERJA              char(1) not null  comment '',
   primary key (KINERJA)
);

/*==============================================================*/
/* Table: LEMBUR                                                */
/*==============================================================*/
create table LEMBUR
(
   ID_LEMBUR            int not null auto_increment  comment '',
   ID_SHIFT             int  comment '',
   ID_KAR               int not null  comment '',
   TANGGAL_LEMBUR       date not null  comment '',
   LAMA_LEMBUR          smallint not null  comment '',
   KETERANGAN_LEMBUR    varchar(50) not null  comment '',
   MAKAN_LEMBUR         tinyint not null  comment '',
   TRANSPORT_LEMBUR     tinyint not null  comment '',
   primary key (ID_LEMBUR)
);

/*==============================================================*/
/* Table: PENDIDIKAN                                            */
/*==============================================================*/
create table PENDIDIKAN
(
   ID_PENDIDIKAN        int not null auto_increment  comment '',
   NAMA_PENDIDIKAN      varchar(15) not null  comment '',
   TINGKAT_PENDIDIKAN   smallint not null  comment '',
   primary key (ID_PENDIDIKAN)
);

/*==============================================================*/
/* Table: PENGATURAN                                            */
/*==============================================================*/
create table PENGATURAN
(
   PERIODE_MULAI_PENG   date not null  comment '',
   PERIODE_AKHIR_PENG   date  comment '',
   BPJS_KERJA_PER       decimal not null  comment '',
   BPJS_KERJA_KAR       decimal not null  comment '',
   BPJS_KES_PER         decimal not null  comment '',
   BPJS_KES_KAR         decimal not null  comment '',
   UMK                  int not null  comment '',
   IURAN_RUMAH          int not null  comment '',
   IURAN_KOP            int not null  comment '',
   DAFTAR_KOP           int not null  comment '',
   UANG_MAKAN           int not null  comment '',
   UANG_TRANSPORT       int not null  comment '',
   NAMA_OWNER           varchar(25) not null  comment '',
   primary key (PERIODE_MULAI_PENG)
);

/*==============================================================*/
/* Table: PERAN_USER                                            */
/*==============================================================*/
create table PERAN_USER
(
   ID_PU                int not null  comment '',
   NAMA_PU              varchar(25) not null  comment '',
   primary key (ID_PU)
);

/*==============================================================*/
/* Table: PERIODE                                               */
/*==============================================================*/
create table PERIODE
(
   PERIODE              date not null  comment '',
   KAR_VALID            datetime  comment '',
   JOB_VALID            datetime  comment '',
   TK_VALID             datetime  comment '',
   LEMBUR_VALID         datetime  comment '',
   ABSEN_VALID          datetime  comment '',
   ANG_VALID            datetime  comment '',
   POT_ANG_VALID        datetime  comment '',
   primary key (PERIODE)
);

/*==============================================================*/
/* Table: PERUSAHAAN                                            */
/*==============================================================*/
create table PERUSAHAAN
(
   ID_PER               int not null auto_increment  comment '',
   primary key (ID_PER)
);

/*==============================================================*/
/* Table: POTONGAN_ANGSURAN                                     */
/*==============================================================*/
create table POTONGAN_ANGSURAN
(
   ID_ANG               int not null  comment '',
   PERIODE              date not null  comment '',
   NILAI_POT_ANG        int not null  comment '',
   primary key (ID_ANG, PERIODE)
);

/*==============================================================*/
/* Table: RIWAYAT_ANGSURAN                                      */
/*==============================================================*/
create table RIWAYAT_ANGSURAN
(
   ID_ANG               int not null  comment '',
   PERIODE_MULAI_ANG    date not null  comment '',
   PERIODE_AKHIR_ANG    date  comment '',
   TANGGAL_ANG          date not null  comment '',
   KETERANGAN_ANG       varchar(50) not null  comment '',
   NILAI_ANG            int not null  comment '',
   PEMBAYARAN_ANG       int not null  comment '',
   primary key (PERIODE_MULAI_ANG, ID_ANG)
);

/*==============================================================*/
/* Table: RIWAYAT_DEPARTEMEN                                    */
/*==============================================================*/
create table RIWAYAT_DEPARTEMEN
(
   ID_DEPT              int not null  comment '',
   PERIODE_MULAI_DEPT   date not null  comment '',
   PERIODE_AKHIR_DEPT   date  comment '',
   ID_PER               int  comment '',
   NAMA_DEPT            varchar(50) not null  comment '',
   primary key (PERIODE_MULAI_DEPT, ID_DEPT)
);

/*==============================================================*/
/* Table: RIWAYAT_JABATAN                                       */
/*==============================================================*/
create table RIWAYAT_JABATAN
(
   ID_JABATAN           int not null  comment '',
   PERIODE_MULAI_JABATAN date not null  comment '',
   PERIODE_AKHIR_JABATAN date  comment '',
   ID_JOB_LEVEL         int  comment '',
   NAMA_JABATAN         varchar(50) not null  comment '',
   primary key (PERIODE_MULAI_JABATAN, ID_JABATAN)
);

/*==============================================================*/
/* Table: RIWAYAT_JOB                                           */
/*==============================================================*/
create table RIWAYAT_JOB
(
   ID_JOB_LEVEL         int not null  comment '',
   PERIODE_MULAI_JOB    date not null  comment '',
   PERIODE_AKHIR_JOB    date  comment '',
   NAMA_JOB             varchar(50) not null  comment '',
   GAJI_POKOK           int  comment '',
   TUNJANGAN_JABATAN    int  comment '',
   primary key (PERIODE_MULAI_JOB, ID_JOB_LEVEL)
);

/*==============================================================*/
/* Table: RIWAYAT_KARYAWAN                                      */
/*==============================================================*/
create table RIWAYAT_KARYAWAN
(
   ID_KAR               int not null  comment '',
   PERIODE_MULAI_KAR    date not null  comment '',
   PERIODE_AKHIR_KAR    date  comment '',
   ID_STATUS_KOP        tinyint  comment '',
   ID_SUBDEPT           int not null  comment '',
   ID_JABATAN           int not null  comment '',
   ID_SK                int not null  comment '',
   ID_AGAMA             int not null  comment '',
   ID_JK                int not null  comment '',
   ID_PENDIDIKAN        int  comment '',
   NIK                  varchar(10)  comment '',
   NAMA_KAR             varchar(50) not null  comment '',
   TANGGAL_MASUK        date not null  comment '',
   ALAMAT_KAR           char(50) not null  comment '',
   NO_KTP_KAR           char(16) not null  comment '',
   TELEPON_KAR          varchar(15)  comment '',
   EMAIL_KAR            varchar(50)  comment '',
   TEMPAT_LAHIR_KAR     varchar(25) not null  comment '',
   TANGGAL_LAHIR_KAR    date not null  comment '',
   GAJI_DILAPORKAN      int  comment '',
   PERUMAHAN            tinyint  comment '',
   primary key (PERIODE_MULAI_KAR, ID_KAR)
);

/*==============================================================*/
/* Table: RIWAYAT_PERUSAHAAN                                    */
/*==============================================================*/
create table RIWAYAT_PERUSAHAAN
(
   ID_PER               int not null  comment '',
   PERIODE_MULAI_PER    date not null  comment '',
   PERIODE_AKHIR_PER    date  comment '',
   NAMA_PER             varchar(50) not null  comment '',
   primary key (PERIODE_MULAI_PER, ID_PER)
);

/*==============================================================*/
/* Table: RIWAYAT_SHIFT                                         */
/*==============================================================*/
create table RIWAYAT_SHIFT
(
   ID_SHIFT             int not null  comment '',
   PERIODE_MULAI_SHIFT  date not null  comment '',
   PERIODE_AKHIR_SHIFT  date  comment '',
   NAMA_SHIFT           varchar(3) not null  comment '',
   MAX_LEMBUR_1         smallint not null  comment '',
   MAX_LEMBUR_2         smallint not null  comment '',
   primary key (PERIODE_MULAI_SHIFT, ID_SHIFT)
);

/*==============================================================*/
/* Table: RIWAYAT_SUBDEPARTEMEN                                 */
/*==============================================================*/
create table RIWAYAT_SUBDEPARTEMEN
(
   ID_SUBDEPT           int not null  comment '',
   PERIODE_MULAI_SUBDEPT date not null  comment '',
   PERIODE_AKHIR_SUBDEPT date  comment '',
   ID_DEPT              int  comment '',
   NAMA_SUBDEPT         varchar(50) not null  comment '',
   primary key (PERIODE_MULAI_SUBDEPT, ID_SUBDEPT)
);

/*==============================================================*/
/* Table: RIWAYAT_TMK                                           */
/*==============================================================*/
create table RIWAYAT_TMK
(
   ID_TMK               int not null  comment '',
   PERIODE_MULAI_TMK    date not null  comment '',
   PERIODE_AKHIR_TMK    date  comment '',
   BATAS_BAWAH_TMK      smallint not null  comment '',
   BATAS_ATAS_TMK       smallint not null  comment '',
   NILAI_TMK            int not null  comment '',
   primary key (PERIODE_MULAI_TMK, ID_TMK)
);

/*==============================================================*/
/* Table: RIWAYAT_TUNJANGAN_KHUSUS                              */
/*==============================================================*/
create table RIWAYAT_TUNJANGAN_KHUSUS
(
   ID_TKH               int not null  comment '',
   PERIODE_MULAI_TKH    date not null  comment '',
   PERIODE_AKHIR_TKH    date  comment '',
   NAMA_TKH             varchar(50) not null  comment '',
   NILAI_TKH            int not null  comment '',
   primary key (PERIODE_MULAI_TKH, ID_TKH)
);

/*==============================================================*/
/* Table: RIWAYAT_TUNJANGAN_KINERJA                             */
/*==============================================================*/
create table RIWAYAT_TUNJANGAN_KINERJA
(
   ID_JOB_LEVEL         int not null  comment '',
   KINERJA              char(1) not null  comment '',
   PERIODE_MULAI_TKI    date not null  comment '',
   PERIODE_AKHIR_TKI    date  comment '',
   NILAI_TKI            int not null  comment '',
   primary key (PERIODE_MULAI_TKI, ID_JOB_LEVEL, KINERJA)
);

/*==============================================================*/
/* Table: RIWAYAT_UL                                            */
/*==============================================================*/
create table RIWAYAT_UL
(
   ID_JOB_LEVEL         int not null  comment '',
   PERIODE_MULAI_UL     date not null  comment '',
   PERIODE_AKHIR_UL     date  comment '',
   JAM_LEMBUR           int not null  comment '',
   UPAH_LEMBUR          int not null  comment '',
   primary key (PERIODE_MULAI_UL, ID_JOB_LEVEL, JAM_LEMBUR)
);

/*==============================================================*/
/* Table: RIWAYAT_USER                                          */
/*==============================================================*/
create table RIWAYAT_USER
(
   ID_USER              int not null  comment '',
   ID_PU                int  comment '',
   WAKTU_MULAI_USER     datetime not null  comment '',
   WAKTU_AKHIR_USER     datetime  comment '',
   NAMA_USER            varchar(50) not null  comment '',
   EMAIL_USER           varchar(50) not null  comment '',
   PASSWORD_USER        char(32) not null  comment '',
   primary key (ID_USER)
);

/*==============================================================*/
/* Table: SHIFT                                                 */
/*==============================================================*/
create table SHIFT
(
   ID_SHIFT             int not null auto_increment  comment '',
   primary key (ID_SHIFT)
);

/*==============================================================*/
/* Table: STATUS_KARYAWAN                                       */
/*==============================================================*/
create table STATUS_KARYAWAN
(
   ID_SK                int not null auto_increment  comment '',
   NAMA_SK              varchar(25) not null  comment '',
   primary key (ID_SK)
);

/*==============================================================*/
/* Table: STATUS_KOPERASI                                       */
/*==============================================================*/
create table STATUS_KOPERASI
(
   ID_STATUS_KOP        tinyint not null  comment '',
   NAMA_STATUS_KOP      varchar(25) not null  comment '',
   primary key (ID_STATUS_KOP)
);

/*==============================================================*/
/* Table: SUBDEPARTEMEN                                         */
/*==============================================================*/
create table SUBDEPARTEMEN
(
   ID_SUBDEPT           int not null auto_increment  comment '',
   primary key (ID_SUBDEPT)
);

/*==============================================================*/
/* Table: TKH_JABATAN                                           */
/*==============================================================*/
create table TKH_JABATAN
(
   ID_JABATAN           int not null  comment '',
   PERIODE_MULAI_TKHJ   date not null  comment '',
   PERIODE_AKHIR_TKHJ   date  comment '',
   ID_TKH               int not null  comment '',
   primary key (PERIODE_MULAI_TKHJ, ID_TKH, ID_JABATAN)
);

/*==============================================================*/
/* Table: TUNJANGAN_KHUSUS                                      */
/*==============================================================*/
create table TUNJANGAN_KHUSUS
(
   ID_TKH               int not null auto_increment  comment '',
   primary key (ID_TKH)
);

/*==============================================================*/
/* Table: TUNJANGAN_KINERJA                                     */
/*==============================================================*/
create table TUNJANGAN_KINERJA
(
   ID_JOB_LEVEL         int not null  comment '',
   KINERJA              char(1) not null  comment '',
   primary key (ID_JOB_LEVEL, KINERJA)
);

/*==============================================================*/
/* Table: TUNJANGAN_MASA_KERJA                                  */
/*==============================================================*/
create table TUNJANGAN_MASA_KERJA
(
   ID_TMK               int not null auto_increment  comment '',
   ID_JOB_LEVEL         int not null  comment '',
   primary key (ID_TMK)
);

/*==============================================================*/
/* Table: UPAH_LEMBUR                                           */
/*==============================================================*/
create table UPAH_LEMBUR
(
   ID_JOB_LEVEL         int not null  comment '',
   JAM_LEMBUR           int not null  comment '',
   primary key (ID_JOB_LEVEL, JAM_LEMBUR)
);

/*==============================================================*/
/* Table: USER                                                  */
/*==============================================================*/
create table USER
(
   ID_USER              int not null auto_increment  comment '',
   USERNAME             varchar(16) not null  comment '',
   primary key (ID_USER)
);

alter table ABSEN add constraint FK_ABSEN_1 foreign key (ID_KAR)
      references KARYAWAN (ID_KAR) on delete cascade on update cascade;

alter table ANGSURAN add constraint FK_ANGSURAN_1 foreign key (ID_KAR)
      references KARYAWAN (ID_KAR) on delete cascade on update cascade;

alter table GAJI add constraint FK_GAJI_3 foreign key (PERIODE)
      references PERIODE (PERIODE) on delete cascade on update cascade;

alter table GAJI add constraint FK_GAJI_1 foreign key (KINERJA)
      references KINERJA (KINERJA) on delete set null on update cascade;

alter table GAJI add constraint FK_GAJI_2 foreign key (ID_KAR)
      references KARYAWAN (ID_KAR) on delete cascade on update cascade;

alter table LEMBUR add constraint FK_LEMBUR_2 foreign key (ID_KAR)
      references KARYAWAN (ID_KAR) on delete cascade on update cascade;

alter table LEMBUR add constraint FK_LEMBUR_1 foreign key (ID_SHIFT)
      references SHIFT (ID_SHIFT) on delete restrict on update restrict;

alter table PENGATURAN add constraint FK_PENGATURAN_1 foreign key (PERIODE_AKHIR_PENG)
      references PERIODE (PERIODE) on delete restrict on update restrict;

alter table PENGATURAN add constraint FK_PENGATURAN_2 foreign key (PERIODE_MULAI_PENG)
      references PERIODE (PERIODE) on delete restrict on update restrict;

alter table POTONGAN_ANGSURAN add constraint FK_POTONGAN_ANGSURAN_2 foreign key (ID_ANG)
      references ANGSURAN (ID_ANG) on delete cascade on update cascade;

alter table POTONGAN_ANGSURAN add constraint FK_POTONGAN_ANGSURAN_1 foreign key (PERIODE)
      references PERIODE (PERIODE) on delete cascade on update cascade;

alter table RIWAYAT_ANGSURAN add constraint FK_RIWAYAT_ANGSURAN_1 foreign key (PERIODE_AKHIR_ANG)
      references PERIODE (PERIODE) on delete restrict on update restrict;

alter table RIWAYAT_ANGSURAN add constraint FK_RIWAYAT_ANGSURAN_2 foreign key (PERIODE_MULAI_ANG)
      references PERIODE (PERIODE) on delete restrict on update restrict;

alter table RIWAYAT_ANGSURAN add constraint FK_RIWAYAT_ANGSURAN_3 foreign key (ID_ANG)
      references ANGSURAN (ID_ANG) on delete cascade on update cascade;

alter table RIWAYAT_DEPARTEMEN add constraint FK_RIWAYAT_DEPARTEMEN_1 foreign key (ID_PER)
      references PERUSAHAAN (ID_PER) on delete cascade on update cascade;

alter table RIWAYAT_DEPARTEMEN add constraint FK_RIWAYAT_DEPARTEMEN_2 foreign key (PERIODE_AKHIR_DEPT)
      references PERIODE (PERIODE) on delete restrict on update restrict;

alter table RIWAYAT_DEPARTEMEN add constraint FK_RIWAYAT_DEPARTEMEN_3 foreign key (PERIODE_MULAI_DEPT)
      references PERIODE (PERIODE) on delete restrict on update restrict;

alter table RIWAYAT_DEPARTEMEN add constraint FK_RIWAYAT_DEPARTEMEN_4 foreign key (ID_DEPT)
      references DEPARTEMEN (ID_DEPT) on delete cascade on update cascade;

alter table RIWAYAT_JABATAN add constraint FK_RIWAYAT_JABATAN_1 foreign key (ID_JOB_LEVEL)
      references JOB_LEVEL (ID_JOB_LEVEL) on delete set null on update cascade;

alter table RIWAYAT_JABATAN add constraint FK_RIWAYAT_JABATAN_2 foreign key (PERIODE_AKHIR_JABATAN)
      references PERIODE (PERIODE) on delete restrict on update restrict;

alter table RIWAYAT_JABATAN add constraint FK_RIWAYAT_JABATAN_3 foreign key (PERIODE_MULAI_JABATAN)
      references PERIODE (PERIODE) on delete restrict on update restrict;

alter table RIWAYAT_JABATAN add constraint FK_RIWAYAT_JABATAN_4 foreign key (ID_JABATAN)
      references JABATAN (ID_JABATAN) on delete restrict on update restrict;

alter table RIWAYAT_JOB add constraint FK_RIWAYAT_JOB_1 foreign key (PERIODE_AKHIR_JOB)
      references PERIODE (PERIODE) on delete restrict on update restrict;

alter table RIWAYAT_JOB add constraint FK_RIWAYAT_JOB_2 foreign key (PERIODE_MULAI_JOB)
      references PERIODE (PERIODE) on delete restrict on update restrict;

alter table RIWAYAT_JOB add constraint FK_RIWAYAT_JOB_3 foreign key (ID_JOB_LEVEL)
      references JOB_LEVEL (ID_JOB_LEVEL) on delete cascade on update cascade;

alter table RIWAYAT_KARYAWAN add constraint FK_RIWAYAT_KARYAWAN_3 foreign key (ID_STATUS_KOP)
      references STATUS_KOPERASI (ID_STATUS_KOP) on delete restrict on update cascade;

alter table RIWAYAT_KARYAWAN add constraint FK_RIWAYAT_KARYAWAN_2 foreign key (ID_SUBDEPT)
      references SUBDEPARTEMEN (ID_SUBDEPT) on delete restrict on update cascade;

alter table RIWAYAT_KARYAWAN add constraint FK_RIWAYAT_KARYAWAN_5 foreign key (ID_AGAMA)
      references AGAMA (ID_AGAMA) on delete restrict on update cascade;

alter table RIWAYAT_KARYAWAN add constraint FK_RIWAYAT_KARYAWAN_1 foreign key (ID_JABATAN)
      references JABATAN (ID_JABATAN) on delete restrict on update cascade;

alter table RIWAYAT_KARYAWAN add constraint FK_RIWAYAT_KARYAWAN_6 foreign key (ID_JK)
      references JENIS_KELAMIN (ID_JK) on delete restrict on update cascade;

alter table RIWAYAT_KARYAWAN add constraint FK_RIWAYAT_KARYAWAN_4 foreign key (ID_SK)
      references STATUS_KARYAWAN (ID_SK) on delete restrict on update cascade;

alter table RIWAYAT_KARYAWAN add constraint FK_RIWAYAT_KARYAWAN_7 foreign key (ID_PENDIDIKAN)
      references PENDIDIKAN (ID_PENDIDIKAN) on delete restrict on update cascade;

alter table RIWAYAT_KARYAWAN add constraint FK_RIWAYAT_KARYAWAN_8 foreign key (PERIODE_AKHIR_KAR)
      references PERIODE (PERIODE) on delete restrict on update restrict;

alter table RIWAYAT_KARYAWAN add constraint FK_RIWAYAT_KARYAWAN_9 foreign key (PERIODE_MULAI_KAR)
      references PERIODE (PERIODE) on delete restrict on update restrict;

alter table RIWAYAT_KARYAWAN add constraint FK_RIWAYAT_KARYAWAN_10 foreign key (ID_KAR)
      references KARYAWAN (ID_KAR) on delete cascade on update cascade;

alter table RIWAYAT_PERUSAHAAN add constraint FK_RIWAYAT_PERUSAHAAN_1 foreign key (PERIODE_AKHIR_PER)
      references PERIODE (PERIODE) on delete restrict on update restrict;

alter table RIWAYAT_PERUSAHAAN add constraint FK_RIWAYAT_PERUSAHAAN_2 foreign key (PERIODE_MULAI_PER)
      references PERIODE (PERIODE) on delete restrict on update restrict;

alter table RIWAYAT_PERUSAHAAN add constraint FK_RIWAYAT_PERUSAHAAN_3 foreign key (ID_PER)
      references PERUSAHAAN (ID_PER) on delete cascade on update cascade;

alter table RIWAYAT_SHIFT add constraint FK_RIWAYAT_SHIFT_1 foreign key (PERIODE_AKHIR_SHIFT)
      references PERIODE (PERIODE) on delete restrict on update restrict;

alter table RIWAYAT_SHIFT add constraint FK_RIWAYAT_SHIFT_2 foreign key (PERIODE_MULAI_SHIFT)
      references PERIODE (PERIODE) on delete restrict on update restrict;

alter table RIWAYAT_SHIFT add constraint FK_RIWAYAT_SHIFT_3 foreign key (ID_SHIFT)
      references SHIFT (ID_SHIFT) on delete cascade on update cascade;

alter table RIWAYAT_SUBDEPARTEMEN add constraint FK_RIWAYAT_SUBDEPARTEMEN_1 foreign key (PERIODE_AKHIR_SUBDEPT)
      references PERIODE (PERIODE) on delete restrict on update restrict;

alter table RIWAYAT_SUBDEPARTEMEN add constraint FK_RIWAYAT_SUBDEPARTEMEN_2 foreign key (PERIODE_MULAI_SUBDEPT)
      references PERIODE (PERIODE) on delete restrict on update restrict;

alter table RIWAYAT_SUBDEPARTEMEN add constraint FK_RIWAYAT_SUBDEPARTEMEN_3 foreign key (ID_SUBDEPT)
      references SUBDEPARTEMEN (ID_SUBDEPT) on delete cascade on update cascade;

alter table RIWAYAT_SUBDEPARTEMEN add constraint FK_RIWAYAT_SUBDEPARTEMEN_4 foreign key (ID_DEPT)
      references DEPARTEMEN (ID_DEPT) on delete cascade on update cascade;

alter table RIWAYAT_TMK add constraint FK_RIWAYAT_TMK_1 foreign key (PERIODE_AKHIR_TMK)
      references PERIODE (PERIODE) on delete restrict on update restrict;

alter table RIWAYAT_TMK add constraint FK_RIWAYAT_TMK_2 foreign key (PERIODE_MULAI_TMK)
      references PERIODE (PERIODE) on delete restrict on update restrict;

alter table RIWAYAT_TMK add constraint FK_RIWAYAT_TMK_3 foreign key (ID_TMK)
      references TUNJANGAN_MASA_KERJA (ID_TMK) on delete cascade on update cascade;

alter table RIWAYAT_TUNJANGAN_KHUSUS add constraint FK_RIWAYAT_TUNJANGAN_KHUSUS_1 foreign key (PERIODE_AKHIR_TKH)
      references PERIODE (PERIODE) on delete restrict on update restrict;

alter table RIWAYAT_TUNJANGAN_KHUSUS add constraint FK_RIWAYAT_TUNJANGAN_KHUSUS_2 foreign key (PERIODE_MULAI_TKH)
      references PERIODE (PERIODE) on delete restrict on update restrict;

alter table RIWAYAT_TUNJANGAN_KHUSUS add constraint FK_RIWAYAT_TUNJANGAN_KHUSUS_3 foreign key (ID_TKH)
      references TUNJANGAN_KHUSUS (ID_TKH) on delete cascade on update cascade;

alter table RIWAYAT_TUNJANGAN_KINERJA add constraint FK_RIWAYAT_TUNJANGAN_KINERJA_1 foreign key (PERIODE_AKHIR_TKI)
      references PERIODE (PERIODE) on delete restrict on update restrict;

alter table RIWAYAT_TUNJANGAN_KINERJA add constraint FK_RIWAYAT_TUNJANGAN_KINERJA_2 foreign key (PERIODE_MULAI_TKI)
      references PERIODE (PERIODE) on delete restrict on update restrict;

alter table RIWAYAT_TUNJANGAN_KINERJA add constraint FK_RIWAYAT_TUNJANGAN_KINERJA_3 foreign key (ID_JOB_LEVEL, KINERJA)
      references TUNJANGAN_KINERJA (ID_JOB_LEVEL, KINERJA) on delete cascade on update cascade;

alter table RIWAYAT_UL add constraint FK_RIWAYAT_UL_1 foreign key (PERIODE_AKHIR_UL)
      references PERIODE (PERIODE) on delete restrict on update restrict;

alter table RIWAYAT_UL add constraint FK_RIWAYAT_UL_2 foreign key (PERIODE_MULAI_UL)
      references PERIODE (PERIODE) on delete restrict on update restrict;

alter table RIWAYAT_UL add constraint FK_RIWAYAT_UL_3 foreign key (ID_JOB_LEVEL, JAM_LEMBUR)
      references UPAH_LEMBUR (ID_JOB_LEVEL, JAM_LEMBUR) on delete cascade on update cascade;

alter table RIWAYAT_USER add constraint FK_RIWAYAT_USER_2 foreign key (ID_USER)
      references USER (ID_USER) on delete cascade on update cascade;

alter table RIWAYAT_USER add constraint FK_RIWAYAT_USER_1 foreign key (ID_PU)
      references PERAN_USER (ID_PU) on delete set null on update cascade;

alter table TKH_JABATAN add constraint FK_TKH_JABATAN_2 foreign key (PERIODE_AKHIR_TKHJ)
      references PERIODE (PERIODE) on delete restrict on update restrict;

alter table TKH_JABATAN add constraint FK_TKH_JABATAN_3 foreign key (PERIODE_MULAI_TKHJ)
      references PERIODE (PERIODE) on delete restrict on update restrict;

alter table TKH_JABATAN add constraint FK_TKH_JABATAN_1 foreign key (ID_JABATAN)
      references JABATAN (ID_JABATAN) on delete cascade on update cascade;

alter table TKH_JABATAN add constraint FK_TKH_JABATAN_4 foreign key (ID_TKH)
      references TUNJANGAN_KHUSUS (ID_TKH) on delete cascade on update cascade;

alter table TUNJANGAN_KINERJA add constraint FK_TUNJANGAN_KINERJA_2 foreign key (ID_JOB_LEVEL)
      references JOB_LEVEL (ID_JOB_LEVEL) on delete cascade on update cascade;

alter table TUNJANGAN_KINERJA add constraint FK_TUNJANGAN_KINERJA_1 foreign key (KINERJA)
      references KINERJA (KINERJA) on delete cascade on update cascade;

alter table TUNJANGAN_MASA_KERJA add constraint FK_TUNJANGAN_MASA_KERJA_1 foreign key (ID_JOB_LEVEL)
      references JOB_LEVEL (ID_JOB_LEVEL) on delete cascade on update cascade;

alter table UPAH_LEMBUR add constraint FK_UPAH_LEMBUR_1 foreign key (ID_JOB_LEVEL)
      references JOB_LEVEL (ID_JOB_LEVEL) on delete cascade on update cascade;

