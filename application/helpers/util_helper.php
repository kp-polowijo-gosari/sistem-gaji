<?php

function filterDictValues($data, $keys){
	$ret = array();
	foreach($keys as $keyCol){
		array_push($ret, $data[$keyCol]);
	}
	return $ret;
}

function filterDict($data, $keys){
	$ret = array();
	foreach($keys as $keyCol){
		$ret[$keyCol] = $data[$keyCol];
	}
	return $ret;
}
function snakeCase($str){
	return str_replace(" ", "_", $str);
}

function snakeCaseLower($str){
	return strtolower(snakeCase($str));
}

function snakeCaseUpper($str){
	return strtoupper(snakeCase($str));
}
function sureArray($x){
	if(!is_array($x))
		return array($x);
	return $x;
}

function isAssoc(array $array)
{
    // Keys of the array
    $keys = array_keys($array);

    // If the array keys of the keys match the keys, then the array must
    // not be associative (e.g. the keys array looked like {0:0, 1:1...}).
    return array_keys($keys) !== $keys;
}
function stringContains($str, $substr){
	return strpos($str, $substr) !== false;
}

function numberToRoman($number) {
    $map = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
    $returnValue = '';
    while ($number > 0) {
        foreach ($map as $roman => $int) {
            if($number >= $int) {
                $number -= $int;
                $returnValue .= $roman;
                break;
            }
        }
    }
    return $returnValue;
}
function toRupiah($amount, $decimal=0){
	return "Rp " . number_format($amount, $decimal, ",", ".");
}

function escape($search)
{
	$search = get_instance()->db->escape($search);
	$search = substr($search, 1, strlen($search)-2);
	return $search;
}


function setIfEmpty($arr, $key, $value){
	if(empty($arr[$key]))
		$arr[$key] = $value;
}



function masaKerja($tahun, $bulan){
	$tkAda = !(empty($tahun) || $bulan == 0);
	$bkAda = !(empty($tahun) || $bulan == 0);
	if(!$tkAda && !$bkAda){
		return "Baru";
	}else if ($tkAda && $bkAda){
		return "{$tahun} tahun {$bulan} bulan";
	}else if ($tkAda && !$bkAda){
		return "{$tahun} tahun";
	}else if (!$tkAda && $bkAda){
		return "{$bulan} bulan";
	}
}
function toTemplate($view, $data=null){
	if(empty($data)) $data = array();
	$row = null;
	unset($row);
	return escape(get_instance()->load->view($view, $data, true));
}

function validateUrl($url){
	if($url) return true;
	return false;
}
/*
//deprecated
function isResponseOk($response){
	return $response["result"] == RESULT_OK;
}

//deprecated
function isLoggedIn(){
	return get_instance()->session->userdata("idUser");
}
*/