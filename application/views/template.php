<!DOCTYPE html>
<html>
<?php
$baseUrl = base_url();
?>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title><?=(isset($title) && $title) ? "{$title} - " : "";?>Sistem Gaji PT. Polowijo Gosari</title>
    <?php $this->view("common-css");?>
    <?php 
	//ADDITIONAL CSS FILES
	if(isset($cssFiles) && $cssFiles){
		foreach($cssFiles as $css){
			echo "<link rel=\"stylesheet\" href=\"{$css}\">";
		}
	}
	?>
</head>

<body id="page-top" class="d-flex flex-column">
    <div id="wrapper" class="flex-grow-1">
        <?php $this->view("sidenav", $sideNavData);?>
        <div id="sidenav-overlay"></div>
        <div class="d-flex flex-column" id="content-wrapper">
            <div id="content">
                <?php $this->view("topnav", $topNavData);?>
            <div class="container-fluid">
				<?php if(isset($breadcrumbs) && $breadcrumbs && count($breadcrumbs) > 0){ ?>
                <div class="row">
                    <div class="col">
						<ol class="breadcrumb">
							<?php
							//BREADCRUMBS
							foreach($breadcrumbs as $bc){
							?>
							<li class="breadcrumb-item">
								<a <?=(isset($bc['url']) && $bc['url']) ? "href=\"{$bc['url']}\"" : "";?>>
									<span><?=$bc['text'];?></span>
								</a>
							</li>
							<?php
							}
							?>
						</ol>
                    </div>
                </div>
				<?php } ?>
				<?php 
				//CONTENT
				$this->view($content, isset($contentData) ? $contentData : null);
				?>
            </div>
        </div>
        <?php $this->view("footer");?>
    </div><a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a></div>
    <?php $this->view("common-modal");?>
    <?php 
	//ADDITIONAL MODALS
	if(isset($modals) && $modals){
		foreach($modals as $modal){
			try{
				if(is_array($modal))
					$this->view($modal["modal"], $modal["data"]);
				else
					$this->view($modal);
			}catch(Exception $ex){
				try{
					if(is_array($modal))
						$this->view("modal/".$modal["modal"], $modal["data"]);
					else
						$this->view("modal/".$modal);
				}catch(Exception $ex2){
					throw $ex;
				}
			}
		}
	}
	?>
    <?php $this->view("common-js");?>
    <?php unset($row); ?>
	<script>
	//ADDITIONAL INLINE JS
	<?php
	if(isset($jsInlines) && $jsInlines){
		foreach($jsInlines as $js){
			try{
				$this->view($js);
			}catch(Exception $ex){
				try{
					$this->view($js);
				}catch(Exception $ex2){
					throw $ex;
				}
			}
		}
	}
	?>
	</script>
    <?php 
	$row = null;
	unset($row);
	//ADDITIONAL JS FILES
	if(isset($jsFiles) && $jsFiles){
		foreach($jsFiles as $js){
			echo "<script src=\"{$js}\"></script>";
		}
	}
	?>
	<script>
	//ADDITIONAL INLINE JS2
	<?php
	$row = null;
	unset($row);
	if(isset($jsInlines2) && $jsInlines2){
		foreach($jsInlines2 as $js){
			try{
				$this->view($js);
			}catch(Exception $ex){
				try{
					$this->view($js);
				}catch(Exception $ex2){
					throw $ex;
				}
			}
		}
	}
	?>
	</script>
</body>

</html>