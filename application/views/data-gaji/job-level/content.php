
<div class="d-sm-flex justify-content-between align-items-center mb-4">
	<h3 class="text-dark mb-0">Job Level</h3>
</div>
<div class="d-sm-flex align-items-start mb-4">
	<!--
	<button class="btn btn-primary mx-1 invalidated-mode" type="button">Impor</button>
	-->
	<div class="dropdown d-inline-block"><button class="btn btn-primary dropdown-toggle mx-1" data-toggle="dropdown" aria-expanded="false" type="button">Ekspor</button>
		<div class="dropdown-menu" role="menu" id="tableActions"></div>
	</div>
	<div class="inline-block">
		<button class="btn btn-success mx-1 btn-validasi invalidated-mode" type="button">Validasi</button>
		<button class="btn btn-danger mx-1 btn-batal-validasi validated-mode d-none" type="button">Batalkan Validasi</button>
	</div>
</div>
<div class="row mb-4">
	<div class="col">
		<div>
			<ul class="nav nav-tabs">
				<li class="nav-item"><a class="nav-link active invalidated-mode" role="tab" data-toggle="tab" href="#tab-1">Baru</a></li>
				<li class="nav-item"><a class="nav-link invalidated-mode" role="tab" data-toggle="tab" href="#tab-2">Lama</a></li>
				<li class="nav-item"><a class="nav-link" role="tab" data-toggle="tab" href="#tab-3">Sekarang</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active invalidated-mode" role="tabpanel" id="tab-1">
					<div class="card tab-card">
						<div class="card-body">
							<div class="table-responsive datatable export auto-number">
								<table class="table" id="table-baru">
									<thead>
										<tr>
											<th class="shrink">No</th>
											<th>Job Level</th>
											<th class="invalidated-mode">Aksi</th>
										</tr>
									</thead>
									<tbody>
										<?php 
										$i = 0;
										foreach($jobBaru as $row){ 
											++$i;
											$this->view("data-gaji/job-level/row-baru", array(
												"row"=>$row,
												"i"=>$i
											));
										} 
										?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane invalidated-mode" role="tabpanel" id="tab-2">
					<div class="card tab-card">
						<div class="card-body">
							<div class="row my-2">
								<div class="col">
									<div class="table-responsive datatable auto-number">
										<table class="table" id="table-lama">
											<thead>
												<tr>
													<th class="shrink">No</th>
													<th>Job Level</th>
													<th>Gaji Pokok</th>
													<th>Tunjangan Jabatan</th>
													<th class="invalidated-mode">Aksi</th>
												</tr>
											</thead>
											<tbody>
												<?php 
												$i = 0;
												foreach($jobLama as $row){ 
													++$i;
													$this->view("data-gaji/job-level/row-lama", array(
														"row"=>$row,
														"i"=>$i
													));
												} 
												?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane" role="tabpanel" id="tab-3">
					<div class="card tab-card">
						<div class="card-body">
							<div class="row my-2">
								<div class="col">
									<div class="table-responsive datatable auto-number">
										<table class="table" id="table-sekarang">
											<thead>
												<tr>
													<th class="shrink">No</th>
													<th>Job Level</th>
													<th>Gaji Pokok</th>
													<th>Tunjangan Jabatan</th>
													<th class="invalidated-mode">Aksi</th>
												</tr>
											</thead>
											<tbody>
												<?php 
												$i = 0;
												foreach($jobLama as $row){ 
													++$i;
													$this->view("data-gaji/job-level/row-lama", array(
														"row"=>$row,
														"i"=>$i
													));
												} 
												?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>