
<div class="d-sm-flex justify-content-between align-items-center mb-4">
	<h3 class="text-dark mb-0">Absen</h3>
</div>
<div class="d-sm-flex align-items-start mb-4">
	<button class="btn btn-primary mx-1 invalidated-mode" type="button" data-toggle="modal" data-target="#modal-add-absen">Tambah</button>
	<!--
	<button class="btn btn-primary mx-1 invalidated-mode" type="button">Impor</button>
	-->
	<div class="dropdown d-inline-block">
		<button class="btn btn-primary dropdown-toggle mx-1" data-toggle="dropdown" aria-expanded="false" type="button">Ekspor</button>
		<div class="dropdown-menu" role="menu" id="tableActions"></div>
	</div>
	<div class="inline-block">
		<button class="btn btn-success mx-1 btn-validasi invalidated-mode" type="button">Validasi</button>
		<button class="btn btn-danger mx-1 btn-batal-validasi validated-mode d-none" type="button">Batalkan Validasi</button>
	</div>
</div>
<div class="row mb-4">
	<div class="col">
		<div class="card">
			<div class="card-body">
				<div class="row my-2">
					<div class="col">
						<div class="table-responsive datatable export auto-number">
							<table class="table" id="table-aktif">
								<thead>
									<tr>
										<th class="shrink">No</th>
										<th>NIK</th>
										<th>Nama</th>
										<th>Jabatan</th>
										<th>Departemen</th>
										<th class="shrink">Total Jam</th>
										<th class="shrink no-export">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="shrink text-right">1</td>
										<td class="shrink">H7621</td>
										<td>Muhammad Rizqi Nur</td>
										<td>Manager R&amp;D</td>
										<td>Research and Development</td>
										<td class="text-right">4</td>
										<td class="shrink"><a class="btn btn-primary btn-sm mx-1" role="button" href="<?=base_url();?>gaji/absen/1">Detail</a></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>