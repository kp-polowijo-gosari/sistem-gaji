
<div class="d-sm-flex justify-content-between align-items-center mb-4">
	<h3 class="text-dark mb-0">Data Gaji Karyawan</h3>
</div>
<div class="mb-4">
	<div class="inline-block float-left">
		<button class="btn btn-primary mx-1 edit-mode btn-ubah-gaji-karyawan-global" type="button">Ubah</button>
		<button class="btn btn-primary mx-1 normal-mode d-none btn-simpan-gaji-karyawan-global" type="button">Simpan</button>
	</div>
	<div class="inline-block">
		<button class="btn btn-success mx-1 btn-validasi invalidated-mode" type="button">Validasi</button>
		<button class="btn btn-danger mx-1 btn-batal-validasi validated-mode d-none" type="button">Batalkan Validasi</button>
	</div>
</div>
<div class="row mb-4">
	<div class="col">
		<div>
			<ul class="nav nav-tabs">
				<li class="nav-item invalidated-mode"><a class="nav-link active" role="tab" data-toggle="tab" href="#tab-1">Belum</a></li>
				<li class="nav-item"><a class="nav-link" role="tab" data-toggle="tab" href="#tab-2">Sudah</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active invalidated-mode" role="tabpanel" id="tab-1">
					<div class="card tab-card">
						<div class="card-body">
							<div class="row my-2">
								<div class="col">
									<div class="table-responsive datatable auto-number">
										<table class="table" id="table-belum">
											<thead class="text-center">
												<tr>
													<th class="shrink">No</th>
													<th>NIK</th>
													<th>Nama</th>
													<th>Jabatan</th>
													<th>Departemen</th>
													<th>Gaji yang<br>Dilaporkan</th>
													<th>Iuran Perumahan</th>
													<th>Iuran Koperasi</th>
													<th>Kinerja</th>
													<th>Lain-Lain</th>
													<th class="no-export">Aksi</th>
												</tr>
											</thead>
											<tbody class="text-center">
												<?php 
												$i = 0;
												foreach($karBelum as $row){ 
													++$i;
													$this->view("data-gaji/karyawan/row-belum", array(
														"row"=>$row,
														"i"=>$i
													));
												} 
												?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane" role="tabpanel" id="tab-2">
					<div class="card tab-card">
						<div class="card-body">
							<div class="row my-2">
								<div class="col">
									<div class="table-responsive datatable auto-number">
										<table class="table" id="table-sudah">
											<thead class="text-center">
												<tr>
													<th class="shrink">No</th>
													<th>NIK</th>
													<th>Nama</th>
													<th>Jabatan</th>
													<th>Departemen</th>
													<th>Gaji yang<br>Dilaporkan</th>
													<th>Iuran Perumahan</th>
													<th>Iuran Koperasi</th>
													<th>Kinerja</th>
													<th>Gaji Lain-Lain</th>
													<th class="invalidated-mode">Aksi</th>
												</tr>
											</thead>
											<tbody class="text-center">
												<?php 
												$i = 0;
												foreach($karSudah as $row){ 
													++$i;
													$this->view("data-gaji/karyawan/row-sudah", array(
														"row"=>$row,
														"i"=>$i
													));
												} 
												?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>