
<tr class="">
	<td class="shrink text-right row-num no"><?=empty($row)?"1":$i;?></td>
	<td class="shrink">
		<input class="id-kar" type="hidden" name="ID_KAR" valtype="input"
			value="<?=(empty($row) || empty($row["ID_KAR"]))?"":$row["ID_KAR"];?>"
		>
		<span class="nik" valtype="text" name="NIK"><?=(empty($row) || empty($row['NIK']))?"-":$row['NIK'];?></span>
	</td>
	<td class="">
		<span class="nama-kar" valtype="text" name="NAMA_KAR"><?=(empty($row) || empty($row['NAMA_KAR']))?"-":$row['NAMA_KAR'];?></span>
	</td>
	<td class="">
		<span class="nama-jabatan" valtype="text" name="NAMA_JABATAN"><?=(empty($row) || empty($row['NAMA_JABATAN']))?"-":$row['NAMA_JABATAN'];?></span>
		<span class="nama-job" valtype="text" name="NAMA_JOB"><?=(empty($row) || empty($row['NAMA_JOB']))?"":" ({$row['NAMA_JOB']})";?></span>
	</td>
	<td class="">
		<span class="nama-subdept" valtype="text" name="NAMA_SUBDEPT"><?=(empty($row) || empty($row['NAMA_SUBDEPT']))?"-":$row['NAMA_SUBDEPT'];?></span>
		<span class="nama-dept" valtype="text" name="NAMA_DEPT"><?=(empty($row) || empty($row['NAMA_DEPT']))?"":" / {$row['NAMA_DEPT']}";?></span>
	</td>
	<td class="text-right">
		<input type="text" class="w-100 edit-mode gaji-dilaporkan-field form-control" data-type="currency" valtype="input" valtarget=".gaji-dilaporkan" name="GAJI_DILAPORKAN"
			value="<?=(empty($row) || empty($row["GAJI_DILAPORKAN"]))?"":toRupiah($row["GAJI_DILAPORKAN"]);?>"
		>
	</td>
	<td class="shrink">
		<input type="checkbox" class="perumahan-field edit-toggle" disabled="">
	</td>
	<td class="shrink">
		<select valtype="select" valtarget="id-sko" valtarget2="nama-sko" name="ID_SKO" class=" id-sko-field w-100 edit-mode form-control">
			<?php 
			$hasRow = !(empty($row) || empty($row['ID_SKO']));
			foreach($statusKop as $sk){?>
				<option value="<?=$sk["ID_SKO"];?>" <?php if($hasRow && $sk["ID_SKO"]==$row['ID_SKO']){ echo ' selected="selected"'; $first = false; } ?> >
					<?=$sk["NAMA_SKO"];?>
				</option>
			<?php } ?>
		</select>
	</td>
	<td class="shrink">
		<select valtype="select" valtarget="kinerja" valtarget2="kinerja" name="KINERJA" class=" kinerja-field w-100 edit-mode form-control">
			<?php 
			$hasRow = !(empty($row) || empty($row['KINERJA']));
			foreach($kinerja as $sk){?>
				<option value="<?=$sk["KINERJA"];?>" <?php if($hasRow && $sk["KINERJA"]==$row['KINERJA']){ echo ' selected="selected"'; $first = false; } ?> >
					<?=$sk["KINERJA"];?>
				</option>
			<?php } ?>
		</select>
	</td>
	<td class="shrink">
		<input type="text" class="w-100 edit-mode gaji-lain-field form-control" data-type="currency" valtype="input" valtarget=".gaji-lain" name="GAJI_LAIN"
			value="<?=(empty($row) || empty($row["GAJI_LAIN"]))?"":toRupiah($row["GAJI_LAIN"]);?>"
		>
	</td>
	<td class="shrink">
		<button class="btn btn-primary btn-sm m-1 btn-simpan-data-gaji-karyawan" type="button">Simpan</button>
	</td>
</tr>