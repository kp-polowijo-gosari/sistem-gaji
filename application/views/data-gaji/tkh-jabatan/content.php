
<div class="d-sm-flex justify-content-between align-items-center mb-4">
	<h3 class="text-dark mb-0">Tunjangan Khusus <?="{$jabatan['NAMA_JABATAN']} ({$jabatan['NAMA_JOB']})";?></h3>
</div>
<div class="d-sm-flex align-items-start mb-4">
	<button class="btn btn-primary mx-1 invalidated-mode" type="button" id="btn-tambah-tkhj">Tambah</button>
	<!--
	<button class="btn btn-primary mx-1 invalidated-mode" type="button">Impor</button>
	-->
	<div class="dropdown d-inline-block">
		<button class="btn btn-primary dropdown-toggle mx-1" data-toggle="dropdown" aria-expanded="false" type="button">Ekspor</button>
		<div class="dropdown-menu" role="menu" id="tableActions"></div>
	</div>
</div>
<div class="row mb-4">
	<div class="col">
		<div>
			<ul class="nav nav-tabs">
				<li class="nav-item"><a class="nav-link active" role="tab" data-toggle="tab" href="#tab-1">Aktif</a></li>
				<li class="nav-item"><a class="nav-link" role="tab" data-toggle="tab" href="#tab-2">Dihapus</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" role="tabpanel" id="tab-1">
					<div class="card tab-card">
						<div class="card-body">
							<div class="row my-2">
								<div class="col">
									<div class="table-responsive datatable export auto-number">
										<table class="table table-edit" id="table-aktif">
											<thead>
												<tr>
													<th class="shrink">No</th>
													<th>Tunjangan</th>
													<th>Nilai</th>
													<th class="invalidated-mode no-export">Aksi</th>
												</tr>
											</thead>
											<tbody>
												<?php 
												$i = 0;
												foreach($tkhjAktif as $row){ 
													++$i;
													$this->view("data-gaji/tkh-jabatan/row-aktif", array(
														"row"=>$row,
														"i"=>$i
													));
												} 
												?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane" role="tabpanel" id="tab-2">
					<div class="card tab-card">
						<div class="card-body">
							<div class="row my-2">
								<div class="col">
									<div class="table-responsive datatable auto-number">
										<table class="table " id="table-dihapus">
											<thead>
												<tr>
													<th class="shrink">No</th>
													<th>Tunjangan</th>
													<th>Nilai</th>
													<th class="invalidated-mode no-export">Aksi</th>
												</tr>
											</thead>
											<tbody>
												<?php 
												$i = 0;
												foreach($tkhjDihapus as $row){ 
													++$i;
													$this->view("data-gaji/tkh-jabatan/row-dihapus", array(
														"row"=>$row,
														"i"=>$i
													));
												} 
												?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>