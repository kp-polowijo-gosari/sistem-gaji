
<tr class="">
	<td class="shrink text-right"><?=empty($row)?"1":$i;?></td>
	<td>
		<input class="id-tkh" type="hidden" name="ID_TKH" valtype="input"
			value="<?=(empty($row) || empty($row["ID_TKH"]))?"":$row["ID_TKH"];?>"
		>
		<span class="normal-mode nama-tkh " name="NAMA_TKH" valtype="text" valtarget=".nama-tkh-field"><?=(empty($row) || empty($row["NAMA_TKH"]))?"":$row["NAMA_TKH"];?></span>
	</td>
	<td class="text-right">
		<span class="normal-mode nilai-tkh" name="NILAI_TKH" data-type="currency" valtype="text" valtarget=".nilai-tkh-field"><?=(empty($row) || empty($row["NILAI_TKH"]))?"":toRupiah($row["NILAI_TKH"]);?></span>
	</td>
	<td class="shrink invalidated-mode">
		<button class="btn btn-danger btn-sm mx-1 btn-hapus btn-hapus-tkh normal-mode" type="button">Hapus</button>
	</td>
</tr>