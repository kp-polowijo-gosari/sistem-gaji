
<div class="row">
	<div class="col">
		<div class="d-sm-flex justify-content-between align-items-center mb-4">
			<h3 class="text-dark mb-0">Lembur Harian</h3>
		</div>
		<div class="d-sm-flex align-items-start mb-4">
			<button class="btn btn-primary mx-1" type="button" data-toggle="modal" data-target="#modal-add-lembur">Tambah</button>
			<button class="btn btn-primary mx-1" type="button">Import</button>
			<div class="dropdown d-inline-block">
				<button class="btn btn-primary dropdown-toggle mx-1" data-toggle="dropdown" aria-expanded="false" type="button">Ekspor</button>
				<div class="dropdown-menu" role="menu" id="tableActions"></div>
			</div>
		</div>
	</div>
	<div class="col-3 mb-3">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">Muhammad Rizqi Nur</h4>
				<h6 class="text-muted card-subtitle">Manager R&amp;D</h6>
			</div>
		</div>
	</div>
</div>
<div class="row mb-4">
	<div class="col">
		<div class="card">
			<div class="card-body">
				<div class="row my-2">
					<div class="col">
						<div class="table-responsive datatable export">
							<table class="table">
								<thead>
									<tr>
										<th class="shrink">No</th>
										<th>Tanggal</th>
										<th>Keterangan</th>
										<th>Shift</th>
										<th>Jam</th>
										<th>Makan</th>
										<th>Transport</th>
										<th class="shrink no-export">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr class="lembur-row">
										<td class="shrink text-right">1</td>
										<td class="shrink"><span class="tanggal normal-mode">2020-01-20</span><input class="tanggal-field edit-mode d-none form-control" type="date"></td>
										<td><span class="keterangan normal-mode">Gtw</span><input type="text" class="keterangan-field edit-mode d-none w-100 form-control"></td>
										<td class="shrink text-center"><span class="shift normal-mode">I</span><select class="shift-field edit-mode d-none small form-control"><option value="1" selected="">I</option><option value="2">II</option><option value="3">III</option><option value="0">Off</option></select></td>
										<td
											class="shrink text-right"><span class="jam normal-mode">4</span><input type="number" class="jam-field d-none edit-mode small form-control"></td>
											<td class="shrink text-center"><input type="checkbox" class="makan-field edit-toggle" disabled="" checked=""></td>
											<td class="shrink text-center"><input type="checkbox" class="transport-field edit-toggle" disabled=""></td>
											<td class="shrink"><button class="btn btn-primary btn-sm mx-1 normal-mode btn-ubah-lembur" type="button">Ubah</button><button class="btn btn-primary btn-sm mx-1 edit-mode btn-simpan-lembur d-none" type="button">Simpan</button>
												<button
													class="btn btn-danger btn-sm mx-1 normal-mode btn-hapus-lembur" type="button">Hapus</button>
											</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>