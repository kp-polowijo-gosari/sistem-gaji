
<div class="d-sm-flex justify-content-between align-items-center mb-4">
	<h3 class="text-dark mb-0">Manager</h3>
</div>
<div class="row mb-4">
	<div class="col-6">
		<div class="row mb-4">
			<div class="col">
				<form id="form-job">
					<div class="card">
						<div class="card-header">
							<h4 class="d-inline-block mr-auto">Data Job Level</h4>
							<button id="btn-simpan-job" class="btn btn-primary btn-sm ml-auto float-right" type="button">Simpan</button>
						</div>
						<div class="card-body">
							<input id="id-job-field" type="hidden" name="ID_JOB" value="<?=empty($job)?"":$job['ID_JOB'];?>">
							<div class="row my-2 align-items-center">
								<div class="col-4 align-items-center w-30"><label class="col-form-label">Nama</label></div>
								<div class="col">
									<!--
									<span class="normal-mode"><?=(empty($job)||empty($job["NAMA_JOB"]))?"":$job["NAMA_JOB"];?></span>
									-->
									<input type="text" class="w-100 form-control  edit-mode align-items-center"  name="NAMA_JOB"
										value="<?=(empty($job)||empty($job["NAMA_JOB"]))?"":$job["NAMA_JOB"];?>"
									>
								</div>
							</div>
							<div class="row my-2 align-items-center">
								<div class="col-4 align-items-center w-30"><label class="col-form-label">Gaji Pokok</label></div>
								<div class="col">
									<!--
									<span class="normal-mode"><?=(empty($job)||empty($job["GAJI_POKOK"]))?"":$job["GAJI_POKOK"];?></span>
									-->
									<input type="text" class="w-100 form-control  edit-mode align-items-center text-right"  data-type="currency" name="GAJI_POKOK"
										value="<?=(empty($job)||empty($job["GAJI_POKOK"]))?"":$job["GAJI_POKOK"];?>"
									>
								</div>
							</div>
							<div class="row my-2 align-items-center">
								<div class="col-4 align-items-center w-30"><label class="col-form-label">Tunjangan Jabatan</label></div>
								<div class="col align-items-center">
									<!--
									<span class="normal-mode"><?=(empty($job)||empty($job["TUNJANGAN_JABATAN"]))?"":$job["TUNJANGAN_JABATAN"];?></span>
									-->
									<input type="text" class="w-100 form-control edit-mode text-right"  data-type="currency" name="TUNJANGAN_JABATAN"
										value="<?=(empty($job)||empty($job["TUNJANGAN_JABATAN"]))?"":$job["TUNJANGAN_JABATAN"];?>"
									>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="row mb-4">
			<div class="col">
				<div class="card">
					<div class="card-header">
						<h4 class="d-inline-block">Tunjangan Kinerja</h4>
						<button id="btn-simpan-tki" class="btn btn-primary btn-sm ml-auto float-right" type="button">Simpan</button>
					</div>
					<div class="card-body">
						<div class="row my-2">
							<div class="col">
								<div class="table-responsive">
									<table class="table">
										<thead>
											<tr>
												<th class="w-30">Kinerja</th>
												<th>Tunjangan</th>
											</tr>
										</thead>
										<tbody>
											<?php 
											$i = 0;
											foreach($tunjanganKinerja as $row){ 
												++$i;
												$this->view("data-gaji/detail-job-level/row-tki", array(
													"row"=>$row,
													"i"=>$i
												));
											} 
											?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row mb-4">
			<div class="col">
				<div class="card">
					<div class="card-header">
						<h4 class="d-inline-block">Upah Lembur</h4>
						<button id="btn-simpan-ul" class="btn btn-primary btn-sm ml-auto float-right" type="button">Simpan</button>
					</div>
					<div class="card-body">
						<div class="row my-2">
							<div class="col">
								<div class="table-responsive">
									<table class="table">
										<thead>
											<tr>
												<th class="w-30">Jam</th>
												<th>Upah Lembur</th>
											</tr>
										</thead>
										<tbody>
											<?php 
											$i = 0;
											foreach($upahLembur as $row){ 
												++$i;
												$this->view("data-gaji/detail-job-level/row-ul", array(
													"row"=>$row,
													"i"=>$i
												));
											} 
											?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-6 float-right">
		<div class="card">
			<div class="card-header">
				<h4 class="d-inline-block">Tunjangan Masa Kerja</h4>
				<button id="btn-simpan-tmk" class="btn btn-primary btn-sm float-right mx-1" type="button">Simpan</button>
				<!--
				<button class="btn btn-primary btn-sm float-right mx-1" type="button">Tambah</button>
				-->
			</div>
			<div class="card-body">
				<div class="row my-2">
					<div class="col">
						<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<th class="w-10">Batas Bawah (Tahun)</th>
										<th class="">Tunjangan Masa Kerja</th>
										<!--
										<th class="shrink no-export">Aksi</th>
										-->
									</tr>
								</thead>
								<tbody>
									<?php 
									$i = 0;
									foreach($tunjanganMasaKerja as $row){ 
										++$i;
										$this->view("data-gaji/detail-job-level/row-tmk", array(
											"row"=>$row,
											"i"=>$i
										));
									} 
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>