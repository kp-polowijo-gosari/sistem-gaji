
<tr class="">
	<td class="shrink text-right"><?=empty($row)?"1":$i;?></td>
	<td>
		<input class="id-tkh" type="hidden" name="ID_TKH" valtype="input"
			value="<?=(empty($row) || empty($row["ID_TKH"]))?"":$row["ID_TKH"];?>"
		>
		<span class="normal-mode nama-tkh " name="NAMA_TKH" valtype="text" valtarget=".nama-tkh-field" copytext=".nama-tkh"><?=(empty($row) || empty($row["NAMA_TKH"]))?"":$row["NAMA_TKH"];?></span>
		<input type="text" class="w-100 d-none edit-mode nama-tkh-field form-control" valtype="input" valtarget=".nama-tkh"  name="NAMA_TKH"
			value="<?=(empty($row) || empty($row["NAMA_TKH"]))?"":$row["NAMA_TKH"];?>"
		>
	</td>
	<td class="text-right">
		<span class="normal-mode nilai-tkh" name="NILAI_TKH" data-type="currency" valtype="text" valtarget=".nilai-tkh-field" copytext=".nilai-tkh"><?=(empty($row) || empty($row["NILAI_TKH"]))?"":toRupiah($row["NILAI_TKH"]);?></span>
		<input type="text" class="w-100 edit-mode d-none nilai-tkh-field form-control" data-type="currency" valtype="input" valtarget=".nilai-tkh" name="NILAI_TKH"
			value="<?=(empty($row) || empty($row["NILAI_TKH"]))?"":toRupiah($row["NILAI_TKH"]);?>"
		>
	</td>
	<td class="text-right">
		<span class="jumlah-jabatan" name="JUMLAH_JABATAN" valtype="text"><?=(empty($row)||empty($row["JUMLAH_JABATAN"]))?"0":$row['JUMLAH_JABATAN'];?></span>
	</td>
	<td class="text-right">
		<span class="jumlah-kar" name="JUMLAH_KAR" valtype="text"><?=(empty($row)||empty($row["JUMLAH_KAR"]))?"0":$row['JUMLAH_KAR'];?></span>
	</td>
	<td class="shrink invalidated-mode">
		<button class="btn btn-primary btn-sm mx-1 btn-ubah btn-ubah-tkh normal-mode" type="button">Ubah</button>
		<button class="btn btn-primary btn-sm mx-1 btn-simpan btn-simpan-tkh edit-mode d-none" type="button">Simpan</button>
		<button class="btn btn-danger btn-sm mx-1 btn-batal-ubah btn-batal-tkh edit-mode d-none" type="button">Batal</button>
		<button class="btn btn-danger btn-sm mx-1 btn-hapus btn-hapus-tkh normal-mode" type="button">Hapus</button>
	</td>
</tr>