<tr class="jabatan-row data-row row-dihapus" data-id="<?=empty($row)?"":$row['ID_JABATAN'];?>">
	<td class="shrink text-right row-num"><?=empty($row)?"1":$i;?></td>
	<td class="nama-jabatan"><?=empty($row)?"":$row['NAMA_JABATAN'];?></td>
	<td class="nama-tkh id-tkh" data-id="<?=empty($row)?"":$row['ID_JOB'];?>">
		<?=empty($row)?"":$row['NAMA_JOB'];?>
	</td>
	<td class="text-right">
		<span class="jumlah-tkh">
			<a href="<?=base_url();?>gaji/tunjangan-khusus<?=empty($row)?"":"?jabatan={$row['NAMA_JABATAN']}";?>">
				<?=empty($row)?"0":$row['JUMLAH_TKH'];?>
			</a>
		</span>
	</td>
	<td class="text-right">
		<span class="jumlah-kar">
			<a class="jumlah-kar" href="<?=base_url();?>karyawan/karyawan<?=empty($row)?"":"?jabatan={$row['NAMA_JABATAN']}";?>">
				<?=empty($row)?"0":$row['JUMLAH_KAR'];?>
			</a>
		</span>
	</td>
	<td class="shrink">
		<a class="btn btn-primary btn-sm mx-1" href="<?=base_url("gaji/tunjangan-khusus/jabatan/{$row['ID_JABATAN']}");?>">Tunjangan</a>
	</td>
</tr>