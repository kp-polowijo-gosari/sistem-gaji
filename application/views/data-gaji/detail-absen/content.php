
<div class="row">
	<div class="col">
		<div class="d-sm-flex justify-content-between align-items-center mb-4">
			<h3 class="text-dark mb-0">Absen Harian</h3>
		</div>
		<div class="d-sm-flex align-items-start mb-4">
			<button class="btn btn-primary mx-1 invalidated-mode" type="button" data-toggle="modal" data-target="#modal-add-absen">Tambah</button>
			<!--
			<button class="btn btn-primary mx-1 invalidated-mode" type="button">Impor</button>
			-->
			<div class="dropdown d-inline-block">
				<button class="btn btn-primary dropdown-toggle mx-1" data-toggle="dropdown" aria-expanded="false" type="button">Ekspor</button>
				<div class="dropdown-menu" role="menu" id="tableActions"></div>
			</div>
			<div class="inline-block">
				<button class="btn btn-success mx-1 btn-validasi invalidated-mode" type="button">Validasi</button>
				<button class="btn btn-danger mx-1 btn-batal-validasi validated-mode d-none" type="button">Batalkan Validasi</button>
			</div>
		</div>
	</div>
	<div class="col-3 mb-3">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">Muhammad Rizqi Nur</h4>
				<h6 class="text-muted card-subtitle">Manager R&amp;D</h6>
			</div>
		</div>
	</div>
</div>
<div class="row mb-4">
	<div class="col">
		<div class="card">
			<div class="card-body">
				<div class="row my-2">
					<div class="col">
						<div class="table-responsive datatable export">
							<table class="table">
								<thead>
									<tr>
										<th class="shrink">No</th>
										<th>Tanggal</th>
										<th>Keterangan</th>
										<th>Jam</th>
										<th class="shrink invalidated-mode">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr class="absen-row">
										<td class="shrink text-right">1</td>
										<td class="shrink"><span class="tanggal normal-mode">2020-01-20</span><input class="tanggal-field edit-mode d-none form-control" type="date"></td>
										<td><span class="keterangan normal-mode">Gtw</span><input type="text" class="keterangan-field edit-mode d-none w-100 form-control"></td>
										<td class="text-right shrink"><span class="jam normal-mode">4</span><input type="number" class="jam-field d-none edit-mode small form-control"></td>
										<td class="shrink invalidated-mode"><button class="btn btn-primary btn-sm mx-1 normal-mode btn-ubah-absen" type="button">Ubah</button><button class="btn btn-primary btn-sm mx-1 edit-mode btn-simpan-absen d-none" type="button">Simpan</button>
											<button
												class="btn btn-danger btn-sm mx-1 normal-mode btn-hapus-absen" type="button">Hapus</button>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>