<div class="d-sm-flex justify-content-between align-items-center mb-4 px-4">
	<h3 class="text-dark mb-0">Beranda</h3>
</div>
<div class="row" id="accordionBeranda">
	<?php if (in_array($_SESSION["role"], array(ROLE_ADMIN_BIASA, ROLE_ADMIN_UTAMA, ROLE_SUPER_ADMIN))){ ?>
	<div class="col-3 my-2 px-4">
		<div class="clickable">
			<div class="row bg-light border min-h-100" href="#collapse-data-karyawan" data-toggle="collapse">
				<div class="col-6 bg-blue square"><img class="center" src="<?=base_url();?>assets/img/icon/002-employee.png"></div>
				<div class="col-6 square bg-white"><span class="center text-center">DATA KARYAWAN</span></div>
			</div>
		</div>
		<div id="collapse-data-karyawan" class="border my-2 collapse" data-parent="#accordionBeranda">
			<div class="row">
				<div class="col border bg-white px-0">
					<ul class="nav flex-column no-decoration">
						<li class="nav-item"><a class="nav-link active" href="<?=base_url();?>karyawan/departemen">Departemen</a></li>
						<li class="nav-item"><a class="nav-link" href="<?=base_url();?>karyawan/job-level">Job Level</a></li>
						<li class="nav-item"><a class="nav-link" href="<?=base_url();?>karyawan/jabatan">Jabatan</a></li>
						<li class="nav-item"><a class="nav-link" href="<?=base_url();?>karyawan/karyawan">Karyawan</a></li>
						<li class="nav-item"><a class="nav-link" href="<?=base_url();?>karyawan/shift">Shift</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="col-3 my-2 px-4">
		<div class="clickable">
			<div class="row bg-light border min-h-100" href="#collapse-data-gaji" data-toggle="collapse">
				<div class="col-6 bg-blue square"><img class="center" src="<?=base_url();?>assets/img/icon/003-salary.png"></div>
				<div class="col-6 square bg-white"><span class="center text-center">DATA GAJI</span></div>
			</div>
		</div>
		<div id="collapse-data-gaji" class="my-2 w-100 collapse">
			<div class="row">
				<div class="col border bg-white px-0">
					<ul class="nav flex-column no-decoration">
						<?php if (in_array($_SESSION["role"], array(ROLE_ADMIN_UTAMA, ROLE_SUPER_ADMIN))){ ?>
						<li class="nav-item"><a class="nav-link active" href="<?=base_url();?>gaji/job-level">Job Level</a></li>
						<li class="nav-item"><a class="nav-link" href="<?=base_url();?>gaji/tunjangan-khusus">Tunjangan Khusus</a></li>
						<li class="nav-item"><a class="nav-link" href="<?=base_url();?>gaji/karyawan">Karyawan</a></li>
						<?php } ?>
						<li class="nav-item"><a class="nav-link" href="<?=base_url();?>gaji/lembur">Lembur</a></li>
						<?php if (in_array($_SESSION["role"], array(ROLE_ADMIN_UTAMA, ROLE_SUPER_ADMIN))){ ?>
						<li class="nav-item"><a class="nav-link" href="<?=base_url();?>gaji/absen">Absen</a></li>
						<?php } ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>
	<?php if (in_array($_SESSION["role"], array(ROLE_ADMIN_UTAMA, ROLE_SUPER_ADMIN))){ ?>
	<div class="col-3 my-2 px-4">
		<div class="clickable">
			<div class="row bg-light border" href="#collapse-angsuran" data-toggle="collapse">
				<div class="col-6 bg-blue square"><img class="center" src="<?=base_url();?>assets/img/icon/004-compensation.png"></div>
				<div class="col-6 square bg-white"><span class="center text-center">ANGSURAN</span></div>
			</div>
			<div id="collapse-angsuran" class="border bg-light my-2 collapse">
				<div class="row">
					<div class="col border bg-white px-0">
						<ul class="nav flex-column no-decoration">
							<li class="nav-item"><a class="nav-link active" href="<?=base_url(); ?>angsuran">Angsuran</a></li>
							<li class="nav-item"><a class="nav-link" href="<?=base_url(); ?>angsuran/potongan">Potongan Periode Ini</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>
	<?php if (in_array($_SESSION["role"], array(ROLE_PENGAWAS, ROLE_ADMIN_BIASA, ROLE_ADMIN_UTAMA, ROLE_SUPER_ADMIN))){ ?>
	<div class="col-3 my-2 px-4">
		<div class="clickable">
			<div class="row bg-light border min-h-100" href="#collapse-data-laporan" data-toggle="collapse">
				<div class="col-6 bg-blue square"><img class="center" src="<?=base_url();?>assets/img/icon/005-report.png"></div>
				<div class="col-6 square bg-white"><span class="center">LAPORAN</span></div>
			</div>
		</div>
		<div id="collapse-data-laporan" class="border bg-light my-2 collapse">
			<div class="row">
				<div class="col border bg-white px-0">
					<ul class="nav flex-column no-decoration">
						<?php if (in_array($_SESSION["role"], array(ROLE_ADMIN_UTAMA, ROLE_SUPER_ADMIN))){ ?>
						<li class="nav-item"><a class="nav-link active" href="<?=base_url();?>laporan/slip-gaji">Slip Gaji</a></li>
						<li class="nav-item"><a class="nav-link" href="<?=base_url();?>laporan/gaji">Laporan Gaji<br></a></li>
						<?php } ?>
						<li class="nav-item"><a class="nav-link" href="<?=base_url();?>laporan/lembur">Laporan Lembur</a></li>
						<?php if (in_array($_SESSION["role"], array(ROLE_ADMIN_UTAMA, ROLE_SUPER_ADMIN))){ ?>
						<li class="nav-item"><a class="nav-link" href="<?=base_url();?>laporan/absen">Laporan Absen</a></li>
						<li class="nav-item"><a class="nav-link" href="<?=base_url();?>laporan/angsuran">Laporan Angsuran</a></li>
						<li class="nav-item"><a class="nav-link" href="<?=base_url();?>laporan/rekap-gaji">Rekap Gaji</a></li>
						<?php } ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>
	<?php if (in_array($_SESSION["role"], array(ROLE_ADMIN_BIASA, ROLE_ADMIN_UTAMA, ROLE_SUPER_ADMIN))){ ?>
	<div class="col-3 my-2 px-4 no-decoration">
		<a class="no-decoration" href="<?=base_url();?>pengaturan">
			<div class="clickable">
				<div class="row bg-light border">
					<div class="col bg-blue square"><img class="center" src="<?=base_url();?>assets/img/icon/001-cog.png"></div>
					<div class="col square bg-white"><span class="center">PENGATURAN</span></div>
				</div>
			</div>
		</a>
	</div>
	<?php } ?>
	<?php if (in_array($_SESSION["role"], array(ROLE_ADMIN_AKUN, ROLE_SUPER_ADMIN))){ ?>
	<div class="col-3 my-2 px-4 no-decoration">
		<a class="no-decoration" href="<?=base_url();?>akun">
			<div>
				<div class="row bg-light border">
					<div class="col-6 bg-blue square"><img class="center" src="<?=base_url();?>assets/img/icon/002-employee.png"></div>
					<div class="col-6 flex-shrink-0 square bg-white"><span class="center">AKUN</span></div>
				</div>
			</div>
		</a>
	</div>
	<?php } ?>
	<div class="col-3 my-2 px-4 no-decoration">
		<a class="clickable btn-logout"  href="<?=base_url();?>logout">
			<div>
				<div class="row bg-light border">
					<div class="col-6 bg-blue square"><img class="center" src="<?=base_url();?>assets/img/icon/logout%20(1).png"></div>
					<div class="col-6 square bg-white"><span class="center">LOGOUT</span></div>
				</div>
			</div>
		</a>
	</div>
</div>