<div class="row">
	<div class="col">
		<h3>Profil</h3>
	</div>
</div>
<div class="row">
	<div class="col">
		<div class="card shadow">
			<div class="card-body">
				<div class="row">
					<div class="col-5 text-center border-right"><img src="assets/img/gambar/Businessman-80_icon-icons.com_57362.png" style="max-height: 250px; max width: 250px;"></div>
					<div class="col">
						<form class="d-flex flex-column h-100">
							<div class="form-row my-1">
								<div class="col-3"><label class="col-form-label">Nama</label></div>
								<div class="col"><input class="form-control" type="text" value="Nama User" disabled=""></div>
							</div>
							<div class="form-row my-1">
								<div class="col-3"><label class="col-form-label">Email</label></div>
								<div class="col"><input class="form-control" type="email" value="email@gmail.com" disabled=""></div>
							</div>
							<div class="form-row my-1">
								<div class="col-3"><label class="col-form-label">Peran</label></div>
								<div class="col"><span class="form-control">Admin Biasa</span></div>
							</div>
							<div id="password-collapse" class="collapse">
								<div class="form-row my-1">
									<div class="col-3"><label class="col-form-label">Password Lama</label></div>
									<div class="col"><input class="form-control" type="password" placeholder="****"></div>
								</div>
								<div class="form-row my-1">
									<div class="col-3"><label class="col-form-label">Password Baru</label></div>
									<div class="col"><input class="form-control" type="password" placeholder="****"><small class="form-text text-muted m-auto font-italic">8-16 karakter</small></div>
								</div>
								<div class="form-row my-1">
									<div class="col-3"><label class="col-form-label">Konfirmasi Password</label></div>
									<div class="col"><input class="form-control" type="password" placeholder="****"><small class="form-text text-muted m-auto font-italic">Isi ulang password baru anda</small></div>
								</div>
							</div>
							<div class="flex-grow-1 flex-fill"></div>
							<div class="d-flex justify-content-end align-items-end my-4"><button class="btn btn-primary mx-1 btn-sm btn-ubah-password" type="button" data-toggle="collapse" data-target="#password-collapse" data-show=".edit-password-mode" data-hide=".btn-ubah-password">Ubah Password</button>
								<button
									class="btn btn-light mx-1 btn-sm edit-password-mode d-none" type="button" data-toggle="collapse" data-target="#password-collapse" data-show=".btn-ubah-password" data-hide=".edit-password-mode">Batal</button><button class="btn btn-primary mx-1 btn-sm btn-simpan-password edit-password-mode d-none" type="button">Simpan</button></div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>