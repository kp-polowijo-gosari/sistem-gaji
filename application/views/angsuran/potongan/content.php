
<div class="d-sm-flex justify-content-between align-items-center mb-4">
	<h3 class="text-dark mb-0">Potongan Angsuran Bulan Ini</h3>
	<div></div>
</div>
<div class="mb-4">
	<button class="btn btn-primary mx-1 invalidated-mode" type="button">Simpan</button>
	<button class="btn btn-success mx-1 btn-validasi invalidated-mode" type="button">Validasi</button>
	<button class="btn btn-danger mx-1 btn-batal-validasi validated-mode d-none" type="button">Batalkan Validasi</button>
</div>
<div class="row mb-4">
	<div class="col">
		<div class="card">
			<div class="card-body">
				<div class="row my-2">
					<div class="col">
						<div class="table-responsive datatable auto-number">
							<table class="table" id="table-aktif">
								<thead>
									<tr>
										<th class="shrink">No</th>
										<th>NIK</th>
										<th>Nama</th>
										<th>Jenis</th>
										<th>Keterangan</th>
										<th class="shrink">Sisa</th>
										<th>Potongan</th>
										<th class="shrink invalidated-mode"><input type="checkbox"></th>
									</tr>
								</thead>
								<tbody>
									<tr class="data-gaji-karyawan-row align-middle">
										<td class="shrink">1</td>
										<td class="shrink">23432124</td>
										<td>Anugrah Putra Eko Wibowo</td>
										<td class="shrink">Koperasi</td>
										<td>Motor</td>
										<td class="shrink"><span class="gaji-dilaporkan normal-mode">Rp 5.000.000</span><input type="number" class="gaji-dilaporkan-field d-none edit-mode form-control" value="5000000"></td>
										<td class="text-right"><input type="number" class="form-control invalidated-mode" value="500000"><span class="text-right shrink validated-mode d-none">Rp 500.000</span></td>
										<td class="shrink invalidated-mode"><input type="checkbox"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>