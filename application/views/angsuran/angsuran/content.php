
<div class="d-sm-flex justify-content-between align-items-center mb-4">
	<h3 class="text-dark mb-0">Kelola Angsuran</h3>
</div>
<div class="d-sm-flex align-items-start mb-4">
	<button class="btn btn-primary mx-1 text-light invalidated-mode" id="btn-tambah-angsuran" type="button" data-toggle="modal" data-target="modal-tambah-angsuran">Tambah</button>
	<!--
	<button class="btn btn-primary invalidated-mode" type="button">Impor</button>
	-->
	<div class="dropdown d-inline-block">
		<button class="btn btn-primary dropdown-toggle mx-1" data-toggle="dropdown" aria-expanded="false" type="button">Ekspor</button>
		<div class="dropdown-menu" role="menu" id="tableActions"></div>
	</div>
	<div class="inline-block">
		<button class="btn btn-success mx-1 btn-validasi invalidated-mode" type="button">Validasi</button>
		<button class="btn btn-danger mx-1 btn-batal-validasi validated-mode d-none" type="button">Batalkan Validasi</button>
	</div>
</div>
<div class="row mb-4">
	<div class="col">
		<div class="card">
			<div class="card-body">
				<div class="row my-2">
					<div class="col">
						<div class="table-responsive datatable export auto-number">
							<table class="table table-hover" id="table-aktif">
								<thead class="text-center">
									<tr>
										<th class="shrink">No</th>
										<th>NIK</th>
										<th>Nama</th>
										<th>Tanggal</th>
										<th>Keterangan</th>
										<th>Jenis</th>
										<th>Nilai</th>
										<th class="shrink">Sisa Angsuran</th>
										<th class="invalidated-mode">Aksi</th>
									</tr>
								</thead>
								<tbody class="text-center">
									<tr class="angsuran-row">
										<td>1</td>
										<td class="shrink nik">23432124</td>
										<td><span class="nama normal-mode">Anugrah Putra Eko Wibowo</span><button class="btn btn-primary btn-block nama-field edit-mode d-none" type="button">Anugrah Putra Eko Wibowo</button></td>
										<td><span class="tanggal normal-mode">2020-01-20</span><input class="tanggal-field edit-mode d-none form-control" type="date"></td>
										<td><span class="keterangan normal-mode">Sepeda Motor</span><input type="text" class="form-control keterangan-field d-none edit-mode"></td>
										<td><span class="jenis normal-mode">Koperasi</span><select class="edit-mode form-control jenis-field d-none"><optgroup label="This is a group"><option value="12" selected="">This is item 1</option><option value="13">This is item 2</option><option value="14">This is item 3</option></optgroup></select></td>
										<td
											class="shrink"><span class="nilai normal-mode">Rp 5.000.000</span><input type="number" class="form-control edit-mode nilai-field d-none"></td>
											<td class="shrink nik">Rp. 1.000.000</td>
											<td class="text-right shrink invalidated-mode"><button class="btn btn-primary btn-sm mx-1 btn-ubah-angsuran normal-mode" type="button" data-toggle="modal" data-target="#modal-tambah-angsuran">Ubah</button><button class="btn btn-primary btn-sm mx-1 btn-simpan-angsuran edit-mode d-none"
													type="button">Simpan</button><button class="btn btn-danger btn-sm mx-1 btn-hapus-angsuran normal-mode" type="button">Hapus</button></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>