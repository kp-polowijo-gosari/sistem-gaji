
<div class="row">
	<div class="col">
		<h3>Profil</h3>
	</div>
</div>
<div class="row mb-4">
	<div class="col">
		<div class="card shadow">
			<div class="card-body">
				<div class="row">
					<div class="col-5 text-center border-right"><img src="<?=base_url();?>assets/img/gambar/Businessman-80_icon-icons.com_57362.png" style="max-height: 250px; max width: 250px;"></div>
					<div class="col">
						<form class="d-flex flex-column h-100">
							<div class="form-row">
								<div class="col-3"><label class="col-form-label">Nama</label></div>
								<div class="col"><input class="form-control" type="text" value="Nama User" disabled=""></div>
							</div>
							<div class="form-row">
								<div class="col-3"><label class="col-form-label">Email</label></div>
								<div class="col"><input class="form-control" type="email" value="email@gmail.com" disabled=""></div>
							</div>
							<div class="form-row">
								<div class="col-3"><label class="col-form-label">Peran</label></div>
								<div class="col"><select class="form-control"><option value="12" selected="">Pengawas</option><option value="13">Admin Biasa</option><option value="14">Admin Akun</option><option value="">Admin Utama</option><option value="">Super Admin</option></select></div>
							</div>
							<div class="d-flex flex-column flex-grow-1 flex-fill"></div>
							<div class="d-flex justify-content-end align-items-end my-4"><button class="btn btn-danger mx-1 btn-sm btn-reset-password" type="button">Reset Password</button><button class="btn btn-primary mx-1 btn-sm btn-simpan-akun" type="button">Simpan</button></div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>