<div class="row">
	<div class="col d-sm-flex justify-content-between align-items-center mb-4">
		<h3 class="text-dark mb-0">Akun</h3>
	</div>
</div>
<div class="row">
	<div class="col d-sm-flex align-items-start mb-4">
		<a class="btn btn-primary mx-1 text-light" role="button" href="#" data-toggle="modal" data-target="#modal-add-akun">Tambah</a>
	</div>
</div>
<div class="row mb-4">
	<div class="col">
		<div class="card">
			<div class="card-body">
				<div class="row my-2">
					<div class="col">
						<div class="table-responsive datatable">
							<table class="table">
								<thead>
									<tr>
										<th class="shrink">No</th>
										<th>Username</th>
										<th>Nama</th>
										<th>Email</th>
										<th>Peran</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="shrink">1</td>
										<td class="shrink">23432124</td>
										<td>Anugrah Putra Eko Wibowo</td>
										<td>anu@gmail.com</td>
										<td>Admin Utama</td>
										<td class="shrink">
											<a class="btn btn-primary btn-sm mx-1" role="button" href="<?=base_url();?>akun/1">Detail</a>
											<button class="btn btn-danger btn-sm mx-1 btn-hapus-akun" type="button" data-toggle="modal" data-target="#modal-add-department">Hapus</button>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>