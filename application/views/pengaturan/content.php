
<div class="row mb-4">
	<div class="col d-sm-flex justify-content-between align-items-center">
		<h3 class="text-dark mb-0">Pengaturan</h3>
		<button class="btn btn-primary float-right" type="button">Simpan</button>
	</div>
</div>
<div class="row">
	<div class="col-md-6 col-xl-3 mb-4">
		<div class="card shadow border-left-primary py-2">
			<div class="card-body">
				<div class="row align-items-center no-gutters">
					<div class="col mr-2">
						<div class="text-uppercase text-primary font-weight-bold text-xs mb-1"><span>BPJS Ketenagakerjaan&nbsp;<br>(Beban Perusahaan, %)</span></div>
						<div class="text-dark font-weight-bold h5 mb-0"><input type="text" class="w-100 text-right form-control" value="6.89"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-xl-3 mb-4">
		<div class="card shadow border-left-primary py-2">
			<div class="card-body">
				<div class="row align-items-center no-gutters">
					<div class="col mr-2">
						<div class="text-uppercase text-primary font-weight-bold text-xs mb-1"><span>BPJS Ketenagakerjaan<br>(Beban Karyawan, %)<br></span></div>
						<div class="text-dark font-weight-bold h5 mb-0"><input type="text" class="w-100 text-right form-control" value="3"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-xl-3 mb-4">
		<div class="card shadow border-left-primary py-2">
			<div class="card-body">
				<div class="row align-items-center no-gutters">
					<div class="col mr-2">
						<div class="text-uppercase text-primary font-weight-bold text-xs mb-1"><span>BPJS Kesehatan<br>(Beban Perusahaan, %)<br></span></div>
						<div class="text-dark font-weight-bold h5 mb-0"><input type="text" class="w-100 text-right form-control" value="4"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-xl-3 mb-4">
		<div class="card shadow border-left-primary py-2">
			<div class="card-body">
				<div class="row align-items-center no-gutters">
					<div class="col mr-2">
						<div class="text-uppercase text-primary font-weight-bold text-xs mb-1"><span>BPJS Kesehatan<br>(Beban Karyawan, %)<br></span></div>
						<div class="text-dark font-weight-bold h5 mb-0"><input type="text" class="w-100 text-right form-control" value="1"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-xl-3 mb-4">
		<div class="card shadow border-left-primary py-2">
			<div class="card-body">
				<div class="row align-items-center no-gutters">
					<div class="col mr-2">
						<div class="text-uppercase text-primary font-weight-bold text-xs mb-1"><span>Upah Minimum Kota</span></div>
						<div class="text-dark font-weight-bold h5 mb-0"><input type="number" class="w-100 text-right form-control" value="4197031"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-xl-3 mb-4">
		<div class="card shadow border-left-primary py-2">
			<div class="card-body">
				<div class="row align-items-center no-gutters">
					<div class="col mr-2">
						<div class="text-uppercase text-primary font-weight-bold text-xs mb-1"><span>Iuran Perumahan</span></div>
						<div class="text-dark font-weight-bold h5 mb-0"><input type="number" class="w-100 text-right form-control" value="20000"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-xl-3 mb-4">
		<div class="card shadow border-left-primary py-2">
			<div class="card-body">
				<div class="row align-items-center no-gutters">
					<div class="col mr-2">
						<div class="text-uppercase text-primary font-weight-bold text-xs mb-1"><span>Iuran Koperasi</span></div>
						<div class="text-dark font-weight-bold h5 mb-0"><input type="number" class="w-100 text-right form-control" value="25000"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-xl-3 mb-4">
		<div class="card shadow border-left-primary py-2">
			<div class="card-body">
				<div class="row align-items-center no-gutters">
					<div class="col mr-2">
						<div class="text-uppercase text-primary font-weight-bold text-xs mb-1"><span>Pendaftaran Koperasi (+Iuran)</span></div>
						<div class="text-dark font-weight-bold h5 mb-0"><input type="number" class="w-100 text-right form-control" value="275000"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-xl-3 mb-4">
		<div class="card shadow border-left-primary py-2">
			<div class="card-body">
				<div class="row align-items-center no-gutters">
					<div class="col mr-2">
						<div class="text-uppercase text-primary font-weight-bold text-xs mb-1"><span>Uang Makan Lembur</span></div>
						<div class="text-dark font-weight-bold h5 mb-0"><input type="number" class="w-100 text-right form-control" value="12000"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-xl-3 mb-4">
		<div class="card shadow border-left-primary py-2">
			<div class="card-body">
				<div class="row align-items-center no-gutters">
					<div class="col mr-2">
						<div class="text-uppercase text-primary font-weight-bold text-xs mb-1"><span>Uang Transport Lembur</span></div>
						<div class="text-dark font-weight-bold h5 mb-0"><input type="number" class="w-100 text-right form-control" value="4000"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>