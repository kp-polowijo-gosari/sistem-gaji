<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Sistem Gaji</title>
	<?php $this->view("common-css"); ?>
</head>

<body>
    <div class="bg w-100 h-100 p-absolute"></div>
    <div class="w-100 h-100 p-absolute">
        <form class="bg-light p-5 m-auto center bg-light box-shadow" id="form-login" method="POST">
            <h2 class="text-center mb-4">Sistem Gaji<br>PT Polowijo Gosari</h2>
            <div class="col"><label>Username</label><input class="form-control p-2" type="text" name="username"></div>
            <div class="col mb-3"><label class="mt-3">Password</label><input class="form-control" type="password" name="password"></div>
            <div class="col text-center"><button type="submit" class="btn btn-primary mt-4 w-100 bg-blue btn-login" role="button" id="btn-login" >Login</button></div>
        </form>
    </div>
	<?php $this->view("common-modal"); ?>
	<?php $this->view("common-js"); ?>
    <script src="<?=base_url();?>assets/js/functions-login.js"></script>
</body>

</html>