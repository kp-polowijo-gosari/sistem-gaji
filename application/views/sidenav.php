<nav class="navbar navbar-dark align-items-start sidebar sidebar-dark accordion p-0" id="sidenav">
	<div class="container-fluid d-flex flex-column p-0">
		<ul class="nav navbar-nav text-light" id="accordionSidebar">
			<li class="nav-item" role="presentation">
				<a class="nav-link active" href="<?=base_url();?>beranda"><span>Beranda</span></a>
			</li>
			<?php if (in_array($_SESSION["role"], array(ROLE_ADMIN_BIASA, ROLE_ADMIN_UTAMA, ROLE_SUPER_ADMIN))){ ?>
			<li class="nav-item" role="presentation">
				<a class="nav-link active" href="#collapse-1" data-toggle="collapse"><span>Data Karyawan</span></a>
			</li>
			<li class="nav-item collapse" role="presentation" id="collapse-1" data-parent="#accordionSidebar">
				<a class="nav-link" href="<?=base_url();?>karyawan/departemen" data-toggle=""><span>Departemen</span></a>
				<a class="nav-link" href="<?=base_url();?>karyawan/job-level" data-toggle=""><span>Job Level</span></a>
				<a class="nav-link" href="<?=base_url();?>karyawan/jabatan" data-toggle=""><span>Jabatan</span></a>
				<a class="nav-link" href="<?=base_url();?>karyawan/karyawan" data-toggle=""><span>Karyawan</span></a>
				<a class="nav-link" href="<?=base_url();?>karyawan/shift" data-toggle=""><span>Shift</span></a>
			</li>
			<li class="nav-item" role="presentation">
				<a class="nav-link active" href="#collapse-2" data-toggle="collapse"><span>Data Gaji</span></a>
			</li>
			<li class="nav-item collapse" role="presentation" id="collapse-2" data-parent="#accordionSidebar">
				<?php if (in_array($_SESSION["role"], array(ROLE_ADMIN_UTAMA, ROLE_SUPER_ADMIN))){ ?>
				<a class="nav-link" href="<?=base_url();?>gaji/job-level" data-toggle=""><span>Job Level</span></a>
				<a class="nav-link" href="<?=base_url();?>gaji/tunjangan-khusus" data-toggle=""><span>Tunjangan Khusus</span></a>
				<a class="nav-link" href="<?=base_url();?>gaji/karyawan" data-toggle=""><span>Karyawan</span></a>
				<?php } ?>
				<a class="nav-link" href="<?=base_url();?>gaji/lembur" data-toggle=""><span>Lembur</span></a>
				<?php if (in_array($_SESSION["role"], array(ROLE_ADMIN_UTAMA, ROLE_SUPER_ADMIN))){ ?>
				<a class="nav-link" href="<?=base_url();?>gaji/absen" data-toggle=""><span>Absen</span></a>
				<?php } ?>
			</li>
			<?php } ?>
			<?php if (in_array($_SESSION["role"], array(ROLE_ADMIN_UTAMA, ROLE_SUPER_ADMIN))){ ?>
			<li class="nav-item" role="presentation">
				<a class="nav-link active" href="#collapse-4" data-toggle="collapse"><span>Angsuran</span></a>
			</li>
			<li class="nav-item collapse" role="presentation" id="collapse-4" data-parent="#accordionSidebar">
				<a class="nav-link" href="<?=base_url();?>angsuran" data-toggle=""><span>Angsuran</span></a>
				<a class="nav-link" href="<?=base_url();?>angsuran/potongan" data-toggle=""><span>Potongan Periode Ini</span></a>
			</li>
			<?php } ?>
			<?php if (in_array($_SESSION["role"], array(ROLE_PENGAWAS, ROLE_ADMIN_BIASA, ROLE_ADMIN_UTAMA, ROLE_SUPER_ADMIN))){ ?>
			<li class="nav-item" role="presentation">
				<a class="nav-link active" href="#collapse-3" data-toggle="collapse"><span>Laporan</span></a>
			</li>
			<li class="nav-item collapse" role="presentation" id="collapse-3" data-parent="#accordionSidebar">
				<?php if (in_array($_SESSION["role"], array(ROLE_ADMIN_UTAMA, ROLE_SUPER_ADMIN))){ ?>
				<a class="nav-link" href="<?=base_url();?>laporan/slip-gaji" data-toggle=""><span>Slip Gaji</span></a>
				<a class="nav-link" href="<?=base_url();?>laporan/gaji" data-toggle=""><span>Laporan Gaji</span></a>
				<?php } ?>
				<a class="nav-link" href="<?=base_url();?>laporan/lembur" data-toggle=""><span>Laporan Lembur</span></a>
				<?php if (in_array($_SESSION["role"], array(ROLE_ADMIN_UTAMA, ROLE_SUPER_ADMIN))){ ?>
				<a class="nav-link" href="<?=base_url();?>laporan/absen" data-toggle=""><span>Laporan Absen</span></a>
				<a class="nav-link" href="<?=base_url();?>laporan/angsuran" data-toggle=""><span>Laporan Angsuran</span></a>
				<a class="nav-link" href="<?=base_url();?>laporan/rekap-gaji" data-toggle=""><span>Rekap Gaji</span></a>
				<?php } ?>
			</li>
			<?php } ?>
			<?php if (in_array($_SESSION["role"], array(ROLE_ADMIN_BIASA, ROLE_ADMIN_UTAMA, ROLE_SUPER_ADMIN))){ ?>
			<li class="nav-item" role="presentation">
				<a class="nav-link active" href="<?=base_url();?>pengaturan"><span>Pengaturan</span></a>
			</li>
			<?php } ?>
			<?php if (in_array($_SESSION["role"], array(ROLE_ADMIN_AKUN, ROLE_SUPER_ADMIN))){ ?>
			<li class="nav-item" role="presentation">
				<a class="nav-link active" href="<?=base_url();?>akun"><span>Akun</span></a>
			</li>
			<?php } ?>
		</ul>
	</div>
</nav>
<div id="sidenav-overlay"></div>