<nav class="navbar navbar-light navbar-expand bg-white shadow mb-4 topbar static-top" id="topnav">
	<div class="container-fluid">
		<button class="btn btn-link d-md-none rounded-circle mr-3" id="sidebar-toggle" type="button"><i class="fas fa-bars"></i></button>
		<li class="dropdown no-decoration navbar-brand clickable h-100">
			<a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Sistem Gaji <?=$perTerpilih;?></a>
			<div class="dropdown-menu" role="menu">
				<?php foreach ($perAktif as $row){ ?>
				<a class="dropdown-item" role="presentation" href="#"><?=$row['NAMA_PER']?></a>
				<?php } ?>
			</div>
		</li>
		<ul class="nav navbar-nav flex-nowrap ml-auto">
			<?php if (in_array($_SESSION["role"], array(ROLE_PENGAWAS, ROLE_ADMIN_BIASA, ROLE_ADMIN_UTAMA, ROLE_SUPER_ADMIN))){ ?>
			<li class="nav-item d-md-flex align-items-md-center align-middle py-auto" role="presentation">
				<div class="input-group">
					<div class="input-group-prepend"><span class="input-group-text">Periode</span></div>
					<input class="my-auto form-control" type="month" value="<?=substr($periode['PERIODE'],0,-3);?>">
				</div>
			</li>
			<?php } ?>
			<?php if (in_array($_SESSION["role"], array(ROLE_ADMIN_BIASA, ROLE_ADMIN_UTAMA, ROLE_SUPER_ADMIN))){ ?>
			<li class="nav-item dropdown no-arrow mx-1" role="presentation">
				<li class="nav-item dropdown no-arrow">
					<a class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false" href="#">
						<span class="badge badge-danger badge-counter">3+</span><i class="fas fa-bell fa-fw"></i>
					</a>
					<div class="dropdown-menu dropdown-menu-right dropdown-list dropdown-menu-right animated--grow-in" role="menu">
						<h6 class="dropdown-header bg-blue">TODO</h6>
						<a class="d-flex align-items-center dropdown-item" href="<?=base_url();?>data-gaji/karyawan">
							<div class="w-100">
								<div class="row align-items-center no-gutters">
									<div class="col mr-2">
										<h6>Data Gaji Karyawan</h6>
									</div>
									<div class="col-auto">
										<div class="custom-control custom-checkbox">
											<input class="custom-control-input" type="checkbox" id="formCheck-1" disabled="" checked="">
											<label class="custom-control-label" for="formCheck-1"></label>
										</div>
									</div>
								</div>
							</div>
						</a>
						<a class="d-flex align-items-center dropdown-item" href="<?=base_url();?>data-gaji/lembur">
							<div class="w-100">
								<div class="row align-items-center no-gutters">
									<div class="col mr-2">
										<h6>Data Lembur</h6>
									</div>
									<div class="col-auto">
										<div class="custom-control custom-checkbox">
											<input class="custom-control-input" type="checkbox" id="formCheck-1" disabled="">
											<label class="custom-control-label" for="formCheck-1"></label>
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>
				</li>
			</li>
			<?php } ?>
			<li class="nav-item py-3" role="presentation">
				<div class="topbar-divider h-100 box-sizing-border"></div>
			</li>
			<li class="nav-item dropdown no-arrow" role="presentation">
				<li class="nav-item dropdown no-arrow">
					<a class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false" href="#">
						<span class="d-none d-lg-inline mr-2 text-gray-600 small"><?=$_SESSION["nama"];?></span>
						<img class="border rounded-circle img-profile" src="<?=base_url();?>assets/img/gambar/Businessman-80_icon-icons.com_57362.png">
					</a>
					<div class="dropdown-menu shadow dropdown-menu-right animated--grow-in" role="menu">
						<a class="dropdown-item" role="presentation" href="<?=base_url();?>profil">
							<i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>&nbsp;Profile
						</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item btn-logout" role="presentation" href="<?=base_url();?>logout">
							<i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>&nbsp;Logout
						</a>
					</div>
				</li>
			</li>
		</ul>
	</div>
</nav>