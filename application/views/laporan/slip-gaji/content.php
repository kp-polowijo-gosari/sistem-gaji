
<div class="d-sm-flex justify-content-between align-items-center mb-4">
	<h3 class="text-dark mb-0">Slip Gaji</h3>
</div>
<div class="d-sm-flex align-items-start mb-4">
	<ul class="nav nav-pills mx-2">
		<li class="nav-item mx-1"><a class="nav-link active btn btn-primary btn-sm" data-toggle="collapse" href="#karyawan-mode-card">Karyawan</a></li>
		<li class="nav-item mx-1"><a class="nav-link btn btn-primary btn-sm" href="#dept-mode-card" data-toggle="collapse">Departemen</a></li>
		<li class="nav-item mx-1"><a class="nav-link btn btn-primary btn-sm" href="#jabatan-mode-card" data-toggle="collapse">Jabatan</a></li>
		<li class="nav-item mx-1"><a class="nav-link btn btn-primary btn-sm" href="#job-mode-card" data-toggle="collapse">Job Level</a></li>
	</ul>
</div>
<div class="row mb-4  d-flex flex-grow-1 flex-shrink-0 p-relative">
	<div class="col  d-flex flex-column flex-grow-1 flex-shrink-0 p-relative" id="mode-accordion">
		<div class="card collapse" id="karyawan-mode-card" data-parent="#mode-accordion">
			<div class="card-body">
				<div class="row">
					<div class="col">
						<h4>Pilih Karyawan</h4>
					</div>
				</div>
				<div class="row my-2">
					<div class="col">
						<div class="table-responsive table-hover datatable auto-number">
							<table class="table">
								<thead>
									<tr>
										<th class="shrink">No</th>
										<th>NIK</th>
										<th>Nama</th>
										<th>Jabatan</th>
										<th>Departemen</th>
										<th class="shrink"><input type="checkbox"></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="shrink text-right">1</td>
										<td class="shrink">H7621</td>
										<td>Muhammad Rizqi Nur</td>
										<td>Manager R&amp;D</td>
										<td>Research and Development</td>
										<td class="shrink text-right"><input type="checkbox"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="row my-2">
					<div class="col"><button class="btn btn-primary btn-sm" type="button" data-toggle="collapse" data-target="#content-holder">Tampilkan</button></div>
				</div>
			</div>
		</div>
		<div class="card collapse" id="dept-mode-card" data-parent="#mode-accordion">
			<div class="card-body">
				<div class="row">
					<div class="col">
						<h4>Pilih Departemen</h4>
					</div>
				</div>
				<div class="row my-2">
					<div class="col">
						<div class="table-responsive table-hover datatable auto-number">
							<table class="table">
								<thead>
									<tr>
										<th class="shrink">No</th>
										<th>Departemen</th>
										<th>Karyawan</th>
										<th class="shrink"><input type="checkbox"></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="shrink">1</td>
										<td>Research and Development</td>
										<td class="shrink">14</td>
										<td class="shrink text-right"><input type="checkbox"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<hr>
				<div class="row my-2">
					<div class="col"><button class="btn btn-primary btn-sm" type="button" data-toggle="collapse" data-target="#content-holder">Tampilkan</button></div>
				</div>
			</div>
		</div>
		<div class="card collapse" id="jabatan-mode-card" data-parent="#mode-accordion">
			<div class="card-body">
				<div class="row">
					<div class="col">
						<h4>Pilih Jabatan</h4>
					</div>
				</div>
				<div class="row my-2">
					<div class="col">
						<div class="table-responsive table-hover datatable auto-number">
							<table class="table">
								<thead>
									<tr>
										<th class="shrink">No</th>
										<th>Jabatan</th>
										<th>Job Level</th>
										<th>Karyawan</th>
										<th class="shrink"><input type="checkbox"></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="shrink">1</td>
										<td>Manager R&amp;D</td>
										<td>Manager</td>
										<td class="shrink">14</td>
										<td class="shrink text-right"><input type="checkbox"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<hr>
				<div class="row my-2">
					<div class="col"><button class="btn btn-primary btn-sm" type="button" data-toggle="collapse" data-target="#content-holder">Tampilkan</button></div>
				</div>
			</div>
		</div>
		<div class="card collapse" id="job-mode-card" data-parent="#mode-accordion">
			<div class="card-body">
				<div class="row">
					<div class="col">
						<h4>Pilih Job Level</h4>
					</div>
				</div>
				<div class="row my-2">
					<div class="col">
						<div class="table-responsive table-hover datatable auto-number">
							<table class="table">
								<thead>
									<tr>
										<th class="shrink">No</th>
										<th>Job Level</th>
										<th>Karyawan</th>
										<th class="shrink"><input type="checkbox"></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="shrink text-right"><input type="checkbox"></td>
										<td class="shrink">1</td>
										<td>Manager</td>
										<td class="shrink">14</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<hr>
				<div class="row my-2">
					<div class="col"><button class="btn btn-primary btn-sm" type="button" data-toggle="collapse" data-target="#content-holder">Tampilkan</button></div>
				</div>
			</div>
		</div>
		<div class=" collapse  flex-column flex-grow-1 flex-shrink-0 p-relative" id="content-holder" data-parent="#mode-accordion">
			<div class="row mb-4">
				<div class="col">
					<div><button class="btn btn-primary mx-1 btn-sm" type="button">Kirim</button>
						<div class="dropdown d-inline-block mx-1"><button class="btn btn-primary dropdown-toggle btn-sm" data-toggle="dropdown" aria-expanded="false" type="button">Ekspor</button>
							<div class="dropdown-menu" role="menu"><a class="dropdown-item" role="presentation" href="#">Cetak</a><a class="dropdown-item" role="presentation" href="#">PDF</a><a class="dropdown-item" role="presentation" href="#">Excel</a><a class="dropdown-item"
									role="presentation" href="#">CSV</a></div>
						</div>
					</div>
				</div>
			</div>
			<!-- TODO: Fix iframe not expanding and not scrollable -->
			<div class="row d-flex flex-column flex-grow-1 flex-shrink-0 p-relative mb-4">
				<div class="col d-flex flex-column flex-grow-1 flex-shrink-0 p-relative" id="mode-accordion">
					<div class="card d-flex flex-column flex-grow-1 flex-shrink-0">
						<div class="card-body d-flex flex-column flex-grow-1 flex-shrink-0"><iframe class="iresize"  src="<?=base_url();?>laporan/slip-gaji/cetak" frameborder="0" allowfullscreen data-iframe-width="100%"></iframe></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>