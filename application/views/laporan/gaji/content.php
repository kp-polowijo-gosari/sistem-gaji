
<div class="row">
	<div class="col d-sm-flex justify-content-between align-items-center mb-4">
		<h3 class="text-dark mb-0">Laporan Gaji</h3>
	</div>
</div>
<div class="row my-2 mb-4">
	<div class="col">
		<div class="dropdown d-inline-block">
			<button class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false" type="button">Ekspor</button>
			<div class="dropdown-menu" role="menu" id="tableActions"></div>
		</div>
	</div>
</div>
<div class="row mb-4">
	<div class="col" id="mode-accordion">
		<div id="content-holder" class="collapse show" data-parent="#mode-accordion">
			<ul class="nav nav-tabs">
				<li class="nav-item"><a class="nav-link active tab-gaji" id="tab-gaji-utama" href="#" data-tab="utama">Utama</a></li>
				<li class="nav-item"><a class="nav-link tab-gaji" id="tab-gaji-gaji" href="#" data-tab="gaji">Gaji</a></li>
				<li class="nav-item"><a class="nav-link tab-gaji" id="tab-gaji-potongan" href="#" data-tab="potongan">Potongan</a></li>
			</ul>
			<div class="card tab-card">
				<div class="card-body">
					<div class="row my-2">
						<div class="col">
							<div class="table-responsive table-hover datatable table-striped export auto-number" id="laporan-gaji">
								<table class="table">
									<thead>
										<tr>
											<th class="shrink">No</th>
											<th class="utama-mode">NIK</th>
											<th>Nama</th>
											<th class="utama-mode">Departemen</th>
											<th>Jabatan</th>
											<th class="utama-mode gaji-mode">Job Level</th>
											<th class="gaji-mode">Gaji Pokok</th>
											<th class="gaji-mode">Tunjangan Jabatan</th>
											<th class="gaji-mode">Tunjangan Khusus</th>
											<th class="gaji-mode">Tunjangan Kinerja</th>
											<th class="gaji-mode">BPJS Kesehatan</th>
											<th class="gaji-mode">BPJS Ketenagakerjaan</th>
											<th class="gaji-mode">Lembur</th>
											<th class="gaji-mode utama-mode">Total Gaji</th>
											<th class="potongan-mode">BPJS Kesehatan</th>
											<th class="potongan-mode">BPJS Ketenagakerjaan</th>
											<th class="potongan-mode">Potongan Koperasi</th>
											<th class="potongan-mode">Iuran Perumahan</th>
											<th class="potongan-mode">Angsuran Bank</th>
											<th class="potongan-mode">PPh21 &amp; Absen</th>
											<th class="potongan-mode utama-mode">Total Potongan</th>
											<th class="utama-mode">Lain-Lain</th>
											<th class="utama-mode">Total Diterima</th>
										</tr>
									</thead>
									<tbody>
										<tr class="gaji-row">
											<td class="shrink">1</td>
											<td class="utama-mode">H78907890</td>
											<td>Muhammad Rizqi Nur</td>
											<td class="utama-mode">Research and Development</td>
											<td>Manager R&amp;D</td>
											<td class="utama-mode gaji-mode">Manager</td>
											<td class="shrink gaji-mode text-right">Rp 4.000.000</td>
											<td class="shrink gaji-mode text-right">Rp 4.000.000</td>
											<td class="shrink gaji-mode text-right">Rp 4.000.000</td>
											<td class="shrink gaji-mode text-right">Rp 4.000.000</td>
											<td class="shrink gaji-mode text-right">Rp 4.000.000</td>
											<td class="shrink gaji-mode text-right">Rp 4.000.000</td>
											<td class="shrink gaji-mode text-right">Rp 4.000.000</td>
											<td class="shrink gaji-mode utama-mode text-right">Rp 4.000.000</td>
											<td class="shrink potongan-mode text-right">Rp 1.000.000</td>
											<td class="shrink potongan-mode text-right">Rp 1.000.000</td>
											<td class="shrink potongan-mode text-right">Rp 1.000.000</td>
											<td class="shrink potongan-mode text-right">Rp 1.000.000</td>
											<td class="shrink potongan-mode text-right">Rp 1.000.000</td>
											<td class="shrink potongan-mode text-right">Rp 1.000.000</td>
											<td class="shrink potongan-mode utama-mode text-right">Rp 1.000.000</td>
											<td class="shrink utama-mode text-right gaji-lain clickable" data-toggle="modal" data-target="#modal-gaji-lain">Rp 1.000.000</td>
											<td class="shrink utama-mode text-right">Rp 14.000.000</td>
										</tr>
									</tbody>
									<tfoot>
										<tr class="font-weight-bold">
											<td class="potongan-mode gaji-mode utama-mode"></td>
											<td class="text-center">Total</td>
											<td class="utama-mode"></td>
											<td class="utama-mode"></td>
											<td></td>
											<td class="utama-mode gaji-mode"></td>
											<td class="shrink text-right gaji-mode">Rp.4.000.000</td>
											<td class="shrink text-right gaji-mode">Rp.4.000.000</td>
											<td class="shrink text-right gaji-mode">Rp.4.000.000</td>
											<td class="shrink text-right gaji-mode">Rp.4.000.000</td>
											<td class="shrink text-right gaji-mode">Rp.4.000.000</td>
											<td class="shrink text-right gaji-mode">Rp.4.000.000</td>
											<td class="shrink text-right gaji-mode">Rp.4.000.000</td>
											<td class="shrink text-right gaji-mode utama-mode">Rp.4.000.000</td>
											<td class="shrink text-right potongan-mode">Rp.1.000.000</td>
											<td class="shrink text-right potongan-mode">Rp.1.000.000</td>
											<td class="shrink text-right potongan-mode">Rp.1.000.000</td>
											<td class="shrink text-right potongan-mode">Rp.1.000.000</td>
											<td class="shrink text-right potongan-mode">Rp.1.000.000</td>
											<td class="shrink text-right potongan-mode">Rp.1.000.000</td>
											<td class="shrink text-right potongan-mode utama-mode">Rp.1.000.000</td>
											<td class="shrink text-right utama-mode">Rp.1.000.000</td>
											<td class="shrink text-right utama-mode">Rp.14.000.000</td>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>