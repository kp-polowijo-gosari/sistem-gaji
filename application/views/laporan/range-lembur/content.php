
<div class="row">
	<div class="col d-sm-flex justify-content-between align-items-center mb-4">
		<h3 class="text-dark mb-0">Laporan Lembur Range</h3>
	</div>
</div>
<div class="row my-2 mb-4">
	<div class="col">
		<div class="dropdown d-inline-block"><button class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false" type="button">Ekspor</button>
			<div class="dropdown-menu" role="menu" id="tableActions"></div>
		</div>
	</div>
	<div class="col">
		<div class="d-xl-flex flex-row align-items-xl-center">
			<div class="input-group mx-1">
				<div class="input-group-prepend"><span class="input-group-text">Awal</span></div><input class="my-auto form-control" type="month" value="2020-01"></div>
			<div class="input-group mx-1">
				<div class="input-group-prepend"><span class="input-group-text">Akhir</span></div><input class="my-auto form-control" type="month" value="2020-01"></div>
		</div>
	</div>
</div>
<div class="row mb-4">
	<div class="col">
		<div class="card">
			<div class="card-body">
				<div class="row my-2">
					<div class="col">
						<div class="table-responsive table-hover table-striped datatable export auto-number">
							<table class="table">
								<thead>
									<tr>
										<th class="shrink">No</th>
										<th>NIK</th>
										<th>Nama</th>
										<th>Jabatan</th>
										<th>Departemen</th>
										<th class="text-center">Jam</th>
										<th class="shrink">Januari</th>
										<th class="shrink">Jam</th>
										<th class="shrink">Februari</th>
										<th class="shrink">Jam</th>
										<th class="shrink">Maret</th>
									</tr>
								</thead>
								<tbody>
									<tr class="clickable" data-href="<?=base_url();?>laporan/lembur/detail">
										<td class="shrink text-right">1</td>
										<td class="shrink">H76211234</td>
										<td>Muhammad Rizqi Nur</td>
										<td>Manager R&amp;D</td>
										<td>Research and Development</td>
										<td class="text-right shrink">4</td>
										<td class="text-right shrink">Rp 4.000.000</td>
										<td class="text-right shrink">4</td>
										<td class="text-right shrink">Rp 4.000.000</td>
										<td class="text-right shrink">4</td>
										<td class="text-right shrink">Rp 4.000.000</td>
									</tr>
								</tbody>
								<tfoot>
									<tr class="font-weight-bold">
										<td></td>
										<td class="text-center" colspan="4">Total</td>
										<td class="text-right shrink">4</td>
										<td class="text-right shrink">Rp 4.000.000</td>
										<td class="text-right shrink">4</td>
										<td class="text-right shrink">Rp 4.000.000</td>
										<td class="text-right shrink">4</td>
										<td class="text-right shrink">Rp 4.000.000</td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>