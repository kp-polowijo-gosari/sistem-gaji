<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Cetak Slip Gaji</title>
    
    <link rel="stylesheet" href="<?=base_url();?>assets/css/paper.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/css/Styles.css">
</head>
<body class="A5 landscape">
    <section class="sheet padding-10mm">
        <div class="font-weight-bold text-dark">PT. POLOWIJO GOSARI</div>
        <div class="font-weight-bold text-dark mb-3">BUKTI PENERIMAAN GAJI</div>

        <table class="custom-font-size">
                <tr>
                    <td class="w-10">
                        <span>No. Slip</span>
                    </td>
                    <td class="w-2">
                        <span>:</span>
                    </td>
                    <td class="w-33 text-right">
                        <span>32</span>
                    </td>
                    <td class="w-5"></td>
                    <td class="w-15">
                        <span>Bulan</span>
                    </td>
                    <td class="w-2">:</td>
                    <td class="w-33">
                        <span>Desember 2019</span>
                    </td>
                </tr>
        </table>
        <hr>
        <table class="custom-font-size">
                <tr> 
                    <td class="w-10">
                        <span>NIK</span>
                    </td>
                    <td class="w-2">
                        <span>:</span>
                    </td>
                    <td class="w-33 text-right"> <!--Row Kolom 2-->
                        <span>32</span>
                    </td>
                    <td class="w-5"></td>
                    <td class="w-15">
                        <span>Jabatan</span>
                    </td>                
                    <td class="w-2">
                        <span>:</span>
                    </td>
                    <td class="w-33">
                        <span> Manager</span>
                    </td>
                </tr>
                <tr> <!--Row 1-->
                    <td class="w-10"> <!--Row Kolom 1-->
                        <span>Nama</span>
                    </td>
                    <td class="w-2">
                        <span>:</span>
                    </td>
                    <td class="w-33"> <!--Row Kolom 2-->
                        <span>Anugrah Eko Putra Wibowo</span>
                    </td>
                    <td class="w-5"></td>
                    <td class="w-15">
                        <span>Job Level</span>
                    </td>
                    <td  class="w-2">
                        <span>:</span>
                    </td>
                    <td class="w-33">
                        <span> 0</span>
                    </td>
                </tr>
        </table>
        <hr>
        <table class="custom-font-size">
            <!--Row 1-->
            <tr>
                <td class="w-15">
                    <span>Gaji Pokok</span>
                </td>
                <td class="w-2">
                    <span>:</span>
                </td>
                <td class="w-3">
                    <span>Rp.</span>
                </td>
                <td class="w-15 text-right">
                    <span>1.000.000</span>
                </td>
                <!--Pemisah tengah-->
                <td class="w-5"></td>
                <!--pemisah tengah-->
                <td class="w-35">
                    <span>Pot. BPJS Ketenagakerjaan (6.89%+3%)</span>
                </td>
                <td class="w-2">
                    <span>:</span>
                </td>
                <td class="w-3">
                    <span>Rp.</span>
                </td>
                <td class="w-15 text-right">
                    <span>-</span>
                </td>
            </tr>
            <!--Row 2-->
            <tr>
                <td class="w-15">
                    <span>Tunj. Jabatan</span>
                </td>
                <td class="w-2">
                    <span>:</span>
                </td>
                <td class="w-3">
                    <span>Rp.</span>
                </td>
                <td class="w-15 text-right">
                    <span>-</span>
                </td>
                <!--Pemisah tengah-->
                <td class="w-5"></td>
                <!--pemisah tengah-->
                <td class="w-35">
                    <span>Pot. Anggs. + Iuran Kop</span>
                </td>
                <td class="w-2">
                    <span>:</span>
                </td>
                <td class="w-3">
                    <span>Rp.</span>
                </td>
                <td class="w-15 text-right">
                    <span>-</span>
                </td>
            </tr>
            <!--Row 3-->
            <tr>
                <td class="w-15">
                    <span>Tunj. Khusus</span>
                </td>
                <td class="w-2">
                    <span>:</span>
                </td>
                <td class="w-3">
                    <span>Rp.</span>
                </td>
                <td class="w-15 text-right">
                    <span>-</span>
                </td>
                <!--Pemisah tengah-->
                <td class="w-5"></td>
                <!--pemisah tengah-->
                <td class="w-35">
                    <span>Pot. Perumahan</span>
                </td>
                <td class="w-2">
                    <span>:</span>
                </td>
                <td class="w-3">
                    <span>Rp.</span>
                </td>
                <td class="w-15 text-right">
                    <span>-</span>
                </td>
            </tr>
            <!--Row 4-->
            <tr>
                <td class="w-15">
                    <span>Bantuan Kehadiran</span>
                </td>
                <td class="w-2">
                    <span>:</span>
                </td>
                <td class="w-3">
                    <span>Rp.</span>
                </td>
                <td class="w-15 text-right">
                    <span>-</span>
                </td>
                <!--Pemisah tengah-->
                <td class="w-5"></td>
                <!--pemisah tengah-->
                <td class="w-35">
                    <span>Pot. Angs. Toko/Bukopin/BTN/Sejahtera</span>
                </td>
                <td class="w-2">
                    <span>:</span>
                </td>
                <td class="w-3">
                    <span>Rp.</span>
                </td>
                <td class="w-15 text-right">
                    <span>-</span>
                </td>
            </tr>
            <!--Row 5-->
            <tr>
                <td class="w-15">
                    <span>Penyesuaian</span>
                </td>
                <td class="w-2">
                    <span>:</span>
                </td>
                <td class="w-3">
                    <span>Rp.</span>
                </td>
                <td class="w-15 text-right">
                    <span>-</span>
                </td>
                <!--Pemisah tengah-->
                <td class="w-5"></td>
                <!--pemisah tengah-->
                <td class="w-35">
                    <span>Pot. Absen/PPH21</span>
                </td>
                <td class="w-2">
                    <span>:</span>
                </td>
                <td class="w-3">
                    <span>Rp.</span>
                </td>
                <td class="w-15 text-right">
                    <span>-</span>
                </td>
            </tr>
            <!--Row 6-->
            <tr>
                <td class="w-15">
                    <span>BPJS Ket. Kerja</span>
                </td>
                <td class="w-2">
                    <span>:</span>
                </td>
                <td class="w-3">
                    <span>Rp.</span>
                </td>
                <td class="w-15 text-right">
                    <span>-</span>
                </td>
                <!--Pemisah tengah-->
                <td class="w-5"></td>
                <!--pemisah tengah-->
                <td class="w-35">
                    <span>Pot. BPJS Kesehatan (4%+1%)</span>
                </td>
                <td class="w-2">
                    <span>:</span>
                </td>
                <td class="w-3 custom-border">
                    <span>Rp.</span>
                </td>
                <td class="w-15 text-right custom-border">
                    <span>-</span>
                </td>
            </tr>
            <!--Row 7-->
            <tr>
                <td class="w-15">
                    <span>BPJS Kesehatan (4%)</span>
                </td>
                <td class="w-2">
                    <span>:</span>
                </td>
                <td class="w-3">
                    <span>Rp.</span>
                </td>
                <td class="w-15 text-right">
                    <span>-</span>
                </td>
                <!--Pemisah tengah-->
                <td class="w-5"></td>
                <!--pemisah tengah-->
                <td class="w-35">
                    <span>Jumlah Potongan</span>
                </td>
                <td class="w-2">
                    <span>:</span>
                </td>
                <td class="w-3">
                    <span>Rp.</span>
                </td>
                <td class="w-15 text-right">
                    <span>-</span>
                </td>
            </tr>
            <!--Row 8-->
            <tr>
                <td class="w-15">
                    <span>Lembur/Insentif</span>
                </td>
                <td class="w-2">
                    <span>:</span>
                </td>
                <td class="w-3 custom-border">
                    <span>Rp.</span>
                </td>
                <td class="w-15 text-right custom-border">
                    <span>-</span></td>
                <!--Pemisah tengah-->
                <td class="w-5"></td>
                <!--pemisah tengah-->
                <td class="w-35"></td>
                <td class="w-2"></td>
                <td class="w-3"></td>
                <td class="w-15"></td>
            </tr>
            <!--Row 9-->
            <tr class="font-weight-bold">
                <td class="w-15">
                    <span>Jumlah</span>
                </td>
                <td class="w-2">
                    <span>:</span>
                </td>
                <td class="w-3">
                    <span>Rp.</span>
                </td>
                <td class="w-15 text-right">
                    <span>1.000.000</span>
                </td>
                <!--Pemisah tengah-->
                <td class="w-5"></td>
                <!--pemisah tengah-->
                <td class="w-35">
                    <span>Diterima</span>
                </td>
                <td class="w-2">
                    <span>:</span>
                </td>
                <td class="w-3">
                    <span>Rp.</span>
                </td>
                <td class="w-15 text-right custom-double-border">
                    <span>1.000.000</span>
                </td>
            </tr>
        </table>
        
    </section>
</body>
</html>