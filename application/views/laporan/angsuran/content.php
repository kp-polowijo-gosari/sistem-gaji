
<div class="row">
	<div class="col d-sm-flex justify-content-between align-items-center mb-4">
		<h3 class="text-dark mb-0">Laporan Angsuran</h3>
	</div>
</div>
<div class="row mb-4">
	<div class="col">
		<div class="dropdown d-inline-block">
			<button class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false" type="button">Ekspor</button>
			<div class="dropdown-menu" role="menu" id="tableActions"></div>
		</div>
	</div>
</div>
<div class="row mb-4">
	<div class="col">
		<div class="card">
			<div class="card-body">
				<div class="row my-2">
					<div class="col">
						<div class="table-responsive datatable export auto-number">
							<table class="table" id="table-aktif">
								<thead>
									<tr>
										<th class="shrink">No</th>
										<th>NIK</th>
										<th>Nama</th>
										<th>Jenis</th>
										<th>Keterangan</th>
										<th class="shrink">Sisa Awal</th>
										<th>Dibayar</th>
										<th>Sisa Akhir</th>
									</tr>
								</thead>
								<tbody>
									<tr class="data-gaji-karyawan-row align-middle">
										<td class="shrink">1</td>
										<td class="shrink">23432124</td>
										<td>Anugrah Putra Eko Wibowo</td>
										<td class="shrink">Bank BTN</td>
										<td>Beli Sepeda Motor</td>
										<td class="shrink">Rp 5.000.000</td>
										<td class="shrink">Rp 500.000</td>
										<td class="shrink">Rp 4.500.000</td>
									</tr>
								</tbody>
								<tfoot>
									<tr class="font-weight-bold">
										<td></td>
										<td class="text-center" colspan="4">Total</td>
										<td class="shrink">Rp 5.000.000</td>
										<td class="shrink">Rp 500.000</td>
										<td class="shrink">Rp 5.000.000</td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>