
<div class="row">
	<div class="col d-sm-flex justify-content-between align-items-center mb-4">
		<h3 class="text-dark mb-0">Rekap Gaji</h3>
	</div>
</div>
<div class="row my-2 mb-4">
	<div class="col">
		<div class="dropdown d-inline-block"><button class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false" type="button">Ekspor</button>
			<div class="dropdown-menu" role="menu" id="tableActions"></div>
		</div>
	</div>
</div>
<div class="row d-flex flex-column flex-grow-1 flex-shrink-0 mb-4">
	<div class="col d-flex flex-column flex-grow-1 flex-shrink-0 p-relative" id="mode-accordion">
		<div class="card d-flex flex-column flex-grow-1 flex-shrink-0">
			<div class="card-body d-flex flex-column flex-grow-1 flex-shrink-0"><iframe class="iresize"  src="<?=base_url();?>laporan/rekap-gaji/cetak" frameborder="0" allowfullscreen data-iframe-width="100%"></iframe></div>
		</div>
	</div>
</div>