
<div class="row">
	<div class="col d-sm-flex justify-content-between align-items-center mb-4">
		<h3 class="text-dark mb-0">Laporan Lembur Harian</h3>
	</div>
</div>
<div class="row my-2 mb-4">
	<div class="col">
		<div class="dropdown d-inline-block">
			<button class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false" type="button">Ekspor</button>
			<div class="dropdown-menu" role="menu" id="tableActions"></div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-9">
		<div class="card">
			<div class="card-body">
				<div class="row my-2">
					<div class="col">
						<div class="table-responsive table-hover table-striped datatable export">
							<table class="table">
								<thead>
									<tr>
										<th class="shrink">No</th>
										<th>Tanggal</th>
										<th>Keterangan</th>
										<th>Shift</th>
										<th class="text-center">Jam I</th>
										<th class="text-center">Jam II</th>
										<th class="text-center">Jam III</th>
										<th class="shrink">Upah Lembur</th>
										<th class="shrink">Uang Makan</th>
										<th class="shrink">Uang Transport</th>
										<th class="shrink">Total</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="shrink text-right">1</td>
										<td class="shrink">2020-01-20</td>
										<td>Banyak kerjaan</td>
										<td class="text-center shrink">I</td>
										<td class="text-right shrink">4</td>
										<td class="text-right shrink">4</td>
										<td class="text-right shrink">4</td>
										<td class="text-right shrink">Rp 4.000.000</td>
										<td class="text-right shrink">Rp 4.000.000</td>
										<td class="text-right shrink">Rp 4.000.000</td>
										<td class="text-right shrink">Rp 4.000.000</td>
									</tr>
								</tbody>
								<tfoot>
									<tr class="font-weight-bold">
										<td></td>
										<td class="text-center" colspan="3">Total</td>
										<td class="shrink text-right">4</td>
										<td class="shrink text-right">4</td>
										<td class="shrink text-right">4</td>
										<td class="shrink text-right">Rp.4.000.000</td>
										<td class="shrink text-right">Rp.4.000.000</td>
										<td class="shrink text-right">Rp.4.000.000</td>
										<td class="shrink text-right">Rp.4.000.000</td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-3">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">Muhammad Rizqi Nur</h4>
				<h6 class="text-muted card-subtitle mb-4">Manager R&amp;D</h6>
				<div class="table-responsive mt-3">
					<table class="table">
						<thead>
							<tr>
								<th>Jam</th>
								<th>Uang Lembur</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>I</td>
								<td>Rp 20.000</td>
							</tr>
							<tr>
								<td>II</td>
								<td>Rp 20.000</td>
							</tr>
							<tr>
								<td>III</td>
								<td>Rp 20.000</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>