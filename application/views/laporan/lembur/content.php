
<div class="row">
	<div class="col d-sm-flex justify-content-between align-items-center mb-4">
		<h3 class="text-dark mb-0">Laporan Lembur</h3>
	</div>
</div>
<div class="row my-2 mb-4">
	<div class="col">
		<div class="dropdown d-inline-block mx-1"><button class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false" type="button">Ekspor</button>
			<div class="dropdown-menu" role="menu" id="tableActions"></div>
		</div><a class="btn btn-primary mx-1" role="button" href="<?=base_url();?>laporan/lembur/full">Harian</a><a class="btn btn-primary mx-1" role="button" href="<?=base_url();?>laporan/lembur/range">Range</a></div>
</div>
<div class="row mb-4">
	<div class="col">
		<div class="card">
			<div class="card-body">
				<div class="row my-2">
					<div class="col">
						<div class="table-responsive table-hover table-striped datatable export auto-number">
							<table class="table">
								<thead>
									<tr>
										<th class="shrink">No</th>
										<th>NIK</th>
										<th>Nama</th>
										<th>Jabatan</th>
										<th>Departemen</th>
										<th class="text-center">Jam I</th>
										<th class="text-center">Jam II</th>
										<th class="text-center">Jam III</th>
										<th class="shrink">Upah Lembur</th>
										<th class="shrink">Uang Makan</th>
										<th class="shrink">Uang Transport</th>
										<th class="shrink">Total</th>
									</tr>
								</thead>
								<tbody>
									<tr class="clickable" data-href="<?=base_url();?>laporan/lembur/1">
										<td class="shrink text-right">1</td>
										<td class="shrink">H76211234</td>
										<td>Muhammad Rizqi Nur</td>
										<td>Manager R&amp;D</td>
										<td>Research and Development</td>
										<td class="text-right shrink">4</td>
										<td class="text-right shrink">4</td>
										<td class="text-right shrink">4</td>
										<td class="text-right shrink">Rp 4.000.000</td>
										<td class="text-right shrink">Rp 4.000.000</td>
										<td class="text-right shrink">Rp 4.000.000</td>
										<td class="text-right shrink">Rp 4.000.000</td>
									</tr>
								</tbody>
								<tfoot>
									<tr class="font-weight-bold">
										<td></td>
										<td class="text-center" colspan="4">Total</td>
										<td class="shrink text-right">4</td>
										<td class="shrink text-right">4</td>
										<td class="shrink text-right">4</td>
										<td class="shrink text-right">Rp 4.000.000</td>
										<td class="shrink text-right">Rp 4.000.000</td>
										<td class="shrink text-right">Rp 4.000.000</td>
										<td class="shrink text-right">Rp 4.000.000</td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>