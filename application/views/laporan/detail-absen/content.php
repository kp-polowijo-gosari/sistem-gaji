
<div class="row">
	<div class="col d-sm-flex justify-content-between align-items-center mb-4">
		<h3 class="text-dark mb-0">Laporan Absen Harian</h3>
	</div>
</div>
<div class="row my-2 mb-4">
	<div class="col">
		<div class="dropdown d-inline-block"><button class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false" type="button">Ekspor</button>
			<div class="dropdown-menu" role="menu" id="tableActions"></div>
		</div>
	</div>
</div>
<div class="row mb-4">
	<div class="col-9">
		<div class="card">
			<div class="card-body">
				<div class="row my-2">
					<div class="col">
						<div class="table-responsive table-hover table-striped datatable export">
							<table class="table">
								<thead>
									<tr>
										<th class="shrink">No</th>
										<th>Tanggal</th>
										<th>Keterangan</th>
										<th>Lama Absen</th>
										<th class="text-center">Potongan (%)</th>
										<th class="text-center">Potongan (Rp)</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="shrink text-right">1</td>
										<td class="shrink">2020-01-20</td>
										<td>Ketiduran</td>
										<td class="text-center shrink">00:30:00</td>
										<td class="text-right shrink">0.5</td>
										<td class="text-right shrink">Rp 4.000.000</td>
									</tr>
								</tbody>
								<tfoot>
									<tr class="font-weight-bold">
										<td></td>
										<td class="text-center" colspan="2">Total</td>
										<td class="text-center shrink">00:30:00</td>
										<td class="text-right shrink">0.5</td>
										<td class="text-right shrink">Rp.4.000.000</td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-3 flex-grow-1 flex-fill">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">Muhammad Rizqi Nur</h4>
				<h6 class="text-muted card-subtitle mb-4">Manager R&amp;D</h6>
				<div class="table-responsive mt-3 overflow-auto small-table">
					<table class="table">
						<thead>
							<tr>
								<th>Jam</th>
								<th>Potongan</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>0-1</td>
								<td>Rp 10.000</td>
							</tr>
							<tr>
								<td>1-2</td>
								<td>Rp 15.000</td>
							</tr>
							<tr>
								<td>2-3</td>
								<td>Rp 20.000</td>
							</tr>
							<tr>
								<td>3-4</td>
								<td>Rp 20.000</td>
							</tr>
							<tr>
								<td>4-5</td>
								<td>Rp 20.000</td>
							</tr>
							<tr>
								<td>5-6</td>
								<td>Rp 20.000</td>
							</tr>
							<tr>
								<td>6-7</td>
								<td>Rp 20.000</td>
							</tr>
							<tr>
								<td>7-8</td>
								<td>Rp 20.000</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>