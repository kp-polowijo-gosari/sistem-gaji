
<div class="row">
	<div class="col d-sm-flex justify-content-between align-items-center mb-4">
		<h3 class="text-dark mb-0">Laporan Absen Harian</h3>
	</div>
</div>
<div class="row my-2 mb-4">
	<div class="col">
		<div class="dropdown d-inline-block"><button class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false" type="button">Ekspor</button>
			<div class="dropdown-menu" role="menu" id="tableActions"></div>
		</div>
	</div>
</div>
<div class="row mb-4">
	<div class="col">
		<div class="card">
			<div class="card-body">
				<div class="row my-2">
					<div class="col">
						<div class="table-responsive table-hover table-striped datatable export">
							<table class="table">
								<thead>
									<tr>
										<th class="shrink">No</th>
										<th>Tanggal</th>
										<th>NIK</th>
										<th>Nama</th>
										<th>Keterangan</th>
										<th>Lama Absen</th>
										<th class="text-center">Potongan (%)</th>
										<th class="text-center">Potongan (Rp)</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="shrink text-right">1</td>
										<td class="shrink">2020-01-20</td>
										<td class="shrink">H76547654</td>
										<td class="shrink">Muhammad Rizqi Nur</td>
										<td>Ketiduran</td>
										<td class="text-center shrink">00:30:00</td>
										<td class="text-right shrink">0.5</td>
										<td class="text-right shrink">Rp 4.000.000</td>
									</tr>
								</tbody>
								<tfoot class="border-bottom">
									<tr class="font-weight-bold" colspan="4">
										<td></td>
										<td class="text-center" colspan="4">Total</td>
										<td class="text-center shrink">00:30:00</td>
										<td class="text-right shrink">0.5</td>
										<td class="shrink text-right">Rp 4.000.000</td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>