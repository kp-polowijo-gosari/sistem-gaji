
<div class="row">
	<div class="col d-sm-flex justify-content-between align-items-center mb-4">
		<h3 class="text-dark mb-0">Laporan Lembur Harian Full</h3>
	</div>
</div>
<div class="row my-2 mb-4">
	<div class="col">
		<div class="dropdown d-inline-block"><button class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false" type="button">Ekspor</button>
			<div class="dropdown-menu" role="menu" id="tableActions"></div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col">
		<div class="card">
			<div class="card-body">
				<div class="row my-2">
					<div class="col">
						<div class="table-responsive table-hover table-striped datatable export auto-number">
							<table class="table">
								<thead>
									<tr>
										<th class="shrink">No</th>
										<th>Tanggal</th>
										<th>NIK</th>
										<th>Nama</th>
										<th>Keterangan</th>
										<th>Shift</th>
										<th class="text-center">Jam I</th>
										<th class="text-center">Jam II</th>
										<th class="text-center">Jam III</th>
										<th>Upah Lembur</th>
										<th>Uang Makan</th>
										<th>Uang Transport</th>
										<th>Total</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="shrink text-right">1</td>
										<td class="shrink">2020-01-20</td>
										<td class="shrink">H78907890</td>
										<td>Muhammad Rizqi Nur</td>
										<td>Banyak Kerjaan</td>
										<td class="text-center shrink">I</td>
										<td class="text-right shrink">4</td>
										<td class="text-right shrink">4</td>
										<td class="text-right shrink">4</td>
										<td class="text-right shrink">Rp 4.000.000</td>
										<td class="text-right shrink">Rp 12.000</td>
										<td class="text-right shrink">Rp 4.000</td>
										<td class="text-right shrink">Rp 4.000.000</td>
									</tr>
								</tbody>
								<tfoot>
									<tr class="font-weight-bold">
										<td></td>
										<td class="text-center" colspan="5">Total</td>
										<td class="text-right shrink">4</td>
										<td class="text-right shrink">4</td>
										<td class="text-right shrink">4</td>
										<td class="text-right shrink">Rp 4.000.000</td>
										<td class="text-right shrink">Rp 12.000</td>
										<td class="text-right shrink">Rp 4.000</td>
										<td class="text-right shrink">Rp 4.000</td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>