<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Sistem Gaji</title>
    <?php $this->view("common-css");?>
</head>

<body class="A4">
    <section class="sheet padding-10mm">
        <header class="text-center font-weight-bold text-dark mb-3 text-dark"><span>REKAPITULASI GAJI KARYAWAN PT. POLOWIJO GOSARI BULAN DESEMBER 2019</span></header>
        <tr>
        <td>
            <div class="text-dark">
                <!--Table Rekap Gaji-->
                    <table class="rekap-table">
                        <thead>
                            <tr class="text-center">
                                <th class="shrink">NO.</th>
                                <th>GAJI KARYAWAN</th>
                                <th class="shrink">PRODUKSI</th>
                                <th class="shrink">SUPPORT ADM</th>
                                <th>SALES &amp; MARKETING</th>
                                <th>TOTAL GAJI DES 2019</th>
                                <th>TOTAL GAJI NOP 2019</th>
                                <th>SELISIH GAJI</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr >
                                <td class="shrink text-right">1</td>
                                <td>GAJI / THP</td>
                                <td class="text-right shrink">100.245.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.000.000</td>
                                <td class="text-right shrink">456.000</td>
                            </tr>
                            <tr >
                                <td class="shrink text-right">2</td>
                                <td>BPJS Ketanagakerjaan 4.89%</td>
                                <td class="text-right shrink">100.245.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.000.000</td>
                                <td class="text-right shrink">456.000</td>
                            </tr>
                            <tr >
                                <td class="shrink text-right">3</td>
                                <td>BPJS Kesehatan 4%</td>
                                <td class="text-right shrink">100.245.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.000.000</td>
                                <td class="text-right shrink">456.000</td>
                            </tr>
                            <tr >
                                <td class="shrink text-right">4</td>
                                <td>Lembur + Overtime</td>
                                <td class="text-right shrink">100.245.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.000.000</td>
                                <td class="text-right shrink">456.000</td>
                            </tr>
                            <tr >
                                <td class="shrink text-right">5</td>
                                <td>THR / Kompensasi Cuti Besar</td>
                                <td class="text-right shrink">100.245.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.000.000</td>
                                <td class="text-right shrink">456.000</td>
                            </tr>
                            <tr class="font-weight-bold" >
                                <td class="shrink text-center"></td>
                                <td class="text-center">Sub Total</td>
                                <td class="text-right shrink">100.245.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.000.000</td>
                                <td class="text-right shrink">456.000</td>
                            </tr>
                            <tr class="font-weight-bold" >
                                <td class="shrink text-center"></td>
                                <td class="text-center">Potongan Gaji</td>
                                <td class="text-right shrink"></td>
                                <td class="text-right shrink"></td>
                                <td class="text-right shrink"></td>
                                <td class="text-right shrink"></td>
                                <td class="text-right shrink"></td>
                                <td class="text-right shrink"></td>
                            </tr>
                            <tr >
                                <td class="shrink text-right">6</td>
                                <td>Pot. BPJS Ketenagakerjaan (4.89%+2%)</td>
                                <td class="text-right shrink">100.245.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.000.000</td>
                                <td class="text-right shrink">456.000</td>
                            </tr>
                            <tr >
                                <td class="shrink text-right">7</td>
                                <td>Pot. Angsuran Koperasi</td>
                                <td class="text-right shrink">100.245.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.000.000</td>
                                <td class="text-right shrink">456.000</td>
                            </tr>
                            <tr >
                                <td class="shrink text-right">8</td>
                                <td>Pot. Perumahan</td>
                                <td class="text-right shrink">100.245.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.000.000</td>
                                <td class="text-right shrink">456.000</td>
                            </tr>
                            <tr >
                                <td class="shrink text-right">9</td>
                                <td>Pot. Bank Bukopin/BTN/Sejahtera</td>
                                <td class="text-right shrink">100.245.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.000.000</td>
                                <td class="text-right shrink">456.000</td>
                            </tr>
                            <tr >
                                <td class="shrink text-right">10</td>
                                <td>Pot. PPH21 / ABSENSI</td>
                                <td class="text-right shrink">100.245.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.000.000</td>
                                <td class="text-right shrink">456.000</td>
                            </tr>
                            <tr >
                                <td class="shrink text-right">11</td>
                                <td>Pot. BPJS Kesehatan(4%+1%)</td>
                                <td class="text-right shrink">100.245.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.000.000</td>
                                <td class="text-right shrink">456.000</td>
                            </tr>
                            <tr class="font-weight-bold">
                                <td class="shrink text-right"></td>
                                <td class="text-center">Sub Total</td>
                                <td class="text-right shrink">100.245.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.000.000</td>
                                <td class="text-right shrink">456.000</td>
                            </tr>
                            <tr class="p-5">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr class="font-weight-bold">
                                <td></td>
                                <td class="text-center">TOTAL GAJI</td>
                                <td class="text-right shrink">100.245.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.000.000</td>
                                <td class="text-right shrink">456.000</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="mt-4 text-dark small-font-size">
                    <tr>
                        <td>
                            <div  class="float-left">Mengetahui,</div>
                        </td>
                        <td>
                            <span  class="float-right">Sekapuk,26 Desember 2019<br>
                            Yang Membuat,</span>
                        </td>
                    </tr>
                    <tr>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <td><div  class="float-left">Nama Owner</div>
                        </td>
                        <td><div  class="float-right mr-5">Nama Akun</div>
                        </td> 
                    </tr>
                </div>
        </td>
        </tr>
    </section>
        
    
        <!--Tabel Rekap 2-->
        <section class="sheet padding-10mm">
            <div class="text-dark">
                <header class="text-center font-weight-bold text-dark mb-3 text-dark"><span>REKAPITULASI GAJI KARYAWAN PT. POLOWIJO GOSARI BULAN DESEMBER 2019</span></header>
                <!--Table Rekap Gaji-->
                    <table class="mx-auto medium-font-size">
                        <thead>
                            <tr class="text-center">
                                <th class="shrink">NO.</th>
                                <th>GAJI KARYAWAN</th>
                                <th class="shrink">SUPPORT ADM</th>
                                <th>TOTAL GAJI DES 2019</th>
                                <th>TOTAL GAJI NOP 2019</th>
                                <th>SELISIH GAJI</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr >
                                <td class="shrink text-right">1</td>
                                <td>GAJI / THP</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.000.000</td>
                                <td class="text-right shrink">456.000</td>
                            </tr>
                            <tr >
                                <td class="shrink text-right">2</td>
                                <td>BPJS Ketanagakerjaan 4.89%</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.000.000</td>
                                <td class="text-right shrink">456.000</td>
                            </tr>
                            <tr >
                                <td class="shrink text-right">3</td>
                                <td>BPJS Kesehatan 4%</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.000.000</td>
                                <td class="text-right shrink">456.000</td>
                            </tr>
                            <tr >
                                <td class="shrink text-right">4</td>
                                <td>Lembur + Overtime</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.000.000</td>
                                <td class="text-right shrink">456.000</td>
                            </tr>
                            <tr >
                                <td class="shrink text-right">5</td>
                                <td>THR / Kompensasi Cuti Besar</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.000.000</td>
                                <td class="text-right shrink">456.000</td>
                            </tr>
                            <tr class="font-weight-bold" >
                                <td class="shrink text-center"></td>
                                <td class="text-center">Sub Total</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.000.000</td>
                                <td class="text-right shrink">456.000</td>
                            </tr>
                            <tr class="font-weight-bold" >
                                <td class="shrink text-center"></td>
                                <td class="text-center">Potongan Gaji</td>
                                <td class="text-right shrink"></td>
                                <td class="text-right shrink"></td>
                                <td class="text-right shrink"></td>
                                <td class="text-right shrink"></td>
                            </tr>
                            <tr >
                                <td class="shrink text-right">6</td>
                                <td>Pot. BPJS Ketenagakerjaan (4.89%+2%)</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.000.000</td>
                                <td class="text-right shrink">456.000</td>
                            </tr>
                            <tr >
                                <td class="shrink text-right">7</td>
                                <td>Pot. Angsuran Koperasi</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.000.000</td>
                                <td class="text-right shrink">456.000</td>
                            </tr>
                            <tr >
                                <td class="shrink text-right">8</td>
                                <td>Pot. Perumahan</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.000.000</td>
                                <td class="text-right shrink">456.000</td>
                            </tr>
                            <tr >
                                <td class="shrink text-right">9</td>
                                <td>Pot. Bank Bukopin/BTN/Sejahtera</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.000.000</td>
                                <td class="text-right shrink">456.000</td>
                            </tr>
                            <tr >
                                <td class="shrink text-right">10</td>
                                <td>Pot. PPH21 / ABSENSI</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.000.000</td>
                                <td class="text-right shrink">456.000</td>
                            </tr>
                            <tr >
                                <td class="shrink text-right">11</td>
                                <td>Pot. BPJS Kesehatan(4%+1%)</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.000.000</td>
                                <td class="text-right shrink">456.000</td>
                            </tr>
                            <tr class="font-weight-bold">
                                <td class="shrink text-right"></td>
                                <td class="text-center">Sub Total</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.000.000</td>
                                <td class="text-right shrink">456.000</td>
                            </tr>
                            <tr class="p-5"> 
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr class="font-weight-bold">
                                <td></td>
                                <td class="text-center">TOTAL GAJI</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.456.000</td>
                                <td class="text-right shrink">127.000.000</td>
                                <td class="text-right shrink">456.000</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="mt-4 text-dark small-font-size">
                    <tr>
                        <td>
                            <div  class="float-left">Mengetahui,</div>
                        </td>
                        <td>
                            <span  class="float-right">Sekapuk,26 Desember 2019<br>
                            Yang Membuat,</span>
                        </td>
                    </tr>
                    <tr>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <td><div  class="float-left">Nama Owner</div>
                        </td>
                        <td><div  class="float-right mr-5">Nama Akun</div>
                        </td> 
                    </tr>
                </div> 
        </td>
    </tr>
    
        </section>
    <?php $this->view("common-modal");?>
    <?php $this->view("common-js");?>
</body>

</html>