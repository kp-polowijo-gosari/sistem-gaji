
<div class="row">
	<div class="col d-sm-flex justify-content-between align-items-center mb-4">
		<h3 class="text-dark mb-0">Laporan Absen</h3>
	</div>
</div>
<div class="row mb-4">
	<div class="col">
		<div class="dropdown d-inline-block">
			<button class="btn btn-primary dropdown-toggle mx-1" data-toggle="dropdown" aria-expanded="false" type="button">Ekspor</button>
			<div class="dropdown-menu" role="menu" id="tableActions"></div>
		</div>
		<a class="btn btn-primary mx-1" role="button" href="<?=base_url();?>laporan/absen/harian">Harian</a>
	</div>
</div>
<div class="row mb-4">
	<div class="col">
		<div class="card">
			<div class="card-body">
				<div class="row my-2">
					<div class="col">
						<div class="table-responsive table-hover table-striped datatable export auto-number">
							<table class="table" id="table-aktif">
								<thead>
									<tr>
										<th class="shrink">No</th>
										<th>NIK</th>
										<th>Nama</th>
										<th>Jabatan</th>
										<th>Departemen</th>
										<th class="text-center">0-1 Jam</th>
										<th class="text-center">1-2 Jam</th>
										<th class="text-center">2-3 Jam</th>
										<th>3-4 Jam</th>
										<th>4-5 Jam</th>
										<th>5-6 Jam</th>
										<th>6-7 Jam</th>
										<th>7-8 Jam</th>
										<th>Total Jam</th>
										<th>Total Potongan</th>
									</tr>
								</thead>
								<tbody>
									<tr class="clickable" data-href="<?=base_url();?>laporan/absen/detail">
										<td class="shrink text-right">1</td>
										<td class="shrink">H76211234</td>
										<td>Muhammad Rizqi Nur</td>
										<td>Manager R&amp;D</td>
										<td>Research and Development</td>
										<td class="text-right shrink">4</td>
										<td class="text-right shrink">4</td>
										<td class="text-right shrink">4</td>
										<td class="text-right shrink">1</td>
										<td class="text-right shrink">1</td>
										<td class="text-right shrink">1</td>
										<td class="text-right shrink">1</td>
										<td class="text-right shrink">1</td>
										<td class="text-right">02:30:50</td>
										<td class="shrink text-right">Rp 2.000.000</td>
									</tr>
									<tr class="clickable" data-href="<?=base_url();?>laporan/absen/detail">
										<td class="shrink text-right">1</td>
										<td class="shrink">H76211234</td>
										<td>Muhammad Rizqi Nur</td>
										<td>Manager R&amp;D</td>
										<td>Research and Development</td>
										<td class="text-right shrink">4</td>
										<td class="text-right shrink">4</td>
										<td class="text-right shrink">4</td>
										<td class="text-right shrink">1</td>
										<td class="text-right shrink">1</td>
										<td class="text-right shrink">1</td>
										<td class="text-right shrink">1</td>
										<td class="text-right shrink">1</td>
										<td class="text-right">02:30:50</td>
										<td class="shrink text-right">Rp 2.000.000</td>
									</tr>
								</tbody>
								<tfoot>
									<tr class="font-weight-bold">
										<td></td>
										<td class="text-center font-weight-bolder" colspan="4">Total</td>
										<td class="text-right shrink">8</td>
										<td class="text-right shrink">8</td>
										<td class="text-right shrink">8</td>
										<td class="text-right shrink">8</td>
										<td class="text-right shrink">8</td>
										<td class="text-right shrink">8</td>
										<td class="text-right shrink">8</td>
										<td class="text-right shrink">8</td>
										<td class="text-right">05:01:40</td>
										<td class="text-right shrink">Rp.4.000.000</td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>