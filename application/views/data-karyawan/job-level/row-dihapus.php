<tr class="">
	<td class="shrink text-right"><?=empty($row)?"1":$i;?></td>
	<td>
		<input class="id-job" type="hidden" name="ID_JOB" valtype="input"
			value="<?=(empty($row) || empty($row["ID_JOB"]))?"":$row["ID_JOB"];?>"
		>
		<span class="normal-mode nama-job" name="NAMA_JOB" valtype="text"><?=(empty($row) || empty($row["NAMA_JOB"]))?"":$row["NAMA_JOB"];?></span>
	</td>
	<td class="text-right">
		<a class="" href="<?=base_url();?>karyawan/jabatan<?=(empty($row) || empty($row["NAMA_JOB"]))?"":"?job={$row['NAMA_JOB']}";?>" name="URL_JABATAN" valtype="href">
			<span class="jumlah-jabatan" name="JUMLAH_JABATAN" valtype="text"><?=(empty($row) || empty($row["JUMLAH_JABATAN"]))?"0":$row['JUMLAH_JABATAN'];?></span>
		</a>
	</td>
	<td class="text-right">
		<a href="<?=base_url();?>karyawan/karyawan<?=(empty($row) || empty($row["NAMA_JOB"]))?"":"?job={$row['NAMA_JOB']}";?>" name="URL_KAR" valtype="href">
			<span class="jumlah-kar" name="JUMLAH_KAR" valtype="text"><?=(empty($row) || empty($row["JUMLAH_KAR"]))?"0":$row['JUMLAH_KAR'];?></span>
		</a>
	</td>
	<td class="shrink"><button class="btn btn-primary btn-sm mx-1 btn-kembalikan-job normal-mode" type="button">Kembalikan</button></td>
</tr>
