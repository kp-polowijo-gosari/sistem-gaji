<tr class="">
	<td class="shrink text-right row-num no"><?=empty($row)?"1":$i;?></td>
	<td>
		<input class="id-job" type="hidden" name="ID_JOB" valtype="input"
			value="<?=(empty($row) || empty($row["ID_JOB"]))?"":$row["ID_JOB"];?>"
		>
		<span class="nama-job normal-mode" name="NAMA_JOB" valtype="text" valtarget=".nama-job-field"><?=(empty($row) || empty($row["NAMA_JOB"]))?"":$row["NAMA_JOB"];?></span>
		<input name="NAMA_JOB" valtype="value" valtarget=".nama-job" type="text" class="nama-job-field d-none w-100 edit-mode form-control" value="<?=(empty($row) || empty($row["NAMA_JOB"]))?"":$row["NAMA_JOB"];?>">
	</td>
	<td class="text-right">
		<a class="" href="<?=base_url();?>karyawan/jabatan<?=(empty($row) || empty($row["NAMA_JOB"]))?"":"?job={$row['NAMA_JOB']}";?>" name="URL_JABATAN" valtype="href">
			<span class="jumlah-jabatan" name="JUMLAH_JABATAN" valtype="text"><?=(empty($row) || empty($row["JUMLAH_JABATAN"]))?"0":$row['JUMLAH_JABATAN'];?></span>
		</a>
	</td>
	<td class="text-right">
		<a href="<?=base_url();?>karyawan/karyawan<?=(empty($row) || empty($row["NAMA_JOB"]))?"":"?job={$row['NAMA_JOB']}";?>" name="URL_KAR" valtype="href">
			<span class="jumlah-kar" name="JUMLAH_KAR" valtype="text"><?=(empty($row) || empty($row["JUMLAH_KAR"]))?"0":$row['JUMLAH_KAR'];?></span>
		</a>
	</td>
	<td class="shrink">
		<button class="btn btn-primary btn-sm mx-1 btn-ubah normal-mode" type="button">Ubah</button>
		<button class="btn btn-primary btn-sm mx-1 btn-simpan-job edit-mode d-none" type="button">Simpan</button>
		<button class="btn btn-danger btn-sm mx-1 btn-hapus-job normal-mode">Hapus</button>
		<button class="btn btn-danger btn-sm mx-1 btn-batal-ubah edit-mode d-none">Batal</button>
	</td>
</tr>