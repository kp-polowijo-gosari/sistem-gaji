
<div id="body-karyawan">
	<form id="form-karyawan">
		<div class="row mb-4">
			<div class="col mb-4">
				<?php if(!empty($karyawan)){ ?>
				<div class="row">
					<div class="col-12">
						<div class="card">
							<div class="card-header">
								<h4><?=$karyawan['NAMA_KAR'];?></h4>
							</div>
							<div class="card-body d-xl-flex justify-content-xl-center"><img src="<?=base_url();?>assets/img/gambar/Businessman-80_icon-icons.com_57362.png" style="max-height: 250px; max width: 250px;"></div>
						</div>
					</div>
				</div>
				<?php } ?>
				<div class="row">
					<div class="col">
						<div class="card">
							<div class="card-header">
								<div></div>
								<h4 class="flex-grow-0 flex-shrink-1">Data Pekejaan</h4>
							</div>
							<input id="input-id-kar" type="hidden" name="ID_KAR" value="<?=empty($karyawan)?"":$karyawan['ID_KAR'];?>" data-default="<?=empty($karyawan)?"":$karyawan['ID_KAR'];?>">
							<div class="card-body mx-4">
								<div class="form-row my-2">
									<div class="col custom-form-column">
										<label class="col-form-label">NIK :</label>
									</div>
									<div class="col">
										<input class="form-control edit-toggle" type="text" name="NIK" value="<?=empty($karyawan)?"":$karyawan['NIK'];?>" data-default="<?=empty($karyawan)?"":$karyawan['NIK'];?>">
									</div>
								</div>
								<div class="form-row my-2">
									<div class="col custom-form-column">
										<label class="col-form-label">Jabatan :</label>
									</div>
									<div class="col">
										<input id="input-id-jabatan" type="hidden" name="ID_JABATAN" value="<?=empty($karyawan)?"":$karyawan['ID_JABATAN'];?>" data-default="<?=empty($karyawan)?"":$karyawan['ID_JABATAN'];?>">
										<button class="btn 
											<?=(!isset($karyawan) || !$karyawan || ($karyawan['AKTIF_JOB'] && $karyawan['AKTIF_JABATAN']))?"btn-light":"btn-danger text-white";?>
										btn-block text-left border edit-toggle" type="button"  id="btn-pilih-jabatan">
											<?=empty($karyawan)?"Pilih Jabatan":"{$karyawan['NAMA_JABATAN']} ({$karyawan['NAMA_JOB']})";?>
										</button>
									</div>
								</div>
								<div class="form-row my-2">
									<div class="col custom-form-column">
										<label class="col-form-label">Departemen :</label>
									</div>
									<div class="col">
										<input id="input-id-subdept" type="hidden" name="ID_SUBDEPT" value="<?=empty($karyawan)?"":$karyawan['ID_SUBDEPT'];?>" data-default="<?=empty($karyawan)?"":$karyawan['ID_SUBDEPT'];?>">
										<button class="btn 
											<?=(!isset($karyawan) || !$karyawan || ($karyawan['AKTIF_DEPT'] && $karyawan['AKTIF_SUBDEPT']))?"btn-light":"btn-danger text-white";?>
										btn-block text-left border edit-toggle" type="button" id="btn-pilih-subdept">
											<?=empty($karyawan)?"Pilih Departemen":"{$karyawan['NAMA_DEPT']} / {$karyawan['NAMA_SUBDEPT']}";?>
										</button>
									</div>
								</div>
								<div class="form-row my-2">
									<div class="col custom-form-column">
										<label class="col-form-label">Tanggal Masuk :</label>
									</div>
									<div class="col">
										<input class="form-control edit-toggle" name="TANGGAL_MASUK" type="date" value="<?=empty($karyawan)?"":$karyawan['TANGGAL_MASUK'];?>" data-default="<?=empty($karyawan)?"":$karyawan['TANGGAL_MASUK'];?>">
									</div>
								</div>
								<?php if(!empty($karyawan)){ ?>
								<div class="form-row my-2">
									<div class="col custom-form-column">
										<label class="col-form-label">Masa Kerja :</label>
									</div>
									<div class="col">
										<span class="form-control masa-kerja">
											<?=$karyawan['TAHUN_KERJA']==0?"":($karyawan['TAHUN_KERJA']." tahun");?>
											<?=$karyawan['BULAN_KERJA']==0?"":($karyawan['BULAN_KERJA']." bulan");?>
										</span>
									</div>
								</div>
								<?php } ?>
								<div class="form-row my-2">
									<div class="col custom-form-column">
										<label class="col-form-label">Status :</label>
									</div>
									<div class="col">
										<select 
											name="ID_SKA"
											class="form-control"
											data-default="<?=empty($karyawan)?"":$karyawan['ID_SKA'];?>"
										>
										<?php foreach($statusKaryawan as $status){ ?>
											<option 
												value="<?=$status['ID_SKA'];?>"
												<?=(empty($karyawan)||$karyawan['ID_SKA']!=$status['ID_SKA'])?"":"selected=\"selected\"";?>
											>
												<?=$status['NAMA_SKA'];?>
											</option>
										<?php } ?>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col">
				<div class="card">
					<div class="card-header">
						<div>
							<h4 class="flex-grow-0 flex-shrink-1">Data Pribadi</h4>
						</div>
					</div>
					<div class="card-body mx-4">
						<div class="form-row my-2">
							<div class="col custom-form-column">
								<label class="col-form-label">Nama :</label>
							</div>
							<div class="col">
								<input class="form-control edit-toggle" type="text" name="NAMA_KAR" value="<?=empty($karyawan)?"":$karyawan['NAMA_KAR'];?>" data-default="<?=empty($karyawan)?"":$karyawan['NAMA_KAR'];?>">
							</div>
						</div>
						<div class="form-row my-2">
							<div class="col custom-form-column">
								<label class="col-form-label">Alamat :</label>
							</div>
							<div class="col">
								<textarea class="form-control edit-toggle" name="ALAMAT_KAR" data-default="<?=empty($karyawan)?"":htmlspecialchars($karyawan['ALAMAT_KAR']);?>"><?=empty($karyawan)?"":htmlspecialchars($karyawan['ALAMAT_KAR']);?></textarea>
							</div>
						</div>
						<div class="form-row my-2">
							<div class="col custom-form-column">
								<label class="col-form-label">No. KTP :</label>
							</div>
							<div class="col">
								<input class="form-control edit-toggle" type="text" name="NO_KTP_KAR" value="<?=empty($karyawan)?"":$karyawan['NO_KTP_KAR'];?>" data-default="<?=empty($karyawan)?"":$karyawan['NO_KTP_KAR'];?>" >
							</div>
						</div>
						<div class="form-row my-2">
							<div class="col custom-form-column">
								<label class="col-form-label">No. Telepon :</label>
							</div>
							<div class="col">
								<input class="form-control" type="tel" name="TELEPON_KAR" value="<?=empty($karyawan)?"":$karyawan['TELEPON_KAR'];?>" data-default="<?=empty($karyawan)?"":$karyawan['TELEPON_KAR'];?>">
							</div>
						</div>
						<div class="form-row my-2">
							<div class="col custom-form-column">
								<label class="col-form-label">Email :</label>
							</div>
							<div class="col">
								<input class="form-control" type="email" name="EMAIL_KAR" value="<?=empty($karyawan)?"":$karyawan['EMAIL_KAR'];?>" data-default="<?=empty($karyawan)?"":$karyawan['EMAIL_KAR'];?>">
							</div>
						</div>
						<div class="form-row my-2">
							<div class="col custom-form-column">
								<label class="col-form-label">Tempat Lahir :</label>
							</div>
							<div class="col">
								<input class="form-control edit-toggle" type="text" name="TEMPAT_LAHIR_KAR" value="<?=empty($karyawan)?"":$karyawan['TEMPAT_LAHIR_KAR'];?>" data-default="<?=empty($karyawan)?"":$karyawan['TEMPAT_LAHIR_KAR'];?>">
							</div>
						</div>
						<div class="form-row my-2">
							<div class="col custom-form-column">
								<label class="col-form-label">Tanggal Lahir :</label>
							</div>
							<div class="col">
								<input class="form-control edit-toggle" type="date" name="TANGGAL_LAHIR_KAR" value="<?=empty($karyawan)?"":$karyawan['TANGGAL_LAHIR_KAR'];?>" data-default="<?=empty($karyawan)?"":$karyawan['TANGGAL_LAHIR_KAR'];?>">
							</div>
						</div>
						<div class="form-row my-2">
							<div class="col custom-form-column">
								<label class="col-form-label">Agama :</label>
							</div>
							<div class="col">
								<select 
									name="ID_AGAMA"
									class="form-control"
									data-default="<?=empty($karyawan)?"":$karyawan['ID_AGAMA'];?>"
								>
								<?php foreach($agama as $ag){ ?>
									<option 
										value="<?=$ag['ID_AGAMA'];?>"
										<?=(empty($karyawan)||$karyawan['ID_AGAMA']!=$ag['ID_AGAMA'])?"":"selected=\"selected\"";?>
									>
										<?=$ag['NAMA_AGAMA'];?>
									</option>
								<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-row my-2">
							<div class="col custom-form-column">
								<label class="col-form-label">Jenis Kelamin :</label>
							</div>
							<div class="col">
								<select 
									name="ID_JK"
									class="form-control"
									data-default="<?=empty($karyawan)?"":$karyawan['ID_JK'];?>"
								>
								<?php foreach($jenisKelamin as $jk){ ?>
									<option 
										value="<?=$jk['ID_JK'];?>"
										<?=(empty($karyawan)||$karyawan['ID_JK']!=$jk['ID_JK'])?"":"selected=\"selected\"";?>
									>
										<?=$jk['NAMA_JK'];?>
									</option>
								<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-row my-2">
							<div class="col custom-form-column">
								<label class="col-form-label">Pendidikan :</label>
							</div>
							<div class="col">
								<select 
									name="ID_PENDIDIKAN"
									class="form-control"
									data-default="<?=empty($karyawan)?"":$karyawan['ID_PENDIDIKAN'];?>"
								>
								<?php foreach($pendidikan as $pend){ ?>
									<option 
										value="<?=$pend['ID_PENDIDIKAN'];?>"
										<?=(empty($karyawan)||$karyawan['ID_PENDIDIKAN']!=$pend['ID_PENDIDIKAN'])?"":"selected=\"selected\"";?>
									>
										<?=$pend['NAMA_PENDIDIKAN'];?>
									</option>
								<?php } ?>
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>