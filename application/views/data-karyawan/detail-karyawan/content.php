
<h3>
	Detail Data Karyawan	
	<?php if($karyawan["AKTIF_KAR"]){ ?>
	<button class="btn btn-primary btn-sm float-right edit-mode mx-1 btn-simpan-kar d-none" type="button">Simpan</button>
	<button class="btn btn-danger btn-sm float-right mx-1 edit-mode btn-batal-kar d-none" type="button">Batal</button>
	<button class="btn btn-primary btn-sm float-right mx-1 normal-mode btn-ubah-kar" type="button">Ubah</button>
	<button class="btn btn-danger btn-sm float-right mx-1 normal-mode" type="button" id="btn-hapus-kar">Hapus</button>
	<?php } else { ?>
	<button class="btn btn-primary btn-sm float-right mx-1 normal-mode" type="button" id="btn-kembalikan-kar"
		<?=($karyawan['AKTIF_JOB'] && $karyawan['AKTIF_JABATAN'] && $karyawan['AKTIF_DEPT'] && $karyawan['AKTIF_SUBDEPT'])?"":"disabled";?>
	>Kembalikan</button>
	<?php } ?>
</h3>
<?php 
	$this->view("data-karyawan/body-karyawan/content", array(
		"karyawan"=>$karyawan,
		"statusKaryawan"=>$statusKaryawan,
		"jenisKelamin"=>$jenisKelamin,
		"agama"=>$agama,
		"pendidikan"=>$pendidikan
	)); 
?>