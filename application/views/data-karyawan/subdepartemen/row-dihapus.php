<tr class="">
	<td class="shrink text-right row-num"><?=empty($row)?"1":$i;?></td>
	<td>
		<input class="id-subdept" type="hidden" name="ID_SUBDEPT" valtype="input"
			value="<?=(empty($row) || empty($row["ID_SUBDEPT"]))?"":$row["ID_SUBDEPT"];?>"
		>
		<span class="normal-mode nama-subdept " name="NAMA_SUBDEPT" valtype="text"><?=(empty($row) || empty($row["NAMA_SUBDEPT"]))?"":$row["NAMA_SUBDEPT"];?></span>
	</td>
	<td class="<?=(empty($row) || $row['AKTIF_DEPT'])?"":"bg-danger text-white";?>">
		<input class="id-dept" type="hidden" name="ID_DEPT" valtype="input"
			value="<?=(empty($row) || empty($row["ID_DEPT"]))?"":$row["ID_DEPT"];?>"
		>
		<span class="normal-mode nama-dept " name="NAMA_DEPT" valtype="text"><?=(empty($row) || empty($row["NAMA_DEPT"]))?"":$row["NAMA_DEPT"];?></span>
	</td>
	<td class="text-right">
		<a href="<?=base_url();?>karyawan/karyawan<?=(empty($row) || empty($row["NAMA_SUBDEPT"]))?"":"?subdept={$row['NAMA_SUBDEPT']}";?>" name="URL_KAR" valtype="href">
			<span class="jumlah-kar" name="JUMLAH_KAR" valtype="text"><?=(empty($row) || empty($row["JUMLAH_KAR"]))?"0":$row['JUMLAH_KAR'];?></span>
		</a>
	</td>
	<td class="shrink">
		<button class="btn btn-primary btn-sm mx-1 btn-kembalikan-subdept normal-mode" type="button"
			<?=(empty($row) || $row['AKTIF_DEPT'])?"":"disabled";?>
		>Kembalikan</button>
	</td>
</tr>
