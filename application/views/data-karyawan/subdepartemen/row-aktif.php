<tr class="">
	<td class="shrink text-right row-num no"><?=empty($row)?"1":$i;?></td>
	<td>
		<input class="id-subdept" type="hidden" name="ID_SUBDEPT" valtype="input" 
			value="<?=(empty($row) || empty($row["ID_SUBDEPT"]))?"":$row["ID_SUBDEPT"];?>"
		>
		<span class="normal-mode nama-subdept " name="NAMA_SUBDEPT" valtype="text" valtarget=".nama-subdept-field"><?=(empty($row) || empty($row["NAMA_SUBDEPT"]))?"":$row["NAMA_SUBDEPT"];?></span>
		<input valtype="value" valtarget=".nama-subdept" type="text" name="NAMA_SUBDEPT" class=" nama-subdept-field d-none w-100 edit-mode form-control" value="<?=(empty($row) || empty($row["NAMA_SUBDEPT"]))?"":$row["NAMA_SUBDEPT"];?>">
	</td>
	<td class="">
		<input class="id-dept normal-mode" type="hidden" name="ID_DEPT" valtype="input" valtarget=".id-dept-field"
			value="<?=(empty($row) || empty($row["ID_DEPT"]))?"":$row["ID_DEPT"];?>"
		>
		<span class="normal-mode nama-dept " name="NAMA_DEPT" valtype="text"><?=(empty($row) || empty($row["NAMA_DEPT"]))?"":$row["NAMA_DEPT"];?></span>
		<select valtype="select" valtarget="id-dept" valtarget2="nama-dept" name="ID_DEPT" class=" id-dept-field d-none w-100 edit-mode form-control">
			<?php 
			$hasRow = !(empty($row) || empty($row['ID_DEPT']));
			foreach($deptAktif as $dept){?>
				<option value="<?=$dept["ID_DEPT"];?>" <?php if($hasRow && $dept["ID_DEPT"]==$row["ID_DEPT"]){ echo ' selected="selected"'; $first = false; } ?> >
					<?=$dept["NAMA_DEPT"];?>
				</option>
			<?php } ?>
		</select>
	</td>
	<td class="text-right">
		<a href="<?=base_url();?>karyawan/karyawan<?=(empty($row) || empty($row["NAMA_SUBDEPT"]))?"":"?subdept={$row['NAMA_SUBDEPT']}";?>" name="URL_KAR" valtype="href">
			<span class="jumlah-kar" name="JUMLAH_KAR" valtype="text"><?=(empty($row) || empty($row["JUMLAH_KAR"]))?"0":$row['JUMLAH_KAR'];?></span>
		</a>
	</td>
	<td class="shrink">
		<button class="btn btn-primary btn-sm mx-1 btn-ubah normal-mode" type="button">Ubah</button>
		<button class="btn btn-primary btn-sm mx-1 btn-simpan-subdept d-none edit-mode" type="button">Simpan</button>
		<button class="btn btn-danger btn-sm mx-1 btn-hapus-subdept normal-mode">Hapus</button>
		<button class="btn btn-danger btn-sm mx-1 btn-batal-ubah edit-mode d-none">Batal</button>
	</td>
</tr>