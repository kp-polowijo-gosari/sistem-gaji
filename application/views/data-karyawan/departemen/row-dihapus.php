
<tr class="">
	<td class="shrink text-right row-num"><?=empty($row)?"1":$i;?></td>
	<td>
		<input class="id-dept" type="hidden" name="ID_DEPT" valtype="input"
			value="<?=(empty($row) || empty($row["ID_DEPT"]))?"":$row["ID_DEPT"];?>"
		>
		<input class="id-per" type="hidden" name="ID_PER" valtype="input"
			value="<?=(empty($row) || empty($row["ID_PER"]))?"":$row["ID_PER"];?>"
		>
		<span class="normal-mode nama-dept " name="NAMA_DEPT" valtype="text"><?=(empty($row) || empty($row["NAMA_DEPT"]))?"":$row["NAMA_DEPT"];?></span>
	</td>
	<td class="text-right">
		<a href="<?=base_url();?>karyawan/subdepartemen<?=(empty($row) || empty($row["NAMA_DEPT"]))?"":"?dept={$row['NAMA_DEPT']}";?>" name="URL_SUBDEPT" valtype="href">
			<span class="jumlah-subdept" name="JUMLAH_SUBDEPT" valtype="text"><?=(empty($row) || empty($row["JUMLAH_SUBDEPT"]))?"0":$row["JUMLAH_SUBDEPT"];?></span>
		</a>
	</td>
	<td class="text-right">
		<a href="<?=base_url();?>karyawan/karyawan<?=(empty($row) || empty($row["NAMA_DEPT"]))?"":"?dept={$row['NAMA_DEPT']}";?>" name="URL_KAR" valtype="href">
			<span class="jumlah-kar" name="JUMLAH_KAR" valtype="text"><?=(empty($row) || empty($row["JUMLAH_KAR"]))?"0":$row['JUMLAH_KAR'];?></span>
		</a>
	</td>
	<td class="shrink">
		<button class="btn btn-primary btn-sm mx-1 btn-kembalikan-dept normal-mode" type="button">Kembalikan</button>
	</td>
</tr>