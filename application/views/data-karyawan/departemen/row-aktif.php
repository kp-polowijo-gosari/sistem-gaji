<tr class="">
	<td class="shrink text-right row-num no"><?=empty($row)?"1":$i;?></td>
	<td>
		<input class="id-dept" type="hidden" name="ID_DEPT" valtype="input"
			value="<?=(empty($row) || empty($row["ID_DEPT"]))?"":$row["ID_DEPT"];?>"
		>
		<input class="id-per" type="hidden" name="ID_PER" valtype="input"
			value="<?=(empty($row) || empty($row["ID_PER"]))?"":$row["ID_PER"];?>"
		>
		<span class="nama-dept normal-mode" name="NAMA_DEPT" valtype="text" valtarget=".nama-dept-field"><?=(empty($row) || empty($row["NAMA_DEPT"]))?"":$row["NAMA_DEPT"];?></span>
		<input name="NAMA_DEPT" valtype="value" valtarget=".nama-dept" type="text" class="nama-dept-field d-none w-100 edit-mode form-control" value="<?=(empty($row) || empty($row["NAMA_DEPT"]))?"":$row["NAMA_DEPT"];?>">
	</td>
	<td class="text-right">
		<a href="<?=base_url();?>karyawan/subdepartemen<?=(empty($row) || empty($row["NAMA_DEPT"]))?"":"?dept={$row['NAMA_DEPT']}";?>" name="URL_SUBDEPT" valtype="href">
			<span class="jumlah-subdept" name="JUMLAH_SUBDEPT" valtype="text"><?=(empty($row) || empty($row["JUMLAH_SUBDEPT"]))?"0":$row["JUMLAH_SUBDEPT"];?></span>
		</a>
	</td>
	<td class="text-right">
		<a href="<?=base_url();?>karyawan/karyawan<?=(empty($row) || empty($row["NAMA_DEPT"]))?"":"?dept={$row['NAMA_DEPT']}";?>" name="URL_KAR" valtype="href">
			<span class="jumlah-kar" name="JUMLAH_KAR" valtype="text"><?=(empty($row) || empty($row["JUMLAH_KAR"]))?"0":$row['JUMLAH_KAR'];?></span>
		</a>
	</td>
	<td class="shrink">
		<button class="btn btn-primary btn-sm mx-1 btn-ubah normal-mode" type="button">Ubah</button>
		<button class="btn btn-primary btn-sm mx-1 btn-simpan-dept d-none edit-mode" type="button">Simpan</button>
		<button class="btn btn-danger btn-sm mx-1 btn-hapus-dept normal-mode">Hapus</button>
		<button class="btn btn-danger btn-sm mx-1 btn-batal-ubah btn-batal-dept edit-mode d-none">Batal</button>
	</td>
</tr>