<tr class="">
	<td class="shrink text-right row-num"><?=empty($row)?"1":$i;?></td>
	<td>
		<input class="id-jabatan" type="hidden" name="ID_JABATAN" valtype="input"
			value="<?=(empty($row) || empty($row["ID_JABATAN"]))?"":$row["ID_JABATAN"];?>"
		>
		<span class="normal-mode nama-jabatan " name="NAMA_JABATAN" valtype="text"><?=(empty($row) || empty($row["NAMA_JABATAN"]))?"":$row["NAMA_JABATAN"];?></span>
	</td>
	<td class="<?=(empty($row) || $row['AKTIF_JOB'])?"":"bg-danger text-white";?>">
		<input class="id-job" type="hidden" name="ID_JOB" valtype="input"
			value="<?=(empty($row) || empty($row["ID_JOB"]))?"":$row["ID_JOB"];?>"
		>
		<span class="normal-mode nama-job " name="NAMA_JOB" valtype="text"><?=(empty($row) || empty($row["NAMA_JOB"]))?"":$row["NAMA_JOB"];?></span>
	</td>
	<td class="text-right">
		<a href="<?=base_url();?>karyawan/karyawan<?=(empty($row) || empty($row["NAMA_JABATAN"]))?"":"?jabatan={$row['NAMA_JABATAN']}";?>" name="URL_KAR" valtype="href">
			<span class="jumlah-kar" name="JUMLAH_KAR" valtype="text"><?=(empty($row) || empty($row["JUMLAH_KAR"]))?"0":$row['JUMLAH_KAR'];?></span>
		</a>
	</td>
	<td class="shrink">
		<button class="btn btn-primary btn-sm mx-1 btn-kembalikan-jabatan normal-mode" type="button"
			<?=(empty($row) || $row['AKTIF_JOB'])?"":"disabled";?>
		>Kembalikan</button>
	</td>
</tr>