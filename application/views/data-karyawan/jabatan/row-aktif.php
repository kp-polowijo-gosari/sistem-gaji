<tr class="">
	<td class="shrink text-right row-num no"><?=empty($row)?"1":$i;?></td>
	<td>
		<input class="id-jabatan" type="hidden" name="ID_JABATAN" valtype="input"
			value="<?=(empty($row) || empty($row["ID_JABATAN"]))?"":$row["ID_JABATAN"];?>"
		>
		<span class="normal-mode nama-jabatan " name="NAMA_JABATAN" valtype="text" valtarget=".nama-jabatan-field"><?=(empty($row) || empty($row["NAMA_JABATAN"]))?"":$row["NAMA_JABATAN"];?></span>
		<input valtype="value" valtarget=".nama-jabatan" type="text" name="NAMA_JABATAN" class=" nama-jabatan-field d-none w-100 edit-mode form-control" value="<?=(empty($row) || empty($row["NAMA_JABATAN"]))?"":$row["NAMA_JABATAN"];?>">
	</td>
	<td class="">
		<input class="id-job normal-mode" type="hidden" name="ID_JOB" valtype="input" valtarget=".id-job-field"
			value="<?=(empty($row) || empty($row["ID_JOB"]))?"":$row["ID_JOB"];?>"
		>
		<span class="normal-mode nama-job " name="NAMA_JOB" valtype="text" ><?=(empty($row) || empty($row["NAMA_JOB"]))?"":$row["NAMA_JOB"];?></span>
		<select valtype="select" valtarget="id-job" valtarget2="nama-job" name="ID_JOB" class=" id-job-field d-none w-100 edit-mode form-control">
			<?php 
			$hasRow = !(empty($row) || empty($row['ID_JOB']));
			foreach($jobAktif as $job){?>
				<option value="<?=$job["ID_JOB"];?>" <?php if($hasRow && $job["ID_JOB"]==$row['ID_JOB']){ echo ' selected="selected"'; $first = false; } ?> >
					<?=$job["NAMA_JOB"];?>
				</option>
			<?php } ?>
		</select>
	</td>
	<td class="text-right">
		<a href="<?=base_url();?>karyawan/karyawan<?=(empty($row) || empty($row["NAMA_JABATAN"]))?"":"?jabatan={$row['NAMA_JABATAN']}";?>" name="URL_KAR" valtype="href">
			<span class="jumlah-kar" name="JUMLAH_KAR" valtype="text"><?=(empty($row) || empty($row["JUMLAH_KAR"]))?"0":$row['JUMLAH_KAR'];?></span>
		</a>
	</td>
	<td class="shrink">
		<button class="btn btn-primary btn-sm mx-1 btn-ubah normal-mode" type="button">Ubah</button>
		<button class="btn btn-primary btn-sm mx-1 btn-simpan-jabatan d-none edit-mode" type="button">Simpan</button>
		<button class="btn btn-danger btn-sm mx-1 btn-hapus-jabatan normal-mode">Hapus</button>
		<button class="btn btn-danger btn-sm mx-1 btn-batal-ubah edit-mode d-none">Batal</button>
	</td>
</tr>