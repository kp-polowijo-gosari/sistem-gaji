<tr class="">
	<td class="shrink text-right row-num no"><?=empty($row)?"1":$i;?></td>
	<td class="shrink">
		<input class="id-kar" type="hidden" name="ID_KAR" valtype="input"
			value="<?=(empty($row) || empty($row["ID_KAR"]))?"":$row["ID_KAR"];?>"
		>
		<span class="nik" valtype="text" name="NIK"><?=(empty($row) || empty($row['NIK']))?"-":$row['NIK'];?></span>
	</td>
	<td class="">
		<span class="nama-kar" valtype="text" name="NAMA_KAR"><?=(empty($row) || empty($row['NAMA_KAR']))?"-":$row['NAMA_KAR'];?></span>
	</td>
	<td class="">
		<span class="nama-jabatan" valtype="text" name="NAMA_JABATAN"><?=(empty($row) || empty($row['NAMA_JABATAN']))?"-":$row['NAMA_JABATAN'];?></span>
		<span class="nama-job" valtype="text" name="NAMA_JOB"><?=(empty($row) || empty($row['NAMA_JOB']))?"":" ({$row['NAMA_JOB']})";?></span>
	</td>
	<td class="">
		<span class="nama-subdept" valtype="text" name="NAMA_SUBDEPT"><?=(empty($row) || empty($row['NAMA_SUBDEPT']))?"-":$row['NAMA_SUBDEPT'];?></span>
		<span class="nama-dept" valtype="text" name="NAMA_DEPT"><?=(empty($row) || empty($row['NAMA_DEPT']))?"":" / {$row['NAMA_DEPT']}";?></span>
	</td>
	<td class="shrink">
		<span class="tanggal-masuk" valtype="text" name="TANGGAL_MASUK"><?=(empty($row) || empty($row['TANGGAL_MASUK']))?"-":$row['TANGGAL_MASUK'];?></span>
	</td>
	<td class="shrink ">
		<span class="masa-kerja" valtype="text" name="MASA_KERJA"><?=(empty($row) || empty($row['MASA_KERJA']))?"-":$row['MASA_KERJA'];?></span>
	</td>
	<td class="text-right shrink">
		<a class="btn btn-primary btn-sm mx-1" role="button" href="<?=base_url();?>karyawan/karyawan/<?=empty($row)?"1":$row['ID_KAR'];?>">Detail</a>
		<button class="btn btn-danger btn-sm mx-1 btn-hapus-kar">Hapus</button>
	</td>
</tr>