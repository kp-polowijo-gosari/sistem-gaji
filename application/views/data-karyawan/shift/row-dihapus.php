<tr class="">
	<td class="shrink text-right"><?=empty($row)?"1":$i;?></td>
	<td>
		<input class="id-shift" type="hidden" name="ID_SHIFT" valtype="input"
			value="<?=(empty($row) || empty($row["ID_SHIFT"]))?"":$row["ID_SHIFT"];?>"
		>
		<span class="normal-mode nama-shift " name="NAMA_SHIFT" valtype="text"><?=(empty($row) || empty($row["NAMA_SHIFT"]))?"":$row["NAMA_SHIFT"];?></span>
	</td>
	<td class="shrink text-right">
		<span class="max-lembur-1 normal-mode" name="MAX_LEMBUR_1" valtype="text"><?=(empty($row) || empty($row["MAX_LEMBUR_1"]))?"":$row['MAX_LEMBUR_1'];?></span>
	</td>
	<td class="shrink text-right">
		<span class="max-lembur-2 normal-mode" name="MAX_LEMBUR_2" valtype="text"><?=(empty($row) || empty($row["MAX_LEMBUR_2"]))?"":$row['MAX_LEMBUR_2'];?></span>
	</td>
	<td class="shrink"><button class="btn btn-primary btn-sm mx-1 btn-kembalikan-shift normal-mode" type="button">Kembalikan</button></td>
</tr>
