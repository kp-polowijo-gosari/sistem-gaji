<tr class="">
	<td class="shrink text-right">1</td>
	<td>
		<input class="id-shift" type="hidden" name="ID_SHIFT" valtype="input"
			value="<?=(empty($row) || empty($row["ID_SHIFT"]))?"":$row["ID_SHIFT"];?>"
		>
		<span class="normal-mode nama-shift " name="NAMA_SHIFT" valtype="text" valtarget=".nama-shift-field"><?=(empty($row) || empty($row["NAMA_SHIFT"]))?"":$row["NAMA_SHIFT"];?></span>
		<input valtype="value" valtarget=".nama-shift" name="NAMA_SHIFT" type="text" class="nama-shift-field d-none edit-mode w-100 form-control" value="<?=(empty($row) || empty($row["NAMA_SHIFT"]))?"":$row["NAMA_SHIFT"];?>">
	</td>
	<td class="text-right shrink">
		<span class="max-lembur-1 normal-mode" name="MAX_LEMBUR_1" valtype="text" valtarget=".max-lembur-1-field"><?=(empty($row) || empty($row["MAX_LEMBUR_1"]))?"":$row['MAX_LEMBUR_1'];?></span>
		<input valtype="value" valtarget=".max-lembur-1" name="MAX_LEMBUR_1" type="number" class="form-control edit-mode max-lembur-1-field d-none">
	</td>
	<td class="text-right shrink">
		<span class="max-lembur-2 normal-mode" name="MAX_LEMBUR_2" valtype="text" valtarget=".max-lembur-2-field"><?=(empty($row) || empty($row["MAX_LEMBUR_2"]))?"":$row['MAX_LEMBUR_2'];?></span>
		<input valtype="value" valtarget=".max-lembur-2" name="MAX_LEMBUR_2" type="number" class="form-control edit-mode max-lembur-2-field d-none">
	</td>
	<td class="shrink">
		<button class="btn btn-primary btn-sm mx-1 btn-ubah normal-mode" type="button">Ubah</button>
		<button class="btn btn-primary btn-sm mx-1 btn-simpan-shift edit-mode d-none" type="button">Simpan</button>
		<button class="btn btn-danger btn-sm mx-1 btn-hapus-shift normal-mode">Hapus</button>
		<button class="btn btn-danger btn-sm mx-1 btn-batal-ubah edit-mode d-none">Batal</button>
	</td>
</tr>
