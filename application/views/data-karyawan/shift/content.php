
<div class="d-sm-flex justify-content-between align-items-center mb-4">
	<h3 class="text-dark mb-0">Shift</h3>
</div>
<div class="d-sm-flex align-items-start mb-4">
	<button class="btn btn-primary mx-1" type="button" data-toggle="modal" data-target="#modal-add-shift">Tambah</button>
	<div class="dropdown d-inline-block"><button class="btn btn-primary dropdown-toggle mx-1" data-toggle="dropdown" aria-expanded="false" type="button">Ekspor</button>
		<div class="dropdown-menu" role="menu" id="tableActions"></div>
	</div>
</div>
<div class="row">
	<div class="col mb-4">
		<div>
			<ul class="nav nav-tabs">
				<li class="nav-item"><a class="nav-link active" role="tab" data-toggle="tab" href="#tab-1">Aktif</a></li>
				<li class="nav-item"><a class="nav-link" role="tab" data-toggle="tab" href="#tab-2">Dihapus</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" role="tabpanel" id="tab-1">
					<div class="card tab-card">
						<div class="card-body">
							<div class="row my-2">
								<div class="col">
									<div class="table-responsive datatable export auto-number">
										<table class="table table-edit" id="table-aktif">
											<thead>
												<tr>
													<th class="shrink">No</th>
													<th>Shift</th>
													<th class="shrink">Max Lembur I</th>
													<th class="shrink">Max Lembur II</th>
													<th class="no-export">Aksi</th>
												</tr>
											</thead>
											<tbody>
												<?php 
												$i = 0;
												foreach($shiftAktif as $row){ 
													++$i;
													$this->view("data-karyawan/shift/row-aktif", array(
														"row"=>$row,
														"i"=>$i
													));
												} 
												?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane" role="tabpanel" id="tab-2">
					<div class="card tab-card">
						<div class="card-body">
							<div class="row my-2">
								<div class="col">
									<div class="table-responsive datatable  auto-number">
										<table class="table" id="table-dihapus">
											<thead>
												<tr>
													<th class="shrink">No</th>
													<th>Shift</th>
													<th class="shrink">Max Lembur I</th>
													<th class="shrink">Max Lembur II</th>
													<th class="no-export">Aksi</th>
												</tr>
											</thead>
											<tbody>
												<?php 
												$i = 0;
												foreach($shiftDihapus as $row){ 
													++$i;
													$this->view("data-karyawan/shift/row-dihapus", array(
														"row"=>$row,
														"i"=>$i
													));
												} 
												?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>