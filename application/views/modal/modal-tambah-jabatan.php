<div class="modal fade" role="dialog" tabindex="-1" id="modal-add-jabatan">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Jabatan</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
            <div class="modal-body">
                <form id="form-add-jabatan">
                    <div class="form-row my-1">
                        <div class="col-3"><label class="col-form-label">Nama Jabatan</label></div>
                        <div class="col"><input name="NAMA_JABATAN" class="form-control" type="text"></div>
                    </div>
                    <div class="form-row my-3">
                        <div class="col-3"><label class="col-form-label">Job Level</label></div>
                        <div class="col">
							<select name="ID_JOB" class="form-control">
								<?php 
								$first = true;
								foreach($jobAktif as $row){
								?>
									<option value="<?=$row['ID_JOB'];?>" <?php if($first){ echo " default selected"; $first = false; } ?> >
										<?=$row['NAMA_JOB'];?>
									</option>
								<?php } ?>
							</select>
						</div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
				<button class="btn btn-light" type="button" data-dismiss="modal">Batal</button>
				<button id="btn-submit-add-jabatan" class="btn btn-primary" type="button">Simpan</button>
			</div>
        </div>
    </div>
</div>