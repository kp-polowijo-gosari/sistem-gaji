<div class="modal fade" role="dialog" tabindex="-1" id="modal-add-absen">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Absen</h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
            <div class="modal-body">
                <form>
                    <div class="form-row my-3">
                        <div class="col-3"><label class="col-form-label">Karyawan</label></div>
                        <div class="col"><label class="col-form-label">Muhammad Rizqi Nur</label></div>
                    </div>
                    <div class="form-row my-3">
                        <div class="col-3"><label class="col-form-label">Tanggal</label></div>
                        <div class="col"><input class="form-control" type="date"></div>
                    </div>
                    <div class="form-row my-3">
                        <div class="col-3"><label class="col-form-label">Keterangan</label></div>
                        <div class="col"><input class="form-control" type="text"></div>
                    </div>
                    <div class="form-row my-3">
                        <div class="col-3"><label class="col-form-label">Lama Absen (Jam)</label></div>
                        <div class="col"><input class="form-control" type="number"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer"><button class="btn btn-light" type="button" data-dismiss="modal">Batal</button><button class="btn btn-primary" type="button">Simpan</button></div>
        </div>
    </div>
</div>