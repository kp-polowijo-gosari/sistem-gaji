<div class="modal fade" role="dialog" tabindex="-1" id="modal-add-lembur">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Lembur</h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
            <div class="modal-body">
                <form>
                    <div class="form-row my-3">
                        <div class="col-3"><label class="col-form-label">Karyawan</label></div>
                        <div class="col"><button class="btn btn-primary btn-block text-left" type="button" data-toggle="modal" data-target="#modal-pilih-karyawan">Pilih Karyawan</button></div>
                    </div>
                    <div class="form-row my-3">
                        <div class="col-3"><label class="col-form-label">Tanggal</label></div>
                        <div class="col"><input class="form-control" type="date"></div>
                    </div>
                    <div class="form-row my-3">
                        <div class="col-3"><label class="col-form-label">Keterangan</label></div>
                        <div class="col"><input class="form-control" type="text"></div>
                    </div>
                    <div class="form-row my-3">
                        <div class="col-3"><label class="col-form-label">Shift</label></div>
                        <div class="col"><select class="form-control"><option value="1" selected="">I</option><option value="2">II</option><option value="3">III</option><option value="0">Off</option></select></div>
                        <div class="col-3"><label class="col-form-label">Lama Lembur (Jam)</label></div>
                        <div class="col"><input class="form-control" type="number"></div>
                    </div>
                    <div class="form-row py-2">
                        <div class="col">
                            <div class="form-check"><input class="form-check-input" type="checkbox" id="formCheck-1"><label class="form-check-label" for="formCheck-1">Makan</label></div>
                        </div>
                        <div class="col">
                            <div class="form-check"><input class="form-check-input" type="checkbox" id="formCheck-2"><label class="form-check-label" for="formCheck-2">Label</label></div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer"><button class="btn btn-light" type="button" data-dismiss="modal">Batal</button><button class="btn btn-primary" type="button">Simpan</button></div>
        </div>
    </div>
</div>