
    <div class="modal fade" role="dialog" tabindex="-1" id="modal-confirmation">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-white">
                    <h4 class="modal-title">Konfirmasi Penghapusan</h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
                <div class="modal-body">
                    <p class="modal-text">Apa Anda yakin ingin menghapus departemen Research and Development?</p>
                </div>
                <div class="modal-footer"><button class="btn btn-light" type="button" data-dismiss="modal">Batal</button><button class="btn btn-danger btn-confirm" type="button">Ya</button></div>
            </div>
        </div>
    </div>