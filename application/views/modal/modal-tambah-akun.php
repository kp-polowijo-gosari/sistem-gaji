<div class="modal fade" role="dialog" tabindex="-1" id="modal-add-akun">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Akun</h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
            <div class="modal-body">
                <form>
                    <div class="form-row">
                        <div class="col-3"><label class="col-form-label">Username</label></div>
                        <div class="col"><input class="form-control" type="text"></div>
                    </div>
                    <div class="form-row">
                        <div class="col-3"><label class="col-form-label">Nama</label></div>
                        <div class="col"><input class="form-control" type="text"></div>
                    </div>
                    <div class="form-row">
                        <div class="col-3"><label class="col-form-label">Email</label></div>
                        <div class="col"><input class="form-control" type="email"></div>
                    </div>
                    <div class="form-row">
                        <div class="col-3"><label class="col-form-label">Peran</label></div>
                        <div class="col"><select class="form-control"><option value="12" selected="">Pengawas</option><option value="13">Admin Biasa</option><option value="14">Admin Akun</option><option value="">Admin Utama</option><option value="">Super Admin</option></select></div>
                    </div>
                    <div class="form-row">
                        <div class="col-3"><label class="col-form-label">Password</label></div>
                        <div class="col"><input class="form-control" type="password"></div>
                    </div>
                    <div class="form-row">
                        <div class="col-3"><label class="col-form-label">Konfirmasi Password</label></div>
                        <div class="col"><input class="form-control" type="password"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer"><button class="btn btn-light" type="button" data-dismiss="modal">Batal</button><button class="btn btn-primary" type="button" data-dismiss="modal">Simpan</button></div>
        </div>
    </div>
</div>