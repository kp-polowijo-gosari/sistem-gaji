<div class="modal fade" role="dialog" tabindex="-1" id="modal-pilih-tkh">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Pilih Tunjangan Khusus</h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
			<div class="modal-body">
				<div class="row my-2">
					<div class="col">
						<div class="table-responsive table-hover datatable auto-number" data-pagelength="5">
							<table class="table">
								<thead>
									<tr>
										<th class="shrink">No</th>
										<th>Tunjangan</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$i = 0;
									foreach($tkh as $row){ ?>
									<tr class="row-pilih-tkh clickable">
										<td class="shrink text-right"><?=++$i;?></td>
										<td class="">
											<input class="id-tkh" type="hidden" name="ID_TKH" valtype="input"
												value="<?=$row["ID_TKH"];?>"
											>
											<span class="nama-tkh"  name="NAMA_TKH" valtype="text"><?=$row['NAMA_TKH'];?></span>
										</td>
										<td class="">
											<span class="nilai-tkh"  name="NILAI_TKH" valtype="text" data-type="currency"><?=$row['NILAI_TKH'];?></span>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>