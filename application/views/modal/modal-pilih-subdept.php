<div class="modal fade" role="dialog" tabindex="-1" id="modal-pilih-subdept">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Pilih Subdepartemen</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
			</div>
			<div class="modal-body">
				<div class="row my-2">
					<div class="col">
						<div class="table-responsive table-hover datatable auto-number" data-pagelength="5">
							<table class="table">
								<thead>
									<tr>
										<th class="shrink">No</th>
										<th>Subdepartemen</th>
										<th>Departemen</th>
									</tr>
								</thead>
								<tbody>
									<?php 
									$i = 0;
									foreach($subdept as $row){ ?>
									<tr class="row-pilih-subdept clickable">
										<td class="shrink text-right"><?=++$i;?></td>
										<td class="">
											<input class="id-subdept" type="hidden" name="ID_SUBDEPT" valtype="input"
												value="<?=$row["ID_SUBDEPT"];?>"
											>
											<span class="nama-subdept"  name="NAMA_SUBDEPT" valtype="text"><?=$row['NAMA_SUBDEPT'];?></span>
										</td>
										<td class="">
											<input class="id-dept" type="hidden" name="ID_DEPT" valtype="input"
												value="<?=$row["ID_DEPT"];?>"
											>
											<span class="nama-dept"  name="NAMA_DEPT" valtype="text"><?=$row['NAMA_DEPT'];?></span>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>