<div class="modal fade" role="dialog" tabindex="-1" id="modal-add-job">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Job Level</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
            <div class="modal-body">
                <form id="form-add-job">
                    <div class="form-row">
                        <div class="col-3"><label class="col-form-label">Nama Job Level</label></div>
                        <div class="col"><input name="NAMA_JOB" class="form-control" type="text"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
				<button class="btn btn-light" type="button" data-dismiss="modal">Batal</button>
				<button id="btn-submit-add-job" class="btn btn-primary" type="button">Simpan</button>
			</div>
        </div>
    </div>
</div>