<div class="modal fade" role="dialog" tabindex="-1" id="modal-pilih-jabatan">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Pilih Jabatan</h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
			<div class="modal-body">
				<div class="row my-2">
					<div class="col">
						<div class="table-responsive table-hover datatable auto-number" data-pagelength="5">
							<table class="table">
								<thead>
									<tr>
										<th class="shrink">No</th>
										<th>Jabatan</th>
										<th>Job Level</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$i = 0;
									foreach($jabatan as $row){ ?>
									<tr class="row-pilih-jabatan clickable">
										<td class="shrink text-right"><?=++$i;?></td>
										<td class="">
											<input class="id-jabatan" type="hidden" name="ID_JABATAN" valtype="input"
												value="<?=$row["ID_JABATAN"];?>"
											>
											<span class="nama-jabatan"  name="NAMA_JABATAN" valtype="text"><?=$row['NAMA_JABATAN'];?></span>
										</td>
										<td class="" data-id="<?=$row['ID_JOB'];?>">
											<input class="id-job" type="hidden" name="ID_JOB" valtype="input"
												value="<?=$row["ID_JOB"];?>"
											>
											<span class="nama-job"  name="NAMA_JOB" valtype="text"><?=$row['NAMA_JOB'];?></span>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>