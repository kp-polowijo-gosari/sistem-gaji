
<div class="modal fade" role="dialog" tabindex="-1" id="modal-add-shift">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Shift</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
            <div class="modal-body">
                <form id="form-add-shift">
                    <div class="form-row my-1">
                        <div class="col-3"><label class="col-form-label">Shift</label></div>
                        <div class="col"><input name="NAMA_SHIFT" class="form-control" type="text"></div>
                    </div>
                    <div class="form-row my-3">
                        <div class="col-3"><label class="col-form-label">Max Lembur I</label></div>
                        <div class="col"><input name="MAX_LEMBUR_1" class="form-control" type="number"></div>
                    </div>
                    <div class="form-row">
                        <div class="col-3"><label class="col-form-label">Max Lembur II</label></div>
                        <div class="col"><input name="MAX_LEMBUR_2" class="form-control" type="number"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
				<button class="btn btn-light" type="button" data-dismiss="modal">Batal</button>
				<button id="btn-submit-add-shift" class="btn btn-primary" type="button">Simpan</button>
			</div>
        </div>
    </div>
</div>