<div class="modal fade" role="dialog" tabindex="-1" id="modal-pilih-karyawan">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Pilih Karyawan</h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
            <div class="modal-body">
                <div class="row my-2">
                    <div class="col">
                        <div class="table-responsive table-hover datatable">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th class="shrink">No</th>
                                        <th>NIK</th>
                                        <th>Nama</th>
                                        <th>Departemen</th>
                                        <th>Jabatan</th>
                                        <th>Job Level</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="jabatan-row-pilih clickable" data-dismiss="modal">
                                        <td class="shrink text-right">1</td>
                                        <td class="shrink">H76767676</td>
                                        <td>Manager R&amp;D</td>
                                        <td>Research and Development</td>
                                        <td>Manager R&amp;D</td>
                                        <td>Manager<button class="btn btn-primary btn-sm edit-mode d-none btn-pilih-jabatan" type="button">Manager</button></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
