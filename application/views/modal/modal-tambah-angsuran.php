<div class="modal fade" role="dialog" tabindex="-1" id="modal-tambah-angsuran">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Angsuran</h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
                <div class="modal-body">
                    <form>
                        <div class="form-row my-2">
                            <div class="col custom-form-modal"><label class="col-form-label">Karyawan</label></div>
                            <div class="col"><button class="btn btn-primary btn-block text-left" type="button" data-toggle="modal" data-target="#modal-pilih-karyawan">Pilih Karyawan</button></div>
                        </div>
                        <div class="form-row my-2">
                            <div class="col custom-form-modal"><label class="col-form-label">Tanggal</label></div>
                            <div class="col"><input class="form-control" type="date"></div>
                        </div>
                        <div class="form-row my-2">
                            <div class="col custom-form-modal"><label class="col-form-label">Keterangan</label></div>
                            <div class="col"><input class="form-control" type="text"></div>
                        </div>
                        <div class="form-row my-2">
                            <div class="col custom-form-modal"><label class="col-form-label">Jenis Angsuran</label></div>
                            <div class="col"><select class="form-control"><option value="12" selected="">Koperasi</option><option value="13">Bank BTN</option><option value="14">Bank Niaga</option></select></div>
                        </div>
                        <div class="form-row my-2">
                            <div class="col custom-form-modal"><label class="col-form-label">Nilai</label></div>
                            <div class="col"><input class="form-control" type="number"></div>
                        </div>
                        <div class="form-row my-2">
                            <div class="col custom-form-modal"><label class="col-form-label">Angsuran</label></div>
                            <div class="col"><input class="form-control" type="number"></div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer"><button class="btn btn-light" type="button" data-dismiss="modal">Batal</button><button class="btn btn-primary" id="btn-simpan-angsuran" type="button">Simpan</button></div>
            </div>
        </div>
    </div>