
<div class="modal fade" role="dialog" tabindex="-1" id="modal-gaji-lain">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Gaji Lain-Lain</h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
            <div class="modal-body">
                <div class="row">
                    <div class="col">
                        <p>Gaji lain-lain untuk Muhammad Rizqi Nur (H12341234) periode Januari 2020.</p>
                    </div>
                </div>
                <form>
                    <div class="form-row">
                        <div class="col-3"><label class="col-form-label">Gaji Lain-Lain</label></div>
                        <div class="col"><input class="form-control" type="number"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer"><button class="btn btn-light" type="button" data-dismiss="modal">Batal</button><button class="btn btn-primary" type="button">Simpan</button></div>
        </div>
    </div>
</div>