
<div class="modal fade" role="dialog" tabindex="-1" id="modal-tambah-tkh">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Jabatan</h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
            <div class="modal-body">
                <form id="form-tambah-tkh">
                    <div class="form-row my-1">
                        <div class="col-3"><label class="col-form-label">Tunjangan</label></div>
                        <div class="col">
							<input class="form-control" type="text" name="NAMA_TKH">
						</div>
                    </div>
                    <div class="form-row">
                        <div class="col-3"><label class="col-form-label">Nilai</label></div>
                        <div class="col">
							<input class="form-control w-100" type="text" data-type="currency" name="NILAI_TKH">
						</div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
				<button class="btn btn-light" type="button" data-dismiss="modal">Batal</button>
				<button class="btn btn-primary" type="button" id="btn-submit-tambah-tkh">Simpan</button>
			</div>
        </div>
    </div>
</div>
