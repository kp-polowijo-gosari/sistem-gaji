<div class="modal fade" role="dialog" tabindex="-1" id="modal-add-subdept">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Subdepartemen</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
            <div class="modal-body">
                <form id="form-add-subdept">
                    <div class="form-row">
                        <div class="col-3"><label class="col-form-label">Nama Subdepartemen</label></div>
                        <div class="col"><input name="NAMA_SUBDEPT" class="form-control" type="text"></div>
                    </div>
                    <div class="form-row my-3">
                        <div class="col-3"><label class="col-form-label">Departemen</label></div>
                        <div class="col">
							<select class="form-control" name="ID_DEPT">
								<?php 
								$first = true;
								foreach($deptAktif as $row){
								?>
									<option value="<?=$row["ID_DEPT"];?>" <?php if($first){ echo " default selected"; $first = false; } ?> >
										<?=$row["NAMA_DEPT"];?>
									</option>
								<?php } ?>
							</select>
						</div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
				<button class="btn btn-light" type="button" data-dismiss="modal">Batal</button>
				<button id="btn-submit-add-subdept"  class="btn btn-primary" type="button">Simpan</button>
			</div>
        </div>
    </div>
</div>