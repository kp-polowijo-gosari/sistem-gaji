<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(CLASSES_DIR  . "Exceptions.php");

class BasicController {
    public function __construct($params=null, $parent=null) {
        $this->CI =& get_instance();
		$this->allowedRoles = null;
	}
	
	function findPerusahaan($result, $idPer){
		foreach($result as $row){
			if($row['ID_PER'] == $idPer)
				return $row['NAMA_PER'];
		}
		return null;
	}
	
	function baseData(){
		
		$periode = $this->CI->MPeriode->get($_SESSION["periode"]);
		$lastPeriode = $this->CI->MPeriode->getLast();
		$idPer = $_SESSION["perusahaan"];
		$perAktif = $this->CI->MPerusahaan->fetchAktif();
		$perTerpilih = $this->findPerusahaan($perAktif, $idPer);
		
		$data = array(
			"topNavData"=>array(
				"periode"=>$periode,
				"lastPeriode"=>$lastPeriode,
				"perTerpilih"=>$perTerpilih,
				"perAktif"=>$perAktif
			),
			"sideNavData"=>array(
			),
			"contentData"=>null
		);
		
		return $data;
	}
	function isLoggedIn(){
		return $this->CI->session->userdata("idUser");
	}

	function response($code, $message=null, $redirect=null){
		$response = array();
		if(is_bool($code)){
			$response["result"]=$code ? RESULT_OK : RESULT_FAIL;
		}else{
			$response["result"]=$code;
		}
		if($message){
			$response["message"] = $message;
		}
		if(is_bool($redirect)){
			$response["reload"] = $redirect;
		}else if (is_string($redirect)){
			$response["redirect"] = $redirect;
		}
		return $response;
	}


	function exceptionResponse($ex, $redirect = null){
		return $this->response($ex->getCode(), $ex->getMessage(), $redirect);
	}
	
	
	function checkPeriode($periode){
		$periode = $_SESSION["periode"];
		$lastPeriode = $this->CI->MPeriode->getLast();
		if($periode < $lastPeriode){
			throw new PerubahanPeriodeLampauException($priode);
		}
	}

	function checkPrivilege($requiredRole=null){
		if(!$this->isLoggedIn()){
			throw new NotLoggedInException();
		}
		if(!$requiredRole || count($requiredRole) == 0){
			return;
		}
		if(!in_array($_SESSION['role'], $requiredRole)){
			throw new InsufficientPrivilegeException();
		}
	}
	
	function loadPage($data){
		if(in_array("jsInlines", $data)){
			if(!is_array($data["jsInlines"])){
				$data["jsInlines"] = array($data["jsInlines"]);
			}
		}
        $this->CI->load->view('template', $data);
	}
	
	function setAllowedRoles($roles){
		$this->allowedRoles =& $roles;
	}
	
	function setModel($model){
		$this->model =& $model;
	}
	
	function setDenganIdPer($denganIdPer){
		$this->denganIdPer = $denganIdPer;
	}
	
	function validatePageRequest(){
		//request validation
		try{
			$this->checkPrivilege($this->allowedRoles);
			return true;
		}catch(NeedRedirectException $ex){
			redirect(base_url() . 'login');
		}catch(GeneralException $ex){
			show_error($ex->getMessage(), 403);
		}
	}
	
	function validateAPIRequest(){
		//request validation
		$periode = $_SESSION["periode"];
		try{
			$this->checkPrivilege($this->allowedRoles);
			$this->checkPeriode($periode);
			return true;
		}catch(GeneralException $ex){
			echo json_encode($this->exceptionResponse($ex));
		}
	}
	
	public function tambah($data, $echo=true){
		//request validation
		if(!$this->validateAPIRequest())
			return;
		
		//request processing
		return $this->prosesTambah($data, $echo);
	}
	
	public function hapus($data, $echo=true){
		$periode = $_SESSION["periode"];
		
		//request validation
		if(!$this->validateAPIRequest())
			return;
		
		//input validation
		$data = $this->input->post();
		
		//request processing
		return $this->prosesHapus($data, $echo);
	}
	public function kembalikan($data, $echo=true){
		$periode = $_SESSION["periode"];
		
		//request validation
		if(!$this->validateAPIRequest())
			return;
		
		//request processing
		return $this->prosesKembalikan($data, $echo);
	}
	public function ubah($data, $echo=true){
		$periode = $_SESSION["periode"];
		
		//request validation
		if(!$this->validateAPIRequest())
			return;
		
		//request processing
		return $this->prosesUbah($data, $echo);
	}


	public function prosesTambah($data, $echo=true){
		$periode = $_SESSION["periode"];
		
		//request processing
		$response = null;
		try{
			$keys = $this->model->tambah($periode, $data);
			$response = $this->response(0);
			$idPer = $_SESSION["perusahaan"];
			if($this->denganIdPer)
				$response["data"] = $this->model->getAktif($periode, $idPer, $keys);
			else
				$response["data"] = $this->model->getAktif($periode, $keys);
		}catch(GeneralException $ex){
			$response = $this->exceptionResponse($ex);
		}
		
		if($echo)
			echo json_encode($response);
		else
			return $response;
	}
	
	public function prosesHapus($data, $echo=true){
		return $this->proses($data, "hapus", false, $echo);
	}
	public function prosesKembalikan($data, $echo=true){
		return $this->proses($data, "kembalikan", true, $echo);
	}
	public function prosesUbah($data, $echo=true, $func="ubah"){
		return $this->proses($data, $func, true, $echo);
	}
	public function proses($data, $func, $getAktif=false, $echo=true){
		$periode = $_SESSION["periode"];
		
		//request processing
		$response = null;
		try{
			$this->model->$func($periode, $data);
			$response = $this->response(0);
			if($getAktif){
				$idPer = $_SESSION["perusahaan"];
				$keys = $this->model->getKeyValuesDict($data);
				if($this->denganIdPer)
					$response["data"] = $this->model->getAktif($periode, $idPer, $keys);
				else
					$response["data"] = $this->model->getAktif($periode, $keys);
			}
		}catch(GeneralException $ex){
			$response = $this->exceptionResponse($ex);
		}
		
		if($echo)
			echo json_encode($response);
		else
			return $response;
	}
}