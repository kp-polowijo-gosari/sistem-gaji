<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BasicCrudModel {
	
    public function __construct($params=null, $parent=null) {
        $this->CI =& get_instance();
		if($parent){
			$this->CI = $parent;
		}
		if($params){
			if(array_key_exists("PARENT", $params)){
				$this->CI = $params["PARENT"];
			}
			if(array_key_exists("ENTITY_LONG", $params)){
				$this->entityLong = $params["ENTITY_LONG"];
				$this->keyTable = snakeCaseLower($this->entityLong);
			}
			if(array_key_exists("ENTITY_SHORT", $params)){
				$this->entityShort = $params["ENTITY_SHORT"];
				$this->riwayatTable = "riwayat_" . snakeCaseLower($this->entityShort);
				$this->suffix = snakeCaseUpper($this->entityShort);
				$this->periodeMulaiCol = "PERIODE_MULAI_" . $this->suffix;
				$this->periodeAkhirCol = "PERIODE_AKHIR_" . $this->suffix;
				$this->keyCols = ["ID_" . $this->suffix];
			}
			if(array_key_exists("RIWAYAT_TABLE", $params)){
				$this->riwayatTable = $params["RIWAYAT_TABLE"];
			}
			if(array_key_exists("KEY_COLS", $params)){
				$this->keyCols = $params["KEY_COLS"];
				//KEY_COLS contains array of key column names
			}
			if(array_key_exists("UPDATE_COLS", $params)){
				$this->updateCols = $params["UPDATE_COLS"];
				//UPDATE_COLS contains array of names of columns that may be updated
			}
			if(array_key_exists("INSERT_COLS", $params)){
				$this->insertCols = $params["INSERT_COLS"];
				//INSERT_COLS contains array of names of columns that may be updated
			}
			if(array_key_exists("FK_COLS", $params)){
				$this->fkCols = $params["FK_COLS"];
				//FK_COLS contains array of arrays of FK details
				//Each array consists of: FK_COL and FK_EXCEPTION
				//FK_COL is the FK column names
				//FK_EXCEPTION is the exception that should be thrown in case of FK failure
				//FK_EXCEPTION should accept the FK value or array of FK values as constructor parameter
			}
			if(isset($this->keyCols) && count($this->keyCols) == 1){
				$this->keyCol = $this->keyCols[0];
			}
			if(array_key_exists("SUMBER_PENGEMBALIAN", $params)){
				$this->sumberPengembalian = $params["SUMBER_PENGEMBALIAN"];
			}
		}
    }
	
	function extractPeriode($data){
		$periode = (isset($this->periodeMulaiCol) && array_key_exists($this->periodeMulaiCol, $data)) ? $data[$this->periodeMulaiCol] : null;
		return $periode;
	}
	
	function insertIfExistsDoNothing($table, $data, $keyOnly=false, $entityKeyDesc=null){
		$periode = $this->extractPeriode($data);
		if(!$entityKeyDesc)
			$entityKeyDesc = $this->entityKeyDesc($this->riwayatTable, $periode, $this->getKeyValuesDict($data));
		$cols = null;
		if($keyOnly){
			$cols = $this->keyCols;
			$vals = $this->getKeyValues($data);
		}else{
			$cols = array_keys($data);
			$vals = filterDictValues($data, $cols);
		}
		$qmarks = array();
		foreach($cols as $col){
			array_push($qmarks, "?");
		}
		$cols = implode(", ", $cols);
		$qmarks = implode(", ", $qmarks);
		$sql = "
			INSERT INTO {$table} ({$cols}) 
			VALUES({$qmarks}) 
			ON DUPLICATE KEY UPDATE {$this->keyCols[0]}={$this->keyCols[0]}";
			
		$success = true;
		$query = $this->CI->db->query($sql, $vals);
		
		try{
			$this->checkDbError();
		}catch(DuplicateKeyException $ex){
			throw new DuplicateKeyException("{$entityKeyDesc} sudah ada.");
		}catch(ForeignKeyFailureException $ex){
			$this->checkPeriodeFailure($ex);
			//check FK failures for defined FK cols
			$this->checkFkFailure($ex, $data);
			//You may also catch ForeignKeyFailureException outside for more complex FK 
			throw $ex;
		}
		
		$success = $this->CI->db->affected_rows() > 0;
		
		return $success;
	}
	
	function insertIfNotExists($table, $data, $keyOnly=false, $entityKeyDesc=null){
		$periode = $this->extractPeriode($data);
		if(!$entityKeyDesc)
			$entityKeyDesc = $this->entityKeyDesc($this->riwayatTable, $periode, $this->getKeyValuesDict($data));
		$cols = null;
		if($keyOnly){
			$cols = $this->keyCols;
			$vals = $this->getKeyValues($data);
		}else{
			$cols = array_keys($data);
			$vals = filterDictValues($data, $cols);
		}
		$qmarks = array();
		foreach($cols as $col){
			array_push($qmarks, "?");
		}
		$cols = implode(", ", $cols);
		$qmarks = implode(", ", $qmarks);
		$keys = implode("=? AND ", $this->keyCols);
		$sql = "
			INSERT INTO {$table} ({$cols}) 
			SELECT {$qmarks}
			WHERE NOT EXISTS (
				SELECT * 
				FROM {$table} 
				WHERE {$keys}=?
			)";
			
		$success = true;
		$vals = array_merge($vals, $this->getKeyValues($data));
		$query = $this->CI->db->query($sql, $vals);
		
		try{
			$this->checkDbError();
		}catch(DuplicateKeyException $ex){
			throw new DuplicateKeyException("{$entityKeyDesc} sudah ada.");
		}catch(ForeignKeyFailureException $ex){
			$this->checkPeriodeFailure($ex);
			//check FK failures for defined FK cols
			$this->checkFkFailure($ex, $data);
			//You may also catch ForeignKeyFailureException outside for more complex FK 
			throw $ex;
		}
		
		$success = $this->CI->db->affected_rows() > 0;
		
		return $success;
	}
	
	function checkDbError(){
		$error = $this->CI->db->error();
		if(isset($error["code"]) && $error["code"]){
			$message = $error["message"];
			$code = $error["code"];
			switch($code){
				case 1062:{
					throw new DuplicateKeyException($message);
				}
				case 1452:{
					throw new ForeignKeyFailureException($message);
				}
				default:{
					throw new UnexpectedException($message, $code);
				}
			}
		}
		return true;
	}
	
	function fetchTable($table, $orderBy=null, $orderMode="ASC"){
		$order = "";
		if($orderBy){
			if(!is_array($orderBy)){
				$orderBy = array($orderBy);
			}
			$order = implode(" {$orderMode}, ", $orderBy);
			$order = " ORDER BY {$order} {$orderMode}";
		}
		$sql = "
			SELECT * 
			FROM {$table}
			{$order}";
		$query = $this->CI->db->query($sql);
		$result = $query->result_array();
		return $result;
	}
		
	function getTable($table, $data){
		
		$where = implode("=? AND ", $this->keyCols);
		
		$sql = "
			SELECT * FROM {$table}
			WHERE {$where}=?";
		$queryParams = $this->getKeyValues($data);
		$query = $this->CI->db->query($sql, $queryParams);
		$count = $query->num_rows();
		if($count == 0) return null;
		$row = $query->row_array();
		if(empty($row))return null;
		return $row;
	}
	function insertEmpty($table){
		$sql = "INSERT INTO {$table}() VALUES();";
		$query = $this->CI->db->query($sql);
		//idk what to catch
		$this->checkDbError();
		return $this->CI->db->insert_id();
	}
	function fetchView($view, $periode, $idPer, $orderBy=null, $orderMode="ASC"){
		$order = "";
		if($orderBy){
			if(!is_array($orderBy)){
				$orderBy = array($orderBy);
			}
			$order = implode(" {$orderMode}, v.", $orderBy);
			$order = " ORDER BY v.{$order} {$orderMode}";
		}
		$sql = "
			SELECT v.* FROM 
			( SELECT 
				@periode:=? periode,
				@id_per:=? id_per
			) param, 
			{$view} v
			{$order}";
		$query = $this->CI->db->query($sql, array(
			$periode,
			$idPer
		));
		$result = $query->result_array();
		return $result;
	}
	
	function fetchViewFiltered($view, $periode, $idPer, $filter, $orderBy=null, $orderMode="ASC"){
		$order = "";
		if($orderBy){
			if(!is_array($orderBy)){
				$orderBy = array($orderBy);
			}
			$order = implode(" {$orderMode}, v.", $orderBy);
			$order = " ORDER BY v.{$order} {$orderMode}";
		}
		$where = "";
		$values = array();
		if($filter && count($filter) > 0){
			$keys = array();
			foreach($filter as $key=>$value){
				array_push($keys, $key);
				array_push($values, $value);
			}
			$where = implode("=? AND ", $keys);
			$where = "WHERE {$where}=?";
		}
		$sql = "
			SELECT v.* FROM 
			( SELECT 
				@periode:=? periode,
				@id_per:=? id_per
			) param, 
			{$view} v
			{$where}
			{$order}";
		$queryParams = array(
			$periode,
			$idPer
		);
		$queryParams = array_merge($queryParams, $values);
		$query = $this->CI->db->query($sql, $queryParams);
		$result = $query->result_array();
		return $result;
	}
	
	function countViewEntry($view, $periode, $idPer, $filter=null){
		$where = "";
		$values = array();
		if($filter && count($filter) > 0){
			$keys = array();
			foreach($filter as $key=>$value){
				array_push($keys, $key);
				array_push($values, $value);
			}
			$where = implode("=? AND ", $keys);
			$where = "WHERE {$where}=?";
		}
		$sql = "
			SELECT COUNT(*) AS COUNT
			FROM 
			( SELECT 
				@periode:=? periode,
				@id_per:=? id_per
			) param, 
			{$view} v
			{$where}";
		$queryParams = array(
			$periode,
			$idPer
		);
		$queryParams = array_merge($queryParams, $values);
		$query = $this->CI->db->query($sql, $queryParams);
		$result = $query->row_array()["COUNT"];
		return $result;
	}
	
	function getView($view, $periode, $idPer, $data){
		
		$where = implode("=? AND ", $this->keyCols);
		
		$sql = "
			SELECT v.* FROM 
			( SELECT 
				@periode:=? periode,
				@id_per:=? id_per
			) param, 
			{$view} v
			WHERE {$where}=?";
		$queryParams = array(
			$periode,
			$idPer
		);
		$queryParams = array_merge($queryParams, $this->getKeyValues($data));
		$query = $this->CI->db->query($sql, $queryParams);
		$count = $query->num_rows();
		if($count == 0) return null;
		$row = $query->row_array();
		if(empty($row))return null;
		return $row;
	}
	
	
	function entityKeyDesc($entity, $periode, $id){
		if($entity){
			$entity = $this->entityLong;
		}
		if(is_array($id)){
			if(isAssoc($id)){
				$id = json_encode($id);
			}else{
				$id = "ID " . json_encode($id);
			}
		}else{
			$id = "ID " . $id;
		}
		return "{$entity} dengan {$id} untuk periode {$periode}";
	}
	
	function getKeyValues($data){
		return filterDictValues($data, $this->keyCols);
	}
	
	function getKeyValuesDict($data){
		return filterDict($data, $this->keyCols);
	}
	
	function getUpdateValues($data){
		return filterDictValues($data, $this->updateCols);
	}
	
	function getInsertValues($data){
		return filterDictValues($data, $this->insertCols);
	}
	
	function checkFkFailure($ex, $data){
		$msg = $ex->getMessage();
		if(isset($this->fkCols)){
			foreach($this->fkCols as $fkCol){
				$fkColName = $fkCol["FK_COL"];
				if(is_array($fkColName)){
					foreach($fkColName as $fkColNameI){
						if(stringContains($msg, $fkColNameI)){
							throw new $fkCol["FK_EXCEPTION"]($data[$fkColName]);
						}
					}
				}else{
					if(stringContains($msg, $fkColName)){
						throw new $fkCol["FK_EXCEPTION"]($data[$fkColName]);
					}
				}
			}
		}
	}
	
	function checkPeriodeFailure($ex){
		$msg = $ex.getMessage();
		if(isset($this->periodeMulaiCol) && stringContains($msg, $this->periodeMulaiCol)){
			throw new PeriodeInvalidException($data[$this->basicmodeltmk->periodeMulaiCol]);
		}
		if(isset($this->periodeAkhirCol) && stringContains($msg, $this->periodeAkhirCol)){
			throw new PeriodeInvalidException($data[$this->periodeAkhirCol]);
		}
	}
	
	function tambah($periode, $data, $standalone=true, $entityKeyDesc=null){
		
		$data[$this->periodeMulaiCol] = $periode;
		
		if($standalone){
			$this->CI->db->trans_start();
		}
		if(isset($this->keyCol) && empty($data[$this->keyCol])){
			$data[$this->keyCol] = $this->insertEmpty($this->keyTable);
		}
		
		if(!$entityKeyDesc)
			$entityKeyDesc = $this->entityKeyDesc($this->riwayatTable, $periode, $this->getKeyValuesDict($data));
		
		$this->CI->db->insert($this->riwayatTable, $data);
		try{
			$this->checkDbError();
		}catch(DuplicateKeyException $ex){
			throw new DuplicateKeyException("{$entityKeyDesc} sudah ada.");
		}catch(ForeignKeyFailureException $ex){
			$this->checkPeriodeFailure($ex);
			//check FK failures for defined FK cols
			$this->checkFkFailure($ex, $data);
			//You may also catch ForeignKeyFailureException outside for more complex FK 
			throw $ex;
		}
		$success = $this->CI->db->affected_rows() > 0;
		if(!$success){
			throw new UnknownException("Penambahan {$entityKeyDesc} gagal tapi tidak ada error");
		}
		
		$this->CI->db->trans_complete();
		
		return $this->getKeyValuesDict($data);
	}
	
	
	function ubah($periode, $data, $standalone=true, $entityKeyDesc=null){
		if(!$entityKeyDesc)
			$entityKeyDesc = $this->entityKeyDesc($this->riwayatTable, $periode, $this->getKeyValuesDict($data));
		
		$data[$this->periodeMulaiCol] = $periode;
		
		if($standalone)
			$this->CI->db->trans_start();
		
		$success = $this->hapus($periode, $data, false, $entityKeyDesc);
		
		if($success){
			$success = $this->tambah($periode, $data, false, $entityKeyDesc) > 0;
		}else{
			$queryUpdate = implode("=?, ", $this->updateCols);
			$queryKeys = implode("=? AND ", $this->keyCols);
			$sql = "
				UPDATE {$this->riwayatTable} 
				SET {$queryUpdate}=?
				WHERE {$this->periodeMulaiCol} = ? 
					AND {$this->periodeAkhirCol} IS NULL
					AND {$queryKeys}=?";
			$queryParams = $this->getUpdateValues($data);
			array_push($queryParams, $data[$this->periodeMulaiCol]);
			$queryParams = array_merge($queryParams, $this->getKeyValues($data));
			$params = json_encode($queryParams);
			$query = $this->CI->db->query($sql, $queryParams);
			try{
				$this->checkDbError();
			}catch(ForeignKeyFailureException $ex){
				//check FK failures for defined FK cols
				$this->checkFkFailure($ex, $data);
				throw $ex;
			}
			$success = $this->CI->db->affected_rows() > 0;
			if(!$success){
				throw new EntryNotFoundException("{$entityKeyDesc} tidak ditemukan (1)");
			}
		}
		
		
		if($standalone){
			if($success){
				$this->CI->db->trans_complete();
			}
		}
		return $success;
	}
	
	function hapus($periode, $data, $standalone=true, $entityKeyDesc=null){
		if(!$entityKeyDesc)
			$entityKeyDesc = $this->entityKeyDesc($this->riwayatTable, $periode, $this->getKeyValuesDict($data));
		
		$data[$this->periodeMulaiCol] = $periode;
		
		if($standalone){
			$this->CI->db->trans_start();
		}
		$where = implode("=? AND ", $this->keyCols);
		
		$deleteThisMonth = "";
		if($standalone){
			$deleteThisMonth = "=";
		}
		
		$sql = "
			UPDATE {$this->riwayatTable}
			SET {$this->periodeAkhirCol} = ?
			WHERE {$this->periodeMulaiCol} <{$deleteThisMonth} ? 
				AND {$this->periodeAkhirCol} IS NULL
				AND {$where}=?";
		$queryParams = array(
			$periode,
			$periode
		);
		$queryParams = array_merge($queryParams, $this->getKeyValues($data));
		$query = $this->CI->db->query($sql, $queryParams);
		
		try{
			$this->checkDbError();
		}catch(ForeignKeyFailureException $ex){
			$this->checkPeriodeFailure($ex);
			//This only "deletes" an entry for a period of time, so no other FK check required other than periode
			throw $ex;
		}
		
		$success = $this->CI->db->affected_rows() > 0;
		
		if($standalone){
			if($success){
				$this->CI->db->trans_complete();
			}else{
				throw new EntryNotFoundException("{$entityKeyDesc} tidak ditemukan (2)");
			}
		}
		
		return $success;
	}
	
	function kembalikan($periode, $data, $standalone=true, $entityKeyDesc=null){
		if(!$entityKeyDesc)
			$entityKeyDesc = $this->entityKeyDesc($this->riwayatTable, $periode, $this->getKeyValuesDict($data));
		
		if($standalone)
			$this->CI->db->trans_start();
		
		$queryKeys = implode("=? AND ", $this->keyCols);
		
		$sql = "
			UPDATE {$this->riwayatTable}
			SET {$this->periodeAkhirCol} = NULL
			WHERE {$this->periodeMulaiCol} = (
					SELECT MAX({$this->periodeMulaiCol})
					FROM (SELECT * FROM {$this->riwayatTable}) r0
					WHERE {$this->periodeMulaiCol} <= ?
					AND {$this->periodeAkhirCol} = ?
					AND {$queryKeys}=?
				)
				AND {$this->periodeAkhirCol} = ?
				AND {$queryKeys}=?";
		$queryParams = array(
			$periode,
			$periode
		);
		$keyValues = $this->getKeyValues($data);
		$queryParams = array_merge($queryParams, $keyValues);
		array_push($queryParams, $periode);
		$queryParams = array_merge($queryParams, $keyValues);
		$query = $this->CI->db->query($sql, $queryParams);
		
		//nothing to catch rn
		$this->checkDbError();
			
		$success = $this->CI->db->affected_rows() > 0;
		
		if(!$success){
			
			$data = $this->getView($this->sumberPengembalian, $periode, null, $data); 
			
			if($data){
				$success = $this->tambah($periode, $data) > 0;
			}else{
				throw new EntryNotFoundException("{$entityKeyDesc} tidak ditemukan (3)");
			}
		}
		
		if($standalone){
			if($success){
				$this->CI->db->trans_complete();
			}
		}
		return $success;
	}
}
?>