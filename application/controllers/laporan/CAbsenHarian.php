<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CAbsenHarian extends CI_Controller {
    public function __construct() {
        parent::__construct();
		//$this->load->model("MAbsen");
		$this->allowedRoles = array(
			ROLE_PENGAWAS,
			ROLE_ADMIN_BIASA,
			ROLE_ADMIN_UTAMA,
			ROLE_SUPER_ADMIN
		);
		$this->basiccontroller->setAllowedRoles($this->allowedRoles);
		//$this->basiccontroller->setModel($this->MAbsen);
		$this->basiccontroller->setDenganIdPer(false);
    }


    public function index() {
		//request validation
		if(!$this->basiccontroller->validatePageRequest())
			return;
		
		$data = $this->basiccontroller->baseData();
		
		$periode = $_SESSION["periode"];
		$idPer = $_SESSION["perusahaan"];
		
		$contentData = array();
		
		$data["title"] = "Laporan Absen Harian";
		$data["content"] = "laporan/absen-harian/content";
		$data["contentData"] = $contentData;
		$data["breadcrumbs"] = array(
			array(
				"url"=>base_url(),
				"text"=>"Beranda"
			),
			array(
				"text"=>"Laporan"
			),
			array(
				"url"=>base_url("laporan/absen"),
				"text"=>"Absen"
			),
			array(
				"text"=>"Harian Full"
			)
		);
		$data["modals"] = array(
			//"modal/modal-tambah-absen"
		);
		$data["jsFiles"] = array(
			base_url("assets/js/angsuran/functions-detail-absen.js")
		);
		$data["jsInlines"] = array(
			//"laporan/absen-harian/js"
		);
		
		$this->basiccontroller->loadPage($data);
    }

}
?>