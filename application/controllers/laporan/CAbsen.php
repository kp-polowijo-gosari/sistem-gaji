<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CAbsen extends CI_Controller {
    public function __construct() {
        parent::__construct();
		//$this->load->model("MAbsen");
		$this->allowedRoles = array(
			ROLE_PENGAWAS,
			ROLE_ADMIN_BIASA,
			ROLE_ADMIN_UTAMA,
			ROLE_SUPER_ADMIN
		);
		$this->basiccontroller->setAllowedRoles($this->allowedRoles);
		//$this->basiccontroller->setModel($this->MAbsen);
		$this->basiccontroller->setDenganIdPer(false);
    }

    public function index() {
		//request validation
		if(!$this->basiccontroller->validatePageRequest())
			return;
		
		$data = $this->basiccontroller->baseData();
		
		$periode = $_SESSION["periode"];
		$idPer = $_SESSION["perusahaan"];
		
		$contentData = array();
		
		$data["title"] = "Laporan Absen";
		$data["content"] = "laporan/absen/content";
		$data["contentData"] = $contentData;
		$data["breadcrumbs"] = array(
			array(
				"url"=>base_url(),
				"text"=>"Beranda"
			),
			array(
				"text"=>"Laporan"
			),
			array(
				"text"=>"Absen"
			)
		);
		$data["modals"] = array(
			//"modal/modal-tambah-absen"
		);
		$data["jsFiles"] = array(
			base_url("assets/js/angsuran/functions-absen.js")
		);
		$data["jsInlines"] = array(
			//"laporan/absen/js"
		);
		
		$this->basiccontroller->loadPage($data);
    }
}
?>