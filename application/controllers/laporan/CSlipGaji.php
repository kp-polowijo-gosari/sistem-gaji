<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CSlipGaji extends CI_Controller {
    public function __construct() {
        parent::__construct();
		//$this->load->model("MGaji");
		$this->allowedRoles = array(
			ROLE_PENGAWAS,
			ROLE_ADMIN_UTAMA,
			ROLE_SUPER_ADMIN
		);
		$this->basiccontroller->setAllowedRoles($this->allowedRoles);
		//$this->basiccontroller->setModel($this->MGaji);
		$this->basiccontroller->setDenganIdPer(false);
    }

    public function index() {
		//request validation
		if(!$this->basiccontroller->validatePageRequest())
			return;
		
		$data = $this->basiccontroller->baseData();
		
		$periode = $_SESSION["periode"];
		$idPer = $_SESSION["perusahaan"];
		
		$contentData = array();
		
		$data["title"] = "Slip Gaji";
		$data["content"] = "laporan/slip-gaji/content";
		$data["contentData"] = $contentData;
		$data["breadcrumbs"] = array(
			array(
				"url"=>base_url(),
				"text"=>"Beranda"
			),
			array(
				"text"=>"Laporan"
			),
			array(
				"text"=>"Slip Gaji"
			)
		);
		$data["modals"] = array(
			//"modal/modal-gaji-lain"
		);
		$data["jsFiles"] = array(
			base_url("assets/js/laporan/functions-slip-gaji.js")
		);
		$data["jsInlines"] = array(
			//"laporan/slip-gaji/js"
		);
		
		$this->basiccontroller->loadPage($data);
    }
    public function cetak() {
		//request validation
		if(!$this->basiccontroller->validatePageRequest())
			return;
		$this->load->view("laporan/cetak-slip-gaji");
    }

}
?>