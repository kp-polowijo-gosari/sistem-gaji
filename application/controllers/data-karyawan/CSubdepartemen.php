<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CSubdepartemen extends CI_Controller {
    public function __construct() {
        parent::__construct();
		$this->load->model("MSubdepartemen");
		$this->load->model("MDepartemen");
		$this->allowedRoles = array(
			ROLE_ADMIN_BIASA,
			ROLE_ADMIN_UTAMA,
			ROLE_SUPER_ADMIN
		);
		$this->basiccontroller->setAllowedRoles($this->allowedRoles);
		$this->basiccontroller->setModel($this->MSubdepartemen);
		$this->basiccontroller->setDenganIdPer(false);
    }


	//subdepartemen
	//subdept
    public function index() {
		//request validation
		if(!$this->basiccontroller->validatePageRequest())
			return;
		
		$data = $this->basiccontroller->baseData();
		
		$periode = $_SESSION["periode"];
		$idPer = $_SESSION["perusahaan"];
		
		$contentData = array();
		$contentData["subdeptAktif"] = $this->MSubdepartemen->fetchAktif($periode,$idPer);
		$contentData["subdeptDihapus"] = $this->MSubdepartemen->fetchDihapus($periode, $idPer);
		$contentData["deptAktif"] = $this->MDepartemen->fetchPilihanAktif($periode,$idPer);
		
		$data["title"] = "Subdepartemen - Data Karyawan";
		$data["content"] = "data-karyawan/subdepartemen/content";
		$data["contentData"] = $contentData;
		$data["breadcrumbs"] = array(
			array(
				"url"=>base_url(),
				"text"=>"Beranda"
			),
			array(
				"text"=>"Data Karyawan"
			),
			array(
				"text"=>"Subdepartemen"
			)
		);
		$data["modals"] = array(
			"modal/modal-tambah-subdepartemen"
		);
		$data["jsFiles"] = array(
			base_url("assets/js/data-karyawan/functions-subdepartemen.js")
		);
		$data["jsInlines"] = array(
			"data-karyawan/subdepartemen/js"
		);
		
		$this->basiccontroller->loadPage($data);
    }
	
	public function tambah(){
		$periode = $_SESSION["periode"];
		
		//request validation
		if(!$this->basiccontroller->validateAPIRequest())
			return;
		
		//input validation
		$data = $this->input->post();
		
		//request processing
		$this->basiccontroller->prosesTambah($data);
		
	}
	
	public function hapus(){
		$periode = $_SESSION["periode"];
		
		//request validation
		if(!$this->basiccontroller->validateAPIRequest())
			return;
		
		//input validation
		$data = $this->input->post();
		
		//request processing
		$this->basiccontroller->prosesHapus($data);
	}
	public function kembalikan(){
		$periode = $_SESSION["periode"];
		
		//request validation
		if(!$this->basiccontroller->validateAPIRequest())
			return;
		
		//input validation
		$data = $this->input->post();
		
		//request processing
		$this->basiccontroller->prosesKembalikan($data);
	}
	public function ubah(){
		$periode = $_SESSION["periode"];
		
		//request validation
		if(!$this->basiccontroller->validateAPIRequest())
			return;
		
		//input validation
		$data = $this->input->post();
		
		//request processing
		$this->basiccontroller->prosesUbah($data);
	}

}
?>