<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CDepartemen extends CI_Controller {
    public function __construct() {
        parent::__construct();
		$this->load->model("MDepartemen");
		$this->allowedRoles = array(
			ROLE_ADMIN_BIASA,
			ROLE_ADMIN_UTAMA,
			ROLE_SUPER_ADMIN
		);
		$this->basiccontroller->setAllowedRoles($this->allowedRoles);
		$this->basiccontroller->setModel($this->MDepartemen);
		$this->basiccontroller->setDenganIdPer(false);
    }

	//departemen
	//dept
    public function index() {
		//request validation
		try{
			$this->basiccontroller->checkPrivilege($this->allowedRoles);
		}catch(NeedRedirectException $ex){
			redirect(base_url() . 'login');
			return;
		}catch(GeneralException $ex){
			show_error($ex->getMessage(), 403);
			return;
		}
		
		$data = $this->basiccontroller->baseData();
		
		$periode = $_SESSION["periode"];
		$idPer = $_SESSION["perusahaan"];
		
		
		$contentData = array();
		$contentData["deptAktif"] = $this->MDepartemen->fetchAktif($periode,$idPer);
		$contentData["deptDihapus"] = $this->MDepartemen->fetchDihapus($periode, $idPer);
		
		
		$data["title"] = "Departemen - Data Karyawan";
		$data["content"] = "data-karyawan/departemen/content";
		$data["contentData"] = $contentData;
		$data["breadcrumbs"] = array(
			array(
				"url"=>base_url(),
				"text"=>"Beranda"
			),
			array(
				"text"=>"Data Karyawan"
			),
			array(
				"text"=>"Departemen"
			)
		);
		$data["modals"] = array(
			"modal/modal-tambah-departemen"
		);
		$data["jsFiles"] = array(
			base_url("assets/js/data-karyawan/functions-departemen.js")
		);
		$data["jsInlines"] = array(
			"data-karyawan/departemen/js"
		);
		
		$this->basiccontroller->loadPage($data);
    }
	
	public function tambah(){
		$periode = $_SESSION["periode"];
		
		//request validation
		if(!$this->basiccontroller->validateAPIRequest())
			return;
		
		//input validation
		$data = $this->input->post();
		
		//request processing
		$this->basiccontroller->prosesTambah($data);
		
	}
	
	public function hapus(){
		$periode = $_SESSION["periode"];
		
		//request validation
		if(!$this->basiccontroller->validateAPIRequest())
			return;
		
		//input validation
		$data = $this->input->post();
		
		//request processing
		$this->basiccontroller->prosesHapus($data);
	}
	public function kembalikan(){
		$periode = $_SESSION["periode"];
		
		//request validation
		if(!$this->basiccontroller->validateAPIRequest())
			return;
		
		//input validation
		$data = $this->input->post();
		
		//request processing
		$this->basiccontroller->prosesKembalikan($data);
	}
	public function ubah(){
		$periode = $_SESSION["periode"];
		
		//request validation
		if(!$this->basiccontroller->validateAPIRequest())
			return;
		
		//input validation
		$data = $this->input->post();
		
		//request processing
		$this->basiccontroller->prosesUbah($data);
	}
}
?>