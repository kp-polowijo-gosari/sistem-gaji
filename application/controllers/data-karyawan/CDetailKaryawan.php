<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CDetailKaryawan extends CI_Controller {
    public function __construct() {
        parent::__construct();
		$this->load->model("MKaryawan");
		$this->load->model("MJabatan");
		$this->load->model("MSubdepartemen");
		$this->allowedRoles = array(
			ROLE_ADMIN_BIASA,
			ROLE_ADMIN_UTAMA,
			ROLE_SUPER_ADMIN
		);
		$this->basiccontroller->setAllowedRoles($this->allowedRoles);
		$this->basiccontroller->setModel($this->MKaryawan);
		$this->basiccontroller->setDenganIdPer(false);
    }

    public function index($idKar) {
		//request validation
		if(!$this->basiccontroller->validatePageRequest())
			return;
		
		$data = $this->basiccontroller->baseData();
		
		$periode = $_SESSION["periode"];
		$idPer = $_SESSION["perusahaan"];
		
		$contentData = array();
		$keys = array("ID_KAR"=>$idKar);
		$kar = $this->MKaryawan->get($periode, $keys);
		
		$contentData["karyawan"] = $kar;
			
		$contentData["statusKaryawan"] = $this->MKaryawan->fetchStatusKaryawan();
		$contentData["jenisKelamin"] = $this->MKaryawan->fetchJenisKelamin();
		$contentData["agama"] = $this->MKaryawan->fetchAgama();
		$contentData["pendidikan"] = $this->MKaryawan->fetchPendidikan();
		
		$data["title"] = "Detail Karyawan - Data Karyawan";
		$data["content"] = "data-karyawan/detail-karyawan/content";
		$data["contentData"] = $contentData;
		$data["breadcrumbs"] = array(
			array(
				"url"=>base_url(),
				"text"=>"Beranda"
			),
			array(
				"text"=>"Data Karyawan"
			),
			array(
				"url"=>base_url("karyawan/karyawan"),
				"text"=>"Karyawan"
			),
			array(
				"text"=>"Detail"
			)
		);
		$data["modals"] = array(
			array(
				"modal"=>"modal/modal-pilih-jabatan",
				"data"=>array("jabatan"=>$this->MJabatan->fetchPilihanAktif($periode))
			),
			array(
				"modal"=>"modal/modal-pilih-subdept",
				"data"=>array("subdept"=>$this->MSubdepartemen->fetchPilihanAktif($periode, $idPer))
			)
		);
		$data["jsFiles"] = array(
			base_url("assets/js/data-karyawan/functions-karyawan.js")
		);
		$data["jsInlines"] = array(
			"data-karyawan/detail-karyawan/js"
		);
		
		$this->basiccontroller->loadPage($data);
    }

	
	public function ubah(){
		$periode = $_SESSION["periode"];
		
		//request validation
		if(!$this->basiccontroller->validateAPIRequest())
			return;
		
		//input validation
		$data = $this->input->post();
		
		//request processing
		$this->basiccontroller->prosesUbah($data);
	}
}
?>