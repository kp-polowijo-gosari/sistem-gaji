<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CKaryawan extends CI_Controller {
    public function __construct() {
        parent::__construct();
		$this->load->model("MKaryawan");
		$this->allowedRoles = array(
			ROLE_ADMIN_BIASA,
			ROLE_ADMIN_UTAMA,
			ROLE_SUPER_ADMIN
		);
		$this->basiccontroller->setAllowedRoles($this->allowedRoles);
		$this->basiccontroller->setModel($this->MKaryawan);
		$this->basiccontroller->setDenganIdPer(false);
    }

	//karyawan
	
    public function index() {
		//request validation
		if(!$this->basiccontroller->validatePageRequest())
			return;
		
		$data = $this->basiccontroller->baseData();
		
		$periode = $_SESSION["periode"];
		$idPer = $_SESSION["perusahaan"];
		
		$contentData = array();
		$contentData["karyawanAktif"] = $this->MKaryawan->fetchAktif($periode, $idPer);
		$contentData["karyawanDihapus"] = $this->MKaryawan->fetchDihapus($periode, $idPer);
		
		$data["title"] = "Karyawan - Data Karyawan";
		$data["content"] = "data-karyawan/karyawan/content";
		$data["contentData"] = $contentData;
		$data["breadcrumbs"] = array(
			array(
				"url"=>base_url(),
				"text"=>"Beranda"
			),
			array(
				"text"=>"Data Karyawan"
			),
			array(
				"text"=>"Karyawan"
			)
		);
		$data["jsFiles"] = array(
			base_url("assets/js/data-karyawan/functions-karyawan.js")
		);
		$data["jsInlines"] = array(
			"data-karyawan/karyawan/js"
		);
		
		$this->basiccontroller->loadPage($data);
    }
	
	public function hapus(){
		$periode = $_SESSION["periode"];
		
		//request validation
		if(!$this->basiccontroller->validateAPIRequest())
			return;
		
		//input validation
		$data = $this->input->post();
		
		//request processing
		$response = $this->basiccontroller->prosesHapus($data, false);
		if($response["result"] == 0)
			$response["redirect"] = base_url("karyawan/karyawan");
		echo json_encode($response);
	}
	public function kembalikan(){
		$periode = $_SESSION["periode"];
		
		//request validation
		if(!$this->basiccontroller->validateAPIRequest())
			return;
		
		//input validation
		$data = $this->input->post();
		
		//request processing
		$response = $this->basiccontroller->prosesKembalikan($data, false);
		if($response["result"] == 0)
			$response["redirect"] = base_url("karyawan/karyawan");
		echo json_encode($response);
	}
}
?>