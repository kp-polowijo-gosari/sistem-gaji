<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CJobLevel extends CI_Controller {
    public function __construct() {
        parent::__construct();
		$this->load->model("MJobLevel");
		$this->allowedRoles = array(
			ROLE_ADMIN_BIASA,
			ROLE_ADMIN_UTAMA,
			ROLE_SUPER_ADMIN
		);
		$this->basiccontroller->setAllowedRoles($this->allowedRoles);
		$this->basiccontroller->setModel($this->MJobLevel);
		$this->basiccontroller->setDenganIdPer(true);
    }

	//job level

    public function index() {
		//request validation
		if(!$this->basiccontroller->validatePageRequest())
			return;
		
		$data = $this->basiccontroller->baseData();
		
		$periode = $_SESSION["periode"];
		$idPer = $_SESSION["perusahaan"];
		
		$contentData = array();
		$contentData["jobAktif"] = $this->MJobLevel->fetchAktif($periode, $idPer);
		$contentData["jobDihapus"] = $this->MJobLevel->fetchDihapus($periode, $idPer);
		
		$data["title"] = "Job Level - Data Karyawan";
		$data["content"] = "data-karyawan/job-level/content";
		$data["contentData"] = $contentData;
		$data["breadcrumbs"] = array(
			array(
				"url"=>base_url(),
				"text"=>"Beranda"
			),
			array(
				"text"=>"Data Karyawan"
			),
			array(
				"text"=>"Job Level"
			)
		);
		$data["modals"] = array(
			"modal/modal-tambah-job-level"
		);
		$data["jsFiles"] = array(
			base_url("assets/js/data-karyawan/functions-job-level.js")
		);
		$data["jsInlines"] = array(
			"data-karyawan/job-level/js"
		);
		
		$this->basiccontroller->loadPage($data);
    }

	
	public function tambah(){
		$periode = $_SESSION["periode"];
		
		//request validation
		if(!$this->basiccontroller->validateAPIRequest())
			return;
		
		//input validation
		$data = $this->input->post();
		
		//request processing
		$this->basiccontroller->prosesTambah($data);
		
	}
	
	public function hapus(){
		$periode = $_SESSION["periode"];
		
		//request validation
		if(!$this->basiccontroller->validateAPIRequest())
			return;
		
		//input validation
		$data = $this->input->post();
		
		//request processing
		$this->basiccontroller->prosesHapus($data);
	}
	public function kembalikan(){
		$periode = $_SESSION["periode"];
		
		//request validation
		if(!$this->basiccontroller->validateAPIRequest())
			return;
		
		//input validation
		$data = $this->input->post();
		
		//request processing
		$this->basiccontroller->prosesKembalikan($data);
	}
	public function ubah(){
		$periode = $_SESSION["periode"];
		
		//request validation
		if(!$this->basiccontroller->validateAPIRequest())
			return;
		
		//input validation
		$data = $this->input->post();
		
		//request processing
		$this->basiccontroller->prosesUbah($data);
	}
}
?>