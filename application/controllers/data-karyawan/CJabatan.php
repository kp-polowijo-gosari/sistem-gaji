<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CJabatan extends CI_Controller {
    public function __construct() {
        parent::__construct();
		$this->load->model("MJabatan");
		$this->load->model("MJobLevel");
		$this->allowedRoles = array(
			ROLE_ADMIN_BIASA,
			ROLE_ADMIN_UTAMA,
			ROLE_SUPER_ADMIN
		);
		$this->basiccontroller->setAllowedRoles($this->allowedRoles);
		$this->basiccontroller->setModel($this->MJabatan);
		$this->basiccontroller->setDenganIdPer(true);
    }

	//jabatan
	
    public function index() {
		//request validation
		if(!$this->basiccontroller->validatePageRequest())
			return;
		
		$data = $this->basiccontroller->baseData();
		
		$periode = $_SESSION["periode"];
		$idPer = $_SESSION["perusahaan"];
		
		$contentData = array();
		$contentData["jabatanAktif"] = $this->MJabatan->fetchAktif($periode,$idPer);
		$contentData["jabatanDihapus"] = $this->MJabatan->fetchDihapus($periode, $idPer);
		$contentData["jobAktif"] = $this->MJobLevel->fetchPilihanAktif($periode);
		
		$data["title"] = "Jabatan - Data Karyawan";
		$data["content"] = "data-karyawan/jabatan/content";
		$data["contentData"] = $contentData;
		$data["breadcrumbs"] = array(
			array(
				"url"=>base_url(),
				"text"=>"Beranda"
			),
			array(
				"text"=>"Data Karyawan"
			),
			array(
				"text"=>"Jabatan"
			)
		);
		$data["modals"] = array(
			"modal/modal-tambah-jabatan"
		);
		$data["jsFiles"] = array(
			base_url("assets/js/data-karyawan/functions-jabatan.js")
		);
		$data["jsInlines"] = array(
			"data-karyawan/jabatan/js"
		);
		
		$this->basiccontroller->loadPage($data);
    }
	public function tambah(){
		//request validation
		if(!$this->basiccontroller->validateAPIRequest())
			return;
		
		//input validation
		$data = $this->input->post();
		
		//request processing
		$this->basiccontroller->prosesTambah($data);
		
	}
	
	public function hapus(){
		//request validation
		if(!$this->basiccontroller->validateAPIRequest())
			return;
		
		//input validation
		$data = $this->input->post();
		
		//request processing
		$this->basiccontroller->prosesHapus($data);
	}
	public function kembalikan(){
		//request validation
		if(!$this->basiccontroller->validateAPIRequest())
			return;
		
		//input validation
		$data = $this->input->post();
		
		//request processing
		$this->basiccontroller->prosesKembalikan($data);
	}
	public function ubah(){
		//request validation
		if(!$this->basiccontroller->validateAPIRequest())
			return;
		
		//input validation
		$data = $this->input->post();
		
		//request processing
		$this->basiccontroller->prosesUbah($data);
	}

}
?>