<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CPengaturan extends CI_Controller {
    public function __construct() {
        parent::__construct();
		//$this->load->model("MPengaturan");
		$this->allowedRoles = array(
			ROLE_ADMIN_BIASA,
			ROLE_ADMIN_UTAMA,
			ROLE_SUPER_ADMIN
		);
		$this->basiccontroller->setAllowedRoles($this->allowedRoles);
		//$this->basiccontroller->setModel($this->MPengaturan);
		$this->basiccontroller->setDenganIdPer(false);
    }
	public function index(){
		//request validation
		if(!$this->basiccontroller->validatePageRequest())
			return;
		
		$data = $this->basiccontroller->baseData();
		
		$periode = $_SESSION["periode"];
		$idPer = $_SESSION["perusahaan"];
		
		$contentData = array();
		
		$data["title"] = "Pengaturan";
		$data["content"] = "pengaturan/content";
		$data["contentData"] = $contentData;
		$data["breadcrumbs"] = array(
			array(
				"url"=>base_url(),
				"text"=>"Beranda"
			),
			array(
				"text"=>"Pengaturan"
			)
		);
		$data["modals"] = array(
			//"modal/modal-tambah-angsuran"
		);
		$data["jsFiles"] = array(
			base_url("assets/js/functions-pengaturan.js")
		);
		$data["jsInlines"] = array(
			//"pengaturan/js"
		);
		
		$this->basiccontroller->loadPage($data);
	}
}
?>