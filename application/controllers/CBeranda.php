<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CBeranda extends CI_Controller {
    public function __construct() {
        parent::__construct();
    }
    public function index() {
		try{
			$this->basiccontroller->checkPrivilege();
		}catch(NeedRedirectException $ex){
			redirect(base_url() . 'login');
			return;
		}catch(GeneralException $ex){
			show_error($ex->getMessage(), 403);
			return;
		}
		
		$data = $this->basiccontroller->baseData();
		$periode = $_SESSION["periode"];
		$idPer = $_SESSION["perusahaan"];
		
		$data["title"] = "Beranda";
		$data["content"] = "beranda";
		$data["breadcrumbs"] = array(
			array(
				"text"=>"Beranda"
			)
		);
				
		
		$this->basiccontroller->loadPage($data);
    }
}
?>