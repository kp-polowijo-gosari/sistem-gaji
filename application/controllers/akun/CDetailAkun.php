<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CDetailAkun extends CI_Controller {
    public function __construct() {
        parent::__construct();
		$this->load->model("MAkun");
		$this->allowedRoles = array(
			ROLE_ADMIN_AKUN,
			ROLE_SUPER_ADMIN
		);
		$this->basiccontroller->setAllowedRoles($this->allowedRoles);
		$this->basiccontroller->setModel($this->MAkun);
		$this->basiccontroller->setDenganIdPer(false);
    }

	public function index($idAkun){
		//request validation
		if(!$this->basiccontroller->validatePageRequest())
			return;
		
		$data = $this->basiccontroller->baseData();
		
		$periode = $_SESSION["periode"];
		$idPer = $_SESSION["perusahaan"];
		
		$contentData = array();
		
		$data["title"] = "Akun";
		$data["content"] = "akun/detail-akun/content";
		$data["contentData"] = $contentData;
		$data["breadcrumbs"] = array(
			array(
				"url"=>base_url(),
				"text"=>"Beranda"
			),
			array(
				"url"=>base_url("akun"),
				"text"=>"Akun"
			),
			array(
				"text"=>"Detail"
			)
		);
		$data["modals"] = array(
			"modal/modal-tambah-akun"
		);
		$data["jsFiles"] = array(
			base_url("assets/js/akun/functions-detail-akun.js")
		);
		$data["jsInlines"] = array(
			//"akun/detail-akun/js"
		);
		
		$this->basiccontroller->loadPage($data);
	}
}
?>