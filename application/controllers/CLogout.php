<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CLogout extends CI_Controller {
    public function __construct() {
        parent::__construct();
    }

	public function index(){
		$this->logout();
	}
	
	public function logout(){
		unset(
			$_SESSION['idUser'],
			$_SESSION['nama'],
			$_SESSION['role']
		);
		session_destroy();
		redirect(base_url());
	}
}
?>