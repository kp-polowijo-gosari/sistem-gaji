<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CPotongan extends CI_Controller {
    public function __construct() {
        parent::__construct();
		$this->allowedRoles = array(
			ROLE_ADMIN_UTAMA,
			ROLE_SUPER_ADMIN
		);
		$this->basiccontroller->setAllowedRoles($this->allowedRoles);
		//$this->basiccontroller->setModel($this->MJabatan);
		$this->basiccontroller->setDenganIdPer(false);
    }

    public function index() {
		//request validation
		if(!$this->basiccontroller->validatePageRequest())
			return;
		
		$data = $this->basiccontroller->baseData();
		
		$periode = $_SESSION["periode"];
		$idPer = $_SESSION["perusahaan"];
		
		$contentData = array();
		
		$data["title"] = "Potongan - Angsuran";
		$data["content"] = "angsuran/potongan/content";
		$data["contentData"] = $contentData;
		$data["breadcrumbs"] = array(
			array(
				"url"=>base_url(),
				"text"=>"Beranda"
			),
			array(
				"text"=>"Angsuran"
			),
			array(
				"text"=>"Potongan"
			)
		);
		$data["modals"] = array(
			//"modal/modal-tambah-potongan"
		);
		$data["jsFiles"] = array(
			base_url("assets/js/angsuran/functions-potongan.js")
		);
		$data["jsInlines"] = array(
			//"angsuran/potongan/js"
		);
		
		$this->basiccontroller->loadPage($data);
    }
}
?>