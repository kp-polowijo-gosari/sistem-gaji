<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CAngsuran extends CI_Controller {

    
    public function __construct() {
        parent::__construct();
		//$this->load->model("MAngsuran");
		$this->allowedRoles = array(
			ROLE_ADMIN_UTAMA,
			ROLE_SUPER_ADMIN
		);
		$this->basiccontroller->setAllowedRoles($this->allowedRoles);
		//$this->basiccontroller->setModel($this->MAngsuran);
		$this->basiccontroller->setDenganIdPer(false);
    }
    public function index() {
		//request validation
		if(!$this->basiccontroller->validatePageRequest())
			return;
		
		$data = $this->basiccontroller->baseData();
		
		$periode = $_SESSION["periode"];
		$idPer = $_SESSION["perusahaan"];
		
		$contentData = array();
		
		$data["title"] = "Angsuran";
		$data["content"] = "angsuran/angsuran/content";
		$data["contentData"] = $contentData;
		$data["breadcrumbs"] = array(
			array(
				"url"=>base_url(),
				"text"=>"Beranda"
			),
			array(
				"text"=>"Angsuran"
			),
			array(
				"text"=>"Angsuran"
			)
		);
		$data["modals"] = array(
			"modal/modal-tambah-angsuran",
			"modal/modal-pilih-karyawan"
		);
		$data["jsFiles"] = array(
			base_url("assets/js/angsuran/functions-angsuran.js")
		);
		$data["jsInlines"] = array(
			//"angsuran/angsuran/js"
		);
		
		$this->basiccontroller->loadPage($data);
    }

}

?>