<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CProfil extends CI_Controller {
    public function __construct() {
        parent::__construct();
		$this->load->model("MAkun");
		//$this->basiccontroller->setModel($this->MAkun);
		$this->basiccontroller->setDenganIdPer(false);
    }

	public function index (){
		//request validation
		try{
			$this->basiccontroller->checkPrivilege();
		}catch(NeedRedirectException $ex){
			redirect(base_url() . 'login');
			return;
		}catch(GeneralException $ex){
			show_error($ex->getMessage(), 403);
			return;
		}
		
		$data = $this->basiccontroller->baseData();
		
		$periode = $_SESSION["periode"];
		$idPer = $_SESSION["perusahaan"];
		
		$contentData = array();
		
		$data["title"] = "Profil";
		$data["content"] = "profil/content";
		$data["breadcrumbs"] = array(
			array(
				"url"=>base_url(),
				"text"=>"Beranda"
			),
			array(
				"text"=>"Profil"
			)
		);
		$data["modals"] = array(
			//"modal/modal-tambah-angsuran"
		);
		$data["jsFiles"] = array(
			base_url("assets/js/functions-profil.js")
		);
		$data["jsInlines"] = array(
			//"profil/js"
		);
		
		$this->basiccontroller->loadPage($data);
	}

}
?>