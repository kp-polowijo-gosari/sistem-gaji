<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CJobLevel extends CI_Controller {
    public function __construct() {
        parent::__construct();
		$this->load->model("MJobLevel");
		$this->allowedRoles = array(
			ROLE_ADMIN_UTAMA,
			ROLE_SUPER_ADMIN
		);
		$this->basiccontroller->setAllowedRoles($this->allowedRoles);
		$this->basiccontroller->setModel($this->MJobLevel);
		$this->basiccontroller->setDenganIdPer(true);
    }

    public function index() {
		//request validation
		if(!$this->basiccontroller->validatePageRequest())
			return;
		
		$data = $this->basiccontroller->baseData();
		
		$periode = $_SESSION["periode"];
		$idPer = $_SESSION["perusahaan"];
		
		$contentData = array();
		$contentData["jobBaru"] = $this->MJobLevel->fetchBaru($periode);
		$contentData["jobLama"] = $this->MJobLevel->fetchLama($periode);
		$contentData["jobSekarang"] = $this->MJobLevel->fetchSekarang($periode);
		
		$data["title"] = "Job Level - Data Gaji";
		$data["content"] = "data-gaji/job-level/content";
		$data["contentData"] = $contentData;
		$data["breadcrumbs"] = array(
			array(
				"url"=>base_url(),
				"text"=>"Beranda"
			),
			array(
				"text"=>"Data Gaji"
			),
			array(
				"text"=>"Job Level"
			)
		);
		$data["jsFiles"] = array(
			base_url("assets/js/data-gaji/functions-job-level.js")
		);
		
		$this->basiccontroller->loadPage($data);
    }

}
?>