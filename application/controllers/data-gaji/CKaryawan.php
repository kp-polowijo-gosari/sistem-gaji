<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CKaryawan extends CI_Controller {
    public function __construct() {
        parent::__construct();
		$this->load->model("MGajiKaryawan");
		$this->allowedRoles = array(
			ROLE_ADMIN_UTAMA,
			ROLE_SUPER_ADMIN
		);
		$this->basiccontroller->setAllowedRoles($this->allowedRoles);
		$this->basiccontroller->setModel($this->MGajiKaryawan);
		$this->basiccontroller->setDenganIdPer(false);
    }

    public function index() {
		//request validation
		if(!$this->basiccontroller->validatePageRequest())
			return;
		
		$data = $this->basiccontroller->baseData();
		
		$periode = $_SESSION["periode"];
		$idPer = $_SESSION["perusahaan"];
		
		$contentData = array();
		$contentData["karBelum"] = array(
			array(
				"ID_KAR"=>1,
				"NIK"=>"123123",
				"NAMA_KAR"=>"Budi",
				"NAMA_JABATAN"=>"Manajer HRD",
				"NAMA_JOB"=>"Manajer",
				"NAMA_SUBDEPT"=>"HRD",
				"NAMA_DEPT"=>"HRD",
				"GAJI_DILAPORKAN"=>null,
				"ID_STATUS_KOP"=>null,
				"KINERJA"=>null,
				"GAJI_LAIN"=>null
			)
		);
		$contentData["karSudah"] = array(
			array(
				"ID_KAR"=>1,
				"NIK"=>"123123",
				"NAMA_KAR"=>"Budi",
				"NAMA_JABATAN"=>"Manajer HRD",
				"NAMA_JOB"=>"Manajer",
				"NAMA_SUBDEPT"=>"HRD",
				"NAMA_DEPT"=>"HRD",
				"GAJI_DILAPORKAN"=>6000000,
				"ID_STATUS_KOP"=>1,
				"KINERJA"=>"A",
				"GAJI_LAIN"=>0
			)
		);
		$contentData["statusKop"] = array(
			array(
				"ID_SKO"=>1,
				"NAMA_SKO"=>"-"
			),
			array(
				"ID_SKO"=>2,
				"NAMA_SKO"=>"Iuran"
			),
			array(
				"ID_SKO"=>3,
				"NAMA_SKO"=>"Daftar"
			)
		);
		$contentData["kinerja"] = array(
			array(
				"KINERJA"=>"A"
			),
			array(
				"KINERJA"=>"B"
			),
			array(
				"KINERJA"=>"C"
			)
		);
		
		$data["title"] = "Karyawan - Data Gaji";
		$data["content"] = "data-gaji/karyawan/content";
		$data["contentData"] = $contentData;
		$data["breadcrumbs"] = array(
			array(
				"url"=>base_url(),
				"text"=>"Beranda"
			),
			array(
				"text"=>"Data Gaji"
			),
			array(
				"text"=>"Karyawan"
			)
		);
		$data["modals"] = array(
		);
		$data["jsFiles"] = array(
			base_url("assets/js/data-gaji/functions-karyawan.js")
		);
		$data["jsInlines"] = array(
			"data-gaji/karyawan/js"
		);
		
		$this->basiccontroller->loadPage($data);
    }
}
?>