<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CTunjanganKhusus extends CI_Controller {
    public function __construct() {
        parent::__construct();
		$this->load->model("MTunjanganKhusus");
		$this->allowedRoles = array(
			ROLE_ADMIN_UTAMA,
			ROLE_SUPER_ADMIN
		);
		$this->basiccontroller->setAllowedRoles($this->allowedRoles);
		$this->basiccontroller->setModel($this->MTunjanganKhusus);
		$this->basiccontroller->setDenganIdPer(true);
    }

    public function index() {
		//request validation
		if(!$this->basiccontroller->validatePageRequest())
			return;
		
		$data = $this->basiccontroller->baseData();
		
		$periode = $_SESSION["periode"];
		$idPer = $_SESSION["perusahaan"];
		
		$contentData = array();
		$contentData["tkhAktif"] = $this->MTunjanganKhusus->fetchAktif($periode, $idPer);
		$contentData["tkhDihapus"] = $this->MTunjanganKhusus->fetchDihapus($periode, $idPer);
		$contentData["jabatanAktif"] = $this->MTunjanganKhusus->fetchJabatanAktif($periode, $idPer);
		
		$data["title"] = "Tunjangan Khusus - Data Gaji";
		$data["content"] = "data-gaji/tunjangan-khusus/content";
		$data["contentData"] = $contentData;
		$data["breadcrumbs"] = array(
			array(
				"url"=>base_url(),
				"text"=>"Beranda"
			),
			array(
				"text"=>"Data Gaji"
			),
			array(
				"text"=>"Tunjangan Khusus"
			)
		);
		$data["modals"] = array(
			"modal/modal-tambah-tunjangan-khusus"
		);
		$data["jsFiles"] = array(
			base_url("assets/js/data-gaji/functions-tkh.js")
		);
		$data["jsInlines"] = array(
			"data-gaji/tunjangan-khusus/js"
		);
		
		$this->basiccontroller->loadPage($data);
    }

	public function tambah(){
		//request validation
		if(!$this->basiccontroller->validateAPIRequest())
			return;
		
		//input validation
		$data = $this->input->post();
		
		//request processing
		$this->basiccontroller->prosesTambah($data);
		
	}
	
	public function hapus(){
		//request validation
		if(!$this->basiccontroller->validateAPIRequest())
			return;
		
		//input validation
		$data = $this->input->post();
		
		//request processing
		$this->basiccontroller->prosesHapus($data);
	}
	public function kembalikan(){
		//request validation
		if(!$this->basiccontroller->validateAPIRequest())
			return;
		
		//input validation
		$data = $this->input->post();
		
		//request processing
		$this->basiccontroller->prosesKembalikan($data);
	}
	public function ubah(){
		//request validation
		if(!$this->basiccontroller->validateAPIRequest())
			return;
		
		//input validation
		$data = $this->input->post();
		
		//request processing
		$this->basiccontroller->prosesUbah($data);
	}
	
}
?>