<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CDetailLembur extends CI_Controller {
    public function __construct() {
        parent::__construct();
		$this->allowedRoles = array(
			ROLE_ADMIN_UTAMA,
			ROLE_SUPER_ADMIN
		);
		$this->basiccontroller->setAllowedRoles($this->allowedRoles);
		//$this->basiccontroller->setModel($this->MJabatan);
		$this->basiccontroller->setDenganIdPer(false);
    }

    public function index($idKar) {
		//request validation
		if(!$this->basiccontroller->validatePageRequest())
			return;
		
		$data = $this->basiccontroller->baseData();
		
		$periode = $_SESSION["periode"];
		$idPer = $_SESSION["perusahaan"];
		
		$contentData = array();
		
		$data["title"] = "Detail Lembur Harian - Data Gaji";
		$data["content"] = "data-gaji/detail-lembur/content";
		$data["contentData"] = $contentData;
		$data["breadcrumbs"] = array(
			array(
				"url"=>base_url(),
				"text"=>"Beranda"
			),
			array(
				"text"=>"Data Gaji"
			),
			array(
				"url"=>base_url("gaji/lembur"),
				"text"=>"Lembur"
			),
			array(
				"text"=>"Harian"
			)
		);
		$data["modals"] = array(
			"modal/modal-tambah-lembur-harian"
		);
		$data["jsFiles"] = array(
			//base_url("assets/js/data-gaji/functions-lembur.js")
		);
		$data["jsInlines"] = array(
			//"data-gaji/detail-lembur/js"
		);
		
		$this->basiccontroller->loadPage($data);
    }
}
?>