<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CDetailJobLevel extends CI_Controller {
    public function __construct() {
        parent::__construct();
		$this->load->model("MJobLevel");
		$this->load->model("MTunjanganKinerja");
		$this->load->model("MUpahLembur");
		$this->load->model("MTunjanganMasaKerja");
		$this->allowedRoles = array(
			ROLE_ADMIN_BIASA,
			ROLE_ADMIN_UTAMA,
			ROLE_SUPER_ADMIN
		);
		$this->basiccontroller->setAllowedRoles($this->allowedRoles);
		//$this->basiccontroller->setModel($this->MJabatan);
		$this->basiccontroller->setDenganIdPer(false);
    }
    public function index($idJob) {
		//request validation
		if(!$this->basiccontroller->validatePageRequest())
			return;
		
		$data = $this->basiccontroller->baseData();
		
		$periode = $_SESSION["periode"];
		$idPer = $_SESSION["perusahaan"];
		
		$keys = array("ID_JOB"=>$idJob);
		$contentData = array();
		$contentData["job"] = $this->MJobLevel->getFull($periode, $keys);
		$contentData["tunjanganKinerja"] = $this->MTunjanganKinerja->fetchAll($periode, $keys);
		$contentData["tunjanganMasaKerja"] = $this->MTunjanganMasaKerja->fetchAktif($periode, $keys);
		$contentData["upahLembur"] = $this->MUpahLembur->fetchAll($periode, $keys);
		
		$data["title"] = "Detail Job Level - Data Gaji";
		$data["content"] = "data-gaji/detail-job-level/content";
		$data["contentData"] = $contentData;
		$data["breadcrumbs"] = array(
			array(
				"url"=>base_url(),
				"text"=>"Beranda"
			),
			array(
				"text"=>"Data Gaji"
			),
			array(
				"url"=>base_url("gaji/job-level"),
				"text"=>"Job Level"
			),
			array(
				"text"=>"Detail"
			)
		);
		$data["modals"] = array(
			"modal/modal-tambah-job-level"
		);
		$data["jsFiles"] = array(
			base_url("assets/js/data-gaji/functions-job-level.js")
		);
		
		$this->basiccontroller->loadPage($data);
    }
	public function simpanGaji(){
		//request validation
		if(!$this->basiccontroller->validateAPIRequest())
			return;
		
		$periode = $_SESSION["periode"];
		
		//input validation
		$data = $this->input->post();
		
		$this->basiccontroller->setModel($this->MJobLevel);
		$this->basiccontroller->proses($data, "simpanGaji", false, true);
	}
	
	public function simpanTunjanganKinerja(){
		//request validation
		if(!$this->basiccontroller->validateAPIRequest())
			return;
		
		$periode = $_SESSION["periode"];
		
		//input validation
		$data = $this->input->post();
		
		$this->basiccontroller->setModel($this->MTunjanganKinerja);
		$this->basiccontroller->proses($data, "simpan", false, true);
	}
	public function simpanTunjanganMasaKerja(){
		//request validation
		if(!$this->basiccontroller->validateAPIRequest())
			return;
		
		$periode = $_SESSION["periode"];
		
		//input validation
		$data = $this->input->post();
		
		$this->basiccontroller->setModel($this->MTunjanganMasaKerja);
		$this->basiccontroller->proses($data, "simpan", false, true);
	}
	public function simpanUpahLembur(){
		//request validation
		if(!$this->basiccontroller->validateAPIRequest())
			return;
		
		$periode = $_SESSION["periode"];
		
		//input validation
		$data = $this->input->post();
		
		$this->basiccontroller->setModel($this->MUpahLembur);
		$this->basiccontroller->proses($data, "simpan", false, true);
	}
	
	public function simpan($type){
		switch($type){
			case "job":
			case "gaji":{
				$this->simpanGaji();
				break;
			}
			case "tki":{
				$this->simpanTunjanganKinerja();
				break;
			}
			case "tmk":{
				$this->simpanTunjanganMasaKerja();
				break;
			}
			case "ul":{
				$this->simpanUpahLembur();
				break;
			}
			default:{
				show_404();
			}
		}
	}
}
?>