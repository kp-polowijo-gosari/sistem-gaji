<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CAbsen extends CI_Controller {
    public function __construct() {
        parent::__construct();
		$this->allowedRoles = array(
			ROLE_ADMIN_UTAMA,
			ROLE_SUPER_ADMIN
		);
		$this->basiccontroller->setAllowedRoles($this->allowedRoles);
		//$this->basiccontroller->setModel($this->MJabatan);
		$this->basiccontroller->setDenganIdPer(false);
    }
    public function index() {
		//request validation
		if(!$this->basiccontroller->validatePageRequest())
			return;
		
		$data = $this->basiccontroller->baseData();
		
		$periode = $_SESSION["periode"];
		$idPer = $_SESSION["perusahaan"];
		
		$contentData = array();
		
		$data["title"] = "Absensi - Data Gaji";
		$data["content"] = "data-gaji/absen/content";
		$data["contentData"] = $contentData;
		$data["breadcrumbs"] = array(
			array(
				"url"=>base_url(),
				"text"=>"Beranda"
			),
			array(
				"text"=>"Data Gaji"
			),
			array(
				"text"=>"Absen"
			)
		);
		$data["modals"] = array(
			"modal/modal-tambah-absen",
			"modal/modal-pilih-karyawan"
		);
		$data["jsFiles"] = array(
			//base_url("assets/js/data-gaji/functions-absen.js")
		);
		$data["jsInlines"] = array(
			//"data-gaji/absen/js"
		);
		
		$this->basiccontroller->loadPage($data);
    }

}
?>