<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(CLASSES_DIR  . "Exceptions.php");

class CTkhJabatan extends CI_Controller {
    public function __construct() {
        parent::__construct();
		$this->load->model("MJabatan");
		$this->load->model("MTunjanganKhusus");
		$this->load->model("MTkhJabatan");
		$this->allowedRoles = array(
			ROLE_ADMIN_UTAMA,
			ROLE_SUPER_ADMIN
		);
		$this->basiccontroller->setAllowedRoles($this->allowedRoles);
		$this->basiccontroller->setModel($this->MTkhJabatan);
		$this->basiccontroller->setDenganIdPer(false);
    }

    public function index($idJabatan) {
		//request validation
		if(!$this->basiccontroller->validatePageRequest())
			return;
		
		$data = $this->basiccontroller->baseData();
		
		$periode = $_SESSION["periode"];
		$idPer = $_SESSION["perusahaan"];
		
		$contentData = array();
		$key = array("ID_JABATAN"=>$idJabatan);
		$contentData["jabatan"] = $this->MJabatan->getAktif($periode, $idPer, $key);
		$contentData["tkhjAktif"] = $this->MTkhJabatan->fetchAktif($periode, $key);
		$contentData["tkhjDihapus"] = $this->MTkhJabatan->fetchDihapus($periode, $key);
		
		$data["title"] = "Tunjangan Khusus Jabatan - Data Gajji";
		$data["content"] = "data-gaji/tkh-jabatan/content";
		$data["contentData"] = $contentData;
		$data["breadcrumbs"] = array(
			array(
				"url"=>base_url(),
				"text"=>"Beranda"
			),
			array(
				"text"=>"Data Gaji"
			),
			array(
				"url"=>base_url("gaji/tunjangan-khusus"),
				"text"=>"Tunjangan Khusus"
			),
			array(
				"text"=>"Jabatan"
			)
		);
		$data["modals"] = array(
			array(
				"modal"=>"modal/modal-pilih-tkh",
				"data"=>array("tkh"=>$this->MTunjanganKhusus->fetchAktif($periode, $idPer))
			)
		);
		$data["jsFiles"] = array(
			base_url("assets/js/data-gaji/functions-tkhj.js")
		);
		$data["jsInlines"] = array(
			"data-gaji/tkh-jabatan/js"
		);
		$data["jsInlines2"] = array(
			"data-gaji/tkh-jabatan/js2"
		);
		
		$this->basiccontroller->loadPage($data);
    }
	
	
	public function tambah($idJabatan){
		//request validation
		if(!$this->basiccontroller->validateAPIRequest())
			return;
		
		//input validation
		$data = $this->input->post();
		$data["ID_JABATAN"] = $idJabatan;
		
		//request processing
		$response = $this->basiccontroller->prosesTambah($data, false);
		if($response["result"] == DuplicateKeyError){
			$periode = $_SESSION["periode"];
			if($this->MTkhJabatan->dihapusExists($periode, $data["ID_JABATAN"], $data["ID_TKH"])){
				try{
					throw new EntryExistsButDeletedException($periode, $data);
				}catch(EntryExistsButDeletedException $ex){
					$response = $this->basiccontroller->exceptionResponse($ex);
				}
			}
		}
		
		echo json_encode($response);
	}
	
	public function hapus($idJabatan){
		//request validation
		if(!$this->basiccontroller->validateAPIRequest())
			return;
		
		//input validation
		$data = $this->input->post();
		$data["ID_JABATAN"] = $idJabatan;
		
		//request processing
		$this->basiccontroller->prosesHapus($data);
	}
	public function kembalikan($idJabatan){
		//request validation
		if(!$this->basiccontroller->validateAPIRequest())
			return;
		
		//input validation
		$data = $this->input->post();
		$data["ID_JABATAN"] = $idJabatan;
		
		//request processing
		$this->basiccontroller->prosesKembalikan($data);
	}
}
?>