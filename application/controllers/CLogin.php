<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CLogin extends CI_Controller {

	 
    public function __construct() {
        parent::__construct();
		$this->load->model("MAkun");
    }

	 
	public function index()
	{
		if($this->basiccontroller->isLoggedIn()){
			redirect(base_url());
			return;
		}
		$this->load->view('login');
	}
	public function login(){
		if($this->basiccontroller->isLoggedIn()){
			$response = result(0);
			$response["data"] = $this->MAkun->getAkun($_SESSION["idUser"]);
			echo json_encode($response);
			return;
		}
		
		//TODO capitalize & js too
		$username = $this->input->post("username");
		$password = $this->input->post("password");
		try{
			$akun = $this->MAkun->login($username, $password);
			$lastPeriode = $this->MPeriode->getLast();
			$firstPer = $this->MPerusahaan->getFirst();
			
			$_SESSION['idUser'] = $akun["ID_USER"];
			$_SESSION['nama'] = $akun["NAMA_USER"];
			$_SESSION['role'] = $akun["ID_PU"];
			$_SESSION['periode'] = $lastPeriode;
			$_SESSION['perusahaan'] = $firstPer;
			
			$response = $this->basiccontroller->response(0);
			
			echo json_encode($response);
		}catch(GeneralException $ex){
			echo json_encode($this->basiccontroller->exceptionResponse($ex));
			return;
		}
		
	}
}
?>