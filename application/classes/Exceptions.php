<?php
require_once("CustomException.php");

//low level errors
class GeneralException extends CustomException{
    public function __construct($message = null, $code = 101)
    {
        parent::__construct($message, $code);
    }
}
class UnexpectedException extends GeneralException{
    public function __construct($message = null, $code = 102)
    {
        parent::__construct($message, $code);
    }
}
class UnknownException extends GeneralException{
    public function __construct($message = null, $code = 103)
    {
        parent::__construct($message, $code);
    }
}
define("DuplicateKeyError", 104);
class DuplicateKeyException extends GeneralException{
    public function __construct($message = null, $code = 104)
    {
        parent::__construct($message, $code);
    }
}
class EntryNotFoundException extends GeneralException{
    public function __construct($message = null, $code = 105)
    {
        parent::__construct($message, $code);
    }
}
class ForeignKeyFailureException extends GeneralException{
    public function __construct($message = null, $code = 106)
    {
        parent::__construct($message, $code);
    }
}
class NeedRedirectException extends GeneralException{
    public function __construct($message = null, $code = 107)
    {
        parent::__construct($message, $code);
    }
}
//invalid FK
class PeriodeInvalidException extends GeneralException{
    public function __construct($periode=null, $code=201)
    {
        parent::__construct("Periode {$periode} tidak valid", $code);
    }
}
class PerusahaanInvalidException extends GeneralException{
    public function __construct($key=null, $code=202)
    {
        parent::__construct("ID Perusahaan {$key} tidak valid", $code);
    }
}
class JobInvalidException extends GeneralException{
    public function __construct($key=null, $code=203)
    {
        parent::__construct("ID Job Level {$key} tidak valid", $code);
    }
}
class DeptInvalidException extends GeneralException{
    public function __construct($key=null, $code=204)
    {
        parent::__construct("ID Departemen {$key} tidak valid", $code);
    }
}
class JabatanInvalidException extends GeneralException{
    public function __construct($key=null, $code=205)
    {
        parent::__construct("ID Jabatan {$key} tidak valid", $code);
    }
}
class SubdeptInvalidException extends GeneralException{
    public function __construct($key=null, $code=206)
    {
        parent::__construct("ID Subdepartemen {$key} tidak valid", $code);
    }
}
class StatusKarInvalidException extends GeneralException{
    public function __construct($key=null, $code=207)
    {
        parent::__construct("ID Status Karyawan {$key} tidak valid", $code);
    }
}
class AgamaInvalidException extends GeneralException{
    public function __construct($key=null, $code=208)
    {
        parent::__construct("ID Agama {$key} tidak valid", $code);
    }
}
class JenisKelaminInvalidException extends GeneralException{
    public function __construct($key=null, $code=209)
    {
        parent::__construct("ID Jenis Kelamin {$key} tidak valid", $code);
    }
}
class PendidikanInvalidException extends GeneralException{
    public function __construct($key=null, $code=210)
    {
        parent::__construct("ID Pendidikan {$key} tidak valid", $code);
    }
}
class TkhInvalidException extends GeneralException{
    public function __construct($key=null, $code=211)
    {
        parent::__construct("ID Tunjangan Khusus {$key} tidak valid", $code);
    }
}
class KinerjaInvalidException extends GeneralException{
    public function __construct($key=null, $code=212)
    {
        parent::__construct("Kinerja {$key} tidak valid", $code);
    }
}

class JamLemburInvalidException extends GeneralException{
    public function __construct($key=null, $code=213)
    {
        parent::__construct("Jam Lembur {$key} tidak valid", $code);
    }
}

class KarInvalidException extends GeneralException{
    public function __construct($key=null, $code=214)
    {
        parent::__construct("ID Karyawan {$key} tidak valid", $code);
    }
}
//policy violation
class NotLoggedInException extends NeedRedirectException{
    public function __construct($message="Anda harus login terlebih dahulu.", $code=301)
    {
        parent::__construct("Anda harus login terlebih dahulu", $code);
    }
}
class InsufficientPrivilegeException extends GeneralException{
    public function __construct($action="melakukan ini", $code=302)
    {
        parent::__construct("Anda tidak berhak {$action}.", $code);
    }
}
class PerubahanPeriodeLampauException extends GeneralException{
    public function __construct($periode=null, $code=303)
    {
        parent::__construct("Anda tidak boleh mengubah data pada periode yang telah lampau ({$periode})", $code);
    }
}
//model violation
class EntryExistsButDeletedException extends GeneralException{
    public function __construct($periode=null, $code=401)
    {
        parent::__construct("Anda tidak boleh mengubah data pada periode yang telah lampau ({$periode})", $code);
    }
}
//invalid input
?>