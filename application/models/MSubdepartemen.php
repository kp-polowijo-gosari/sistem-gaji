<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(CLASSES_DIR  . "Exceptions.php");
require_once(LIBRARIES_DIR  . "BasicCrudModel.php");

class MSubdepartemen extends CI_Model {
    public function __construct() {
        parent::__construct();
		$params = array(
			"ENTITY_LONG"=>"Subdepartemen",
			"ENTITY_SHORT"=>"Subdept",
			"KEY_COLS"=>array(
				"ID_SUBDEPT"
			),
			"UPDATE_COLS"=>array(
				"ID_DEPT",
				"NAMA_SUBDEPT"
			),
			"FK_COLS"=>array(
				array(
					"FK_COL"=>"ID_DEPT",
					"FK_EXCEPTION"=>DeptInvalidException::class
				)
			),
			"SUMBER_PENGEMBALIAN"=>"subdept_dihapus_pure"
		);
		$this->basicmodel = new BasicCrudModel($params);
    }
	
	function getKeyValuesDict($data){
		return $this->basicmodel->getKeyValuesDict($data);
	}
	//subdepartemen
	//subdept
	
	function fetchPilihanAktif($periode, $idPer){
		return $this->basicmodel->fetchView("subdept_aktif", $periode, $idPer, "ID_SUBDEPT");
	}
	
	function fetchAktif($periode, $idPer){
		return $this->basicmodel->fetchView("subdept_aktif_row", $periode, $idPer, "ID_SUBDEPT");
	}
	
	
	function getAktif($periode, $data){
		return $this->basicmodel->getView("subdept_aktif_row", $periode, null,$data);
	}
	
	function fetchDihapus($periode, $idPer){
		return $this->basicmodel->fetchView("subdept_dihapus_row", $periode, $idPer, "ID_SUBDEPT");
	}
	
	function tambah($periode, $data){
		return $this->basicmodel->tambah($periode, $data);
	}
	
	
	function ubah($periode, $data){
		return $this->basicmodel->ubah($periode, $data);
	}
	
	function hapus($periode, $data){
		return $this->basicmodel->hapus($periode, $data);
	}
	
	function kembalikan($periode, $data){
		return $this->basicmodel->kembalikan($periode, $data);
	}
}
?>