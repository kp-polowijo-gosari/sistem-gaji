<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


require_once(CLASSES_DIR  . "Exceptions.php");
require_once(LIBRARIES_DIR  . "BasicCrudModel.php");

class MAkun extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
	function getAkun($idUser){
		$sql = "
			SELECT u.ID_USER, u.NAMA_USER, u.ID_PU, u.NAMA_PU 
			FROM user_aktif u 
			WHERE u.ID_USER=?;";
		$query = $this->db->query($sql, array($idUser));
		$count = $query->num_rows();
		$success = $count == 1;
		if (!$success){
			throw new EntryNotFoundException("Username atau password salah");
		}
		
		return $query->row_array();
	}
	function login($username, $password){
 
		$sql = "
			SELECT u.ID_USER, u.NAMA_USER, u.ID_PU, u.NAMA_PU 
			FROM user_aktif u 
			WHERE u.USERNAME=? AND u.PASSWORD_USER=?;";
		$query = $this->db->query($sql, array($username, $password));
		$count = $query->num_rows();
		$success = $count == 1;
		if (!$success){
			throw new EntryNotFoundException("Username atau password salah");
		}
		
		return $query->row_array();
	}
	
}