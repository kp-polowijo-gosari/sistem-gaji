<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(CLASSES_DIR  . "Exceptions.php");
require_once(LIBRARIES_DIR  . "BasicCrudModel.php");

class MJobLevel extends CI_Model {
    public function __construct() {
        parent::__construct();
		$params = array(
			"ENTITY_LONG"=>"Job Level",
			"ENTITY_SHORT"=>"Job",
			"KEY_COLS"=>array(
				"ID_JOB"
			),
			"UPDATE_COLS"=>array(
				"NAMA_JOB"
			),
			"SUMBER_PENGEMBALIAN"=>"job_dihapus_pure"
		);
		$this->basicmodel = new BasicCrudModel($params);
		$params2 = array(
			"ENTITY_LONG"=>"Job Level",
			"ENTITY_SHORT"=>"Job",
			"KEY_COLS"=>array(
				"ID_JOB"
			),
			"UPDATE_COLS"=>array(
				"GAJI_POKOK",
				"TUNJANGAN_JABATAN",
				"NAMA_JOB"
			),
			"SUMBER_PENGEMBALIAN"=>"job_dihapus_pure"
		);
		$this->basicmodelgaji = new BasicCrudModel($params2);
		$params3 = array(
			"ENTITY_LONG"=>"Tunjangan Masa Kerja",
			"ENTITY_SHORT"=>"Tmk",
			"KEY_COLS"=>array(
				"ID_TMK"
			),
			"FK_COLS"=>array(
				array(
					"FK_COL"=>"ID_JOB",
					"FK_EXCEPTION"=>JobInvalidException::class
				)
			),
			//Later, if any
			//"SUMBER_PENGEMBALIAN"=>"job_dihapus_pure"
		);
		$this->basicmodeltmk = new BasicCrudModel($params3);
    }
	
	function getKeyValuesDict($data){
		return $this->basicmodel->getKeyValuesDict($data);
	}
	//job level
	
	
	function fetchPilihanAktif($periode){
		return $this->basicmodel->fetchView("job_aktif", $periode, null, "ID_JOB");
	}
	
	function fetchAktif($periode, $idPer){
		return $this->basicmodel->fetchView("job_aktif_row", $periode, $idPer, "ID_JOB");
	}
	
	
	function getAktif($periode, $idPer, $data){
		return $this->basicmodel->getView("job_aktif_row", $periode, $idPer, $data);
	}
	
	function fetchDihapus($periode, $idPer){
		return $this->basicmodel->fetchView("job_dihapus_row", $periode, $idPer, "ID_JOB");
	}
	
	function tambahTmk($periode, $idJob){
		$masaKerja = $this->basicmodeltmk->fetchTable("masa_kerja");
		$tmk = array();
		foreach($masaKerja as $mk){
			array_push($tmk, array(
				"ID_JOB"=>$idJob
			));
		}
		$this->db->insert_batch("tunjangan_masa_kerja", $tmk);
		
		//copied from tambah in BasicCrudModel
		try{
			$this->basicmodeltmk->checkDbError();
		}catch(ForeignKeyFailureException $ex){
			$this->basicmodeltmk->checkPeriodeFailure($ex);
			//check FK failures for defined FK cols
			$this->basicmodeltmk->checkFkFailure($ex, $data);
			//You may also catch ForeignKeyFailureException outside for more complex FK 
			throw $ex;
		}
		
		$done = $this->db->affected_rows();
		$needed = count($masaKerja);
		$success = $done == $needed;
		if(!$success){
			throw UnknownException("Gagal insert tunjangan masa kerja, entah knp. Berhasil: {$done}, Harusnya: {$needed}");
		}
		
		$rtmk = array();
		$firstId = $this->db->insert_id();
		$i = 0;
		foreach($masaKerja as $mk){
			array_push($rtmk, array(
				"ID_TMK"=>($firstId + $i++),
				"PERIODE_MULAI_TMK"=>$periode,
				"BATAS_BAWAH_TMK"=>$mk["MASA_KERJA"]
			));
		}
		$this->db->insert_batch("riwayat_tmk", $rtmk);
		
		
		//copied from tambah in BasicCrudModel
		try{
			$this->basicmodeltmk->checkDbError();
		}catch(ForeignKeyFailureException $ex){
			$this->basicmodeltmk->checkPeriodeFailure($ex);
			//check FK failures for defined FK cols
			$this->basicmodeltmk->checkFkFailure($ex, $data);
			//You may also catch ForeignKeyFailureException outside for more complex FK 
			throw $ex;
		}
		
		$done = $this->db->affected_rows();
		$success = $done == $needed;
		if(!$success){
			throw UnknownException("Gagal insert riwayat tunjangan masa kerja, entah knp. Berhasil: {$done}, Harusnya: {$needed}");
		}
		return $success;
	}
	
	function tambah($periode, $data){
		
		$this->db->trans_start();
		
		$keys = $this->basicmodel->tambah($periode, $data, false);
		
		$this->tambahTmk($periode, $keys["ID_JOB"]);
		
		$this->db->trans_complete();
		
		return $keys;
	}
	
	
	
	function ubah($periode, $data){
		return $this->basicmodel->ubah($periode, $data);
	}
	
	function hapus($periode, $data){
		return $this->basicmodel->hapus($periode, $data);
	}
	
	function kembalikan($periode, $data){
		return $this->basicmodel->kembalikan($periode, $data);
	}
	
	//job
	public function fetchBaru($periode){
		return $this->basicmodel->fetchView("job_gaji_baru", $periode, null, "ID_JOB");
	}
	public function fetchLama($periode){
		return $this->basicmodel->fetchView("job_gaji_lama", $periode, null, "ID_JOB");
	}
	public function fetchSekarang($periode){
		return $this->basicmodel->fetchView("job_gaji_sekarang", $periode, null, "ID_JOB");
	}
	
	function simpanGaji($periode, $data){
		return $this->basicmodelgaji->ubah($periode, $data);
	}
	
	
	function getFull($periode, $data){
		return $this->basicmodel->getView("job_gaji_all", $periode, null, $data);
	}
	
	
}
?>