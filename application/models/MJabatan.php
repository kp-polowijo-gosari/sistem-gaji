<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(CLASSES_DIR  . "Exceptions.php");
require_once(LIBRARIES_DIR  . "BasicCrudModel.php");

class MJabatan extends CI_Model {
    public function __construct() {
        parent::__construct();
		$params = array(
			"ENTITY_LONG"=>"Jabatan",
			"ENTITY_SHORT"=>"Jabatan",
			"KEY_COLS"=>array(
				"ID_JABATAN"
			),
			"UPDATE_COLS"=>array(
				"ID_JOB",
				"NAMA_JABATAN"
			),
			"FK_COLS"=>array(
				array(
					"FK_COL"=>"ID_JOB",
					"FK_EXCEPTION"=>JobInvalidException::class
				)
			),
			"SUMBER_PENGEMBALIAN"=>"jabatan_dihapus_pure"
		);
		$this->basicmodel = new BasicCrudModel($params);
		
    }
	
	
	//jabatan
	
	function getKeyValuesDict($data){
		return $this->basicmodel->getKeyValuesDict($data);
	}
	
	function fetchPilihanAktif($periode){
		return $this->basicmodel->fetchView("jabatan_aktif", $periode, null, "ID_JABATAN");
	}
	
	
	function fetchAktif($periode, $idPer){
		return $this->basicmodel->fetchView("jabatan_aktif_row", $periode, $idPer, "ID_JABATAN");
	}
	
	
	function getAktif($periode, $idPer, $data){
		return $this->basicmodel->getView("jabatan_aktif_row", $periode, $idPer, $data);
	}
	
	function fetchDihapus($periode, $idPer){
		return $this->basicmodel->fetchView("jabatan_dihapus_row", $periode, $idPer, "ID_JABATAN");
	}
	
	
	function tambah($periode, $data){
		return $this->basicmodel->tambah($periode, $data);
	}
	
	
	function ubah($periode, $data){
		return $this->basicmodel->ubah($periode, $data);
	}
	
	function hapus($periode, $data){
		return $this->basicmodel->hapus($periode, $data);
	}
	
	function kembalikan($periode, $data){
		return $this->basicmodel->kembalikan($periode, $data);
	}
}
?>