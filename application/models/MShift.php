<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(CLASSES_DIR  . "Exceptions.php");
require_once(LIBRARIES_DIR  . "BasicCrudModel.php");

class MShift extends CI_Model {
    public function __construct() {
        parent::__construct();
		$params = array(
			"ENTITY_LONG"=>"Shift",
			"ENTITY_SHORT"=>"Shift",
			"KEY_COLS"=>array(
				"ID_SHIFT"
			),
			"UPDATE_COLS"=>array(
				"NAMA_SHIFT",
				"MAX_LEMBUR_1",
				"MAX_LEMBUR_2"
			),
			"SUMBER_PENGEMBALIAN"=>"shift_dihapus"
		);
		$this->basicmodel = new BasicCrudModel($params);
    }
	
	function getKeyValuesDict($data){
		return $this->basicmodel->getKeyValuesDict($data);
	}
	
	//shift
	
	function fetchAktif($periode){
		return $this->basicmodel->fetchView("shift_aktif", $periode, null, "ID_SHIFT");
	}
	
	function getAktif($periode, $data){
		return $this->basicmodel->getView("shift_aktif", $periode, null,$data);
	}
	
	function fetchDihapus($periode){
		return $this->basicmodel->fetchView("shift_dihapus", $periode, null, "ID_SHIFT");
	}
	
	function tambah($periode, $data){
		return $this->basicmodel->tambah($periode, $data);
	}
	
	
	function ubah($periode, $data){
		return $this->basicmodel->ubah($periode, $data);
	}
	
	function hapus($periode, $data){
		return $this->basicmodel->hapus($periode, $data);
	}
	
	function kembalikan($periode, $data){
		return $this->basicmodel->kembalikan($periode, $data);
	}
}
?>