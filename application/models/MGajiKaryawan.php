<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(CLASSES_DIR  . "Exceptions.php");
require_once(LIBRARIES_DIR  . "BasicCrudModel.php");

class MGajiKaryawan extends CI_Model {
    public function __construct() {
        parent::__construct();
		$params = array(
			"ENTITY_LONG"=>"Gaji",
			"ENTITY_SHORT"=>"Gaji",
			"KEY_COLS"=>array(
				"ID_KAR",
				"PERIODE"
			),
			"FK_COLS"=>array(
				array(
					"FK_COL"=>"ID_KAR",
					"FK_EXCEPTION"=>KarInvalidException::class
				),
				array(
					"FK_COL"=>"PERIODE",
					"FK_EXCEPTION"=>PeriodeInvalidException::class
				)
			),
			//TODO
			"SUMBER_PENGEMBALIAN"=>"jabatan_dihapus_pure"
		);
    }
	
	function getKeyValuesDict($data){
		return $this->basicmodel->getKeyValuesDict($data);
	}
	function fetchBelum($periode, $idPer){
		return $this->basicmodel->fetchView("kar_gaji_belum_row", $periode, $idPer, "ID_KAR");
	}
	function fetchSudah($periode, $idPer){
		return $this->basicmodel->fetchView("kar_gaji_sudah_row", $periode, $idPer, "ID_KAR");
	}
	
	function getSudah($periode, $data){
		return $this->basicmodel->getView("kar_gaji_sudah_row", $periode, $idPer, $data);
	}
	//TODO BELOW
	function tambahGaji($periode, $data){
		$data["PERIODE_MULAI_KAR"] = $periode;
		$this->db->trans_start();
		
		if(empty($data["ID_KAR"])){
			$data["ID_KAR"] = insertEmpty("karyawan");
		}
		
		$this->db->insert("riwayat_karyawan", $data);
		$success = $this->db->affected_rows() > 0;
		$response = dbErrorResponse($this->db->error(), response($success));
		
		if(isResponseOk($response)){
			$this->db->trans_complete();
			
			$response["ID_KAR"] = $data["ID_KAR"];
		}else if (empty($response["message"])){
			$response["message"] = "Gagal menambahkan karyawan. Karyawan sudah ada?";
		}
		
		return $response;
	}
	
	
	function simpan($periode, $data){
		$data["ALAMAT_KAR"]=htmlspecialchars_decode($data["ALAMAT_KAR"]);
		
		$this->db->trans_start();
		
		$sql = "
			UPDATE riwayat_karyawan
			SET PERIODE_AKHIR_KAR = ?
			WHERE PERIODE_MULAI_KAR < ? 
				AND PERIODE_AKHIR_KAR IS NULL
				AND ID_KAR=?";
		$query = $this->db->query($sql, array(
			$periode,
			$periode,
			$data["ID_KAR"]
		));
		$success = $this->db->affected_rows() > 0;
		
		if($success){
			$response0 = $this->tambah($periode, $data);
		}else{
			$data["PERIODE_MULAI_KAR"] = $periode;
			$sql = "
				UPDATE riwayat_karyawan 
				SET 
					NIK=?,
					ID_JABATAN=?,
					ID_SUBDEPT=?,
					TANGGAL_MASUK=?,
					ID_SKA=?,
					NAMA_KAR=?,
					ALAMAT_KAR=?,
					NO_KTP_KAR=?,
					TELEPON_KAR=?,
					EMAIL_KAR=?,
					TEMPAT_LAHIR_KAR=?,
					TANGGAL_LAHIR_KAR=?,
					ID_AGAMA=?,
					ID_JK=?,
					ID_PENDIDIKAN=?
				WHERE ID_KAR=?
					AND PERIODE_MULAI_KAR = ? 
					AND PERIODE_AKHIR_KAR IS NULL";
			$query = $this->db->query($sql, array(
				$data["NIK"],
				$data["ID_JABATAN"],
				$data["ID_SUBDEPT"],
				$data["TANGGAL_MASUK"],
				$data["ID_SKA"],
				$data["NAMA_KAR"],
				$data["ALAMAT_KAR"],
				$data["NO_KTP_KAR"],
				$data["TELEPON_KAR"],
				$data["EMAIL_KAR"],
				$data["TEMPAT_LAHIR_KAR"],
				$data["TANGGAL_LAHIR_KAR"],
				$data["ID_AGAMA"],
				$data["ID_JK"],
				$data["ID_PENDIDIKAN"],
				$data["ID_KAR"], 
				$data["PERIODE_MULAI_KAR"]
			));
		}
		
		$success = $this->db->affected_rows() > 0;
		
		if($success){
			$this->db->trans_complete();
		}
		
		$response = dbErrorResponse($this->db->error(), response($success));
		
		if(!isResponseOk($response) && empty($response["message"])){
			$response["message"] = "Gagal mengubah karyawan. Karyawan tidak ada, perusahaan tidak valid, atau periode tidak valid.";
		}
		
		return $response;
	}
	
}
?>