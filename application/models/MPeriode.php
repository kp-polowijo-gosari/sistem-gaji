<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


require_once(CLASSES_DIR  . "Exceptions.php");
require_once(LIBRARIES_DIR  . "BasicCrudModel.php");

class MPeriode extends CI_Model {
    public function __construct() {
        parent::__construct();
		$params = array(
			"ENTITY_LONG"=>"Periode",
			"ENTITY_SHORT"=>"Periode",
			"KEY_COLS"=>array(
				"PERIODE"
			)
		);
		$this->basicmodel = new BasicCrudModel($params);
    }
	
	public function get($periode){
		return $this->basicmodel->getTable("periode", array(
			"PERIODE"=>$periode
		));
	}
	
	
	public function getLast(){
		$sql = "
			SELECT MAX(p0.PERIODE) AS PERIODE
			FROM periode p0";
		$query = $this->db->query($sql);
		$row = $query->row_array();
		if($row == null) return null;
		return $row['PERIODE'];
	}
}
?>