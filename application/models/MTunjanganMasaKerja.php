<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(CLASSES_DIR  . "Exceptions.php");
require_once(LIBRARIES_DIR  . "BasicCrudModel.php");

class MTunjanganMasaKerja extends CI_Model {
    public function __construct() {
        parent::__construct();
		$params = array(
			"ENTITY_LONG"=>"Tunjangan Masa Kerja",
			"ENTITY_SHORT"=>"Tmk",
			"KEY_COLS"=>array(
				"ID_TMK"
			),
			"UPDATE_COLS"=>array(
				"BATAS_BAWAH_TMK",
				"BATAS_ATAS_TMK",
				"NILAI_TMK"
			),
			"FK_COLS"=>array(
				array(
					"FK_COL"=>"ID_JOB",
					"FK_EXCEPTION"=>JobInvalidException::class
				)
			)
		);
		$this->basicmodel = new BasicCrudModel($params);
    }
	
	function getKeyValuesDict($data){
		return $this->basicmodel->getKeyValuesDict($data);
	}
	function fetchAktif($periode, $data){
		return $this->basicmodel->fetchViewFiltered("tmk_aktif", $periode, null, $data, "ID_TMK");
	}
	
	
	function simpanSatuan($periode, $row){
		return $this->basicmodel->ubah($periode, $row, false);
	}
	
	function simpan($periode, $data){
		
		$this->db->trans_start();
		
		usort($data["rows"], function($a, $b) {
			return strcmp($a["BATAS_BAWAH_TMK"], $b["BATAS_BAWAH_TMK"]);
		});
		
		$len = count($data["rows"]);
		for($i = 1; $i < $len; ++$i){
			$data["rows"][$i-1]["BATAS_ATAS_TMK"] = $data["rows"][$i]["BATAS_BAWAH_TMK"]; 
		}
		$data["rows"][$len-1]["BATAS_ATAS_TMK"] = null;
		
		foreach($data["rows"] as $row){
			$row["PERIODE_MULAI_TMK"] = $periode;
			$success = $this->simpanSatuan(
				$row["PERIODE_MULAI_TMK"],
				$row
			);
			if (!$success) break;
		}
		
		if($success){
			$this->db->trans_complete();
		}
		
		return $success;
	}
	function ubah($periode, $data){
		return $this->simpan($periode, $data);
	}
}
?>