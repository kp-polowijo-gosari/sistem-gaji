<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(CLASSES_DIR  . "Exceptions.php");
require_once(LIBRARIES_DIR  . "BasicCrudModel.php");

class MDepartemen extends CI_Model {
    public function __construct() {
        parent::__construct();
		$params = array(
			"ENTITY_LONG"=>"Departemen",
			"ENTITY_SHORT"=>"Dept",
			"KEY_COLS"=>array(
				"ID_DEPT"
			),
			"UPDATE_COLS"=>array(
				"ID_PER",
				"NAMA_DEPT"
			),
			"FK_COLS"=>array(
				array(
					"FK_COL"=>"ID_PER",
					"FK_EXCEPTION"=>PerusahaanInvalidException::class
				)
			),
			"SUMBER_PENGEMBALIAN"=>"dept_dihapus"
		);
		$this->basicmodel = new BasicCrudModel($params);
    }
	function getKeyValuesDict($data){
		return $this->basicmodel->getKeyValuesDict($data);
	}
	//departemen
	//dept
	
	function fetchPilihanAktif($periode, $idPer){
		return $this->basicmodel->fetchView("dept_aktif", $periode, $idPer, "ID_DEPT");
	}
	
	
	function fetchAktif($periode, $idPer){
		return $this->basicmodel->fetchView("dept_aktif_row", $periode, $idPer, "ID_DEPT");
	}
	
	
	function getAktif($periode, $data){
		return $this->basicmodel->getView("dept_aktif_row", $periode, null, $data);
	}
	
	function fetchDihapus($periode, $idPer){
		return $this->basicmodel->fetchView("dept_dihapus_row", $periode, $idPer, "ID_DEPT");
	}
	
	function tambah($periode, $data){
		return $this->basicmodel->tambah($periode, $data);
	}
	
	
	function ubah($periode, $data){
		return $this->basicmodel->ubah($periode, $data);
	}
	
	function hapus($periode, $data){
		return $this->basicmodel->hapus($periode, $data);
	}
	
	function kembalikan($periode, $data){
		return $this->basicmodel->kembalikan($periode, $data);
	}
}
?>
	