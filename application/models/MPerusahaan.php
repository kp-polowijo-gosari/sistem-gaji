<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(CLASSES_DIR  . "Exceptions.php");
require_once(LIBRARIES_DIR  . "BasicCrudModel.php");

class MPerusahaan extends CI_Model {
    public function __construct() {
        parent::__construct();
		$params = array(
			"ENTITY_LONG"=>"Perusahaan",
			"ENTITY_SHORT"=>"Per",
			"KEY_COLS"=>array(
				"ID_PER"
			)
		);
		$this->basicmodel = new BasicCrudModel($params);
    }
	
	function getKeyValuesDict($data){
		return $this->basicmodel->getKeyValuesDict($data);
	}
	function fetchAktif(){
		return $this->basicmodel->fetchTable("perusahaan", "ID_PER");
	}
	public function get($idPer){
		return $this->basicmodel->getTable("perusahaan", array(
			"ID_PER"=>$periode
		));
	}
	public function getFirst(){
		$sql = "
			SELECT MIN(rp.ID_PER) AS ID_PER
			FROM perusahaan rp;";
		$query = $this->db->query($sql);
		$row = $query->row_array();
		if($row == null) return null;
		return $row['ID_PER'];
	}
}
?>