<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(CLASSES_DIR  . "Exceptions.php");
require_once(LIBRARIES_DIR  . "BasicCrudModel.php");

class MStatusKoperasi extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
	
	function getKeyValuesDict($data){
		return $this->basicmodel->getKeyValuesDict($data);
	}
	
	function fetchPilihan(){
		return $this->basicmodel->fetchTable("status_koperasi", "ID_SKAO");
	}
	
	function fetchFull($periode){
		return $this->basicmodel->fetchView("status_koperasi_full", $periode, null, "ID_SKAO");
	}
	
}
?>