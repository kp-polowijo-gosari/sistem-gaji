<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(CLASSES_DIR  . "Exceptions.php");
require_once(LIBRARIES_DIR  . "BasicCrudModel.php");

class MUpahLembur extends CI_Model {
    public function __construct() {
        parent::__construct();
		$params = array(
			"ENTITY_LONG"=>"Upah Lembur",
			"ENTITY_SHORT"=>"Ul",
			"KEY_COLS"=>array(
				"ID_JOB",
				"JAM_LEMBUR"
			),
			"UPDATE_COLS"=>array(
				"UPAH_LEMBUR"
			),
			"INSERT_COLS"=>array(
				"ID_JOB",
				"JAM_LEMBUR",
				"PERIODE_MULAI_UL",
				"UPAH_LEMBUR"
			),
			"FK_COLS"=>array(
				array(
					"FK_COL"=>"ID_JOB",
					"FK_EXCEPTION"=>JobInvalidException::class
				),
				array(
					"FK_COL"=>"JAM_LEMBUR",
					"FK_EXCEPTION"=>JamLemburInvalidException::class
				)
			)
		);
		$this->basicmodel = new BasicCrudModel($params);
    }
	
	function getKeyValuesDict($data){
		return $this->basicmodel->getKeyValuesDict($data);
	}
	function fetchAll($periode, $data){
		return $this->basicmodel->fetchViewFiltered("ul_all_aktif", $periode, null, $data, array("ID_JOB", "JAM_LEMBUR"));
	}
	
	function periksaSatuan($idJob, $row){
		$row["ID_JOB"] = $idJob;
		return $this->basicmodel->insertIfExistsDoNothing("upah_lembur", $row, true);
	}
	function simpanSatuan($periode, $idJob, $row){
		$row["PERIODE_MULAI_UL"] = $periode;
		$row["ID_JOB"] = $idJob;
		
		$success = $this->basicmodel->insertIfNotExists("riwayat_ul", $row, false);
		
		if($success) return true;
		
		return $this->basicmodel->ubah($periode, $row, false);
	}
	
	function simpan($periode, $data){
		
		$this->db->trans_start();
			
		$success = true;
		foreach($data["rows"] as $row){
			$row["ID_JOB"] = $data["ID_JOB"];
			$success = $this->periksaSatuan($row["ID_JOB"], $row);
			if (!$success) return false;
		}

		foreach($data["rows"] as $row){
			$row["PERIODE_MULAI_UL"] = $periode;
			$row["ID_JOB"] = $data["ID_JOB"];
			$success = $this->simpanSatuan(
				$row["PERIODE_MULAI_UL"],
				$row["ID_JOB"],
				$row
			);
			if (!$success) break;
		}
		
		$this->db->trans_complete();
		
		return true;
	}
	function ubah($periode, $data){
		return $this->simpan($periode, $data);
	}
}
?>