<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(CLASSES_DIR  . "Exceptions.php");
require_once(LIBRARIES_DIR  . "BasicCrudModel.php");

class MTkhJabatan extends CI_Model {
    public function __construct() {
        parent::__construct();
		$params = array(
			"ENTITY_LONG"=>"Tkh Jabatan",
			"ENTITY_SHORT"=>"Tkhj",
			"RIWAYAT_TABLE"=>"tkh_jabatan",
			"KEY_COLS"=>array(
				"ID_JABATAN",
				"ID_TKH"
			),
			"FK_COLS"=>array(
				array(
					"FK_COL"=>"ID_JABATAN",
					"FK_EXCEPTION"=>JabatanInvalidException::class
				),
				array(
					"FK_COL"=>"ID_TKH",
					"FK_EXCEPTION"=>TkhInvalidException::class
				)
			),
			"SUMBER_PENGEMBALIAN"=>"tkhj_dihapus"
		);
		$this->basicmodel = new BasicCrudModel($params);
    }
	
	function entityKeyDesc($periode, $data){
		$entityKeyDesc = $this->basicmodel->entityKeyDesc($this->basicmodel->riwayatTable, $periode, $this->basicmodel->getKeyValuesDict($data));
	}
	
	function getKeyValuesDict($data){
		return $this->basicmodel->getKeyValuesDict($data);
	}
	
	//TkhJabatan
	
	function getAktif($periode, $data){
		return $this->basicmodel->getView("tkhj_aktif_row", $periode, null, $data);
	}
	function fetchAktif($periode, $data){
		return $this->basicmodel->fetchViewFiltered("tkhj_aktif_row", $periode, null, $data, array("ID_JABATAN", "ID_TKH"));
	}
	
	function fetchDihapus($periode, $data){
		return $this->basicmodel->fetchViewFiltered("tkhj_dihapus_row", $periode, null, $data, array("ID_JABATAN", "ID_TKH"));
	}
	
	
	function dihapusExists($periode, $data){
		return $this->basicmodel->countViewEntry("tkhj_dihapus_row", $periode, null, $data);
	}
	
	function tambah($periode, $data){
		return $this->basicmodel->tambah($periode, $data);
	}
	
	
	function hapus($periode, $data){
		return $this->basicmodel->hapus($periode, $data);
	}
	
	function kembalikan($periode, $data){
		return $this->basicmodel->kembalikan($periode, $data);
	}
	
}
?>