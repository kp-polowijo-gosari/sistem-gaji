<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(CLASSES_DIR  . "Exceptions.php");
require_once(LIBRARIES_DIR  . "BasicCrudModel.php");

class MTunjanganKinerja extends CI_Model {
    public function __construct() {
        parent::__construct();
		$params = array(
			"ENTITY_LONG"=>"Tunjangan Kinerja",
			"ENTITY_SHORT"=>"Tki",
			"KEY_COLS"=>array(
				"ID_JOB",
				"KINERJA"
			),
			"UPDATE_COLS"=>array(
				"NILAI_TKI"
			),
			"FK_COLS"=>array(
				array(
					"FK_COL"=>"ID_JOB",
					"FK_EXCEPTION"=>JobInvalidException::class
				),
				array(
					"FK_COL"=>"KINERJA",
					"FK_EXCEPTION"=>KinerjaInvalidException::class
				)
			)
		);
		$this->basicmodel = new BasicCrudModel($params);
    }
	
	function fetchAll($periode, $data){
		return $this->basicmodel->fetchViewFiltered("tki_all", $periode, null, $data, "KINERJA");
	}
	
	function periksaSatuan($idJob, $row){
		$row["ID_JOB"] = $idJob;
		return $this->basicmodel->insertIfExistsDoNothing("tunjangan_kinerja", $row, true);
		
	}
	
	function simpanSatuan($periode, $idJob, $row){
		
		$row["PERIODE_MULAI_TKI"] = $periode;
		$row["ID_JOB"] = $idJob;
		
		$success = $this->basicmodel->insertIfNotExists("riwayat_tki", $row, false);
		
		if($success) return true;
		
		return $this->basicmodel->ubah($periode, $row, false);
	}
	function simpan($periode, $data){
		
		$this->db->trans_start();
			
		$success = true;
		foreach($data["rows"] as $row){
			$row["ID_JOB"] = $data["ID_JOB"];
			$success = $this->periksaSatuan($data["ID_JOB"], $row);
			if (!$success) return false;
		}
		
		
		foreach($data["rows"] as $row){
			$row["PERIODE_MULAI_TKI"] = $periode;
			$row["ID_JOB"] = $data["ID_JOB"];
			$success = $this->simpanSatuan(
				$row["PERIODE_MULAI_TKI"],
				$row["ID_JOB"],
				$row
			);
			if (!$success) return false;
		}
		
		$this->db->trans_complete();
		
		return true;
	}
	function ubah($periode, $data){
		return $this->simpan($periode, $data);
	}
}
?>