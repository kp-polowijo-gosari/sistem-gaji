<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(CLASSES_DIR  . "Exceptions.php");
require_once(LIBRARIES_DIR  . "BasicCrudModel.php");

class MTunjanganKhusus extends CI_Model {
    public function __construct() {
        parent::__construct();
		$params = array(
			"ENTITY_LONG"=>"Tunjangan Khusus",
			"ENTITY_SHORT"=>"Tkh",
			"KEY_COLS"=>array(
				"ID_TKH"
			),
			"UPDATE_COLS"=>array(
				"NAMA_TKH",
				"NILAI_TKH"
			),
			"SUMBER_PENGEMBALIAN"=>"tkh_dihapus"
		);
		$this->basicmodel = new BasicCrudModel($params);
		
    }
	
	function getKeyValuesDict($data){
		return $this->basicmodel->getKeyValuesDict($data);
	}
	//tunjangan khusus
	
	function fetchJabatanAktif($periode, $idPer){
		return $this->basicmodel->fetchView("jabatan_aktif_row_tkh", $periode, $idPer, "ID_JABATAN");
	}
	
	function fetchAktif($periode, $idPer){
		return $this->basicmodel->fetchView("tkh_aktif_row", $periode, $idPer, "ID_TKH");
	}
	
	function getAktif($periode, $idPer, $data){
		return $this->basicmodel->getView("tkh_aktif_row", $periode, $idPer, $data);
	}
	function fetchDihapus($periode, $idPer){
		return $this->basicmodel->fetchView("tkh_dihapus_row", $periode, $idPer, "ID_TKH");
	}
	
	function tambah($periode, $data){
		return $this->basicmodel->tambah($periode, $data);
	}
	
	
	function ubah($periode, $data){
		return $this->basicmodel->ubah($periode, $data);
	}
	
	function hapus($periode, $data){
		return $this->basicmodel->hapus($periode, $data);
	}
	
	function kembalikan($periode, $data){
		return $this->basicmodel->kembalikan($periode, $data);
	}
	
}
?>