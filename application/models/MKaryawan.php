<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MKaryawan extends CI_Model {
    public function __construct() {
        parent::__construct();
		$params = array(
			"ENTITY_LONG"=>"Karyawan",
			"ENTITY_SHORT"=>"Kar",
			"KEY_COLS"=>array(
				"ID_KAR"
			),
			"UPDATE_COLS"=>array(
				"NIK",
				"ID_JABATAN",
				"ID_SUBDEPT",
				"TANGGAL_MASUK",
				"ID_SKA",
				"NAMA_KAR",
				"ALAMAT_KAR",
				"NO_KTP_KAR",
				"TELEPON_KAR",
				"EMAIL_KAR",
				"TEMPAT_LAHIR_KAR",
				"TANGGAL_LAHIR_KAR",
				"ID_AGAMA",
				"ID_JK",
				"ID_PENDIDIKAN"
			),
			"FK_COLS"=>array(
				array(
					"FK_COL"=>"ID_JABATAN",
					"FK_EXCEPTION"=>JabatanInvalidException::class
				),
				array(
					"FK_COL"=>"ID_SUBDEPT",
					"FK_EXCEPTION"=>SubdeptInvalidException::class
				),
				array(
					"FK_COL"=>"ID_SKA",
					"FK_EXCEPTION"=>StatusKarInvalidException::class
				),
				array(
					"FK_COL"=>"ID_AGAMA",
					"FK_EXCEPTION"=>AgamaInvalidException::class
				),
				array(
					"FK_COL"=>"ID_JK",
					"FK_EXCEPTION"=>JenisKelaminInvalidException::class
				),
				array(
					"FK_COL"=>"ID_PENDIDIKAN",
					"FK_EXCEPTION"=>PendidikanInvalidException::class
				)
			),
			//TODO
			"SUMBER_PENGEMBALIAN"=>"kar_dihapus_pure"
		);
		$this->basicmodel = new BasicCrudModel($params);
    }
	
	function getKeyValuesDict($data){
		return $this->basicmodel->getKeyValuesDict($data);
	}
	
	function fetchAktif($periode, $idPer){
		return $this->basicmodel->fetchView("kar_aktif_row", $periode, $idPer, "ID_KAR");
	}
	
	
	function get($periode, $data){
		return $this->basicmodel->getView("kar_all_full", $periode, null,$data);
	}
	
	function getAktif($periode, $data){
		return $this->basicmodel->getView("kar_aktif_full", $periode, null,$data);
	}
	function getDihapus($periode, $data){
		return $this->basicmodel->getView("kar_dihapus_full", $periode, null,$data);
	}
	
	function fetchDihapus($periode, $idPer){
		return $this->basicmodel->fetchView("kar_dihapus_row", $periode, $idPer, "ID_KAR");
	}
	
	function tambah($periode, $data){
		$data["ALAMAT_KAR"]=htmlspecialchars_decode($data["ALAMAT_KAR"]);
		return $this->basicmodel->tambah($periode, $data);
	}
	
	
	function ubah($periode, $data){
		$data["ALAMAT_KAR"]=htmlspecialchars_decode($data["ALAMAT_KAR"]);
		return $this->basicmodel->ubah($periode, $data);
	}
	
	function hapus($periode, $data){
		return $this->basicmodel->hapus($periode, $data);
	}
	
	function kembalikan($periode, $data){
		return $this->basicmodel->kembalikan($periode, $data);
	}
	
	function fetchStatusKaryawan(){
		return $this->basicmodel->fetchTable("status_karyawan");
	}
	
	function fetchJenisKelamin(){
		return $this->basicmodel->fetchTable("jenis_kelamin");
	}
	
	function fetchAgama(){
		return $this->basicmodel->fetchTable("agama");
	}
	
	function fetchPendidikan(){
		return $this->basicmodel->fetchTable("pendidikan");
	}
}
?>