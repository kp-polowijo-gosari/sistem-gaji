<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific C functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding C class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_C'] = 'welcome';
|
| This route indicates which C class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which C/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| C and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| C and method URI segments.
|
| Examples:	my-C/index	-> my_C/index
|		my-C/my-method	-> my_C/my_method
*/


//DATA KARYAWAN
//dept
//departemen
$route['karyawan/departemen'] = 'data-karyawan/CDepartemen';
$route['karyawan/departemen/(:any)'] = 'data-karyawan/CDepartemen/$1';

//subdept
//subdepartemen
$route['karyawan/subdepartemen'] = 'data-karyawan/CSubdepartemen';
$route['karyawan/subdepartemen/(:any)'] = 'data-karyawan/CSubdepartemen/$1';

//job level
$route['karyawan/job-level'] = 'data-karyawan/CJobLevel';
$route['karyawan/job-level/(:any)'] = 'data-karyawan/CJobLevel/$1';

//jabatan
$route['karyawan/jabatan'] = 'data-karyawan/CJabatan';
$route['karyawan/jabatan/(:any)'] = 'data-karyawan/CJabatan/$1';

//karyawan
$route['karyawan/karyawan'] = 'data-karyawan/CKaryawan';
$route['karyawan/karyawan/hapus'] = 'data-karyawan/CKaryawan/hapus';
$route['karyawan/karyawan/kembalikan'] = 'data-karyawan/CKaryawan/kembalikan';
$route['karyawan/karyawan/baru'] = 'data-karyawan/CTambahKaryawan';
$route['karyawan/karyawan/tambah'] = 'data-karyawan/CTambahKaryawan/tambah';
$route['karyawan/karyawan/(:num)'] = 'data-karyawan/CDetailKaryawan/index/$1';
$route['karyawan/karyawan/ubah'] = 'data-karyawan/CDetailKaryawan/ubah';

//shift
$route['karyawan/shift'] = 'data-karyawan/CShift';
$route['karyawan/shift/(:any)'] = 'data-karyawan/CShift/$1';

//DATA GAJI
//job level
$route['gaji/job-level'] = 'data-gaji/CJobLevel';
//$route['gaji/job-level/baru'];
$route['gaji/job-level/(:num)'] = 'data-gaji/CDetailJobLevel/index/$1';
$route['gaji/job-level/simpan/(:any)'] = 'data-gaji/CDetailJobLevel/simpan/$1';

//tunjangan khusus
$route['gaji/tunjangan-khusus'] = 'data-gaji/CTunjanganKhusus';
$route['gaji/tunjangan-khusus/jabatan/(:num)'] = 'data-gaji/CTkhJabatan/index/$1';
//$route['gaji/tunjangan-khusus/jabatan/(:any)'] = 'data-gaji/CTkhJabatan/$1';
$route['gaji/tunjangan-khusus/jabatan/(:num)/(:any)'] = 'data-gaji/CTkhJabatan/$2/$1';
$route['gaji/tunjangan-khusus/(:any)'] = 'data-gaji/CTunjanganKhusus/$1';

$route['gaji/karyawan'] = 'data-gaji/CKaryawan';
$route['gaji/lembur'] = 'data-gaji/CLembur';
$route['gaji/lembur/(:num)'] = 'data-gaji/CDetailLembur/index/$1';
$route['gaji/absen'] = 'data-gaji/CAbsen';
$route['gaji/absen/(:num)'] = 'data-gaji/CDetailAbsen/index/$1';

//LAPORAN
$route['laporan/slip-gaji'] = 'laporan/CSlipGaji';
$route['laporan/slip-gaji/cetak'] = 'laporan/CSlipGaji/cetak';
$route['laporan/rekap-gaji'] = 'laporan/CRekapGaji';
$route['laporan/rekap-gaji/cetak'] = 'laporan/CRekapGaji/cetak';
$route['laporan/gaji'] = 'laporan/CGaji';
$route['laporan/lembur'] = 'laporan/CLembur';
$route['laporan/lembur/full'] = 'laporan/CLemburFull';
$route['laporan/lembur/(:num)'] = 'laporan/CDetailLembur/index/$1';
$route['laporan/lembur/range'] = 'laporan/CRangeLembur';
$route['laporan/absen'] = 'laporan/CAbsen';
$route['laporan/absen/harian'] = 'laporan/CAbsenHarian';
$route['laporan/absen/detail'] = 'laporan/CDetailAbsen';
$route['laporan/angsuran'] = 'laporan/CAngsuran';

//ANGSURAN
$route['angsuran'] = 'angsuran/CAngsuran';
$route['angsuran/potongan'] = 'angsuran/CPotongan';

//PENGATURAN
$route['pengaturan'] = 'CPengaturan';

//AKUN

$route['akun'] = 'akun/CAkun';
$route['akun/(:num)'] = 'akun/CDetailAkun/index/$1';

//common
$route['beranda'] = 'CBeranda';
$route['default_controller'] = 'CBeranda';

$route['profil'] = 'CProfil';
$route['logout'] = 'CLogout';

$route['login'] = 'CLogin';
$route['login/(:any)'] = 'CLogin/login';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
